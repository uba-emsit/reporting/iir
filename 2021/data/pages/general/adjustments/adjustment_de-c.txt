====== Adjustment DE-B and DE-C for NOx and NMVOC emissions from 3.D and 3.B ======
   
===== Justification =====

Germany applies for adjustments of both NO<sub>x</sub> and NMVOC emissions from manure management and agricultural soils as these emission sources were not accounted for at the time when emission reduction commitments were set, according to

EMEP Executive Body Decision 3/2012 ECE/EB.AIR/111/Add.1, para 6(a) ((EMEP EB, 2012: EMEP Executive Body Decision 3/2012 in ECE/EB.AIR/111/Add.1 - Adjustments under the Gothenburg Protocol to emission reduction commitments or to inventories for the purposes of comparing total national emissions with them, URL: http://www.ceip.at/fileadmin/inhalte/emep/Adjustments/ECE_EB.AIR_111_Add.1__ENG_DECISION_3.pdf)):

>Emission source categories are identified that were not accounted for at the time when emission reduction commitments were set.

and Decision 2014/1 ECE/EB.AIR/127/Add.1, Annex para 3(a) ((EMEP EB, 2014: EMEP Executive Body Decision 2014/1 - Improving the guidance for adjustments under the 1999 Protocol to Abate Acidification, Eutrophication and Ground-level Ozone to emission reduction commitments or to inventories for the purposes of comparing total national emissions with them, URL: http://www.ceip.at/fileadmin/inhalte/emep/Adjustments/Decision_2014_1.pdf)):


>[…] an emission source category for a specific pollutant will qualify as a new emission source category if emission estimates for that source category were introduced to the national emission inventory after the emission reduction commitment for that pollutant was set and where no methodology was provided in the EMEP/EEA air pollutant emission inventory guidebook for determining emissions from that source category at the time that the emission reduction commitment was set.


**Documentation**

According to ECE/EB.AIR/113/Add.1 Annex para 2, (i) b the Party shall support documentation that the source category was not included in the relevant historic national emission inventory at the time when the emission reduction commitment was set.

**NO<sub>x</sub> emissions from manure management and agricultural soils**

Since 2002, Germany reported NOx emissions for the agriculture sector under the framework of the CLRTAP. This emission source was not considered in the RAINS model (Cofala J., Syri S. (1998) ((Cofala J., Syri S. (1998): Nitrogen Oxides Emissions, Abatement Technologies and Related Costs for Europe in the RAINS Model Database. URL: http://www.iiasa.ac.at/publication/more_IR-98-088.php))) at the time when the emission reduction commitment was set. This issue was pointed out in the national programmes (National Programm 2002, Table 2, p. 13 ((Nationales Programm 2002 : National Programme of the Federal Republic of Germany pursuant to Article 6 of Directive 2001/81/EC of 23 October 2001 on national emission ceilings for certain atmospheric pollutants (NEC Directive) , 2003, Umweltbundesamt,
URL: http://ec.europa.eu/environment/air/pdf/200181_progr_de_en.pdf)); Nationales Programm 2006,Table 16, p. 64 ((Nationales Programm 2006: Nationales Programm zur Verminderung der Ozonkonzentration und zur Einhaltung der Emissionshöchstmengen - Programm gemäß § 8 der 33. BImSchV, 2006, URL: http://ec.europa.eu/environment/air/pdf/nat_prog/germany_final_de.pdf))) and thus it was not considered in the calculation of the national totals.

Since 2004, NO<sub>x</sub> emissions were also reported for the subsector ‘agricultural soils’ and considered under the category 'Agriculture'. Previously, emissions were calculated according Döhler et al. ((Döhler et al.: DÖHLER, H.,DÄMMGEN, U.,EURICH-MENDEN,B.,OSTERBURG, B.,LÜTTICH, M.,BERG, W., BERGSCHMIDT, A., BRUNSCH, R.(2002): Anpassung der deutschen Methodik zur rechnerischen Emissionsermittlung an internationale Richtlinien sowie Erfassung und Prognose der Ammoniak-Emissionen der deutschen Landwirtschaft und Szenarien zu deren Minderung bis zum Jahre 2010. UBA-Texte 05/02. PDF,360 S.)) and CORINAIR Guidebook 2002 ((CORINAIR Guidebook 2002: EMEP/CORINAIR Emission Inventory Guidebook - 3rd edition October 2002 UPDATE
URL: http://www.eea.europa.eu/publications/EMEPCORINAIR3/page019.html)).

**VOC emissions from manure management and agricultural soils**

VOC emissions from agriculture were also not considered in the RAINS model at the time when the emission reduction commitment was set. This issue was pointed out in the national programmes (National Programm 2002, Table 2, p. 13; Nationales Programm 2006, Table 19, p. 68 and chapter 9.6.1) and thus it was not considered in the calculation of the national totals.

VOC emissions from agriculture (former NFR code 4.B and 4.D) were reported in the national submissions 2004 until 2011. The emission factors came from the EMEP/EEA Guidebook. The chapter has been revised in 2012 because the source of the published data has been assessed as not reliable. The updated Guidebook 2013 offers new EF for these sources (manure management, 3.B and cultivated crops, 3.D). Thus, these emissions are considered again in the national inventory.

**Approval**

>This adjustment has been **reviewed and approved in 2014**.
