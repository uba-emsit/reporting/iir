=== Additional measures that have not yet been implemented are assigned to the WAM scenario===   

**Reduction in pulp and paper production through amendment of the 13<sup>th</sup> BImSchV**

According to the existing 13<sup>th</sup> BImSchV (as of 2017), different maximum amounts of NO<sub>X</sub> emissions are permitted according to the production process (sulphate and sulphite process) and the size of the plant (measured in RTI in MW) in pulp and paper production. A relating amendment of the 13<sup>th</sup> BImSchV results in reductions in the emission factor in the NFR sector 2.H.1.

It is assumed for the sulphite process that all four plants located in Germany are operated with RTI of 50-300 MW. In the sense of a conservative estimate of the reduction potential, a maximum current emission factor of 300 mg / Nm<sup>3</sup> for all plants according to the 13<sup>th</sup> BImSchV is assumed for the further calculation of the reduction potential. The emission factor for the sulphite process will be taken over from the 2020 submission in 2020, as no reduction is expected from the amendment of the 13<sup>th</sup> BImSchV in 2020. As a result, the emission factor for 2020 will be 2 kg / t. The new emission factor results from the current emission factor (2 kg / t) and the maximum emission value proposed in the amendment (85 mg / Nm<sup>3</sup>) divided by the calculated mean value of the currently applicable law (300 mg / Nm<sup>3</sup>). This results in an emission factor of 0.57 kg / t for 2025, 2030 and 2035 as shown in (8).

    (8) emission factor (sulphite process) = (2 kg/t * 85 mg/Nm^3) / (300 mg/Nm^3) = 0.57 kg/t

In the field of the sulphate process there are two plants with different boiler sizes in Germany. To calculate the reduction potential, the percentage distribution of the two plants per boiler size was calculated according to a combustion heat output in the range of 100-300 MW and more than 300 MW over all time series (2006 to 2018). For this purpose, the emission values of the individual years for the individual location or the individual plant are divided by the annual activity of both plants for each considered time series. The data basis for the calculation is the 2020 submission. This results in the estimates of the proportionate use of the various plant sizes for the past years up to 2018 with the plant-size-specific maximum emissions according to the daily mean value with 250 mg/Nm<sup>3</sup> for the plant with a thermal output of 100-300 MW and 200 mg/Nm<sup>3</sup> for the plant with more than 300 MW. The mean value of the current NO<sub>X</sub> emissions from the sulphate process results from the sum of the maximum permitted emissions per boiler size multiplied by the current proportionate NO<sub>X</sub> emissions. Equation (9) indicates the calculation.

    (9) mean NOx-emission (sulphate process) = 0.36 t/a * 250 mg/Nm^3 + 0.64 t/a * 200 mg/Nm^3 = 217.78 mg/Nm^3

The emission factor for the sulfate process will be taken over from the 2020 submission in 2020, as no reduction is to be expected from the amendment to the 13<sup>th</sup> BImSchV in 2020. The new emission factor results from the emission factor according to the current status and the maximum emission value proposed in the amendment of the 13<sup>th</sup> BImSchV divided by the calculated mean value of the applicable law. This results in an emission factor of 0.68 kg / t for 2025, 2030 and 2035, as shown in equation (10). 

    (10) emission factor (sulphate process) = (1.75 kg/t * 85 mg/Nm^3) / (217.78 mg/Nm^3) = 0.68 kg/t


**Reduction in refineries through amendment of the 13<sup>th</sup> BImSchV:**

A possible amendment of the 13<sup>th</sup> BImSchV can lead to emission reductions in the area of refineries and is assigned to the WAM scenario. It causes a reduction in the emission factors in the affected time series of the NFR sector 1.A.1.b. A distinction must be made between refinery plants and the fuel input used by them. For plants using raw petrol (naphtha), light heating oil or other petroleum products, the proposed limit value is set 85 mg / Nm<sup>3</sup> and adopted as the maximum emission level. When using heavy fuel oil, there is a bell control for the plants, whereby individual parts of the plant are allowed to exceed the limit value of 85 mg / Nm<sup>3</sup> if other parts of the plant fall below the limit value and the plant emission in on average not above the limit value.

First reductions are not expected until 2025, which is why the emission factors of the concerned source categories for 2020 correspond to the reference value from the 2020 submission. For plants using raw petrol (naphtha), light heating oil or other petroleum products as fuel, the new maximum emission level corresponds to the limit value of 85 mg / Nm<sup>3</sup> NO<sub>X</sub>. Consequently, only the conversion factor of the specific flue gas volume for heavy fuel oil or light heating oil (see Table 1) has to be used to convert to kg / TJ NO<sub>X</sub>.

The conversion is carried out for all source groups as shown in (11) using the example of refinery underfiring in LCP with light heating oil as fuel. 

    (11) NOx-emission (refinery underfiring with light heating oil) = 85 mg/Nm^3 / 3.49 = 24.4 kg/TJ

This results in emission factors of 24.4 kg / TJ for light heating oil and 25.1 kg / TJ for other petroleum products for 2025, 2030 and 2035. 

For a total of twelve plants with heavy fuel oil as fuel input the bell regulation apply. First of all, the emission limit value according to the current 13<sup>th</sup> BImSchV and to its specific RTI is assigned to each plant and the mean value is calculated across all plants (274.85 mg / Nm<sup>3</sup>). The bell regulation allows parts of plants to exceed the maximum emission level if another part of the plant emits proportionally less. The estimated percentage reduction, taking into account the bell control, is calculated as shown in (12) by setting the limit value of 85 mg / Nm<sup>3</sup> NO<sub>X</sub> in relation to the mean value of the current emission limit values. 

    (12) percentage reductio of NOx-emission (refineries) = 1 - (85 mg/Nm^3 / 274.75 mg/Nm^3) = 0.69

A calculated reduction of approximate 69 per cent is assumed for the bell. The projected emission factors for the concerned source categories for 2025, 2030 and 2035 are now derived from the current emission factor of the source category under consideration from the 2020 submission minus the proportional reduction.

The conversion is carried out in the same way as in (13) for all source groups as shown in the example of refinery underfiring in LCP with light heating oil as fuel.

    (13) NOx-emission (refinery underfiring with light heating oil) = (400 mg/Nm^3 * (1 - 0.69) / 3.39 = 36.5 kg/TJ


**Other reductions in large combustion plants through amendment of the 13<sup>th</sup> BImSchV:**

Emissions from other LCPs, which emerge from the energy balances, but cannot be clearly assigned to a specific fuel use or fuel mix and also show a reduction potential in the event of an amendment of the 13<sup>th</sup> BImSchV are assigned to the NFR sector 1.A.1.c and a reduction in the emission factor was calculated.

The emission factors for all non-gaseous materials other than coal for electricity and heat generation are considered and the maximum emission amount for NO<sub>X</sub> is assumed to be 85 mg / Nm<sup>3</sup>. The relevant fuels are heavy fuel oil, light heating oil and other petroleum products. According to the 13<sup>th</sup> BImSchV, only plants with more than 1500 operating hours per year are taken into account for which the new limit value of 85 mg / Nm<sup>3</sup> NO<sub>X</sub> applies. Table 10 shows the estimated relative and absolute plant split of the LCP according to its annual operating time assuming an equal fuel use distribution. 

__10: Estimated relative and absolute plant split of LCP according to operating time in the year__
^ Operation time ^  RTI in MW  ^  Proportion  ^
| <1500 h/a	 |   46573   |	17.8 %    |
| >1500 h/a	 |  214990   |	82.2 %    |
| Total 	 |  261563   |	100 %     |

Since the first reduction effects are not expected until 2025, the emission factors of the affected source groups for 2020 correspond to those of the reference value from the 2020 submission. The emission factors will be recalculated for 2025, 2030 and 2035. First, the limit value of 85 mg / Nm<sup>3</sup> is converted into kg / TJ using the specific conversion factor (see Table 1). The new emission factor results from the sum of the reduction for the 82.2 per cent of the fuel use with an operating time of more than 1500 h / a and the unchanged value from the 2020 submission for the 17.8 per cent of the fuel use with less than 1500 h / a operating time that is not obliged to reduce it by the amended 13<sup>th</sup> BImSchV. 

The calculation is shown using the example of the source category of electricity generation in LCP of the other industrial power plants with the fuel consumption of light heating oil (reference value: 103.2 kg / TJ) in (14), whereby the procedure is analogous for all other source categories.

    (14) NOx-emission (electicity generation in LCP of the other industiral power plants) = (85 mg/Nm^3 / 3.39) * 82.2% + 103.2 kg/TJ * 17.8% = 39.0 kg/TJ


**Reduction in gas and steam turbines through amendment of the 13<sup>th</sup> BImSchV:**

In the case of LCPs with gas and steam turbines, the assumed requirement is a stricter limit value of 20 mg / Nm<sup>3</sup> NO<sub>X</sub> for plants with more than 1500 operating hours per year and assigned to the WAM scenario. The affected time series in which the emission factor is reduced are in the NFR sectors 1.A.1.a, 1.A.1.b, 1.A.2.g and 1.A.3.e. It is assumed that as a result of the regulations, SCR technology will have to be retrofitted for the first time from 2021 on. According to an expert estimate, this affects 40 per cent of the plants in the gas and steam turbine sector (GuD) and 30 per cent of the plants in the gas turbine sector. Since the first reduction effects are expected from 2021 on, the emission factors of the concerned source groups for 2020 correspond to the reference value from the 2020 submission.

For GuD, the proportional NO<sub>X</sub> reductions are finally calculated based on the assumption that 40 per cent of the plants as a result of SCR retrofitting have a maximum emission value of 20 mg / Nm<sup>3</sup> NO<sub>X</sub> and that 60 per cent of the plants retain the existing emission factor. The values converted into kg / TJ for the emission factors from 2021 on are assumed for the projections for 2025, 2030 and 2035. As an example, the calculation for electricity generation in LCP of the combined cycle plants of public power plants with the reference value 27.48 kg / TJ (31.6 mg / Nm<sup>3</sup>) is shown in (15). The procedure is identical for all other source groups.

    (15) NOx-emission (electricity generation in LCP of the combined cycle plants of public power plants) = (31.602 mg/(Nm^3) * 60% + 20 mg/Nm^3 * 40%) / 1.15 = 23.44 kg/TJ

The calculation of the reductions from 2021 on in the area of gas turbines is considered analogous to that of GuD with a reduction of 30 per cent to 20 mg / Nm<sup>3</sup> NO<sub>X</sub> as a result of the SCR retrofitting and keeping the emission factors constant for 70 per cent of the plants. The exception in the area of gas turbines is the source category of gas turbines in natural gas compressor stations. According to expert estimation, an additional reduction in the existing plants without SCR retrofitting by a delta of 10 mg / Nm<sup>3</sup> to the reference value can be expected. The calculation is given in (16).

    (16) NOx-emission (gas turbines in natural gas compressor stations) = ((72.45 mg/Nm^3 - 10 mg/Nm^3) * 70% + 20 mg/Nm^3 * 30%) / 1.15 = 43.23 kg/TJ


**Reduction of motorised private transport by strengthening the environmental alliance (e. g. public transport, cycling and walking):**

The WAM scenario includes one further measure in the transport sector: the promotion of public transport, cycling and walking. Therefore, the activity rates for in town road transport with passenger cars were reduced by 5 per cent compared to the WM scenario.

**Reduction in agriculture through a bundle of measures quantified as an agricultural package:**

Basis for modeling of NH<sub>3</sub> emissions was the 2020 submission of emission reporting (Thünen-Report 77((Thuenen-Report 77 (2020): Calculations of gaseous and particulate emissions from German agriculture 1990 – 2018, https://www.thuenen.de/media/publikationen/thuenen-report/Thuenen_Report_77.pdf))). Starting point were the projections of the Thünen baseline projection 2020 - 2030((Thuenen-Report 82 (2020): Thünen-Baseline 2020 – 2030: Agrarökonomische Projektionen für Deutschland, https://www.thuenen.de/media/publikationen/thuenen-report/Thuenen_Report_82.pdf)). For further description please check the chapter ‚WM scenario‘. Modeled mitigation measures are according to the National Air Pollution Control Programme 2019 (NAPCP 2019) and, additionally, from the Climate Protection Programme 2030.

In the projections of NAPCP 2019, two variants had been calculated in 2030: 

   - The measures are carried out in full.
   - Small and very small farms are excluded from the measures.

Small farm exclusions resulted in mitigation being smaller by approx. 3 per cent. In the updated projections, small farm exclusions, other exceptions from and implementation of measures deviating from the assumptions were not modeled explicitly. Instead, it is assumed that they result in 10 per cent lower mitigation.

For calculating the emission mitigation potential in 2025 the assumptions for 2030 (described below) were assumed either to be only partially achieved or to be already fully achieved depending on the measure. That is determined by the assumed time that it will take for each measure until it reaches the assumed effect in practice after implementation.

The inventory model can only calculate complete scenarios. The effect of individual measures was quantified by starting with the baseline scenario and sequentially calculating scenarios with mitigation measures added until arriving at the complete WAM scenario. Because mitigation effects of measures are interdependent, the quantified effects of individual measures partly depend on the order of scenarios and cannot account for all interactions. Therefore, the reported additional reduction achieved by a measure cannot to be equated with an isolated effect of the measure. Focus of interpretation should be the complete WAM scenario that includes all mitigation measures and the effect of their interactions. In the WAM scenario, the measures listed below reduce NH<sub>3</sub> emissions by 57.8 kt NH<sub>3</sub>.

   * 70 per cent of the cattle and pig slurry is digested in biogas plants (Measure 3.4.5.1 of the Climate Protection Programme 2030).\\ \\ __Assumptions to model the mitigation potential in 2030:__ The proportion of liquid manure going into a biogas plant was set to 70 per cent for both cattle and pigs (the proportions of solid cattle manure and poultry manure that are digested remain as in 2018).\\ \\ Calculated emission reduction in kt NH<sub>3</sub>: - 5.06 (i. e. emission increase)
   
   * No use of broadcast application on uncultivated arable land and incorporation of liquid manure within an hour. This measure only affects liquid manure (slurry, leachate, digestates).\\ \\ __Assumptions to model the mitigation potential in 2030:__ The distribution frequencies already reduced in the baseline for broadcast application with incorporation <1h and <4h (the latter only exists for manure according to DÜV 2020) were added to the corresponding frequencies for trailing hose application and set to zero for broadcast application. In addition, the incorporation of leachate within 4 hours, which is still permitted under DÜV 2020, was reduced to incorporation within 1 hour.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 1.47\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: - 3.59

   * Uncovered external storage facilities for liquid manure / digestates are at least covered with a plastic film or comparable technology. A one-to-one implementation in inventory model GAS-EM is not possible, since for digestates only "gas-tight storage" and "non-gas-tight storage" is implemented.\\ \\ __Assumptions to model the mitigation potential in 2030:__ The current distribution frequencies for "external slurry storage facility without cover", "external slurry storage facility with natural floating cover" and "external slurry storage facility with artificial floating cover" have been set to zero and added to the distribution frequency for "external slurry storage facility with foil cover". In the case of digestate storage facilities, it was assumed that 90 per cent of the digestate storage facilities are gas-tight. In the baseline (and also currently) around 60 per cent of the digestate storage facilities are gas-tight.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 6.76\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 3.16


   * Air scrubber systems in 75 per cent of the agricultural operations regulated under IED (permitted after type of procedure G in the 4<sup>th</sup> BImSchV), 25 per cent of the agricultural IED operations reduce 40 per cent of emissions through further system-integrated measures in housing.\\ Air scrubber systems for pigs reduce the NH<sub>3</sub> emissions in the stable by 80 per cent on average; this percentage is retained. A reduction rate of 70 per cent is assumed for air scrubber systems for poultry. It should be noted that the reductions were not calculated for turkeys, as these are excluded from the requirement in the Technical Instructions on Air Quality Control (TA-Luft).\\ \\ __Assumptions to model the mitigation potential in 2030:__ For pigs and poultry, the mean reduction performance from air scrubber systems and “further system-integrated measures” was calculated (pigs: 0.75 * 80 % + 0.25 * 40 % = 70 %; poultry: 0.75 * 70 % + 0.25 * 40% = 62.5 %) and with this the emissions of the animals in agricultural IED operations are calculated as if they were air scrubber systems with a correspondingly lower efficiency.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 13.61\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 16.77


   * 75 per cent of the agricultural operations that are permitted after type of procedure V in the 4<sup>th</sup> BImSchV (smaller than IED operations) reduce 40 per cent through system-integrated measures in housing, 25 per cent of these operations do not reduce emissions.\\ \\ __Assumptions to model the mitigation potential in 2030:__ Agricultural operations (type of procedure V) reduce the emissions from housing by a total of 30 % (0.75 * 40 % + 0.25 * 0 % = 30 %). This was mathematically integrated into the measure above. This results in the following total housing reduction performances for the individual animal categories (rounded reduction percentages are shown, unrounded numbers were used for calculation):\\ \\ 
    * Sows: an effective emission reduction of 63.0 per cent was calculated for 54.2 per cent of the animals
    * Weaners: an effective emission reduction of 59.4 per cent was calculated for 45.8 per cent of the animals
    * Fattening pigs: an effective emission reduction of 59.4 per cent was calculated for 27.1 per cent of the animals
    * Laying hens: an effective emission reduction of 53.2 per cent was calculated for 85.1 per cent of the animals
    * Broilers: an effective emission reduction of 59.0 per cent was calculated for 92.8 per cent of the animals
    * Pullets: an effective emission reduction of 58.9 per cent was calculated for 82.1 per cent of the animals
    * Ducks: an effective emission reduction of 62.5 per cent was calculated for 20.6 per cent of the animals\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 2.13\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 18.90


   * 50 per cent of slurry storage underneath slatted floors is replaced by external storage with at least a plastic film cover\\ \\ __Assumptions to model the mitigation potential in 2030:__ The current distribution frequency for "storage under slatted floor" has been halved and this amount has been added to the distribution frequency for "external slurry storage with foil cover".\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 0.77\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 19.68


   * 5 per cent reduction of N excretion by protein-optimized feeding in cattle husbandry\\ \\ __Assumptions to model the mitigation potential in 2030:__ The N and TAN excretions in the inventory model were reduced with a reduction factor of 0.95.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 11.44\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 31.12


   *System-integrated measures in cattle housing (> 100 cattle), 50 per cent implemented\\ The introduction of a "grooved floor" was calculated as an additional measure in cattle housing, i.e. the stable is kept clean by regularly wiping the floor. For this, an emission reduction of 25 per cent compared to a normal, slurry-based loose housing is assumed. Since 50 per cent implementation is assumed for this measure, the average reduction is 12.5 per cent.\\ Stable size distributions are not known for Germany. As an alternative, herd sizes for 2016 were calculated and made available by the Federal Statistical Office. According to this, 80 per cent of all dairy cows and 72 per cent of all heifers and male beef cattle are kept in herds with greater than / equal to 100 cattle.\\ \\ __Assumptions to model the mitigation potential in 2030:__ For dairy cows, heifers and male beef cattle with herd sizes greater than or equal to 100 cattle, the emission factor for “slurry-based loose housing” was reduced by 12.5 per cent. Mathematically, the following emission reductions in the EF result: For dairy cows from 19.7 to 17.730; for heifers and male beef cattle from 19.7 to 17.927 kg NH<sub>3</sub>-N per kg TAN. For the other cattle categories, “slurry-based loose housing” plays only a minor or no role and no effect of this measure was projected.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 4.34\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 35.46


   * Application of liquid manure on tilled fields and grassland only with injection / slot techniques or acidification, 50 per cent implemented\\ For the sake of simplicity and due to data limitations, it was assumed for the calculation that the emissions are reduced in the same way with acidification as with the use of injection / slot techniques.\\ \\ __Assumptions to model the mitigation potential in 2030:__ The current distribution frequencies for application on grassland and in the stand (except for "slurry cultivators") have been halved and the remaining half has been added to the distribution frequency for "injection techniques". The EFs for injection / slot techniques / acidification (based on TAN, in kg per kg N) are: for cattle slurry or digestates: 0.24; for pig slurry: 0.06 and for leachate 0.04.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 22.23\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 57.69


   * Organic farming on 20 per cent of the area (Measure 3.4.5.3 of the Climate Protection Programme 2030)\\ Underlying changes were taken from parallel projections for the 2021 Projection Report. With an increased expansion of organic farming to 20 per cent of the agricultural area by the year 2030 (at the same time the goal of the German Sustainability Strategy), in comparison to a more moderate expansion to 14 per cent, there is in particular a reduction in the mineral fertilizer applied. In addition, projected increase of animal performance is slightly reduced compared to the baseline. There are further changes for the cultivated areas and yields. However, the latter have no additional impact on the level of NH<sub>3</sub> emissions.\\ \\ __Assumptions to model the mitigation potential in 2030:__ A new input data set concerning mineral fertilizers, animal performance, cultivated areas and yields was taken from the GHG projections. Otherwise it was calculated as in measure 3.9.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 0.30 (through animal performance), 2.29 (through less mineral fertilizers)\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 60.28
	
   * Reduction of the N balance to 70 kg / ha (Measure 3.4.5.1 of the Climate Protection Programme 2030)\\ To achieve the climate protection goal (also a goal of the German Sustainability Strategy) of the overall balance of 70 kg N / ha (three-year average) in 2030, the N input must be further reduced beyond the previous measures (see 2021 Projection Report).\\ \\ __Assumptions to model the mitigation potential in 2030:__ The N supply via mineral fertilizers was reduced by 8 kg / ha.\\ \\ Calculated additional emission reduction in kt NH<sub>3</sub>: 3.94\\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 64.22


   * Subtraction of 10 per cent on the total reduction\\ \\ __Assumptions to model the mitigation potential in 2030:__ In order to take into account an incomplete implementation of the measures, such as exceptions for small and very small farms, the overall reduction is reduced by 10 per cent at the end.\\ \\ Calculated cumulative emission reduction in kt NH<sub>3</sub>: 57.80


**Reduction in industrial processes through the optional measure g) of the National Air Pollution Control Programme:**

For the additional emission reduction of sulfur dioxide, the optional measure g) from the National Air Pollution Control Programme according to Article 6 and Article 10 of Directive (EU) 2016/2284 is assumed to be adopted and continued for the WAM scenario. It is assumed that a future lower-sulfur fuel use or more efficient exhaust gas cleaning technology will result in a 20 per cent reduction in the emission factor for sulfur dioxide in the source groups with the highest sulfur dioxide emissions in the NFR sectors of industrial processes (NFR 2). It is further assumed that the first reduction effects will show up by 1 January 2025 at the latest and that implementation has to be completed beforehand. 

Since the first reduction effects are to be expected from 2025 on, it is assumed that the emission value for 2020 corresponds to that of the reference value from the 2020 submission. Thus, the emission factors for 2025, 2030 and 2035, as shown in (17) using the example of the glass production of flat glass (reference value 1.96 kg / TJ), are recalculated.

    (17) SO2-emission (glass production of flat glas) = 1.96 kg/TJ * 80% = 1.57 kg/TJ


The results as presented at the top of the page have been widely circulated and discussed with sector experts from industry, science and public authorities.
