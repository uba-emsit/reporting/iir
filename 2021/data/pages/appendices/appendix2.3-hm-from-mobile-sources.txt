====== Appendix 2.3 - Heavy Metal Exhaust Emissions From Mobile Sources ======

===== Road Transport =====

For heavy-metal exhaust emissions (other then lead from leaded gasoline), tier1 values have been derived from tier1 default values provided in the 2019 EMEP/EEA Guidebook.

__Table 1: Tier1 default emisison factors applied to road vehicles, in g/TJ__
|                        ^  **Pb**  ^  **Cd**  ^  **Hg**  ^  **As**  ^  **Cr**  ^  **Cu**  ^  **Ni**  ^  **Se**  ^  **Zn**  ^
^ Diesel oil             |  0.012   |  0.001   |  0.123   |  0.002   |  0.198   |  0.133   |  0.005   |  0.002   |  0.419   |
^ Biodiesel<sup>1</sup>  |  0.013   |  0.001   |  0.142   |  0.003   |  0.228   |  0.153   |  0.005   |  0.003   |  0.483   |
^ Gasoline fuels         |  0.051   |  2.10    |  0.196   |  0.007   |  8.96    |  357     |  14.7    |  2.09    |  208     |
^ CNG                    |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |
^ LPG                    |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |
^ Biogas                 |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |
<sup>1</sup> values differ from EFs applied for fossil diesel oil to take into account the specific NCV of biodiesel

The 2019 GB provides separate values for Hm from fuel combustion (icluding engine wear) and the unintended co-incineration of lube oil.
The latter are reported in NFR 2.D as emissions from product use. (//Note: This country-specific approach has been discussed and accepted at both the 2018 TFEIP meeting and the 2018 NEC review.//)

{{ :general:appendices:emepeea2019_1a3b_table3.78_ef_hm_fuel.png ?700 }}
{{ :general:appendices:emepeea2019_1a3b_table3.78_ef_hm_lubricant.png ?700 }}

===== Non-road Mobile Machinery in 1.A.2.g vii, 1.A.4.a.ii,1.A.4.b.i, 1.A.4.c.ii and 1.A.5.b i =====

Without country-specific information, tier1 values are applied. 

However, instead of using the emission factors provided in (EMEP/EEA, 2019) [(EMEPEEA2019)], Table 3-1 Tier 1 emission factors for off-road machinery, EF for exhaust HMs from GB chapter 1.A.3.b.i, 1.A.3.b.ii, 1.A.3.b.iii, 1.A.3.b.iv, page 93 ff are applied here too to allow for the separate reporting of emissions from fuel and enigine wear and the unintended co-incineration of lube oil. 
Here, separate tier1 default EFs are provided there in tables 3.77 and 3.78 of the GB chapter for road transport. 

Heavy-metal emissions from lubricants (as far as not used in 2-stroke mix) are reported under NFR 2.G as emissions from product use. 

//(Note: Until submission 2017, the EMEP/EEA default EFs provided for NRMM were used in the German inventory. As these EFs do not differentiate between fuel combustion and lubricant co-incineration, the inventory compiler decided to apply the more specific EFs from road transport to NRMM in 1.A.2.g vii, 1.A.4.a ii, b ii and c ii and 1.A.5.b, too. This country-specific approach has been discussed and accepted at both the 2018 TFEIP meeting and the 2018 NEC review.)//

__Table 2: Tier1 default emission factors applied to NRMM, in g/TJ__
|                                        ^  **Pb**  ^  **Cd**  ^  **Hg**  ^  **As**  ^  **Cr**  ^  **Cu**  ^  **Ni**  ^  **Se**  ^  **Zn**  ^
^ Diesel oil                             |  0.012   |  0.001   |  0.123   |  0.002   |  0.198   |  0.133   |  0.005   |  0.002   |  0.419   |
^ Biodiesel<sup>1</sup>                  |  0.013   |  0.001   |  0.142   |  0.003   |  0.228   |  0.153   |  0.005   |  0.003   |  0.483   |
^ Gasoline fuels - 4-stroke              |  0.037   |  0.005   |  0.200   |  0.007   |  0.145   |  0.103   |  0.053   |  0.005   |  0.758   |
^ Gasoline fuels - 2-stroke<sup>2</sup>  |  0.051   |  2.10    |  0.196   |  0.007   |  8.96    |  357     |  14.7    |  2.09    |  208     |
^ LPG (1.A.4.a ii only)                  |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |
<sup>1</sup> values differ from EFs applied for fossil diesel oil to take into account the specific NCV of biodiesel \\
<sup>2</sup> including the HM of 1:50 lube oil mixed to the gasoline

===== Railways =====

__Table 3: Tier1 default emission factors applied to railway vehicles, in g/TJ__
|                        ^  **Pb**            ^  **Cd**            ^  **Hg**            ^  **As**             ^  **Cr**            ^  **Cu**             ^  **Ni**            ^  **Se**            ^  **Zn**             ^
^ Diesel oil             |  1.21<sup>2</sup>  |  0.23<sup>1</sup>  |  0.12<sup>1</sup>  |  0.002<sup>2</sup>  |  1.16<sup>2</sup>  |  39.57<sup>2</sup>  |  1.63<sup>2</sup>  |  0.23<sup>2</sup>  |  23.28<sup>2</sup>  |
^ Biodiesel<sup>3</sup>  |  0.01              |  0.001             |  0.14              |  0.003              |  0.23              |  0.15               |  0.01              |  0.003             |  0.48               |
^ Lignite Briquettes     |   NE                                                                                                                                                                                  |||||||||
^ Raw Lignite            |   NE                                                                                                                                                                                  |||||||||
^ Hard Coal              |   NE                                                                                                                                                                                  |||||||||
^ Hard Coal Coke         |   NE                                                                                                                                                                                  |||||||||
<sup>1</sup> tier1 default from [(EMEPEEA2019)], chapter: 1.A.3.b i-iv - Road transport: exhaust emissions: tier1 value for diesel vehicles \\
<sup>2</sup> tier1 default from [(EMEPEEA2019)], chapter: 1.A.3.c - Railways \\
<sup>3</sup> values differ from EFs applied for fossil diesel oil to take into account the specific NCV of biodiesel \\
(//NOTE: Assuming that biodiesel contains far less HMs than fossil diesel oil, similar values are applied to all mobile sources using this biogenic fuel.//)

As the EMEP/EEA GB 2019 does not provide specific defaults for **Pb, Hg and As**, the EF applied here has been derived from chapter: 1.A.3.b i-iv - Road transport: exhaust emissions: tier1 value for diesel vehicles. 

===== Inland Vessels and Ships in 1.A.3.d ii =====

__Table 4: Tier1 default emission factors applied to inland ships and vessels, in g/TJ__
|                        ^  **Pb**  ^  **Cd**  ^  **Hg**  ^  **As**  ^  **Cr**  ^  **Cu**  ^  **Ni**  ^  **Se**  ^  **Zn**  ^
^  Diesel oil            |  1.21    |  0.233   |  0.123   |  0.002   |  1.16    |  39.6    |  1.63    |  0.233   |  23.3    |
^  Biodiesel<sup>1</sup> |  0.013   |  0.001   |  0.142   |  0.003   |  0.23    |  0.15    |  0.005   |  0.003   |  0.48    |
<sup>1</sup> similar EF for biodiesel applied for all mobile sources

===== Maritime Vessels and Ships in 1.A.3.d i, 1.A.3.d ii, 1.A.4.c iii and 1.A.5.b iii =====

The following table provides the tier1 EF applied for HMs from ships and vessels in both civil and military operation in NFR categories 1.A.3.d i -International Maritime Navigation, 1.A.3.d ii - National Navigation (Shipping), 1.A.4.c iii -Fisheryand 1.A.5.b iii - Other: Military Navigation.

__Table 4: Tier1 default emission factors applied to maritime ships and vessels in g/TJ__
|                             ^  **Pb**  ^  **Cd**  ^  **Hg**  ^  **As**  ^  **Cr**  ^  **Cu**  ^  **Ni**  ^  **Se**  ^  **Zn**  ^
^ Heavy Fuel oil<sup>1</sup>  |  4.46    |  0.50    |  0.50    |  16.9    |  17.8    |  31.0    |  793     |  5.20    |  29.7    |
^ Diesel oil<sup>2</sup>      |  3.03    |  0.23    |  0.70    |  0.93    |  1.16    |  20.5    |  23.3    |  2.33    |  27.9    |
^ Biodiesel<sup>3</sup>       |  0.013   |  0.0013  |  0.142   |  0.003   |  0.23    |  0.15    |  0.005   |  0.003   |  0.48    |
<sup>1</sup> tier1 defaults from [(EMEPEEA2019)], Chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii Navigation: Table 3-1 \\
<sup>2</sup> tier1 defaults from [(EMEPEEA2019)], Chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii Navigation: Table 3-2 \\
<sup>3</sup> similar EF for biodiesel applied for all mobile sources
(//NOTE: Assuming that biodiesel contains far less HMs than fossil diesel oil, similar values are applied to all mobile sources using this biogenic fuel.//)

===== Aircraft in 1.A.3.a and 1.A.5.b ii =====

The EMEP/EEA GB 2019 does not provide specific defaults for HM emissions from the combustion of jet kerosene and aviation gasoline, stating that for for aviation gasoline these emissions are //not estimated// (NE): 

Therefore, the inventory compiler decided to apply the tier1 EF from gasoline fuel used in non-road mobile machinery here, too.
Although the Party assumes that HM emissions are also likely to occur from the combustion of jet kerosene, no gap-filling is carried out for this fuel. Instead, all HM emission from jet kerosene are reported as //not estimated// (NE).

__Table 5: Tier1 default emisison factors applied to aircraft, in g/TJ__
|                    ^  **Pb**             ^  **Cd**  ^  **Hg**  ^  **As**  ^  **Cr**  ^  **Cu**  ^  **Ni**  ^  **Se**  ^  **Zn**  ^
^ Kerosene           |   NE                                                                                                |||||||||
^ Aviation gasoline  |  9,481<sup>1</sup>  |  0.005   |  0.200   |  0.007   |  0.145   |  0.103   |  0.053   |  0.005   |  0.758   |
<sup>1</sup> estimated from average lead content of AvGas 100 LL (see also: 1.A.3.a ii (i) and FAQs) of 0.56 g Pb/liter

[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019; Chapter 1.A - Combustion; URL: https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion, Copenhagen, 2019.)]