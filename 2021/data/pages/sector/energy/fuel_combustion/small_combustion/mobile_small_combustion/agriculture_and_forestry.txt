====== 1.A.4.c ii - Agriculture/Forestry/Fishing: Off-Road Vehicles and Other Machinery ======

===== Short description =====

Under sub-category //1.A.4.c ii - Agriculture/Forestry/Fishing: Off-road Vehicles and other Machinery// 
{{  :sector:energy:fuel_combustion:small_combustion:chainsaw.png?nolink&200}} fuel combustion activities and resulting emissions from off-road vehicles and machinery used in agriculture and forestry are reported seperately.


^                                 NFR Code ^ Source category                                                                                                                                                       ^ Method   ^  AD     ^  EF        ^  Key Category Analysis                                                     ^
| 1.A.4.c ii                               | Agriculture/Forestry/Fishing: Off-Road Vehicles and Other Machinery                                                                                                   |  T1, T2  |  NS, M  |  CS, D, M  |  **L & T**: BC / **L**: NO<sub>x</sub>, PM<sub>2.5</sub>, PM<sub>10</sub>  |
| including mobile sources sub-categories                                                                                                                                                                                                                                                                                    ||||||
| 1.A.4.c ii (a)                           | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry:agriculture| Off-road Vehicles and Other Machinery: Agriculture ]]  |  T1, T2  |  NS, M  |  CS, D, M  |  -                                                                         |
| 1.A.4.c ii (b)                           | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry:forestry| Off-road Vehicles and Other Machinery: Forestry ]]        |  T1, T2  |  NS, M  |  CS, D, M  |  -                                                                         |


===== Methodology=====

====Activity data====

Sector-specific consumption data is included in the primary fuel-delivery data are available from NEB line 67: 'Commercial, trade, services and other consumers' (AGEB, 2020) [(AGEB2020)].

{{  :sector:energy:fuel_combustion:small_combustion:traktor.png?nolink&300}}

__Table 1: Sources for primary fuel-delivery data__
|| through 1994 || **AGEB** - National Energy Balance, line 79: 'Haushalte und Kleinverbraucher insgesamt'  ||
|| as of 1995 || **AGEB** - National Energy Balance, line 67: 'Gewerbe, Handel, Dienstleistungen u. übrige Verbraucher'  ||

Following the deduction of energy inputs for military vehicles as provided in (BAFA, 2020) [(BAFA2020)], the remaining amounts of gasoline and diesel oil are apportioned onto off-road construction vehicles (NFR 1.A.2.g vii) and commercial/institutional used off-road vehicles (1.A.4.a ii) as well as agriculture and forestry (NFR 1.A.4.c ii) based upon annual shares derived from TREMOD MM (Knörr et al. (2020b)) [(KNOERR2020b)] (cf. superordinate chapter).

To provide more specific information on mobile sources in agriculture and forestry, the inventory compiler further devides NFR sector 1.A.4.c ii into **1.A.4.c ii (i) - NRMM in agriculture** in and **1.A.4.c ii (ii) - NRMM in forestry**. 

__Table 2: Annual percentual contribution of NFR 1.A.4.c ii to the primary fuel delivery data provided in NEB line 67__
|                              |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  | **2019**  |
^ Diesel fuels                                                                                                                                                                                        |||||||||||||||
| **1.A.4.c ii (i)**           |      47,6% |      45,6% |      43,9% |      46,2% |      47,5% |      47,2% |      47,3% |      48,0% |      47,8% |      48,3% |      48,5% |      48,5% |      48,4% |     48,4% |
| **1.A.4.c ii (ii)**          |      2,41% |      1,36% |      2,16% |      2,88% |      2,92% |      2,99% |      2,77% |      2,76% |      2,81% |      2,89% |      2,72% |      2,79% |      3,35% |     3,54% |
^ Gasoline fuels <sup>1</sup>                                                                                                                                                                         |||||||||||||||
| **1.A.4.c ii (ii)**          |      68,5% |      40,3% |      44,9% |      41,4% |      35,5% |      35,6% |      33,1% |      32,9% |      33,1% |      33,3% |      31,6% |      31,9% |      35,8% |     36,8% |
source: own estimations based on Knörr et al. (2020b) [(KNOERR2020b)]
<sup>1</sup> no gasoline used in agriculatural vehicles and mobile machinery 

__Table 3: Annual mobile fuel consumption in agriculture and forestry, in terajoules__
|                   |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  |  **2019**  |
^ Diesel oil        |     55.958 |     45.954 |     43.747 |     40.309 |     44.606 |     45.576 |     44.609 |     47.090 |     48.977 |     51.836 |     53.891 |     55.585 |     52.402 |     53.216 |
^ Biodiesel         |      3.093 |      3.004 |      3.325 |      3.022 |      1.543 |      1.404 |        392 |        383 |        412 |      1.660 |      1.575 |      1.588 |      1.741 |      1.739 |
^ Gasoline          |          0 |          0 |          0 |      2.576 |      3.420 |      3.180 |      3.148 |      2.793 |      3.010 |      2.835 |      2.857 |      2.967 |      3.046 |      3.030 |
^ Biogasoline       |          0 |          0 |          0 |         21 |         60 |         58 |         17 |         16 |         18 |         72 |         68 |         67 |         78 |         75 |
| **Ʃ 1.A.4.c ii**  ^ 59.051     ^ 48.958     ^ 47.071     ^ 45.928     ^ 49.629     ^ 50.218     ^ 48.166     ^ 50.282     ^ 52.417     ^ 56.404     ^ 58.392     ^ 60.207     ^ 57.267     ^ 58.060     ^

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_ad.png?700 }}
{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_ad_bio.png?700 }}

==== Emission factors ====

The emission factors applied here are of rather different quality:

Basically, for all **main pollutants**, **carbon monoxide** and **particulate matter**, annual IEF modelled within TREMOD MM [(KNOERR2020b)] are used, representing the sector's vehicle-fleet composition, the development of mitigation technologies and the effect of fuel-quality legislation. 

For Information on the country-specific implied emission factors applied to mobile machinery in agriculture and forestry, please refer to the respective sub-chapters linked above.

For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.

===== Discussion of emission trends =====

__Table: Outcome of Key Catgegory Analysis__
|  for: ^  NO<sub>x</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  BC             ^
|   by: |  Level           |  L                 |  L                |  Level & Trend  |

>  **NFR 1.A.4.c ii** is key source for emissions of **NO<sub>x</sub>**, **BC**, **PM<sub>2.5</sub>** and **PM<sub>10</sub>**.

==== Unregulated pollutants (Ammonia, HMs, POPs, ...)====

For all unregulated pollutants, emission trends directly follow the trend in fuel consumption.

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_em_nh3.png?700 }}

In contrast to the main pollutants, all heavy-metal and POP emissions are calculated based on default EF from [(EMEPEEA2019)]. 

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_em_cd.png?700 }}

Here, exemplary for cadmium, the extreme steps in emission estimates result from two effects: 

(i) the annual amounts of gasoline fuels allocated to NFR 1.A.4.c ii depend on the amounts delivered to the military also covered in NEB line 67. (see superordinate chapter for further information). This approach results in strong declines in gasoline consumption after 2007 and 2011 followed by an increase after 2014.

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_ad_ok.png?700 }}

__Table 4: Development of gasoline consumption in NFR 1.A.4.c ii, in terajoules__
|              ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^
^ Gasoline     |  1.543 |  1.404 |    392 |    383 |    412 |  1.660 |  1.575 |  1.588 |  1.741 |  1.739 |
^ Biogasoline  |     60 |     58 |     17 |     16 |     18 |     72 |     68 |     67 |     78 |     75 |

(ii) All gasoline fuels allocated to NFR 1.A.4.c ii are used in 2-stroke-engines in forestry equipment. As the 2-stroke fuel also includes lubricant oil, the fuel's heavy metal content is significantly higher than that of 4-stroke gasoline (or diesel fuels). 
(see Appendix 2.3 for more information on the reporting of HM emissions.)

__Table 5: Tier1 default emission factors applied to NRMM, in g/TJ__
|                                        ^  Pb    ^  Cd    ^  Hg    ^  As    ^  Cr    ^  Cu    ^  Ni    ^  Se    ^  Zn    ^
^ Diesel oil                             |  0.012 |  0.001 |  0.123 |  0.002 |  0.198 |  0.133 |  0.005 |  0.002 |  0.419 |
^ Biodiesel<sup>1</sup>                  |  0.013 |  0.001 |  0.142 |  0.003 |  0.228 |  0.153 |  0.005 |  0.003 |  0.483 |
^ Gasoline fuels - 4-stroke              |  0.037 |  0.005 |  0.200 |  0.007 |  0.145 |  0.103 |  0.053 |  0.005 |  0.758 |
^ Gasoline fuels - 2-stroke<sup>2</sup>  |  0.051 |   2.10 |  0.196 |  0.007 |   8.96 |    357 |   14.7 |   2.09 |    208 |
^ LPG (1.A.4.a ii only)                  |  NE                                                                    |||||||||
<sup>1</sup> values differ from EFs applied for fossil diesel oil to take into account the specific NCV of biodiesel
<sup>2</sup> including the HM of 1:50 lube oil mixed to the gasoline
Hence, emission estimates reported for cadmium are significantly higher for years with higher gasoline use (in 2-stroke enignes). 

==== Regulated pollutants ====

For all regulated pollutants, emission trends follow not only the trend in fuel consumption but also reflect the impact of fuel-quality and exhaust-emission legislation.

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_em_nox.png?700 }}
{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_em_sox.png?700 }}

=== Particulate matter & Black carbon===

Over-all PM emissions are by far dominated by emissions from diesel oil combustion with the falling trend basically following the decline in fuel consumption between 2000 and 2005. 
Nonetheless, the decrease of the over-all emission trend was and still is amplified by the expanding use of particle filters especially to eliminate soot emissions.

Additional contributors such as the impact of TSP emissions from the use of leaded gasoline (until 1997) have no significant effect onto over-all emission estimates.

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4cii_em_pm.png?700 }}

===== Recalculations =====

Revisions in **activity data** result from revised percental shares with respect to NEB line 67 as well as the implementation of primary activity data from the now finalised NEB 2018.

__Table 6: Revised percental shares__
|                                   |  **1990**  |  **1995**  |  **2000**  | **2005**  | **2010**  | **2011**  |  **2012**  |  **2013**  | **2014**  | **2015**  |  **2016**  |  **2017**  |  **2018**  |
| 1.A.4.c ii (i) - diesel fuels                                                                                                                                                            ||||||||||||||
^ Submission 2021                   |      0,476 |      0,456 |      0,439 |     0,462 |     0,475 |     0,472 |      0,473 |      0,480 |     0,478 |     0,483 |      0,485 |      0,485 |      0,484 |
^ Submission 2020                   |      0,484 |      0,467 |      0,468 |     0,501 |     0,509 |     0,506 |      0,506 |      0,505 |     0,503 |     0,507 |      0,510 |      0,511 |      0,512 |
^ absolute change                   |     -0,008 |     -0,011 |     -0,030 |    -0,038 |    -0,034 |    -0,034 |     -0,033 |     -0,024 |    -0,025 |    -0,025 |     -0,025 |     -0,025 |     -0,028 |
^ relative change                   |     -1,62% |     -2,30% |     -6,33% |    -7,66% |    -6,75% |    -6,65% |     -6,47% |     -4,80% |    -4,98% |    -4,87% |     -5,00% |     -4,97% |     -5,52% |
| 1.A.4.c ii (ii) - diesel fuels                                                                                                                                                           ||||||||||||||
^ Submission 2021                   |      0,024 |      0,014 |      0,022 |     0,029 |     0,029 |     0,030 |      0,028 |      0,028 |     0,028 |     0,029 |      0,027 |      0,028 |      0,034 |
^ Submission 2020                   |      0,024 |      0,013 |      0,020 |     0,027 |     0,027 |     0,027 |      0,025 |      0,026 |     0,026 |     0,027 |      0,025 |      0,026 |      0,026 |
^ absolute change                   | 0,000      | 0,000      | 0,001      |     0,002 |     0,002 |     0,003 |      0,002 |      0,002 |     0,002 |     0,002 |      0,002 |      0,002 |      0,008 |
^ relative change                   | 1,08%      | 1,62%      | 5,38%      |     8,71% |     9,16% |     9,27% |      9,36% |      7,86% |     8,29% |     8,56% |      8,54% |      8,63% |      29,4% |
| 1.A.4.c ii (ii) - gasoline fuels                                                                                                                                                         ||||||||||||||
^ Submission 2021                   |      0,685 |      0,403 |      0,449 |     0,414 |     0,355 |     0,356 |      0,331 |      0,329 |     0,331 |     0,333 |      0,316 |      0,319 |      0,358 |
^ Submission 2020                   |      0,685 |      0,403 |      0,449 |     0,416 |     0,360 |     0,362 |      0,337 |      0,335 |     0,338 |     0,341 |      0,324 |      0,327 |      0,327 |
^ absolute change                   |      0,000 |      0,000 |      0,000 |    -0,002 |    -0,005 |    -0,005 |     -0,006 |     -0,006 |    -0,007 |    -0,008 |     -0,008 |     -0,009 |      0,031 |
^ relative change                   |      0,00% |      0,00% |      0,00% |    -0,46% |    -1,29% |    -1,46% |     -1,70% |     -1,88% |    -2,06% |    -2,23% |     -2,48% |     -2,66% |      9,34% |

__Table 7: Resulting revision of annual activity data, in terajoules__
|                                |  **1990**  |  **1995**  |  **2000**  | **2005**  | **2010**  | **2011**  |  **2012**  |  **2013**  | **2014**  | **2015**  |  **2016**  |  **2017**  |  **2018**  |
| **Diesel fuels**                                                                                                                                                                      ||||||||||||||
^ Submission 2021                |     55.958 |     45.954 |     43.747 |    42.885 |    48.026 |    48.756 |     47.757 |     49.883 |    51.987 |    54.671 |     56.749 |     58.552 |     55.448 |
^ Submission 2020                |     56.808 |     46.985 |     46.460 |    44.126 |    50.499 |    51.573 |     50.396 |     51.992 |    54.297 |    57.036 |     59.309 |     61.176 |     57.312 |
^ absolute change                |       -849 |     -1.031 |     -2.713 |    -1.241 |    -2.473 |    -2.816 |     -2.640 |     -2.110 |    -2.310 |    -2.364 |     -2.560 |     -2.624 |     -1.864 |
^ relative change                |     -1,50% |     -2,19% |     -5,84% |    -2,81% |    -4,90% |    -5,46% |     -5,24% |     -4,06% |    -4,25% |    -4,15% |     -4,32% |     -4,29% |     -3,25% |
| **Gasoline fuels**                                                                                                                                                                    ||||||||||||||
^ Submission 2021                |      3.093 |      3.004 |      3.325 |     3.043 |     1.603 |     1.462 |        409 |        400 |       430 |     1.732 |      1.644 |      1.655 |      1.819 |
^ Submission 2020                |      3.093 |      3.004 |      3.325 |     3.057 |     1.624 |     1.484 |        416 |        407 |       439 |     1.772 |      1.685 |      1.700 |      1.663 |
^ absolute change                | 0,00       | 0,00       | 0,00       |    -13,96 |    -20,98 |    -21,66 |      -7,06 |      -7,67 |     -9,04 |    -39,59 |     -41,76 |     -45,14 |     155,38 |
^ relative change                | 0,00%      | 0,00%      | 0,00%      |    -0,46% |    -1,29% |    -1,46% |     -1,70% |     -1,88% |    -2,06% |    -2,23% |     -2,48% |     -2,66% |      9,34% |
| **Over-all fuel consumption**                                                                                                                                                         ||||||||||||||
^ Submission 2021                |     59.051 |     48.958 |     47.071 |    45.928 |    49.629 |    50.218 |     48.166 |     50.282 |    52.417 |    56.404 |     58.392 |     60.207 |     57.267 |
^ Submission 2020                |     59.900 |     49.989 |     49.784 |    47.183 |    52.123 |    53.056 |     50.813 |     52.399 |    54.736 |    58.808 |     60.994 |     62.876 |     58.975 |
^ absolute change                |       -849 |     -1.031 |     -2.713 |    -1.255 |    -2.494 |    -2.838 |     -2.647 |     -2.117 |    -2.319 |    -2.404 |     -2.602 |     -2.669 |     -1.708 |
^ relative change                |     -1,42% |     -2,06% |     -5,45% |    -2,66% |    -4,78% |    -5,35% |     -5,21% |     -4,04% |    -4,24% |    -4,09% |     -4,27% |     -4,24% |     -2,90% |

As, in contrast, all **emission factors** remain unrevised compared to last year's susbmission, emission estimates for the years as of 2015 change in accordance with the underlying activity data.

<WRAP center round info 60%>
For pollutant-specific information on recalculated emission estimates for Base Year and 2018, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Uncertainties =====

Uncertainty estimates for **activity data** of mobile sources derive from research project FKZ 360 16 023: "Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland" by (Knörr et al. (2009)) [(KNOERR2009)].

Uncertainty estimates for **emission factors** were compiled during the PAREST research project. Here, the final report has not yet been published.

===== Planned improvements =====

Besides a **routine revision of TREMOD MM**, no specific improvements are planned.

===== FAQs =====

**//Why are similar EF applied for estimating exhaust heavy metal emissions from both fossil and biofuels?//**

The EF provided in [(EMEPEEA2019)] represent summatory values for (i) the fuel's and (ii) the lubricant's heavy-metal content as well as (iii) engine wear. Here, there might be no heavy metal contained the biofuels. But since the specific shares of (i), (ii) and (iii) cannot be separated, and since the contributions of lubricant and engine wear might be dominant, the same emission factors are applied to biodiesel and bioethanol.

[(AGEB2020>AGEB, 2020: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; URL: http://www.ag-energiebilanzen.de/7-0-Bilanzen-1990-2018.html, (Aufruf: 29.11.2020), Köln & Berlin, 2020.)]
[(BAFA2020>BAFA, 2020: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
URL: https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2018_dezember.html, Eschborn, 2020.)]   
[(KNOERR2020b> Knörr et al. (2020b): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Aktualisierung des Modells TREMOD-Mobile Machinery (TREMOD MM) 2020, Heidelberg, 2020.)]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook – 2019, Copenhagen, 2019.)]
[(RENTZ2008> Rentz et al., 2008: Nationaler Durchführungsplan unter dem Stockholmer Abkommen zu persistenten organischen Schadstoffen (POPs), im Auftrag des Umweltbundesamtes, FKZ 205 67 444, UBA Texte | 01/2008, January 2008 - URL: http://www.umweltbundesamt.de/en/publikationen/nationaler-durchfuehrungsplan-unter-stockholmer )]
[(KNOERR2009> Knörr et al. (2009): Knörr, W., Heldstab, J., & Kasser, F.: Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland; final report; URL: https://www.umweltbundesamt.de/sites/default/files/medien/461/publikationen/3937.pdf, FKZ 360 16 023, Heidelberg & Zürich, 2009.)]