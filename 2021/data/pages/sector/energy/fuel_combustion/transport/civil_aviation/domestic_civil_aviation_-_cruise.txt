====== 1.A.3.a ii (ii) - Domestic Civil Aviation: Cruise ======

===== Short description =====

^ Category Code   ^  Method                                                                  ||||^  AD                               ||||^  EF                                       |||||
| 1.A.3.a ii (ii)  |  T1, T2, T3                                                              |||||  NS, M                            |||||  CS, D, M                                 |||||
^  Key Category   ^  SO₂                                        ^  NOₓ  ^  NH₃  ^  NMVOC  ^  CO  ^  BC     ^  Pb  ^  Hg  ^  Cd  ^  Diox  ^  PAH       ^  HCB  ^  TSP  ^  PM₁₀  ^  PM₂.₅  ^
| 1.A.3.a ii (ii)  |  //not included in key category analysis//                                                                                                             |||||||||||||||

In NFR category //1.A.3.a ii (ii) - Domestic Civil Aviation: Cruise// emissions from domestic flights between German airports during cruise stage (above 3,000 feet of altitude) are reported.

In the following, information on sub-category specific activity data, (implied) emission factors and emission estimates are provided.

===== Methodology =====

==== Actitvity Data ====

Specific fuel consumption during LTO-stage is calculated within TREMOD AV as described in the [[sector:energy:fuel_combustion:transport:civil_aviation:start|superordinate chapter]].

__Table 1: annual jet kerosene & avgas consumption during cruise-stage, in terajoules__
|           |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2006**  |  **2007**  |  **2008**  |  **2009**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  |  **2019**  |
^ Kerosene  |     21.690 |     19.937 |     25.301 |     24.071 |     24.736 |     25.337 |     25.111 |     24.048 |     22.503 |     20.552 |     21.026 |     19.762 |     19.038 |     19.195 |     20.067 |     20.793 |     21.067 |     21.573 |
^ Avgas     |      1.580 |        614 |        614 |        291 |        260 |        228 |        259 |        244 |        237 |        283 |        246 |        199 |        180 |        233 |        145 |        142 |        116 |         72 |
source: Knörr et al. (2020c) [(KNOERR2020c)] & Gores (2020) [(GORES2020)]

==== Emission factors ====

All country specific emission factors used for emission reporting were basically ascertained within UBA project FKZ 360 16 029 [(KNOERR2010)] and have since then been compiled, revised and maintained in TREMOD AV [(KNOERR2020c)]. 

For more information, please see the [[sector:energy:fuel_combustion:transport:civil_aviation:start|superordinate chapter]] on civil aviation.

__Table 2: Annual country-specific emission factors, in kg/TJ__
|                   |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2006**  |  **2007**  |  **2008**  |  **2009**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  | **2019**  |
| **JET KEROSENE**                                                                                                                                                                                                                         |||||||||||||||||||
^ NH<sub>3</sub>    |       3,98 |       3,95 |       3,95 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |       3,97 |      3,97 |
^ NMVOC             |       14,4 |       16,0 |       16,6 |       18,9 |       19,9 |       20,4 |       20,6 |       20,3 |       19,9 |       19,9 |       20,2 |       21,7 |       22,7 |       22,0 |       17,7 |       18,1 |       17,7 |      19,2 |
^ NO<sub>x</sub>    |        337 |        375 |        348 |        340 |        341 |        347 |        358 |        368 |        374 |        376 |        381 |        383 |        381 |        386 |        397 |        400 |        396 |       390 |
^ SO<sub>x</sub>    |       19,7 |       19,5 |       19,5 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |       19,6 |      19,6 |
^ BC                |       1,82 |       2,15 |       2,00 |       2,20 |       2,28 |       2,23 |       1,98 |       1,97 |       2,02 |       2,00 |       2,02 |       2,02 |       1,95 |       2,05 |       1,95 |       1,99 |       2,18 |      2,26 |
^ PM                |       3,80 |       4,48 |       4,16 |       4,58 |       4,75 |       4,65 |       4,13 |       4,11 |       4,22 |       4,17 |       4,20 |       4,21 |       4,07 |       4,27 |       4,06 |       4,15 |       4,54 |      4,71 |
^ CO                |        147 |        149 |        186 |        203 |        203 |        203 |        204 |        202 |        197 |        197 |        201 |        212 |        219 |        214 |        154 |        151 |        152 |       155 |
| **AVGAS**         |            |            |            |            |            |            |            |            |            |            |            |            |            |            |            |            |            |           |
^ NH<sub>3</sub>    |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE        |  NE       |
^ NMVOC             |        549 |        559 |        545 |        551 |        550 |        548 |        549 |        551 |        548 |        558 |        557 |        557 |        556 |        555 |        557 |        552 |        561 |       579 |
^ NO<sub>x</sub>    |        132 |        134 |        130 |        132 |        131 |        130 |        131 |        131 |        130 |        134 |        133 |        134 |        133 |        132 |        133 |        130 |        135 |       143 |
^ SO<sub>x</sub>    |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |       0,46 |      0,46 |
^ BC                |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,42 |       0,43 |       0,42 |      0,42 |
^ PM                |       2,78 |       2,81 |       2,78 |       2,80 |       2,80 |       2,81 |       2,80 |       2,81 |       2,81 |       2,82 |       2,83 |       2,82 |       2,83 |       2,83 |       2,82 |       2,85 |       2,83 |      2,80 |
^ TSP               |       17,9 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |       18,0 |      18,0 |
^ CO                |      21184 |      21581 |      21324 |      21543 |      21663 |      21780 |      21630 |      21761 |      21850 |      21864 |      21979 |      21921 |      22002 |      22097 |      21979 |      22459 |      21962 |     21025 |
<sup>1</sup> EF(TSP) also applied for PM<sub>10</sub> and PM<sub>2.5</sub> (assumption: > 99% of TSP consists of PM<sub>2.5</sub>) \\
<sup>2</sup> estimated via a f-BC of 0.48 as provided in [(EMEPEEA2019)], Chapter: 1.A.3.a, 1.A.5.b Aviation, page 49: "Conclusion". 
      
<WRAP center round info 100%>
For the country-specific emission factors applied for particulate matter, no clear indication is available, whether or not condensables are included.
</WRAP> 

<WRAP center round info 100%>
For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.
</WRAP>

===== Trend discussion for Key Sources =====

NFR 1.A.3.a ii (ii) - Domestic Civil Aviation - Cruise is **not included in the national emission totals** and hence **not included in the key category analysis**.

===== Recalculations =====

**Activity data** have been revised for all years within TREMOD AV to keep in line with information available from Eurocontrol's AEM model [(EUROCONTROL2020)]. 

Furthermore, for the first time, the use of aviation gasoline (avgas) for international flights has been estimated. These amounts have been re-allocated from NFR 1.A.3.a ii (i), where 100% of avgas consumption has been allocated so far. 

__Table 3: Revised annual fuel consumption in 1.A.3.a ii (ii), in terajoules__
|                   |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2006**  |  **2007**  |  **2008**  |  **2009**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  |
| **JET KEROSENE**                                                                                                                                                                                                              ^^^^^^^^^^^^^^^^^^
^ Submission 2021   |     21.690 |     19.937 |     25.301 |     24.071 |     24.736 |     25.337 |     25.111 |     24.048 |     22.503 |     20.552 |     21.026 |     19.762 |     19.038 |     19.195 |     20.067 |     20.793 |     21.067 |
^ Submission 2020   |     20.024 |     20.875 |     22.967 |     21.565 |     22.122 |     22.904 |     23.145 |     21.771 |     21.579 |     21.776 |     20.673 |     18.717 |     19.614 |     19.730 |     19.746 |     19.074 |     19.178 |
^ absolute change   |      1.667 |       -938 |      2.334 |      2.505 |      2.615 |      2.433 |      1.966 |      2.277 |        923 |     -1.224 |        354 |      1.045 |       -576 |       -535 |        321 |      1.719 |      1.888 |
^ relative change   |      8,32% |     -4,49% |     10,16% |     11,62% |     11,82% |     10,62% |      8,49% |     10,46% |      4,28% |     -5,62% |      1,71% |      5,58% |     -2,94% |     -2,71% |      1,63% |      9,01% |      9,85% |
| **AVGAS**                                                                                                                                                                                                                     ||||||||||||||||||
^ Submission 2021   |      1.580 |        614 |        614 |        291 |        260 |        228 |        259 |        244 |        237 |        283 |        246 |        199 |        180 |        233 |        145 |        142 |        116 |
^ Submission 2020   |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |  IE        |
^ absolute change   |      1.580 |        614 |        614 |        291 |        260 |        228 |        259 |        244 |        237 |        283 |        246 |        199 |        180 |        233 |        145 |        142 |        116 |

In parallel, the majority of **country-specific emission factors**  has been revised within TREMOD AV based on information available from Eurocontrol's AEM model [(EUROCONTROL2020)].

<WRAP center round info 60%>
For more information on recalculated emission estimates for Base Year and 2018, please see the pollutant specific recalculation tables following chapter [[general:recalculations:start | 8.1 - Recalculations]].
</WRAP>

===== Uncertainties =====

For uncertainties information, please see the [[sector:energy:fuel_combustion:transport:civil_aviation:start|main chapter]] on civil aviation.

===== Planned improvements =====

For information on planned improvements, please see the [[sector:energy:fuel_combustion:transport:civil_aviation:start|main chapter]] on civil aviation.



[(KNOERR2010> Knörr, W., Schacht, A., & Gores, S. (2010): Entwicklung eines eigenständigen Modells zur Berechnung des Flugverkehrs (TREMOD-AV) : Endbericht. Endbericht zum F+E-Vorhaben 360 16 029, URL: https://www.umweltbundesamt.de/publikationen/entwicklung-eines-modells-zur-berechnung; Berlin & Heidelberg, 2012.)]
[(KNOERR2020c> Knörr et al. (2020c): Knörr, W., Schacht, A., & Gores, S.: TREMOD Aviation (TREMOD AV) 2018 - Revision des Modells zur Berechnung des Flugverkehrs (TREMOD-AV). Heidelberg, Berlin: Ifeu Institut für Energie- und Umweltforschung Heidelberg GmbH & Öko-Institut e.V., Berlin & Heidelberg, 2020.)]
[(GORES2020> Gores (2020): Inventartool zum deutschen Flugverkehrsinventar 1990-2018, im Rahmen der Aktualisierung des Moduls TREMOD-AV im Transportemissionsmodell TREMOD, Berlin, 2020.)]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019, https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-3-a-aviation/view; Copenhagen, 2019.)]
[(EUROCONTROL2020> Eurocontrol (2020): Advanced emission model (AEM); https://www.eurocontrol.int/model/advanced-emission-model; 2020.)]