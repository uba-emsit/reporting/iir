====== 1.A.3.d - Navigation ======

===== Short description =====

Category //1.A.3.d - Navigation// includes emissions from national and international inland and maritime navigation. 

^  NFR-Code                                    ^  Name of Category                   ^  Method                                        ^  AD  ^  EF  ^  Key Category Analysis ^
| **1.A.3.d**                                  |  **Navigation**                     |  //see sub-category details//                                                               ||||
| consisting of / including source categories                                                                                                                                    ||||||
| 1.A.3.d i (ii)                               | [[sector:energy:fuel_combustion:transport:navigation:international_inland_waterways|International Inland Waterways]]  |  //Germany does not report emissions from this sub-category.//                              ||||
| 1.A.3.d ii                                   | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| National Navigation (Shipping) ]]     |  //see sub-category details//                                                               ||||
| 1.A.3.d i (i)                                | [[sector:energy:fuel_combustion:transport:navigation:international_maritime_navigation| International Maritime Navigation ]]  |  //see sub-category details//                                                               ||||

===== Methodology =====

==== Activity Data ====

Primary fuel deliveries data for the entire navigation sector (maritime and inland waterways) is included in lines 6 ('International Maritime Bunkers') and 64 ('Coastal and Inland Navigation') of the National Energy Balance (NEB) (AGEB, 2020) [(AGEB2020)]. (For comparison, official mineral-oil data of the Federal Office of Economics and Export Control (BAFA, 2020) [(BAFA2020)] and sales data of the German Petroleum Industry Association (MWV, 2020) [(MWV20920)] are used, too.)

Data on the consumption of //biodiesel// is provided in NEB line 64 from 2004 onward. However, as this data appears to be rather inconsistent, the consumption of biofuels is calculated within TREMOD via the official annual blending rates.

__Table 1: Primary fuel deliveries as listed in the National Energy Balance, in terajoules__
|                                                                                    |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  | **2019**  |
| **NEB line 6 - Maritime Bunkers ('Hochseebunkerungen')**                                                                                                                                                                                       ||||||||||||||           |
^ Diesel / Light heating oil                                                         |     23,336 |     20,426 |     21,542 |     18,636 |     22,483 |     21,046 |     18,617 |     18,333 |     20,898 |     43,376 |     42,606 |     36,872 |     31,406 |    30,214 |
^ Heavy fuel oil                                                                     |     80,230 |     64,382 |     69,578 |     85,370 |     93,063 |     92,649 |     87,595 |     77,754 |     73,729 |     57,900 |     74,844 |     58,788 |     39,570 |    26,959 |
| **NEB line 64 - Coastal and Inland Navigation ('Küsten- und Binnenschifffahrt')**                                                                                                                                                                         |||||||||||||||
^ Diesel oil                                                                         |     27,710 |     23,562 |     11,864 |     12,831 |     11,182 |     12,050 |     11,322 |     11,635 |     12,112 |     13,321 |     11,131 |     10,150 |     10,619 |    10,739 |
| **TOTAL Navigation**                                                               ^  131,276   ^  108,370   ^  102,984   ^  116,837   ^  126,728   ^  125,745   ^  117,534   ^  107,722   ^  106,739   ^  114,597   ^  128,581   ^  105,81    ^  81,595    ^    67,912 ^
source: National Energy Balances [(AGEB2020)]

As the statistical allocation of fuels delivered to the navigation (shipping) sector follows tax aspects, NEB line 6 ('International Maritime Bunkers') includes all fuel deliveries to IMO-registered ship involved in both national and international maritime activities. On the other hand, NEB line 64 ('Coastal and Inland Navigation') includes all fuel deliveries to ship involved in inland and non-IMO maritime navigation.

__Table 2: Allocation of for subsector-specfic fuel deliveries data in the NEB__
^ NEB line                              ^ including fuel deliveries to navigation sub-sectors...                                                                                   ||
| 6 - 'International Maritime Bunkers'  | ...international maritime navigation / national maritime navigation (IMO) /  national fishing (IMO) / military navigation (IMO)          ||
| 64 - 'Coastal and Inland Navigation'  | ...national inland navigation / national maritime navigation (non-IMO) /  national fishing (non-IMO) / military navigation (non-IMO)     ||

Therefore, the amounts of fuels listed in NEB lines 6 and 64 are broken down on several sub-sectors. 

Regarding all national maritime activities, taking place in National Maritime Navigation, national fishing, and military navigation, a country-specific approach allows for estimating tier3 fuel consumption data based on ship movement information (AIS signal) for IMO- and non-IMO ships. 

In contrast to this bottom-up approach, fuel consumption in both //international maritime navigation// and //national inland navigation// are calculated as tier1 estimates. The following equations and charts try to illustrate the way of deducing these tier1 activity data:

**Estimating the tier1 activity data for International maritime navigation:**

| <WRAP left round info 100%>  AD<sub>1.A.3.d i</sub> = PAD<sub>NEB line 6</sub> - AD<sub>1.A.3.d ii (a) - IMO</sub>  - AD<sub>1.A.4.c iii - IMO</sub> - AD<sub>1.A.5.b iii - IMO</sub>  </WRAP>  | with 		\\   * AD<sub>1.A.3.d i</sub> - tier1 activity data for International maritime navigation\\   * PAD<sub>NEB line 6</sub> - primary over-all fuel deliveries data from NEB line 6 - 'International Maritime Bunkers'\\   * AD<sub>1.A.3.d ii (a) - IMO</sub> - tier3 activity data for IMO-registered ships involved in national maritime navigation\\   * AD<sub>1.A.4.c iii - IMO</sub> - tier3 activity data for IMO-registered ships involved in national fishing\\   * AD<sub>1.A.5.b iii - IMO</sub> - tier3 activity data for IMO-registered ships involved in military navigation  |

**Estimating the tier1 activity data for National inland navigation:**


| <WRAP left round info 100%>    AD<sub>1.A.3.d ii (b)</sub> = PAD<sub>NEB line 64</sub> - AD<sub>1.A.3.d ii (a) - non-IMO</sub>  - AD<sub>1.A.4.c iii - non-IMO</sub> - AD<sub>1.A.5.b iii - non-IMO</sub>\\   </WRAP>  | with 	\\   * AD<sub>1.A.3.d ii (b)</sub> - tier1 activity data for National inland navigation\\   * PAD<sub>NEB line 64</sub> - primary over-all fuel deliveries data from NEB line 64 - 'Coastal and Inland Navigation'\\   * AD<sub>1.A.3.d ii (a) - non-IMO</sub> - tier3 activity data for non-IMO ships involved in national maritime navigation\\   * AD<sub>1.A.4.c iii -non-IMO</sub> - tier3 activity data for for non-IMO ships involved in national fishing\\   * AD<sub>1.A.5.b iii - non-IMO</sub> - tier3 activity data for for non-IMO ships involved in military navigation  |


__Table 3: Resulting breakdown of primary fuel deliveries onto the different navigation sub-sectors, in terajoules__
|                                                    |  **1990**  |  **1995**  |  **2000**  |  **2005**  |  **2006**  |  **2007**  |  **2008**  |  **2009**  |  **2010**  |  **2011**  |  **2012**  |  **2013**  |  **2014**  |  **2015**  |  **2016**  |  **2017**  |  **2018**  | **2019**  |
|  **Navigation TOTAL**                              ^  131,276   ^    108,370 ^  102,984   ^  116,837   ^  126,728   ^  125,745   ^  117,534   ^  107,722   ^  106,739   ^  114,597   ^  128,581   ^  105,81    ^  81,595    ^     67,912 ^  131,276   ^  108,37    ^  102,984   ^  116,837  ^
|                                                    |            |            |            |            |            |            |            |            |            |            |            |            |            |            |            |            |            |           |
| **1.A.3.d i - International maritime navigation**                                                                                                                                                                                                                         |||||||||||||||||||
^ Diesel oil / Light heating oil                     |     12.748 |     12.919 |     13.664 |     11.993 |     15.817 |     17.524 |     13.105 |     14.412 |     16.662 |     15.370 |     12.594 |     12.414 |     13.674 |     33.088 |     28.093 |     22.924 |     15.213 |    18.327 |
^ Heavy fuel oil                                     |     68.484 |     56.323 |     60.984 |     78.182 |     78.257 |     96.625 |     96.017 |     85.865 |     86.934 |     86.687 |     81.171 |     71.364 |     67.670 |     57.850 |     74.837 |     58.781 |     39.380 |    26.601 |
| **1.A.3.d ii (a) - National maritime navigation**                                                                                                                                                                                                                         |||||||||||||||||||
^ Diesel oil / Light heating oil                     |     15,940 |     11,258 |     11,860 |      9,962 |      9,845 |     10,395 |     10,834 |      9,486 |      8,685 |      8,489 |      9,046 |      9,047 |      9,965 |     13,359 |     16,295 |     15,221 |     16,336 |    13,961 |
^ Heavy fuel oil                                     |     11,723 |      8,041 |      8,577 |      7,172 |      7,004 |      7,425 |      7,797 |      6,733 |      6,114 |      5,961 |      6,410 |      6,376 |      6,046 |       50.0 |       7.05 |       7.01 |        190 |       358 |
| **1.A.3.d ii (b) - National inland navigation**                                                                                                                                                                                                                           |||||||||||||||||||
^ Diesel oil                                         |     20,664 |     18,597 |      6,788 |      8,634 |      7,050 |      6,836 |      5,683 |      7,129 |      7,497 |      8,466 |      7,556 |      7,777 |      8,567 |      9,422 |      7,873 |      7,179 |      7,511 |     7,595 |
| **1.A.4.c iii - Fishing**                                                                                                                                                                                                                                                 |||||||||||||||||||
^ Diesel oil / Light heating oil                     |  711       |  549       |  531       |  488       |  474       |  486       |  484       |  482       |  473       |  442       |  431       |  429       |  472       |  555       |  1,117     |  1,208     |  2,455     |           |
^ Heavy fuel oil                                     |  24        |  18        |  18        |  16        |  16        |  16        |  16        |  16        |  16        |  15        |  14        |  14        |  13        |  0         |  0         |  0         |  0         |           |
| **1.A.5.b iii - Military navigation**                                                                                                                                                                                                                                     |||||||||||||||||||
^ Diesel oil / Light heating oil                     |  983       |  665       |  563       |  410       |  383       |  366       |  360       |  349       |  347       |  330       |  313       |  302       |  332       |  273       |  359       |  489       |  423       |           |
^ Heavy fuel oil                                     |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |  NO        |           |

==== Emission factors ====

Annual country-specific emission factors have been developed within the underlying models maintained at the ifeu Institute for Energy and Environmental Research (Knörr et al. (2020a): TREMOD) [(KNOERR2020a)] and the Federal Maritime and Hydrographic Agency (Deichnik (2020): BSH model) [(DEICHNIK2020)].

For information on these country-specific emission factors, please refer to the sub-chapters linked above.

Furthermore, for heavy metal and POP emissions tier1 EF have been derived from the EMEP/EEA Guidebook 2019 mainly [(EMEPEEA2019)].

<WRAP center round info 100%>
For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.
</WRAP>

Table 4 shows the tier1 emission factors for exhaust emissions of **heavy-metals** and **POPs** as applied to all navigation sub-categories in 1.A.3.d as well as NFRs 1.A.4.c iii and 1.A.5.b iii.
The listed values have been derived from default values provided in the EMEP/EEA air pollutant emission inventory guidebook (EMEP/EEA, 2019) [(EMEPEEA2019)] and (Rentz et al., 2008) [(RENTZ2008)].

Here, as the guidebook does not provide source-specific values for **PAHs**, respective values provided for diesel in railways and heavy duty road vehicles have been applied as a gap-filling proxy.

__Table 4: Tier1 emission factors for heavy-metal and POP exhaust emissions__
|                 |  **Pb**            |  **Cd**             |  **Hg**             |  **As**            |  **Cr**            |  **Cu**            |  **Ni**            |  **Se**            |  **Zn**            |  **B[a]P**        |  **B[b]F**          |  **B[k]F**        |  **I[...]p**      |  **PAH 1-4**        |  **PCBs**           |  **HCB**           |  **PCDD/F**        |
|                 |  [g/TJ]                                                                                                                                                                              |||||||||  [mg/TJ]                                                                                          |||||                                                             |||
^      Diesel oil |  3.03 <sup>2</sup> |  0.233 <sup>2</sup> | 0.698 <sup>2</sup>  | 0.93 <sup>2</sup>  | 1.16 <sup>2</sup>  | 20.5 <sup>2</sup>  | 23.3 <sup>2</sup>  | 2.33 <sup>2</sup>  | 27.9 <sup>2</sup>  | 698 <sup>5</sup>  | 1,164 <sup>5</sup>  | 801 <sup>6</sup>  |   184<sup>6</sup> | 2,847 <sup>4</sup>  | 0.885 <sup>2</sup>  | 1.86 <sup>2</sup>  | 93.0 <sup>7</sup>  |
^  Heavy fuel oil |  4.46 <sup>3</sup> |  0.496 <sup>3</sup> | 0.496 <sup>3</sup>  | 16.9 <sup>3</sup>  | 17.8 <sup>3</sup>  | 31.0 <sup>3</sup>  | 793 <sup>3</sup>   | 5.20 <sup>3</sup>  | 29.7 <sup>3</sup>  | 741 <sup>5</sup>  |  1,235	<sup>5</sup> |  849 <sup>6</sup> | 195 <sup>6</sup>  | 3,020 <sup>4</sup>  | 14.1 <sup>3</sup>   | 3.46 <sup>3</sup>  | 98.7 <sup>7</sup>  |
<sup>2</sup> tier1 defaults from [(EMEPEEA2019)], Chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii Navigation: Table 3-2 \\
<sup>3</sup> tier1 defaults from [(EMEPEEA2019)], Chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii Navigation: Table 3-1 \\
<sup>4</sup> sum of tier1 default values applied for B[a]P, B[b]F, B[k]F, and I[1,2,3-c,d]P \\
<sup>5</sup> tier1 defaults from [(EMEPEEA2019)], Chapter: 1.A.3.c Railways: Diesel, Table 3-1 \\
<sup>6</sup> tier1 defaults from [(EMEPEEA2019)], Chapter: 1.A.3.b.i, 1.A.3.b.ii, 1.A.3.b.iii, 1.A.3.b.iv - Road transport, Table 3-8: HDV, Diesel \\
<sup>7</sup> tier1 value derived from [(RENTZ2008)]

[(AGEB2020> AGEB, 2020: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; URL: http://www.ag-energiebilanzen.de/7-0-Bilanzen-1990-2018.html, (Aufruf: 29.11.2020), Köln & Berlin, 2020)]
[(BAFA2020> BAFA, 2020: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
URL: https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2018_dezember.html, Eschborn, 2020.)]
[(MWV2020> MWV (2020): Association of the German Petroleum Industry (Mineralölwirtschaftsverband, MWV): Annual Report 2018, page 65, Table 'Sektoraler Verbrauch von Dieselkraftstoff 2012-2018'; URL: https://www.mwv.de/wp-content/uploads/2020/09/MWV_Mineraloelwirtschaftsverband-e.V.-Jahresbericht-2020-Webversion.pdf, Berlin, 2020.)]
[(KNOERR2020a> Knörr et al. (2020a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg & Berlin, 2020.)]
[(DEICHNIK2020> Deichnik (2020): Aktualisierung und Revision des Modells zur Berechnung der spezifischen Verbräuche und Emissionen des von Deutschland ausgehenden Seeverkehrs. from Bundesamts für Seeschifffahrt und Hydrographie (BSH - Federal Maritime and Hydrographic Agency); Hamburg, 2020.)]
[(EMEPEEA2019>EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook – 2019, Copenhagen, 2019.)]
[(RENTZ2008>Rentz et al., 2008: Nationaler Durchführungsplan unter dem Stockholmer Abkommen zu persistenten organischen Schadstoffen (POPs), im Auftrag des Umweltbundesamtes, FKZ 205 67 444, UBA Texte | 01/2008, January 2008 - URL: http://www.umweltbundesamt.de/en/publikationen/nationaler-durchfuehrungsplan-unter-stockholmer )]
[(KNOERR2009>Knörr et al. (2009): Knörr, W., Heldstab, J., & Kasser, F.: Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland; final report; URL: https://www.umweltbundesamt.de/sites/default/files/medien/461/publikationen/3937.pdf, FKZ 360 16 023, Heidelberg & Zürich, 2009.)]