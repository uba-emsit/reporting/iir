====== 1.A.2.f - Stationary Combustion in Manufacturing Industries and Construction: Non-Metallic Minerals ======

===== Short Description =====

Sub-category //1.A.2.f - Non Ferrous Metals// refers to emissions from fuel consumption for burning processes in energy-intensive mineral industries.

^ Category Code  ^  Method                                ||||^  AD                                ||||^  EF                                  |||||
| 1.A.2.f        |  T1                                    |||||  NS                                |||||  CS                                  |||||
^  Key Category  ^  SO₂     ^  NOₓ  ^  NH₃  ^  NMVOC  ^  CO   ^  BC   ^  Pb   ^  Hg   ^  Cd   ^  PCDD/F  ^  PAH  ^  HCB  ^  TSP  ^  PM₁₀  ^  PM₂ ₅  ^
| 1.A.2.f         |  -/-        |  -/-  |  -/-  |  -/-    |  L/-  |  -    |  -    |  -    |  -    |  -     |  -    |  -    |  -/-  |  -     |  -      |

{{page>general:Misc:LegendEIT:start}}

{{ :sector:energy:fuel_combustion:industry:rotary_kiln.png?nolink&400|?nolink|| Illustration of Rotary Kiln }}

In order of significance relating energy use and emissions, the covered industries are:

  * burning of cement clinker,
  * burning of quicklime,
  * melting of glass,
  * burning of ceramics.

===== Method =====

Regarding the burning processes emissions can allocated to the use of fuels or to the production process. Current allocation is regarding the main importance of the production process.

==== Activity data ====

The key source of all conventional fuel data is the national energy balance. Moreover the use of additional statistical data is necessary in order to disaggregate data. Data source for fuel inputs for energy-related process combustion in cement industry are manufacturing-sector statistics (Statistik des produzierenden Gewerbes); reporting number (Melde-Nr.) 23.51, Cement production. Furthermore the cement industry uses significant amounts of substitute fuels that do not appear in national statistics and in the Energy Balance. Relevant production figures and fuel-use amounts have been taken from statistics of the VDZ cement-industry association. The fuel-input data for ceramics production has also been taken from manufacturing industry statistics (Statistik des produzierenden Gewerbes); reporting no. (Melde-Nr.) 23.32, brickworks (Ziegelei), production of other construction ceramics. The same statistic is also used as source for fuel input of glass ( reporting number: 23.1, Production of glass and glassware) and lime production (reporting number: 23.52, Lime).

==== Emissions ====

Due to allocating emissions to process part we have removed most of time series inconsistencies. The current situation is the following:

__Table 1: relevance of emission sources regarding the fuel use due to burning processes in 1.A.2.f__
^           ^  SO<sub>x</sub>  ^  NO<sub>x</sub>  ^  CO              ^  NMVOC           ^  NH<sub>3</sub>  ^  TSP             ^  BC             ^
^ cement    |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  medium          |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  IE<sup>2</sup>  |  NE  |
^ lime      |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  high            |  IE<sup>1</sup>  |  low             |  IE<sup>2</sup>  |  NE  |
^ glass     |  IE<sup>2</sup>  |  IE<sup>1</sup>  |  low             |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  IE<sup>2</sup>  |  NE  |
^ ceramics  |  IE<sup>3</sup>  |  IE<sup>3</sup>  |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  IE<sup>1</sup>  |  NE  |
<hidden Show Legend>
<sup>1</sup> Included in process related emissions, in all cases it is the link to complementary source category.

<sup>2</sup> Some artifacts occur for 1990 emissions that cannot be shifted.

<sup>3</sup> Inclusion in process related emissions occurs from different time points onwards.
</hidden>


The entire appraisal of the emissions situation succeeds only in connection with the process related emissions. Especially further relevant pollutants as heavy metals or persistent organics are shown as process related generally.

===== Planned improvements =====

At the moment, no category-specific improvements are planned.


===== Recalculations =====
<WRAP center round info 60%>
For specific information on recalculated emission estimates for Base Year and 2018, please see the pollutant specific recalculation tables following chapter [[General:Recalculations:Start| 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

At the moment, no category-specific improvements are planned.