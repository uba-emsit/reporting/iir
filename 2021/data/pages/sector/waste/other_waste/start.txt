====== 5.E - Other Waste (please specify in IIR)  ======

Under NFR category **5.E - Other Waste**, Germany so far reports __greenhouse gas emissions from the mechanical biological treatment (MBT) of waste__ as well as __air-pollutant emissions from building and car fires__.

^  NFR-Code                                    ^  Name of Category                                                                   ^  Method                        ^  AD  ^  EF  ^  Key Category  ^
|  **5.E**                                     | **Other Waste **                                                                    |  //see sub-category details//                              ||||
| consisting of / including source categories                                                                                                                                                   ||||||
|  **5.E.1**                                   | Other Waste - Mechanical Biological Treatment (MBT)                                 |  //GHG emissions only//                                    ||||
|  **5.E.2**                                   | [[Sector:Waste:Other_Waste:Building_And_Car_Fires:Start| Building and Car Fires ]]  |  //see sub-category details//                              ||||

^ Category Code  ^  Method                                ||||^  AD                                ||||^  EF                                  |||||
| 5.E.1 - Mechanical Biological Treatment        |  NA                                    |||||  NA                                |||||  NA                                  |||||
| 5.E.2 - Building and Car Fires        |  D                                    |||||  NS                                |||||  D                                  |||||
^  Key Category  ^  SO₂     ^  NOₓ  ^  NH₃  ^  NMVOC  ^  CO   ^  BC   ^  Pb   ^  Hg   ^  Cd   ^  Diox  ^  PAH  ^  HCB  ^  TSP  ^  PM₁₀  ^  PM₂ ₅  ^
| 5.E             |  -          |  -    |  -    |  -      |  -    |  -/-  |  -/-  |  -/-  |  -/-  |  L/-   |  -    |  -    |  -/-  |  L/-   |  L/-    |
 {{page>general:Misc:LegendEIT:start}}
\\