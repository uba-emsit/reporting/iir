====== 2.B.10.b - Storage, Handling and Transport of Chemical Products ======

<WRAP center round info 100%>
All emissions from storage, handling and transport of chemical products are included elsewhere ('IE') in the values reported in NFR 2.L - Other production, consumption, storage, transportation or handling of bulk products.
</WRAP>

