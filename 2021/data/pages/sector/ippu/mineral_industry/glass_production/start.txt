====== 2.A.3 - Glass Production ======

===== Short description =====
^ Category Code  ^  Method                                ||||^  AD                                ||||^  EF                                  |||||
| 2.A.3        |  T2                                    |||||  AS                                |||||  CS                                  |||||
^  Key Category  ^  SO₂     ^  NOₓ  ^  NH₃  ^  NMVOC  ^  CO   ^  BC   ^  Pb   ^  Hg   ^  Cd   ^  Diox  ^  PAH  ^  HCB  ^  TSP  ^  PM₁₀  ^  PM₂ ₅  ^
| 2.A.3           |  L/-        |  -/-  |  -/-  |  -/-    |  -    |  -    |  -/-  |  -    |  -/-  |  -     |  -    |  -    |  -/-  |  -/-   |  -/-    |
{{page>general:Misc:LegendEIT:start}}
\\
Germany's glass industry produces a wide range of different glass types that differ in their chemical composition. Germany's glass sector comprises the following sub-sectors: container glass, flat glass, domestic glass, special glass and mineral fibres (glass and stone wool). The largest production quantities are found in the sectors of container glass and flat glass. Further processing and treatment of glass and glass objects are not considered.
===== Methodology =====

The emissions are calculated via a higher Tier method resembling a Tier 2 method, as the activity rates are tied to specific emission factors for different glass types.

==== Activity data ====

The production figures are taken from the regularly appearing annual reports of the [[https://www.bvglas.de/en/|Federal Association of the German Glass Industry]] (Bundesverband Glasindustrie; BV Glas). "Production" refers to the amount of glass produced, which is considered to be equivalent to the amount of glass melted down. 

==== Emission factors ====

The procedure used to determine emission factors for the various glass types involved and the pertinent emissions is described in detail in reports of research projects (2008: Report-No. 001264[(UFoPlan FKZ 206 42 300/02: Teilvorhaben 02: „Providing up-to-date emission data for the glass and mineral fiber industry“ downloading via search “UBA-FB 001264” in (https://doku.uba.de ⇒ OPAC ⇒ use parameter ‘Signatur’)]. The emission factors were calculated for the various industry sectors. The factors vary annually in keeping with industry monitoring, not only as steady trends, but as time ranges. Ranges below are given as averages over all glass types for main pollutants, but as averages over time for heavy metals:

__Table 1: Overview of applied emission factors__
^ Pollutant     ^^ Products            ^^  EF             ^^  Unit      ^^  Current trend      ^^
^ NOₓ           ^| all glass types     ||  1.0-3.4        ||  kg/t      ||  constant           ||
^ SO₂           ^| all glass types     ||  0.39-1.9       ||  kg/t      ||  constant           ||
^ NMVOC         ^| all glass types     ||  0.96           ||  kg/t      ||  constant           ||
^ NH₃           ^| two glass types     ||  0.03/0.7       ||  kg/t      ||  constant           ||
^ TSP           ^| all glass types     ||  0.02-0.04      ||  kg/t      ||  constant           ||
^ PM₁₀          ^| all glass types     ||  0.01-0.03      ||  kg/t      ||  constant           ||
^ PM₂.₅         ^| all glass types     ||  0.01-0.02      ||  kg/t      ||  constant           ||
^ As            ^| container glass     ||  0.079      ||  g/t       ||  constant           ||
^ Pb            ^| container glass     ||  0.41      ||  g/t       ||  constant           ||
^ Cd            ^| container glass     ||  0.04      ||  g/t       ||  constant           ||
^ Cr            ^| container glass     ||  0.04      ||  g/t       ||  constant           ||
^ Cu            ^| container glass     ||  0.10      ||  g/t       ||  constant           ||
^ Ni            ^| container glass     ||  0.014      ||  g/t       ||  constant           ||
^ Se            ^| container glass     ||  1.4        ||  g/t       ||  constant           ||

===== Trends in emissions =====

Trends in emissions correspond to trends of emission factors and of production development. The resulting trends are not constant, as a result of different EF for various glass types. So emissions of SO₂ couldn't decrease due to increased production Level of a relevant product.
[{{:sector:ippu:mineral_industry:em_2a3_since_1990.png|**Emission trends in NFR 2.A.3**}}]


===== Recalculations =====

Recalculations were necessary due to updated activity data for the last reported year.

<WRAP center round info 60%>
For pollutant-specific information on recalculated emission estimates for Base Year and 2018, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

=====Planned improvements =====

For purposes of updating the EF project has started in 2019, results from 2020 [(ReFoPlan FKZ – 3719 52 1010: „Überarbeitung der Emissionsfaktoren für Luftschadstoffe in den Branchen Zementklinkerproduktion und Glasherstellung“)] are planned to be use for Submission 2022.
