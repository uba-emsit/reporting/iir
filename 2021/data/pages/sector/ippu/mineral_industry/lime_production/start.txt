====== 2.A.2 - Lime Production ======

===== Short description =====

^ Category Code  ^  Method                                ||||^  AD                                ||||^  EF                                  |||||
| 2.A.2        |  T1                                    |||||  AS                                |||||  CS                                  |||||
^  Key Category  ^  SO₂     ^  NOₓ  ^  NH₃  ^  NMVOC  ^  CO   ^  BC   ^  Pb   ^  Hg   ^  Cd   ^  Diox  ^  PAH  ^  HCB  ^  TSP  ^  PM₁₀  ^  PM₂ ₅  ^
| 2.A.2           |  -/-        |  -/-  |  -    |  -/-    |  -    |  -    |  -    |  -/-  |  -    |  -     |  -    |  -    |  -/-  |  -/-   |  -/-    |

{{page>general:Misc:LegendEIT:start}}
\\
The statements made below regarding source category 2.A.2 refer solely to the amounts of burnt lime and dolomite lime produced in German lime works. 
Other lime-producing processes are included in NFR 2.C.1 and 2.H.2. 

Because of the wide range of applications covered by the sector's products, lime production is normally more isolated from economic fluctuations than is production of other mineral products such as cement. Production has fluctuated relatively little since the end of the 1990s. Dolomite-lime production, of which significantly smaller amounts are produced, basically exhibits similar fluctuations.

===== Methodology =====

The pertinent emissions level is obtained by multiplying the amount of product in question (quick lime or dolomite lime) and the relevant emission factor.

==== Activity data ====

The German Lime Association (BVK) collects the production data for the entire time series on a plant-specific basis, and makes it available for reporting purposes. Production amounts are determined via several different concurrent procedures; their quality is thus adequately assured (Tier 2). Most companies are also required to report lime-production data within the framework of CO₂-emissions trading. The EU monitoring guidelines for emissions trading specify a maximum accuracy of 2.5%. It is additionally assumed that 2% of the burnt lime is separated as dust in all years of the reporting period from 1990 onwards via appropriate exhaust gas purification systems and is not returned to the production process. This is taken into account by a potential 2% increase in activity rates.

==== Emission factors ====

__Table 1: Emission factors for quick-lime production__
^ pollutant     |^ Name of Category     |^  EF       |^  unit     |^  Trend        ||
^ NOₓ           || quicklime            ||      0.59 ||  kg/t     ||  falling      ||
^ SO₂           || quicklime            ||      0.12 ||  kg/t     ||  falling      ||
^ NMVOC         || quicklime            ||     0.041 ||  kg/t     ||  constant     ||
^ TSP           || quicklime            ||     0.050 ||  kg/t     ||  falling      ||
^ PM₁₀          || quicklime            ||     0.038 ||  kg/t     ||  falling      ||
^ PM₂.₅         || quicklime            ||     0.023 ||  kg/t     ||  falling      ||
^ Hg            || quicklime            ||      2.62 ||  mg/t     ||  falling      ||

__Table 2: Emission factors for dolomite production__
^ pollutant     |^ Name of Category     |^  EF       |^  unit     |^  Trend        ||
^ NOₓ           || dolomite             ||      1.73 ||  kg/t     ||  falling      ||
^ SO₂           || dolomite             ||      0.58 ||  kg/t     ||  falling      ||
^ NMVOC         || dolomite             ||     0.041 ||  kg/t     ||  constant     ||
^ TSP           || dolomite             ||     0.034 ||  kg/t     ||  falling      ||
^ PM₁₀          || dolomite             ||     0.026 ||  kg/t     ||  falling      ||
^ PM₂.₅         || dolomite             ||     0.015 ||  kg/t     ||  falling      ||
^ Hg            || quicklime            ||      2.63 ||  mg/t     ||  falling      ||

===== Trends in emissions =====

All trends in emissions correspond to trends of emission factors in table above. No rising trends are identified.
[{{:sector:ippu:mineral_industry:em_2a2_since_1990.png|**Emission trends in NFR 2.A.2**}}]

=====Recalculations=====

With **activity data** and **emission factors** remaining unrevised, no recalculations have been carried out compared to last year's submission.

<WRAP center round info 60%>
For pollutant-specific information on recalculated emission estimates for Base Year and 2018, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

At the moment, no category-specific improvements are planned.

