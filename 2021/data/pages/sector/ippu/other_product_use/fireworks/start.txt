====== 2.G(a) - Fireworks ======


==== Short description ====

In this sub-category of //2.G(a) - Other product use: Fireworks// Germany reports NO<sub>x</sub>, SO<sub>x</sub>, CO, TSP, PM<sub>10</sub>, PM<sub>2.5</sub>, Cu, Pb and Zn emissions from fireworks.

^  NFR-Code  ^  Name of Category             ^  Method  ^  AD                 ^  EF     ^
|  2.G(a)    | Other Product use: Fireworks  |  CS      | NS and association  |  D, CS  |

===== Methodology =====

In 2019, measurements were made by a Finnish laboratory for the VPI – Verband der pyrotechnischen Industrie (Association of the pyrotechnical industry) of dust emissions during the burning of fire works. The experiments were made in a container in which the whole fireworks were burned. \\

In 2020, VPI and UBA had an intensive information exchange, in which the VPI presented the results of the measurements to the UBA. The different emission factors were discussed and finally based on the expert judgement it was decided which EFs shall be used for the reporting. In the next step the activity data were updated more differentiated. 

Furthermore, the other EFs have been discussed resultung in some changes to these values. 

The results are presented below. In February 2021 the VPI has published an article in the paper "Propellants, Explosives,  Pyrotechnics" a description of the experiment together with the measurement results[(https://onlinelibrary.wiley.com/doi/epdf/10.1002/prep.202000292)]. 

==== Activity data ====

For the calculation of the activity data the following formula is used: 

<WRAP center round box 60%>
AD = production + import – export – disposal + return<sub>previous year</sub> – return<sub>recent year</sub>
</WRAP>


The **production, disposal, return from the year before and return of the year** data are yearly updated by the VPI. 

**Import and export:** For the import and export data statistical data from the statistical federal office of Germany were taken (foreign statistics of federal office of statistics)[(Statistisches Bundesamt (51000-0013): Aus- und Einfuhr (Außenhandel), URL: https://www-genesis.destatis.de/genesis//online?operation=table&code=51000-0013&bypass=true&levelindex=1&levelid=1664263187988)]. 

The sold amounts of fireworks have increased strongly from 1990 to 1995. From 1995 to 1997 the emissions were relatively high but decreased from 1997 to 2005. Since then, the emissions have been relatively constant with small fluctuations.

**Return**: Amount of unsold fireworks returned to producer

**Disposal**: Amount of disposed unsold fireworks damaged during transport from producer to seller


==== Emission factors ====

The emission factors of SO<sub>2</sub>, CO, NO<sub>x</sub>, Cu, Pb and Zn are the Default-EFs derived from the EMEP Guidebook[(EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019, Copenhagen, 2019.)], page 22, table 3-14: Tier 2 emission factor for source category 2.D.3.i, 2.G Other solvent and product use, Other, Use of Fireworks.

__Table 1: Default emission factors applied, in g/t product__
|       ^  Default-EF  ^
^  SO<sub>2</sub>  |  3.020       |
^  CO   |  7.150       |
^  NO<sub>x</sub>  |  260         |
^  Cu   |  444         |
^  Pb   |  784         |
^  Zn   |  260         |

The emission factors for PM<sub>10</sub>, PM<sub>2.5</sub> and TSP are measured values from the VPI.  

__Table 2: Country-specific PM emission factors applied, in g/t product__
|             ^  PM<sub>10</sub>                  |^  PM<sub>2.5</sub>                  |^  TSP                            ||
|             ^  New Years Eve    ^  Rest of Year  ^  New Years Eve     ^  Rest of Year  ^  New Years Eve  ^  Rest of Year  ^
^  1990-2004  |  52.002,56        |  62.799,96     |  41.463,05         |  49.644,24     |  52.002,56      |  62.799,96     |
^  2005       |  47.509,31        |  72.317,11     |  38.129,60         |  57.167,68     |  47.509,31      |  72.317,11     |
^  2006       |  45.793,40        |  71.986,67     |  36.930,61         |  56.906,46     |  45.793,40      |  71.986,67     |
^  2007       |  45.174,65        |  72.071,88     |  36.615,74         |  56.973,82     |  45.174,65      |  72.071,88     |
^  2008       |  45.955,36        |  71.471,31     |  37.390,41         |  56.499,06     |  45.955,36      |  71.471,31     |
^  2009       |  45.701,68        |  70.204,58     |  37.132,12         |  55.497,69     |  45.701,68      |  70.204,58     |
^  2010       |  44.826,79        |  69.253,15     |  36.536,80         |  54.745,57     |  44.826,79      |  69.253,15     |
^  2011       |  44.068,30        |  68.877,53     |  36.121,87         |  54.448,64     |  44.068,30      |  68.877,53     |
^  2012       |  45.566,16        |  69.993,91     |  37.527,36         |  55.331,16     |  45.566,16      |  69.993,91     |
^  2013       |  46.098,42        |  67.212,39     |  38.026,91         |  53.132,33     |  46.098,42      |  67.212,39     |
^  2014       |  46.621,17        |  67.680,72     |  38.595,22         |  53.502,55     |  46.621,17      |  67.680,72     |
^  2015       |  47.474,24        |  67.313,58     |  39.383,93         |  53.212,31     |  47.474,24      |  67.313,58     |
^  2016       |  47.523,35        |  66.094,38     |  39.539,55         |  52.248,52     |  47.523,35      |  66.094,38     |
^  2017       |  47.853,44        |  65.938,58     |  39.907,83         |  52.125,36     |  47.853,44      |  65.938,58     |
^  2018       |  48.270,00        |  63.519,57     |  39.713,09         |  50.213,10     |  48.270,00      |  63.519,57     |
^  2019       |  48.085,00        |  63.217,87     |  40.033,58         |  49.974,60     |  48.085,00      |  63.217,87     |


The EMEP Guidebook offers Default-EFs for the pollutants Ar, Hg, Ni and Cr. But the VPI has proofed that these emissions does not occur in Germany. And the VPI has further proofed that Pb emissions does not anymore occur since 2003. See the following explanations:

**As and Hg:** For As and Hg the members of the VPI have confirmed that Ar and Hg are not anymore used since 1980. Since About 1980 the explosives administrative regulation (Sprengverwaltungsvorschrift) is regulating which substances are allowed to be used and As and Hg are forbidden to be used. Since 2003 the DIN EN  14035:2003 went in force, which did forbit these substances. The actual follow up norm DIN EN 15947-5 was published in February 2016 and describes the german implementation of the harmonized and in the official journal of the European union 2017, C 149/2 published norm EN 15947:2015.

**Pb:** As the DIN EN 14035:2003 entered into force as from 2003, which did forbid this substance, there are no Pb-emissions from fireworks from 2003 onwards. The actual follow up norm DIN EN 15947-5 was published in February 2016 and describes the german implementation of the harmonized and in the official journal of the European union 2017, C 149/2 published norm EN 15947:2015.

 **Cd:** The members of the VPI were asked and did explain, that Cd was never used, because it has no pyrotechnical effect. Since 2013 Cd is on the candidates list of the substances of Very High Concern (SVHC), published according article 59, para. 10 of the REACH-ordinance. 

**Ni:** The members of the VPI informed that Ni was never used, because it has no pyrotechnical effect. It is part of the harmonized assessment according the ordinance (EG) Nr. 1272/2008 (CLP). Belonging to this, it is assessed as cancerogen category 2. 

**Cr:** According the information from the members of the VPI Cr is not anymore used since the beginning of the 1980. Since 2012 (REACH Annex XIV (Ordinance (EU) Nr. 125/2012) Cr was implemented in the REACH Annex XIV. So from that year a permission duty is necessary. So far, none of the fireworks producers has requested for a permission.  

===== Recalculations =====

**Activity data** has changed as follows: 

__Table 3: Change of AD between Submission 2020 and Submission 2021, in t__
^                  |                  ^  1990   ^  1995   ^  2000    ^  2005   ^  2006   ^  2007   ^  2008   ^  2009   ^  2010   ^  2011   ^  2012   ^  2013   ^  2014   ^  2015   ^  2016   ^  2017   ^  2018   ^
^ Submission 2021  | New Years Eve    |  13,939 |  51,421 |   26,283 |  28,856 |  30,491 |  33,396 |  34,461 |  30,075 |  31,440 |  29,795 |  33,086 |  29,131 |  33,241 |  34,999 |  32,572 |  33,936 |  32,980 |
| :::              | during the year  |   4,130 |   7,447 |   10,906 |   7,506 |  10,755 |   8,235 |   8,088 |   7,521 |   8,007 |   7,499 |   8,137 |   7,247 |   8,465 |   8,832 |   9,487 |   8,544 |   9,088 |
| :::              ^ SUM              ^  18,069 ^ 58,869  ^ 37,188   ^ 36,362  ^ 41,247  ^ 41,631  ^ 42,550  ^ 37,595  ^ 39,446  ^ 37,294  ^ 41,223  ^ 36,378  ^ 41,706  ^ 43,830  ^ 42,059  ^ 42,480  ^ 42,068  ^
^ Submission 2020  ^ SUM              ^  17,957 ^  66,272 ^   48,355 ^  38,148 ^  43,487 ^  44,705 ^  42,228 ^  41,839 ^  42,652 ^  38,638 ^  42,857 ^  37,847 ^  45,164 ^  45,656 ^  45,208 ^  44,111 ^  46,462 ^
^ Change                             ^|     112 |  -7,403 |  -11,167 |  -1,786 |  -2,240 |  -3,073 |     322 |  -4,244 |  -3,206 |  -1,344 |  -1,634 |  -1,469 |  -3,458 |  -1,826 |  -3,149 |  -1,631 |  -4,394 |

{{ :sector:ippu:other_product_use:fireworks:fireworks_emissions.png |}}

The **emissions** from As, Cd, Cr, Hg and Ni were deleted. The VPI proofed that these emissions does not occur. 
For Pb the emissions are from 2003 onwards changed to NA because the VPI proofed that the usage of Pb is forbidden since 2003 by a DIN Norm. 
The emissions of CO, Cu, NOx, SOₓ, Zn and Pb are changed because of changed AD. The emissions of PM10, PM2.5 and TSP are changed because of changed AD and new EFs from the VPI.  
  
__Table 4: Change of emissions between Submission 2020 and Submission 2021, in t__
^                  ^  Pollutant  ^ Source            ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^
^ Submission 2020  ^  As         ^                   |    0,024 |    0,088 |    0,064 |    0,051 |    0,057 |    0,061 |    0,060 |    0,059 |    0,062 |
^ Submission 2021  | :::         ^ New Years Eve     |  NA                                                                                      |||||||||
^ Submission 2021  | :::         ^ rest of the year  | :::                                                                                      | :::      | :::      | :::      | :::      | :::      | :::      | :::      | :::      |
^ Difference       | :::         ^                   |   -0,024 |   -0,088 |   -0,064 |   -0,051 |   -0,057 |   -0,061 |   -0,060 |   -0,059 |   -0,062 |
^ Submission 2020  ^  Cd         ^                   |    0,027 |    0,098 |    0,072 |    0,057 |    0,063 |    0,068 |    0,067 |    0,065 |    0,069 |
^ Submission 2021  | :::         ^ New Years Eve     |  NA                                                                                      |||||||||
^ Submission 2021  | :::         ^ rest of the year  | :::                                                                                      | :::      | :::      | :::      | :::      | :::      | :::      | :::      | :::      |
^ Difference       | :::         ^                   |   -0,027 |   -0,098 |   -0,072 |   -0,056 |   -0,063 |   -0,068 |   -0,067 |   -0,065 |   -0,069 |
^ Submission 2020  ^  CO         ^                   |  128,392 |  473,843 |  345,740 |  272,759 |  304,960 |  326,443 |  323,239 |  315,392 |  332,201 |
^ Submission 2021  | :::         ^ New Years Eve     |   99,664 |  367,662 |  187,921 |  206,322 |  224,792 |  250,241 |  232,888 |  242,640 |  235,805 |
^ Submission 2021  | :::         ^ rest of the year  |   29,530 |   53,247 |   77,976 |   53,664 |   57,247 |   63,145 |   67,833 |   61,090 |   64,979 |
^ Difference       | :::         ^                   |    0,802 |  -52,934 |  -79,843 |  -12,773 |  -22,920 |  -13,057 |  -22,518 |  -11,662 |  -31,417 |
^ Submission 2020  ^  Cr         ^                   |    0,280 |    1,034 |    0,754 |    0,595 |    0,665 |    0,712 |    0,705 |    0,688 |    0,725 |
^ Submission 2021  | :::         ^ New Years Eve     |  NA                                                                                      |||||||||
^ Submission 2021  | :::         ^ rest of the year  | :::                                                                                      | :::      | :::      | :::      | :::      | :::      | :::      | :::      | :::      |
^ Difference       | :::         ^                   |   -0,280 |   -1,034 |   -0,754 |   -0,595 |   -0,665 |   -0,712 |   -0,705 |   -0,688 |   -0,725 |
^ Submission 2020  ^  Cu         ^                   |    7,973 |   29,425 |   21,470 |   16,938 |   18,937 |   20,271 |   20,072 |   19,585 |   20,629 |
^ Submission 2021  | :::         ^ New Years Eve     |    6,189 |   22,831 |   11,670 |   12,812 |   13,959 |   15,539 |   14,462 |   15,067 |   14,643 |
^ Submission 2021  | :::         ^ rest of the year  |    1,834 |    3,307 |    4,842 |    3,332 |    3,555 |    3,921 |    4,212 |    3,794 |    4,035 |
^ Difference       | :::         ^                   |    0,050 |   -3,287 |   -4,958 |   -0,793 |   -1,423 |   -0,811 |   -1,398 |   -0,724 |   -1,951 |
^ Submission 2020  ^  Hg         ^                   |    0,001 |    0,004 |    0,003 |    0,002 |    0,002 |    0,003 |    0,003 |    0,003 |    0,003 |
^ Submission 2021  | :::         ^ New Years Eve     |  NA                                                                                      |||||||||
^ Submission 2021  | :::         ^ rest of the year  | :::                                                                                      | :::      | :::      | :::      | :::      | :::      | :::      | :::      | :::      |
^ Difference       | :::         ^                   |   -0,001 |   -0,004 |   -0,003 |   -0,002 |   -0,002 |   -0,003 |   -0,003 |   -0,003 |   -0,003 |
^ Submission 2020  ^  Ni         ^                   |    0,539 |    1,988 |    1,451 |    1,144 |    1,280 |    1,370 |    1,356 |    1,323 |    1,394 |
^ Submission 2021  | :::         ^ New Years Eve     |  NA                                                                                      |||||||||
^ Submission 2021  | :::         ^ rest of the year  | :::                                                                                      | :::      | :::      | :::      | :::      | :::      | :::      | :::      | :::      |
^ Difference       | :::         ^                   |   -0,539 |   -1,988 |   -1,451 |   -1,144 |   -1,280 |   -1,370 |   -1,356 |   -1,323 |   -1,394 |
^ Submission 2020  ^  NOx        ^                   |    4,669 |   17,231 |   12,572 |    9,919 |   11,089 |   11,871 |   11,754 |   11,469 |   12,080 |
^ Submission 2021  | :::         ^ New Years Eve     |    3,624 |   13,370 |    6,834 |    7,503 |    8,174 |    9,100 |    8,469 |    8,823 |    8,575 |
^ Submission 2021  | :::         ^ rest of the year  |    1,074 |    1,936 |    2,835 |    1,951 |    2,082 |    2,296 |    2,467 |    2,221 |    2,363 |
^ Difference       | :::         ^                   |    0,029 |   -1,925 |   -2,903 |   -0,464 |   -0,833 |   -0,475 |   -0,819 |   -0,424 |   -1,142 |
^ Submission 2020  ^  Pb         ^                   |   14,078 |   51,957 |   37,911 |   29,908 |   33,439 |   35,795 |   35,443 |   34,583 |   36,426 |
^ Submission 2021  | :::         ^ New Years Eve     |   10,928 |   24,809 |    4,755 |    0,000 |    0,000 |    0,000 |    0,000 |    0,000 |    0,000 |
^ Submission 2021  | :::         ^ rest of the year  |    3,238 |    3,593 |    1,973 |    0,000 |    0,000 |    0,000 |    0,000 |    0,000 |    0,000 |
^ Difference       | :::         ^                   |    0,088 |  -23,555 |  -31,182 |  -29,908 |  -33,439 |  -35,795 |  -35,443 |  -34,583 |  -36,426 |
^ Submission 2020  ^  PM10       ^                   |          |     6622 |     4832 |     3812 |     4262 |     4562 |     4517 |     4408 |     4642 |
^ Submission 2021  | :::         ^ New Years Eve     |          |     2674 |     1367 |     1371 |     1409 |     1662 |     1548 |     1624 |     1592 |
^ Submission 2021  | :::         ^ rest of the year  |          |      468 |      685 |      543 |      554 |      594 |      627 |      563 |      577 |
^ Difference       | :::         ^                   |          |    -3480 |    -2780 |    -1898 |    -2298 |    -2306 |    -2342 |    -2220 |    -2473 |
^ Submission 2020  ^  PM2.5      ^                   |          |     3442 |     2512 |     1981 |     2215 |     2371 |     2348 |     2291 |     2413 |
^ Submission 2021  | :::         ^ New Years Eve     |          |     2132 |     1090 |     1100 |     1149 |     1378 |     1288 |     1354 |     1310 |
^ Submission 2021  | :::         ^ rest of the year  |          |      370 |      541 |      429 |      438 |      470 |      496 |      445 |      456 |
^ Difference       | :::         ^                   |          |     -940 |     -880 |     -452 |     -628 |     -523 |     -565 |     -491 |     -647 |
^ Submission 2020  ^  SO2        ^                   |   54,230 |  200,141 |  146,033 |  115,207 |  128,808 |  137,882 |  136,529 |  133,214 |  140,314 |
^ Submission 2021  | :::         ^ New Years Eve     |   42,096 |  155,292 |   79,374 |   87,146 |   94,947 |  105,696 |   98,367 |  102,486 |   99,599 |
^ Submission 2021  | :::         ^ rest of the year  |   12,473 |   22,491 |   32,935 |   22,667 |   24,180 |   26,671 |   28,651 |   25,803 |   27,446 |
^ Difference       | :::         ^                   |    0,339 |  -22,358 |  -33,724 |   -5,395 |   -9,681 |   -5,515 |   -9,511 |   -4,926 |  -13,270 |
^ Submission 2020  ^  TSP        ^                   |     1972 |     7279 |     5311 |     4190 |     4684 |     5014 |     4965 |     4845 |     5103 |
^ Submission 2021  | :::         ^ New Years Eve     |      725 |     2674 |     1367 |     1371 |     1409 |     1662 |     1548 |     1624 |     1592 |
^ Submission 2021  | :::         ^ rest of the year  |      259 |      468 |      685 |      543 |      554 |      594 |      627 |      563 |      577 |
^ Difference       | :::         ^                   |     -988 |    -4137 |    -3259 |    -2276 |    -2721 |    -2758 |    -2790 |    -2657 |    -2934 |
^ Submission 2020  ^  Zn         ^                   |    4,669 |   17,231 |   12,572 |    9,919 |   11,089 |   11,871 |   11,754 |   11,469 |   12,080 |
^ Submission 2021  | :::         ^ New Years Eve     |    3,624 |   13,370 |    6,834 |    7,503 |    8,174 |    9,100 |    8,469 |    8,823 |    8,575 |
^ Submission 2021  | :::         ^ rest of the year  |    1,074 |    1,936 |    2,835 |    1,951 |    2,082 |    2,296 |    2,467 |    2,221 |    2,363 |
^ Difference       | :::         ^                   |    0,029 |   -1,925 |   -2,903 |   -0,464 |   -0,833 |   -0,475 |   -0,819 |   -0,424 |   -1,142 |

<WRAP center round info 60%>
For pollutant-specific information on recalculated emission estimates for Base Year and 2018, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

==== Uncertainties ==== 
The uncertainty for the AD is given as 10%.

==== Planned improvements ==== 
No improvements are planned. 
 
