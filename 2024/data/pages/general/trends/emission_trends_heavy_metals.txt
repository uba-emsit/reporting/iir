====== Explanation of Key Trends - Heavy Metals ======

<WRAP center round important 60%>
Please note: Data for heavy metals may have issues such as missing sources. It features considerably higher uncertainties then data for other pollutants covered in this report.
</WRAP>

===== Obligations =====

The 1998 [[https://www.unece.org/env/lrtap/hm_h1.html|Aarhus Protocol on Heavy Metals]] under the CLRTAP entered into force late in 2003. It targets three particularly harmful metals: cadmium, lead and mercury. According to one of the basic obligations, Germany has to reduce its emissions for these three metals below their levels in 1990. 

The Protocol aims to cut emissions from industrial sources (iron and steel industry, non-ferrous metal industry), combustion processes (power generation, road transport) and waste incineration. It defines stringent limit values for emissions from stationary sources and suggests best available techniques (BAT) for these sources, such as special filters or scrubbers for combustion sources or mercury-free processes. The Protocol requires Parties to phase out leaded petrol. 

It also introduces measures to lower heavy metal emissions from other products, such as mercury in batteries, and proposes the introduction of management measures for other mercury-containing products, such as electrical components (thermostats, switches), measuring devices (thermometers, manometers, barometers), fluorescent lamps, dental amalgam, pesticides and paint.

===== Main drivers =====

Emission of priority heavy metals (cadmium, lead and mercury) **decreased significantly since 1990**. Most values show reductions by about 50 to 93% compared to the base year, with the majority of the achievements originating from the early 1990's though.

__Overview of percental decreases in HM emissions since 1990:__
  * Arsenic:	-93.5%
  * Cadmium:	-64.5%
  * Copper:	-9.6%
  * Chrome:	-57.3%
  * Mercury:	-81.5%
  * Nickel:	-56.9%
  * Lead:	-92.0%
  * Selenium:	-49.7%
  * Zinc:	-38.3%


2019 and 2020 emissions saw a substantial reduction trend for most heavy metals.

The main source for most heavy metals is fuel combustion and production processes: 
**Energy Industries (NFR 1.A.1)** and **Industrial Processes (NFR 2)**, especially, of course, the Metal Industries (NFR 2.C) emit the majority of **arsenic, cadmium, chrome, lead, mercury and nickel**.
 
In contrast, **copper and zinc** emissions are mostly governed by the **Transport (NFR 1.A.3)** sector, resulting mostly from brake and tyre wear. Thus, trends are connected directly with the annual mileage. 

**Selenium** on the other hand originates mainly from **Mineral Industry (NFR 2.A)** and to a lesser degree from Transport (NFR 1.A.3). 

Other sources are still to be investigated but generally expected to add little to the total trend.

===== Trends =====

The figure below shows emission trends for heavy metals:

{{ :general:trends:iir_hm_all.png?direct&800 | trends of heavy metal emissions}}