====== Recalculations - Ammonia ======

The changes within the **National Total** reported for **1990 (+8.8 kt or +1.2 %)** are dominated by **newly implemented emission estimates for NFR 6.A (+ 6.25 kt)** together with less significant changes troughout NFRs 1 and 3.

The most significant percental changes occur for NFRs **3.D.a.2.c** (plus 559 %) and **6.A** (plus 78 %).	

__Table 1: Changes in emission estimates for 1990__
^                      ^  Submission 2023  ^  Submission 2024  ^  Difference               |^  Reasoning                                                                                                      ^
^  NFR Sector          ^  [kt]                                              ||^  relative   ^  see description and reasoning in:                                                                              ^
^ NATIONAL TOTAL       ^  725.52           ^  734.36           ^  8.84        ^  1.22%      ^ sub-category chapters                                                                                           ^
^ NFR 1 - Energy       ^  15.8891          ^  15.8897          ^  0.0006      ^  0.004%     ^ sub-category chapters                                                                                           ^
| 1.A.2.g vii          |  0.008            |  0.006            |  -0.002      |  -28.7%     | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]    |
| 1.A.3.a i(i)         |  0.0493           |  0.0492           |  -0.0001     |  -0.13%     | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]            |
| 1.A.3.a ii(i)        |  0.0349           |  0.0353           |  0.0004      |  1.16%      | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                 |
| 1.A.3.c              |  0.0318           |  0.0319           |  0.0001      |  0.25%      | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                               |
| 1.A.3.d ii           |  0.01056          |  0.01055          |  -0.00001    |  -0.07%     | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                               |
| 1.A.4.a ii           |  0.0018           |  0.0016           |  -0.0002     |  -8.57%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]  |
| 1.A.4.c ii           |  0.009            |  0.011            |  0.003       |  29.6%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]      |
^ NFR 2  - IPPU        ^  14.68            ^  14.68            ^  0.00        ^  0.00%      ^                                                                                                                 ^
^ NFR 3 - Agriculture  ^  686.62           ^  689.20           ^  2.58        ^  0.38%      ^ sub-category chapters                                                                                           ^
| 3.B.4.g i            |  11.41            |  11.40            |  -0.02       |  -0.14%     | [[Sector:Agriculture:Manure_Management:Start| here]]                                                            |
| 3.B.4.g iv           |  2.470            |  2.472            |  0.002       |  0.08%      | [[Sector:Agriculture:Manure_Management:Start| here]]                                                            |
| 3.D.a.1              |  78.82            |  78.71            |  -0.11       |  -0.14%     | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                           |
| 3.D.a.2.a            |  285.58           |  286.21           |  0.63        |  0.22%      | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                           |
| 3.D.a.2.c            |  0.24             |  1.55             |  1.32        |  __559%__   | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                           |
| 3.D.a.3              |  22.24            |  22.37            |  0.14        |  0.61%      | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                           |
| 3.I                  |  0.002            |  0.001            |  0.00        |  -5.19%     | [[Sector:Agriculture:Agricultural_Other:Start| here ]]                                                          |
^ NFR 5 - Waste        ^  0.34             ^  0.34             ^  0.00        ^  0.00%      ^                                                                                                                 ^
^ NFR 6 - Other        ^  8.00             ^  14.25            ^  6.25        ^  78.2%      ^ sub-category chapters                                                                                           ^
| 6.A                  |  8.00             |  14.25            |  __6.25__    |  __78.2%__  |                                                                                                                 |

----

Changes within the **National Total** reported for **2021 (+9.7 kt or +1.88 %)** result mainly from **newly implemented emission estimates in NFR 6.A (+9.86 kt)** together with less significant changes troughout NFRs 1, 2, 3 and 5.

The most significant percental change occurs for **NFR 6.A** with plus 118 %.	

__Table 2: Changes in emission estimates for 2020__
^                      ^  Submission 2023  ^  Submission 2024  ^  Difference                |^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [kt]                                              ||^  relative    ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  515.77           ^  525.48           ^  9.71        ^  1.88%       ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  12.72            ^  12.50            ^  -0.22       ^  -1.71%      ^ sub-category chapters                                                                                                                     ^
| 1.A.1.a              |  0.624            |  0.620            |  -0.004      |  -0.70%      | [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| here ]]                                         |
| 1.A.1.b              |  0.528            |  0.527            |  -0.001      |  -0.19%      | [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| here ]]                                                             |
| 1.A.1.c              |  0.0244           |  0.0249           |  0.0005      |  1.89%       | [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| here ]]                         |
| 1.A.2.a              |  0.103            |  0.099            |  -0.004      |  -3.74%      | [[sector:energy:fuel_combustion:industry:iron_and_steel| here ]]                                                                          |
| 1.A.2.b              |  0.0059           |  0.0075           |  0.002       |  27.5%       | [[sector:energy:fuel_combustion:industry:non-ferrous_metals| here ]]                                                                      |
| 1.A.2.e              |  0.0010           |  0.0012           |  0.0002      |  25.3%       | [[sector:energy:fuel_combustion:industry:food_processing_beverages_and_tobacco| here  ]]                                                  |
| 1.A.2.f              |  0.020            |  0.022            |  0.003       |  13.5%       | [[sector:energy:fuel_combustion:industry:non-metallic_minerals| here ]]                                                                   |
| 1.A.2.g vii          |  0.008            |  0.006            |  -0.003      |  -33.8%      | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.2.g viii         |  0.391            |  0.381            |  -0.010      |  -2.51%      | [[sector:energy:fuel_combustion:industry:other:start| here  ]]                                                                            |
| 1.A.3.a i(i)         |  0.060            |  0.060            |  0.000       |  -0.22%      | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0.0132           |  0.0125           |  -0.0007     |  -5.40%      | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  8.253            |  8.010            |  -0.24       |  -2.93%      | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0.261            |  0.251            |  -0.010      |  -3.85%      | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0.716            |  0.718            |  0.002       |  0.31%       | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0.0205           |  0.0200           |  -0.0005     |  -2.24%      | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.c              |  0.008            |  0.008            |  0.000       |  0.05%       | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                                                         |
| 1.A.3.d ii           |  0.0053           |  0.0057           |  0.0004      |  7.28%       | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a i            |  0.457            |  0.233            |  -0.22       |  __-49.1%__  | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| here ]]                            |
^ 1.A.4.a ii           ^  0.001838         ^  0.001898         ^  0.000060    ^  3.26%       ^ [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]                            ^
| 1.A.4.b i            |  1.126            |  1.388            |  0.26        |  23.3%       | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  0.00029          |  0.00031          |  0.00001     |  4.60%       | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]                                             |
| 1.A.4.c i            |  0.050            |  0.059            |  0.009       |  17.8%       | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| here ]]                        |
| 1.A.4.c ii           |  0.010            |  0.013            |  0.003       |  30.3%       | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]                                |
^ 1.A.4.c iii          ^  0.00009          ^  0.00009          ^  0.00000     ^  0.00%       ^ [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| here ]]                                                 ^
| 1.A.5.a              |  0.0042297        |  0.0042302        |  0.0000005   |  0.01%       | [[sector:energy:fuel_combustion:other_including_military:stationary_fuel_combustion_in_military_facilities| here ]]                       |
^ NFR 2  - IPPU        ^  8.85             ^  8.69             ^  -0.16       ^  -1.80%      ^ sub-category chapters                                                                                                                     ^
| 2.A.3                |  1.16             |  1.00             |  -0.16       |  -13.8%      | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                                                            |
^ NFR 3 - Agriculture  ^  482.32           ^  482.30           ^  -0.02       ^  -0.003%     ^ sub-category chapters                                                                                                                     ^
| 3.B.1.a              |  53.81            |  53.91            |  0.10        |  0.19%       | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.B.1.b              |  48.55            |  48.75            |  0.20        |  0.41%       | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.B.3                |  72.41            |  72.37            |  -0.04       |  -0.05%      | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.B.4.g i            |  6.91             |  6.87             |  -0.04       |  -0.63%      | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.B.4.g ii           |  8.13             |  8.11             |  -0.03       |  -0.31%      | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.B.4.g iii          |  9.07             |  9.04             |  -0.03       |  -0.35%      | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.B.4.g iv           |  1.53             |  1.52             |  -0.01       |  -0.92%      | [[Sector:Agriculture:Manure_Management:Start| here]]                                                                                      |
| 3.D.a.1              |  34.87            |  35.02            |  0.15        |  0.44%       | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                                                     |
| 3.D.a.2.a            |  167.43           |  168.12           |  0.69        |  0.41%       | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                                                     |
| 3.D.a.2.b            |  1.67             |  1.61             |  -0.06       |  -3.85%      | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                                                     |
| 3.D.a.2.c            |  54.31            |  53.83            |  -0.48       |  -0.89%      | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                                                     |
| 3.D.a.3              |  12.47            |  12.75            |  0.29        |  2.30%       | [[Sector:Agriculture:Agricultural_Soils:Start| here]]                                                                                     |
| 3.I                  |  3.18             |  2.31             |  -0.86       |  -27.2%      | [[Sector:Agriculture:Agricultural_Other:Start| here ]]                                                                                    |
^ NFR 5 - Waste        ^  3.54             ^  3.78             ^  0.24        ^  6.92%       ^ sub-category chapters                                                                                                                     ^
| 5.B.1                |  2.02             |  2.00             |  -0.02       |  -1.05%      | [[Sector:Waste:Biological_Treatment_Composting:Start| here ]]                                                                             |
| 5.B.2                |  1.52             |  1.78             |  0.27        |  17.5%       | [[Sector:Waste:Biological_Treatment_Anaerobic_Digestion_At_Biogas_Facilities:Start| here ]]                                               |
^ NFR 6 - Other        ^  8.35             ^  18.21            ^  9.86        ^  118%        ^ sub-category chapters                                                                                                                     ^
| 6.A                  |  8.35             |  18.21            |  __9.86__    |  __118%__    |                                                                                                                                           |

                                                                                                         