====== Recalculations - Nickel (Ni) ======

The marginally small changes within the **National Total** reported for **1990 (+0.0004 t or +0.0001 %)** result from a variety of revisions throughout NFRS 1 and 2 - with the strongest percental change occuring in **NFR 1.A.3.a i(i) with minus 62 %**.
 
__Table 1: Changes in emission estimates for 1990__
|                      ^ Submission 2023  ^ Submission 2024  ^ Difference   ^              ^ Reasoning                                                                                                       ^
^ NFR Sector           ^  [t]                                             ||^ relative     ^ see description and reasoning in:                                                                               ^
^ NATIONAL TOTAL       ^  332.7447        ^  332.7450        ^  0.0004      ^  0.0001%     ^ sub-category chapters                                                                                           ^
^ NFR 1 - Energy       ^  305.1336        ^  305.1339        ^  0.0002      ^  0.0001%     ^ sub-category chapters                                                                                           ^
| 1.A.2.g vii          |  0.00030         |  0.00023         |  -0.00007    |  -22.0%      | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]    |
| 1.A.3.a i(i)         |  0.0000009       |  0.0000003       |  -0.0000006  |  __-62.4%__  | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]            |
| 1.A.3.a ii(i)        |  0.000013        |  0.000014        |  0.000001    |  7.61%       | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                 |
| 1.A.3.c              |  55.2569         |  55.2571         |  __0.0002__  |  0.0004%     | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                               |
| 1.A.4.a ii           |  0.000037        |  0.000032        |  -0.000005   |  -12.7%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]  |
| 1.A.4.c ii           |  0.04572         |  0.04579         |  0.00007     |  0.15%       | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]      |
^ NFR 2  - IPPU        ^  27.6111         ^  27.6112         ^  0.0001      ^  0.0004%     ^ sub-category chapters                                                                                           ^
| 2.G                  |  1.5785          |  1.5786          |  0.0001      |  0.01%       | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                                  |
^ NFR 3 - Agriculture  ^  NA                                                            ^^^^                                                                                                                 ^
^ NFR 5 - Waste        ^  NA                                                            ^^^^                                                                                                                 ^
^ NFR 6 - Other        ^  NA                                                            ^^^^                                                                                                                 ^

----

The significant changes within the **National Total** reported for **2021 (+5.80 t or +4.43 %)** are domainated by revisions in **NFR sub-categories 1.A.1.b** (+2.89 t) **and 1.A.3.c**           (3.18 t) together with a **variety of revisions throughout NFRs 1 and 2**.

The significant percental change occuring in NFR 1.A.3.b iii (+130 %) results from a correction of errenous data reported with the previous submission.

__Table 2: Changes in emission estimates for 2021__

^                      ^  Submission 2023  ^  Submission 2024  ^  Difference    ^            ^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [t]                                                 ||^  relative  ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  130.89           ^  136.69           ^  5.80          ^  4.43%     ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  123.18           ^  128.95           ^  5.77          ^  4.69%     ^ sub-category chapters                                                                                                                     ^
| 1.A.1.a              |  3.208            |  3.214            |  0.006         |  0.19%     | [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| here ]]                                         |
| 1.A.1.b              |  71.85            |  74.74            |  __2.89__      |  4.03%     | [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| here ]]                                                             |
| 1.A.1.c              |  0.0265           |  0.0282           |  0.0017        |  6.46%     | [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| here ]]                         |
| 1.A.2.g vii          |  0.00045          |  0.00043          |  -0.00001      |  -2.9%     | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.2.g viii         |  2.04             |  1.71             |  -0.33         |  -16.1%    | [[sector:energy:fuel_combustion:industry:other:start| here  ]]                                                                            |
| 1.A.3.a i(i)         |  0.000000134      |  0.000000127      |  -0.000000007  |  -5.24%    | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0.000001688      |  0.000001689      |  0.000000001   |  0.07%     | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  0.0365           |  0.0366           |  0.0001        |  0.30%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0.00124          |  0.00123          |  -0.00001      |  -1.02%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0.0012           |  0.0028           |  0.0016        |  __130%__  | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0.00092          |  0.00093          |  0.00001       |  1.02%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.b vi           |  2.5539           |  2.5575           |  0.0036        |  0.14%     | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:tyre_and_brake_wear| here ]]               |
| 1.A.3.b vii          |  0.5373           |  0.5375           |  0.0002        |  0.03%     | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:road_abrasion| here]]                      |
| 1.A.3.c              |  41.78            |  44.96            |  __3.18__      |  7.60%     | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                                                         |
| 1.A.3.d ii           |  0.5255           |  0.5279           |  0.0025        |  0.47%     | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a i            |  0.046            |  0.047            |  0.001         |  2.03%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| here ]]                            |
| 1.A.4.a ii           |  0.000028         |  0.000030         |  0.000002      |  6.02%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]                            |
| 1.A.4.b i            |  0.50907          |  0.50908          |  0.00001       |  0.002%    | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  0.0105           |  0.0110           |  0.0005        |  4.60%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]                                             |
| 1.A.4.c i            |  0.005968         |  0.005984         |  0.000016      |  0.27%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| here ]]                        |
| 1.A.4.c ii           |  0.043            |  0.056            |  0.014         |  31.6%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]                                |
| 1.A.5.b              |  0.00285568       |  0.00285570       |  0.00000002    |  0.001%    | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]                                                      |
^ NFR 2  - IPPU        ^  7.71101          ^  7.73736          ^  0.02634       ^  0.34%     ^ sub-category chapters                                                                                                                     ^
| 2.A.3                |  0.04095          |  0.04099          |  0.00004       |  0.09%     | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                                                            |
| 2.G                  |  1.71721          |  1.74352          |  0.02630       |  1.53%     | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                                                            |
^ NFR 3 - Agriculture  ^  NA                                                              |||^                                                                                                                                           ^
^ NFR 5 - Waste        ^  NA                                                              |||^                                                                                                                                           ^
^ NFR 6 - Other        ^  NA                                                              |||^                                                                                                                                           ^