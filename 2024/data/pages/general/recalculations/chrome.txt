====== Recalculations - Chromium (Cr)======

The negligibly small changes within the **National Total** reported for **1990 (-0.001 t or -0.001 %)** are dominated by **revisions in NFRs 1.A.2.g vii, 1.A.4.c ii and 5.E** together with some less significant changes throughout NFRs 1 and 2.

Here, the strongest percental change occurs in NFR **1.A.3.a i(i) with minus 62 %**.
 
__Table 1: Changes in emission estimates for 1990__
^                      ^  Submission 2023  ^  Submission 2024  ^  Difference                |^  Reasoning                                                                                                      ^
^ NFR Sector           ^  [t]                                               ||^  relative    ^  see description and reasoning in:                                                                              ^
^ NATIONAL TOTAL       ^  165.692          ^  165.690          ^  -0.002      ^  -0.001%     ^ sub-category chapters                                                                                           ^
^ NFR 1 - Energy       ^  122.2858         ^  122.2860         ^  0.0002      ^  0.0001%     ^ sub-category chapters                                                                                           ^
| 1.A.2.g vii          |  0.010            |  0.007            |  -0.003      |  -28.8%      | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]    |
| 1.A.3.a i(i)         |  0.0000025        |  0.0000009        |  -0.0000015  |  __-62.4%__  | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]            |
| 1.A.3.a ii(i)        |  0.0000355        | 0.0000382         | 0.0000027    |  7.61%       | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                 |
| 1.A.3.c              |  27.6524          |  27.6526          |  0.0002      |  0.001%      | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                               |
| 1.A.4.a ii           |  0.0016           |  0.0014           |  -0.0002     |  -12.7%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]  |
| 1.A.4.c ii           |  0.039            |  0.042            |  0.003       |  7.74%       | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]      |
^ NFR 2  - IPPU        ^  43.3769          ^  43.3769          ^  0.0001      ^  0.0002%     ^ sub-category chapters                                                                                           ^
| 2.G                  |  0.6744           |  0.6745           |  0.0001      |  0.01%       | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                                  |
^ NFR 3 - Agriculture  ^  NA                                                              ^^^^                                                                                                                 ^
^ NFR 5 - Waste        ^  0.029            ^  0.027            ^  -0.002      ^  -5.96%      ^ sub-category chapters                                                                                           ^
| 5.E                  |  0.029            |  0.027            |  __-0.002__  |  -5.96%      | [[Sector:Waste:Other_Waste:Start| here ]]                                                                       |
^ NFR 6 - Other        ^  NA                                                              ^^^^                                                                                                                 ^

----

The changes within the **National Total** reported for **2021 (+1.82 t or +2.66 %)** result from a **variety of revisions throughout NFRs 1, 2 and 5**.

The significant percental change occuring in NFR 1.A.3.b iii (+130 %) results from a correction of errenous data reported with the previous submission.

__Table 2: Changes of emission estimates for 2021__

^                      ^  Submission 2023  ^  Submission 2024  ^  Difference               |^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [t]                                                ||^  relative  ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  68.17            ^  69.98            ^  1.82         ^  2.66%     ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  49.56            ^  51.36            ^  1.80         ^  3.63%     ^ sub-category chapters                                                                                                                     ^
| 1.A.1.a              |  3.83             |  3.84             |  0.01         |  0.30%     | [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| here ]]                                         |
| 1.A.1.b              |  1.25             |  1.31             |  0.06         |  5.20%     | [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| here ]]                                                             |
| 1.A.1.c              |  0.0409           |  0.0444           |  0.0035       |  8.58%     | [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| here ]]                         |
| 1.A.2.g vii          |  0.010            |  0.007            |  -0.003       |  -33.0%    | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.2.g viii         |  0.23             |  0.22             |  -0.01        |  -5.55%    | [[sector:energy:fuel_combustion:industry:other:start| here  ]]                                                                            |
| 1.A.3.a i(i)         |  0.00000037       |  0.00000035       |  -0.00000002  |  -5.24%    | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0.000004623      |  0.000004627      |  0.000000003  |  0.07%     | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  0.208            |  0.206            |  -0.002       |  -1.08%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0.0353           |  0.0347           |  -0.0007      |  -1.88%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0.05             |  0.12             |  0.07         |  __130%__  | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0.00252          |  0.00255          |  0.00003      |  1.02%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.b vi           |  16.46            |  16.48            |  0.02         |  0.15%     | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:tyre_and_brake_wear| here ]]               |
| 1.A.3.b vii          |  1.0181           |  1.0184           |  0.0003       |  0.03%     | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:road_abrasion| here]]                      |
| 1.A.3.c              |  20.90            |  22.48            |  1.59         |  7.60%     | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                                                         |
| 1.A.3.d ii           |  0.029            |  0.031            |  0.002        |  6.10%     | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a i            |  2.15             |  2.20             |  0.05         |  2.14%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| here ]]                            |
| 1.A.4.a ii           |  0.00121          |  0.00128          |  0.00007      |  6.02%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]                            |
| 1.A.4.b i            |  3.22116          |  3.22117          |  0.00001      |  0.0003%   | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  0.0067           |  0.0070           |  0.0003       |  4.60%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]                                             |
| 1.A.4.c i            |  0.0780           |  0.0783           |  0.0002       |  0.27%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| here ]]                        |
| 1.A.4.c ii           |  0.04             |  0.05             |  0.01         |  30.76%    | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]                                |
| 1.A.5.b              |  0.00045806       |  0.00045811       |  0.00000005   |  0.01%     | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]                                                      |
^ NFR 2  - IPPU        ^  18.58006         ^  18.59597         ^  0.01590      ^  0.09%     ^ sub-category chapters                                                                                                                     ^
| 2.A.3                |  0.09895          |  0.09901          |  0.00007      |  0.07%     | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                                                            |
| 2.G                  |  0.840583         |  0.856420         |  0.015837     |  1.88%     | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                                                            |
^ NFR 3 - Agriculture  ^  NA                                                             |||^                                                                                                                                           ^
^ NFR 5 - Waste        ^  0.030            ^  0.028            ^  -0.002       ^  -5.81%    ^ sub-category chapters                                                                                                                     ^
| 5.E                  |  0.030            |  0.028            |  -0.002       |  -5.81%    | [[Sector:Waste:Other_Waste:Start| here ]]                                                                                                 |
^ NFR 6 - Other        ^  NA                                                             |||^                                                                                                                                           ^