====== Recalculations - Lead (Pb) ======

The marginally small changes within the **National Total** reported for **1990 (+0.08 t or +0.004 %)** are dominated by revisions in **NFRs 1.A.3.a i(i) and 1.A.3.a i(ii)** where leaded avgas is applied. 

Here, the most significant percental change occurs for NFR **1.A.3.a i(i) with minus 62 %**.

__Table 1: Changes of emission estimates 1990__
^                      ^  Submission 2023  ^  Submission 2024  ^  Difference                |^  Reasoning                                                                                                      ^
^ NFR Sector           ^  [t]                                               ||^  relative    ^  see description and reasoning in:                                                                              ^
^ NATIONAL TOTAL       ^  1,899.19         ^  1,899.27         ^  0.08        ^  0.004%      ^ sub-category chapters                                                                                           ^
^ NFR 1 - Energy       ^  1,499.00         ^  1,499.08         ^  0.08        ^  0.005%      ^ sub-category chapters                                                                                           ^
| 1.A.2.g vii          |  2.0893           |  2.0891           |  -0.0002     |  -0.01%      | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]    |
| 1.A.3.a i(i)         |  0.16             |  0.06             |  __-0.10__   |  __-62.4%__  | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]            |
| 1.A.3.a ii(i)        |  2.33             |  2.50             |  __0.18__    |  7.61%       | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                 |
| 1.A.3.c              |  0.2468           |  0.2470           |  0.0002      |  0.07%       | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                               |
| 1.A.4.a ii           |  0.00009          |  0.00008          |  -0.00001    |  -12.7%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]  |
^ NFR 2  - IPPU        ^  400.05           ^  400.05           ^  0.00        ^  0.00%       ^                                                                                                                 ^
^ NFR 3 - Agriculture  ^  NA                                                              ^^^^                                                                                                                 ^
^ NFR 5 - Waste        ^  0.1475           ^  0.1469           ^  -0.0006     ^  -0.39%      ^ sub-category chapters                                                                                           ^
| 5.E                  |  0.0095           |  0.0089           |  -0.0006     |  -6.07%      | [[Sector:Waste:Other_Waste:Start| here ]]                                                                       |
^ NFR 6 - Other        ^  NA                                                              ^^^^                                                                                                                 ^

----

The small changes within the **National Total** reported for **2021 (+0.82 kt or +0.53 %)** are dominated by a handful of **stonger revisions in NFR 1** with the biggest change occuring for NFR sub-category **1.A.4.a i with plus 0.44 kt** together with a variety of less significant revisions throughout NFRs 1, 2, 3 and 5.

The significant percental change occuring in NFR 1.A.3.b iii (+130 %) results from a correction of errenous data reported with the previous submission.

__Table 2: Changes of emission estimates 2021__

^                      ^  Submission 2023  ^  Submission 2024  ^  Difference               |^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [t]                                               ||^  relative   ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  154.45           ^  155.27           ^  0.82        ^  0.53%      ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  82.06            ^  82.88            ^  0.82        ^  1.00%      ^ sub-category chapters                                                                                                                     ^
| 1.A.1.a              |  6.66             |  6.71             |  0.06        |  0.83%      | [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| here ]]                                         |
| 1.A.1.b              |  4.84             |  5.12             |  __0.28__    |  5.87%      | [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| here ]]                                                             |
| 1.A.1.c              |  0.068            |  0.074            |  0.005       |  7.39%      | [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| here ]]                         |
| 1.A.2.g vii          |  0.00071          |  0.00055          |  -0.00016    |  -22.3%     | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.2.g viii         |  0.56             |  0.53             |  -0.03       |  -5.92%     | [[sector:energy:fuel_combustion:industry:other:start| here  ]]                                                                            |
| 1.A.3.a i(i)         |  0.024            |  0.023            |  -0.001      |  -5.24%     | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0.3028           |  0.3030           |  0.0002      |  0.07%      | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  0.03022          |  0.03018          |  -0.00004    |  -0.13%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0.00230          |  0.00227          |  -0.00004    |  -1.62%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0.003            |  0.007            |  0.004       |  __130%__   | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0.000639         |  0.000646         |  0.000007    |  1.02%      | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.b vi           |  44.21            |  44.27            |  0.06        |  0.15%      | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:tyre_and_brake_wear| here ]]               |
| 1.A.3.b vii          |  0.05797          |  0.05799          |  0.00002     |  0.03%      | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:road_abrasion| here]]                      |
| 1.A.3.d ii           |  0.040            |  0.042            |  0.002       |  4.60%      | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a i            |  20.45            |  20.89            |  __0.44__    |  2.14%      | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| here ]]                            |
| 1.A.4.a ii           |  0.000071         |  0.000075         |  0.000004    |  6.02%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]                            |
| 1.A.4.b i            |  4.6614           |  4.6615           |  0.0001      |  0.003%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  0.000129         |  0.000135         |  0.000006    |  4.60%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]                                             |
| 1.A.4.c i            |  0.0685           |  0.0687           |  0.0002      |  0.27%      | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| here ]]                        |
| 1.A.4.c ii           |  0.00085          |  0.00110          |  0.00025     |  __29.5%__  | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]                                |
^ NFR 2  - IPPU        ^  72.3353          ^  72.3358          ^  0.00048     ^  0.001%     ^ sub-category chapters                                                                                                                     ^
| 2.A.3                |  0.64401          |  0.64446          |  0.00045     |  0.07%      | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                                                            |
| 2.G                  |  0.00141          |  0.00144          |  0.00003     |  1.88%      | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                                                            |
^ NFR 3 - Agriculture  ^  NA                                                             |||^                                                                                                                                           ^
^ NFR 5 - Waste        ^  0.0527           ^  0.0521           ^  -0.0006     ^  -1.09%     ^ sub-category chapters                                                                                                                     ^
| 5.E                  |  0.0097           |  0.0092           |  -0.0006     |  -5.92%     | [[Sector:Waste:Other_Waste:Start| here ]]                                                                                                 |
^ NFR 6 - Other        ^  NA                                                             |||^                                                                                                                                           ^