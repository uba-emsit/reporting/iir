====== Recalculations - Benzo[k]Fluoranthene======

The marginally small changes within the **National Total** reported for **1990 (+0.0001 t or +0.001 %)** result almost entirely from **revisions in several sub-categories of NFR 1** with the most significant percental change occurs for **1.A.3.a i(i) with minus 62 %**.

__Table 1: Changes of emission estimates 1990__
^                      ^  Submission 2023  ^  Submission 2024  ^  Difference                |^ Reasoning                                                                                                       ^
^ NFR Sector           ^  [t]                                               ||^  relative    ^ see description and reasoning in:                                                                               ^
^ NATIONAL TOTAL       ^  16.2592          ^  16.2593          ^  0.0001      ^  0.001%      ^ sub-category chapters                                                                                           ^
^ NFR 1 - Energy       ^  15.9993          ^  15.9994          ^  0.0001      ^  0.001%      ^ sub-category chapters                                                                                           ^
| 1.A.2.g vii          |  0.0387           |  0.0274           |  -0.0113     |  -29.31%     | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]    |
| 1.A.3.a i(i)         |  0.000002         |  0.000001         |  -0.000001   |  __-62.4%__  | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]            |
| 1.A.3.a ii(i)        |  0.000022         |  0.000024         |  0.000002    |  7.61%       | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                 |
| 1.A.3.c              |  0.0308           |  0.0309           |  0.0001      |  0.38%       | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                               |
| 1.A.4.a ii           |  0.0063           |  0.0055           |  -0.0008     |  -12.7%      | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]  |
| 1.A.4.c ii           |  0.045            |  0.057            |  0.012       |  27.0%       | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]      |
^ NFR 2  - IPPU        ^  0.04             ^  0.04             ^  0.00        ^  0.00%       ^                                                                                                                 ^
^ NFR 3 - Agriculture  ^  NA                                                              ^^^^                                                                                                                 ^
^ NFR 5 - Waste        ^  0.22             ^  0.22             ^  0.00        ^  0.00%       ^                                                                                                                 ^
^ NFR 6 - Other        ^  NA                                                              ^^^^                                                                                                                 ^

----

The changes within the **National Total** reported for **2021 (+0.30 t or +2.7 %)** are dominated by a correction in **NFR 1.A.3.b iii with +0.27 t**, accompanied by a variety of small revisions throughout NFR 1.

The significant percental change occuring in NFR 1.A.3.b iii (+130 %) results from a correction of errenous data reported with the previous submission.
 
__Table 2: Changes of emission estimates 2021__

^                      ^  Submission 2023  ^  Submission 2024  ^  Difference               |^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [t]                                                ||^  relative  ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  11.36            ^  11.66            ^  0.30         ^  2.68%     ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  11.26            ^  11.56            ^  0.30         ^  2.70%     ^ sub-category chapters                                                                                                                     ^
| 1.A.2.g vii          |  0.039            |  0.025            |  -0.014       |  -36.5%    | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.3.a i(i)         |  0.00000023       |  0.00000022       |  -0.00000001  |  -5.24%    | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0.000002862      |  0.000002864      |  0.000000002  |  0.07%     | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  0.2177           |  0.2142           |  -0.0035      |  -1.60%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0.0355           |  0.0348           |  -0.0007      |  -1.93%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0.21             |  0.48             |  __0.27__         |  __130%__      | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0.00272          |  0.00275          |  0.00003      |  1.02%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.c              |  0,009576         |  0,009583         |  0,000007     |  0,07%     | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                                                         |
| 1.A.3.d ii           |  0.016            |  0.017            |  0.001        |  7.86%     | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a i            |  1.29             |  1.32             |  0.03         |  2.13%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| here ]]                            |
| 1.A.4.a ii           |  1.29             |  1.32             |  0.03         |  2.13%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional | here ]]                           |
| 1.A.4.b i            |  0.0049           |  0.0052           |  0.0003       |  6.00%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  9.235            |  9.242            |  0.007        |  0.08%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential | here ]]                                            |
| 1.A.4.c i            |  0.00029          |  0.00030          |  0.00001      |  4.60%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| here ]]                        |
| 1.A.4.c ii           |  0.1049           |  0.1052           |  0.0003       |  0.28%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry | here ]]                               |
| 1.A.5.a              |  0.049            |  0.063            |  0.014        |  29.0%     | [[sector:energy:fuel_combustion:other_including_military:stationary_fuel_combustion_in_military_facilities| here ]]                       |
| 1.A.5.b              |  0.00039378       |  0.00039381       |  0.00000003   |  0.01%     | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]                                                      |
^ NFR 2  - IPPU        ^  0.03             ^  0.03             ^  0.00         ^  0.00%     ^                                                                                                                                           ^
^ NFR 3 - Agriculture  ^  NA                                                             ^^^^                                                                                                                                           ^
^ NFR 5 - Waste        ^  0.07             ^  0.07             ^  0.00         ^  0.00%     ^                                                                                                                                           ^
^ NFR 6 - Other        ^  NA                                                             ^^^^                                                                                                                                           ^
