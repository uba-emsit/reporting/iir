=== Measures that have already been implemented or measures whose implementation has been decided are assigned to the WM scenario. ===


**Reductions in large combustion plants through implementation of the 13<sup>th</sup> and 17<sup>th</sup> BImSchV as well as minimum requirements of recent BAT conclusions:**

Measures for large combustion plants (LCP) that have already been implemented through the 13<sup>th</sup> and 17<sup>th</sup> BImSchV or do have future reduction effects from the existing regulations as well as minimum requirements of recent BAT conclusions are considered in the WM scenario. The measures affect time series of NFR sectors under 1.A and lead to a reduction in the emission factors. Potential mitigation effects emerge from BAT conclusions according to Directive 2010/75/EU. If the current submission 2022 shows that the emissions in the time series are already below the upper ends of the specified emission ranges and thus the statutory maximum limit values will be fulfilled, these will be updated unchanged. In the case of time series above the upper range, the maximum permitted limit values are used as a result of the measure in the sense of a conservative estimation and the emission factors of the pollutants for each source group are recalculated.

The calculations always follow the same procedure. Important elements are the specific limit values of the 13<sup>th</sup> and 17<sup>th</sup> BImSchV as well as the distribution of the plants according to their rated thermal input (RTI) in megawatts (MW). In addition, it is assumed that all new and existing plants correspond at least to the standard of the upper range of the associated BAT conclusions. The lower emission factor out of both calculations is than compared with the reference value. If the recalculated emission factor of the source category under consideration is greater than the current reference value, the reference value from the 2022 submission will be updated unchanged. If the reference value is larger, the new value is set and projected((Methodology and calculations for large combustion plants are based on Jakobs, H., Schneider, C., Handke, V. (2019): NEC-Richtlinie: Weiterentwicklung von Prognosen für Luftschadstoffe für nationale Luftreinhalteprogramme, Project-Number FKZ 3716512020, on behalf of the German Environment Agency (UBA).)). 

According to expert estimates, the plant inventory is split as in Table 2 according to the RTI (in MW). These (cumulative) proportions are necessary for the calculation of the mean values in relation to the upper range of limit values for each source category and pollutant. 

__Table 2: Proportionate inventory of LCPs according to their power range__ 
^  RTI in MW  ^  Proportion  ^ 
|  <100	      |   4.5 %	     |			
|  100-300    |   14.5 %     |		
|  300-1000   |   68 %       | 	
|  >1000      |   13 %       | 	
|<sub>The limit values of LCP are set according to their power ranges. The table shows the estimated proportion of LCP in Germany in relation to the RTI provided.</sub>||

__Example 1__

The concrete procedure is illustrated using the example of NO<sub>X</sub> emissions from the use of raw lignite as fuel for heat generation in public district heating plants. 

The specific BAT-associated emission levels for lignite can be found in Commission Implementing Decision (EU) 2017/1442 BAT 20. With a reference oxygen of 6 %, the plants are differentiated according to size and specified with the emission levels in mg/Nm<sup>3</sup>. The upper end of the emission levels is interpreted as a maximum limit value and converted into kg/TJ using the specific conversion factor of 2.40 (see Table 1). The calculated maximum limit value is therefore averaged for each plant size, taking into account the number of plants, and thus, the estimated value for the necessary NO<sub>X</sub> emission factor for compliance with the maximum limit value is calculated in accordance with the BAT conclusions. The necessary data can be found in Table 3. This shows the plants subdivision according to their RTI with the assigned maximum limit values in mg/Nm<sup>3</sup> and kg/TJ. 

__Table 3: Emission limit values (yearly averages) when using raw lignite in existing plants__ 
^  Plant size according to RTI in MW  ^  max limit value in mg/m<sup>3</sup>  ^  max limit value in kg/TJ  ^  Proportion  ^
| 		<100	  	      |		270           		 | 	112.70	  	    |	4.5 %  	 |
| 		100-300		      |		180	       		 |	75.13	  	    |  14.5 %  	 |
| 		>300		      |		175	       		 |	73.04     	    |	81 %   	 |
|<sub>The LCP emission limit values for the use of raw lignite are regulated in (EU) 2017/1442 BAT 20. There are separate limit values for each RTI of the plant. The upper range is shown here as a limit value for existing plants as yearly averages in mg/Nm<sup>3</sup> and kg/TJ.</sub>|||| 

The emission factor is calculated in (1).

    (1) emission factor (lignite) = 112.70 kg/TJ * 4.5 % + 75.13 kg/TJ * 14.5 % + 73.04 kg/TJ * 81 % = 75.13 kg/TJ

The comparison with the current submission 2022 shows that the calculated emission factor (75.13 kg/TJ) is lower than that of the reference value from 2020 (76.8 kg/TJ). Thus from 2025 onwards the emission factor will be replaced by the new value and used for the projection. 

This procedure is analogous for the evaluation of all source groups and pollutants.  

__Example 2__

According to the Commission Implementing Decision (EU) 2017/1442 of 31<sup>st</sup> of July 2017 on Conclusions on Best Available Techniques (BAT) according to Directive 2010/75/EU of the European Parliament and of the Council for large combustion plants, the maximum permissible pollutant emission for NO<sub>X</sub> while using heavy fuel oil in plants < 100 MW is 270 mg/Nm<sup>th</sup> and in plants > 100 MW is 110 mg/Nm<sup>th</sup> as yearly average for existing plants with more than 1500 operating hours per year (BAT 28). The values are converted into kg/TJ according to the specific flue gas volume of heavy fuel oil (table 1). Assuming all plants in 2025 and beyond are existing plants, as defined in the Implementing Decision and are operated more than 1500 hours per year, a projected implied NO<sub>X</sub> emission factor of 34.6 kg/TJ results after conversion as indicated in equation (2). 

    (2) emission factor (heavy fuel oil) = (270 mg/Nm3 / 3.39) * 4,5 % + (110 mg/Nm3 / 3.39) * 95.5 % = 34.6 kg/TJ.

Thus, the maximum emission quantity is applicable law and is below the inventory emission factor for the reference year 2020 under conservative assumptions and therefore assigned to the WM scenario for 2025 and beyond. 

__Special features of the evaluation of the emission factors__

When using liquid fuels (specified in the database as “other mineral oil products”) in LCP, the specific conversion factor of 3.39 (see Table 1) is used for the assessment of NO<sub>X</sub> emissions, analogous to heavy fuel oil. 

When calculating the potential SO<sub>2</sub> emissions from source group “Mitverbrennung in öffentlichen Fernheizwerken” and “Mitverbrennung in öffentlichen Kraftwerken” for other liquid fuels, a clear distinction is made in the 17<sup>th</sup> BImSchV between existing plants and new plants. The implied emission limit value of existing plants is 78.44 kg/TJ. It is assumed that by 2030 all plants will correspond to the latest technology and will therefore from 2030 onwards retain at least the limit value for new plants, estimated at 61.81 kg/TJ. Furthermore, it is assumed that a continuous renewal takes place, so that the mean value from 2020 and 2030 is calculated for 2025 (70.13 kg/TJ).

**Reduction in large combustion plants burning lignite through the coal phase-out:**

The German Coal Power Generation Termination Act (“Kohleverstromungsbeendigungsgesetz”) from August 2020 stipulates to gradually phase out coal power plants burning lignite until 31<sup>st</sup> December 2038. The latest change in this law from December 2022 is part of the WAM scenario and is described there. 

Projection of the activity rates was taken from the “Projektionsbericht 2021 für Deutschland” and disaggregated to the German lignite mining districts within the project as shown in Table 4.

__Table 4: Primary energy use for lignite in LCP (> 50 MW) according to the decommissioning path in the years 2018 to 2040__ 
^ District	  ^ Primary Energy Use 2018 ^ Primary Energy Use 2025 ^  Primary Energy Use 2030 ^  Primary Energy Use 2035 ^ Primary Energy Use 2040 ^
|		  ^		in TJ	    ^	  	in TJ	      ^		in TJ	   	 ^		in TJ	    ^		in TJ	      ^
| Lausitz	  | 	470199		    |     	400372	      |     	194503	         |     	139239	            |     	0	      |
| Central Germany | 	155146		    |     	177529	      |     	173767	         |     	109198	            |     	0	      |
| Rhineland	  | 	675897		    |     	384178	      |     	190843	         |     	187791	            |     	0	      |
^ Total		  ^ 	1318381		    ^     	962079	      ^      	559113	         ^     	486228	            ^     	0	      |

Emission factors of public heating and thermal power plants for NO<sub>X</sub> are therefore reassessed. When calculating the NO<sub>X</sub> emission factors as a result of the phase-out, the districts of Central Germany, Lausitz and Rhineland are considered separately. The individual districts will be subdivided into their existing power plants. For each power plant, the total activity rate and the emission factors for NO<sub>X</sub> for the years 2004 to 2017 in TJ or kg/TJ according to the 2020 submission are adopted as data basis. In order to take into account fluctuations in the emission factors, the emission factors are averaged per plant over the last years, in which no new blocks went into operation (e.g. Block R of Boxberg IV in the Lausitz district started continuous operation in 2012). In addition, the mean value for all power plants in a district is calculated for the formation of the implied emission factor by weighting according to their activity rates. Hence, each district is assigned a current implied emission factor. With the shutdown of the last block of a power plant, this plant is considered to be shut down and from this point in time it is no longer included in the calculation of the implied emission factor of a specific district. This applies to the Schkopau power plants (Central Germany district) from 2035 onwards, to Jänschwalde, Boxberg III (both: Lausitz district) and Weisweiler (Rhineland district) from 2030 onwards. 

**Reduction in small combustion installations through the 1<sup>st</sup> BImSchV and funding programmes:**

Reductions of dust emissions from small combustion installations are achieved in the NFR sectors 1.A.4 and 1.A.5 through the implementation of the 1<sup>st</sup> BImSchV. The calculation of the future emission factors is based on the projection of the "Energiewende" scenario (EWS) from Tebert et al. (2016)((Tebert, C., Volz, F., Töfke, K. (2016): Development and update of emission factors for the National Inventory regarding small and medium-size combustion plants of households and small consumers, on behalf of the German Environment Agency (UBA), Project-Nr. 3712 42 313 2)), while the current underlying projection is containing a greater use of solid biomass in 2030 than the EWS. The developments in the area of small combustion installations, in particular the development of fuel use and the existing plant inventory, are difficult to assess and emission calculation is fraught with uncertainties. According to expert assessments, with an increase of solid biomass use the implied emission factor will further decrease as the share of newer and cleaner installations will go up. Therefore, the projected implied emission factors based on the EWS used here are expected to be conservative.

Based on the inventory, a distinction is only made between households (“Haushalte” (HH)) and commerce, trade, services (“Gewerbe, Handel, Dienstleistungen” (GHD)), but the calculation of the emissions factors is further sub-divided in several installation type categories of local space heaters and solid fuel boilers, with different emission limit values set by the 1<sup>st</sup> BImSchV. In addition, emission factors are expected to decrease through several funding programmes, last the “Bundesförderung effiziente Gebäude” (BEG)((https://www.energiewechsel.de/KAENEF/Redaktion/DE/FAQ/FAQ-Uebersicht/Richtlinien/bundesfoerderung-fuer-effiziente-gebaeude-beg.html)), whereas activity rates of heat supply from solid biomass in buildings is expected to increase until 2030, before decreasing again by 2040. Resulting emission factors for TSP (total suspended particles) used in the WM scenario are shown in table 5.

__Table 5: TSP emission factors in small combustion installations for solid biomass in the WM scenario__
^ sub-sector  ^ 2020 in kg/TJ	^ 2025 in kg/TJ ^ 2030 in kg/TJ	^ 2035 in kg/TJ	 ^ 2040 in kg/TJ ^
^ households (HH)  |  64.2 	|		|		|		 |		 |	
|  1<sup>st</sup> BImSchV| 		|	55.72   |	47.24	|	45.16	 |	43.08	 |	
|  1<sup>st</sup> BImSchV + funding| 		|	55.67   |	47.15	|	45.08	 |	43.00	 |
^ households (HH) commerce, trade, service   |  40.0 	|		|		|		 |		 |	
|  1<sup>st</sup> BImSchV| 		|	33.57   |	27.12	|	25.68	 |	24.24	 |	
|  1<sup>st</sup> BImSchV + funding| 		|	33.40   |	26.78	|	25.36	 |	23.94	 |

For calculation of PM<sub>2.5</sub> and PM<sub>10</sub> emission factors specific shares per installation type category were used. For 2030 the proportions of PM<sub>10</sub> and PM<sub>2.5</sub> in TSP (total suspended particles) are given in table 6.

__Table 6: Proportions of PM<sub>10</sub> and PM<sub>2.5</sub> in TSP in 2030 for categories of small combustion installations__
^ installation type category  ^  PM<sub>10</sub>/TSP in %  ^  PM<sub>2.5</sub>/TSP in %  ^
^ local space heaters (solid biomass)  |	   99          	   |		95.5		  | 
^ solid fuel boilers (biomass)  |	             	   |				  | 
|  pellet boilers|	   97          	   |		84		  | 
|  log boilers|	   92          	   |		79		  | 
|  wood chip boilers|	   94          	   |		87		  | 

**Reduction in industrial processes through low-dust filter technology in sinter plants:**
 
The assumed potential for reducing dust emissions from sinter plants is taken from the final report of the UBA project LUFT 2030 (Jörß et al., 2014)((Jörß, W., Emele, L., Scheffler, M., Cook, V., Theloke, J., Thiruchittampalam, B., Dünnebeil, F., Knörr, W., Heidt, C., Jozwicka, M., Kuenen, J.J.P., Denier van der Gon, H.A.C., Visschedijk, A.J.H., van Gijlswijk, R.N., Osterburg, B., Laggner, B., Stern, R., Handke, V. (2014): Luftqualität 2020/2030: Weiterentwicklung von Prognosen für Luftschadstoffe unter Berücksichtigung von Klimastrategien, on behalf of the German Envrionment Agency (UBA), Project-Nr. 3710 43 219, UBA-Texte 35/2014, https://www.umweltbundesamt.de/publikationen/luftqualitaet-20202030-weiterentwicklung-von)), where measure P 009 results in dust emissions of less than 10 mg/Nm<sup>3</sup> due to better filter technology. It is assumed that only half of the potential from the LUFT 2030 project will be reached in average. Thus, the emission factors for PM<sub>2.5</sub> and PM<sub>10</sub> result from the mean value of the current submission 2022 and the emission factor from the LUFT 2030 project at 50 per cent each. The affected time series are assigned to the NFR sector 2.C.1. This technology also causes new split factors for the calculation of PM<sub>2.5</sub> and PM<sub>10</sub>. Therefore, the split factor for PM<sub>10</sub> is taken from the LUFT 2030 project, too. 

The emission factor for dust is calculated by dividing the given sizes of the emission factor for PM<sub>10</sub> by the split factor for PM<sub>10</sub>. Consequently, the split factor for PM<sub>2.5</sub> can be calculated by dividing the emission factor for PM<sub>2.5</sub> by the emission factor for dust. 

These calculated factors (emission factor dust and the split factors for PM<sub>2.5</sub> and PM<sub>10</sub>) for the recorded emission sources are used for the projection and transferred to the database.

**Reduction in medium combustion plants through implementation of the 44<sup>th</sup> BImSchV:**

Medium combustion plants (MCP), including gas turbines and combustion engine plants are regulated by the national 44<sup>th</sup> BImSchV, which entered into force in June 2019, and are therefore part of the WM scenario. The underlying limit values of the emission calculation are taken from the 44<sup>th</sup> BImSchV. The measure leads to a reduction in the emission factors of the affected time series in several NFR sectors under 1.A.

The data basis for the calculation is the submission 2022. The source categories are reassessed separately according to the pollutants and the relevant fuel inputs. The expected service life of the plants (in years) is taken into account (see Table 8) as well as a distinction between old and new plants and the RTI of the plants in MW (see Table 7). Table 7 shows the plant split for the various fuel uses taking into account the RTI.

__Table 7: Proportional plant split of the MCP according to fuel consumption and RTI__
^ Plant split according to fuel consumption ^  RTI in MW  ^  Proportion  ^
^ Biomass			    	    | 	 1-5	|   6.5 %    |
|		:::			    | 	5-20	|  17.7 %    |
|		:::		            |  20-50	|  75.8 %    |
^ Lignite				    |   1-20	|  95.8 %    |
| 		:::			    |  20-50	|   4.2 %    | 
^ Hard coal				    |   1-20	|  90.2 %    |
|		:::			    |  20-50	|   9.8 %    |
^ Heavy fuel				    |   5-20	|  68.0 %    |
|	       :::                          |  20-50	|  32.0 %    |
|<sub>The limit values of the MCP are specified in the 44<sup>th</sup> BImSchV according to their performance ranges. The table shows the estimated proportion of MCP in Germany in relation to the RTI provided and the fuel input used.</sub>|||

__Table 8: Expected service life of MCP according to type of plant, pollutant and fuel use__ 
|							^  Expected average service life 	^
^ Combustion plants - solid fuels			|	20 years 			|				    
^ Combustion plants – liquid and gaseous fuels		|	15 years 			| 
^ gas and steam turbines (GuD) and gas turbines (GT)	|	22 years 			| 
^ internal combustion engines - biogas			|	 5 years			| 
^ internal combustion engines – other fuels		|	10 years			|

The new emission factors are always calculated according to the same pattern. The limit values of the 44<sup>th</sup> BImSchV are weighted for each RTI range of the plants and calculated for old and new plants. Assuming that a constant rate of existing plants, depending on the assumed service life, is renewed or upgraded annually, the weighting of the limit values for new plants for the projections in 2025, 2030, 2035 and 2040 is increased or, depending on the expected service life of the plant, only the limit values for new plants are taken into account. 

If the current emission factor from the 2022 submission undercuts the calculated value, the current reference value is updated because it is already below the upper range according to the 44<sup>th</sup> BImSchV and thus complies with the maximum limit values. The recalculated values for the time series are adopted and the maximum permitted limit value is assigned to time series when the current emission factor is above the upper range.

__Example:__

The exact procedure is exemplified by the NO<sub>X</sub> emission factors when using other solid biomass (than wood) as fuel. The procedure is in principle the same for all pollutants and fuels. 

The basis for the calculation is the maximum amount of NO<sub>X</sub> emissions per cubic meter permitted in the 44<sup>th</sup> BImSchV §10 (4) and (15) when using other solid biomass (than wood) as fuel (Table 9). After conversion with the specific conversion factor for lignite, assumed as similar to other solid biomass, of 2.39 (see Table 1), the limit values for old and new plants are available in kg/TJ. Table 9 shows the NO<sub>X</sub> limit values for solid biomass according to the RTI range for old and new plants in mg/Nm<sup>3</sup> and kg/TJ.

__Table 9: NO<sub>X</sub> limit values for other solid biomass (than wood) in MCP according to the RTI for old and new plants__
^ Fuel					^ Plant		^  NO<sub>X</sub> limit value according to 44<sup>th</sup> BImSchV in mg/Nm<sup>3</sup>   ^^^ NO<sub>X</sub> limit value in kg/TJ    ^^^
| 	::: 				| ::: 		|  RTI in MW				      		|||  RTI in MW	      ||| 
| 	::: 				| ::: 		|  1-5	|  >5	|  >20						|   1-5	   |    >5   |   >20    | 
^ other solid biomass (than wood) 	| existing	| 	  600  ||  370						|        250.4      ||  154.4  | 
^ other solid biomass (than wood)	| new		|  370	|  300	|  200						|   154.4  |  125.2  |   83.5	| 
|<sup>Limit values for solid biomass in MCP for old and new plants according to the 44th BImSchV in mg/Nm^3 and kg/TJ.</sup>||||||||

It is assumed that the average service life of the plants is 20 years (Table 8). In addition, it is assumed that an annual renewal of the plant will be implemented after the 44<sup>th</sup> BImSchV comes into force in 2019 and that the limit values for new plants getting greater weight each year. 

According to the assumption in 2025 (6 years after the regulation came into force) there is a proportion of 6/20 which fulfil the requirements of new plants and 14/20 which adhere to the limit values of old plants. In 2030, eleven years after the 44<sup>th</sup> BImSchV was introduced, the proportion of new plants is 11/20 compared to 9/20 old plants. After 16 years in 2035, the limit value for new plants is included in the calculation with 16/20. 

Taking into account the plants proportions per size measured in RTI in WM (Table 9), a new emission factor of 153.0 kg/TJ for 2025 results, as shown in (3). 

    (3) emission factor (other solid biomass than wood in 2025) = 14/20 * {(6.5 % + 17.7 %) * 250.4 kg/TJ + 75.8 % * 154.4 kg/TJ} + 6/20 * {6.5 % * 154.4 kg/TJ + 17.7 % * 125.2 kg/TJ + 75.8 % * 83.5 kg/TJ} = 153.0 kg/TJ.

Since the maximum reference value from the 2022 submission for the year 2020 (137.5 kg/TJ) is already below the calculated limit, it will be kept constant for the year 2025. The procedure for calculating the emission factor in 2030 is identical and is shown in (4).

    (4) emission factor (other solid biomass than wood in 2030) = 9/20 * {(6.5 % + 17.7 %) * 250.4 kg/TJ + 75.8 % * 154.4 kg/TJ} + 11/20 * {6.5 % * 154.4 kg/TJ + 17.7 % * 125.2 kg/TJ + 75.8 % * 83.5 kg/TJ} = 132.45 kg/TJ

In 2030 the newly calculated limit value will be below the reference value, so that the calculated one is adopted as the new NO<sub>X</sub> emission factor. 

__Special Feature:__

When calculating the NO<sub>X</sub> emission factors for using lignite and hard coal as fuel, the plant split is only differentiated according to the RTI of less than 20 MW and greater than 20 MW. The limit values given in the 44<sup>th</sup> BImSchV are differentiated according to 1-5 MW, 5-20 MW and more than 20 MW. 

As a result, the assumption was made that the plant split between 1-5 MW and 5-20 MW in equal proportions would be valued with a factor of 0.5. 

According to the 44<sup>th</sup> BImSchV § 16, the emission limit values for combustion engines will only apply from 1<sup>st</sup> of January 2025 on, so that the assumption of the partial renewal of plants will only apply from 2025 on. As a result, the reference values from the 2022 submission will be kept constant for 2025 and calculation of implied emission factors considering the limit values for new plants starts from the year 2025.

