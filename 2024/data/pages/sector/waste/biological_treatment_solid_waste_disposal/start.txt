====== 5.A - Biological Treatment of Waste: Solid Waste Disposal on Land ======

===== Short description ===== 

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  5.A                                    |  T1      |  NS  |  D   |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|  NO<sub>x</sub>                         ^  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   |  CO  |  Heavy Metals  |  POPs  |
|  NA                                     |  -/-    |  NA              |  NA              |  -/-               |  -/-              |  -/-  |  -/-  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                  |||||||||||

In category **5.A**, __NMVOC and PM<sub>2.5</sub> emissions from managed disposal in landfills__ are reported in accordance with review recommendation DE-5A-2017-0001. In addition to that, for the sake of completeness, __PM<sub>10</sub> and TSP emissions were also reported__.

In the period since 1990 (and previously, to some extent), a number of legal provisions have been issued pertaining to Germany's waste-management sector, and a number of relevant measures have been initiated. These moves have had a strong impact on trends in emissions from waste-landfilling. Relevant developments have included intensified collection of biodegradable waste from households and the commercial sector, intensified collection of other recyclable materials, such as glass, paper/cardboard, metals and plastics; separate collection of packaging and recycling of packaging. In addition, incineration of municipal waste has been expanded, and mechanical biological treatment of residual waste has been introduced. As a result, the amounts of landfilled municipal waste decreased very sharply from 1990 to 2006, and stabilised at a low level since 2006. Today over half of municipal waste produced in Germany is collected separately and gleaned for recyclable materials (separate collection of recyclable materials and biodegradable waste). National statistical data are used (see sub-chapter "activity- data").

In 2004, about 2000 landfills of relevance for this category were in operation in the Federal Republic of Germany.\\
In June 2005, in keeping with new, stricter requirements under the Ordinance on Environmentally Compatible Storage of Waste from Human Settlements (Abfallablagerungsverordnung) and the Landfill Ordinance (Deponieverordnung), nearly half of those landfills were closed. As a result, in 2017 less than 1100 landfills, divided into 5 deposition classes are still in operation.\\
Also, pursuant to regulations in force since June 2005, landfilling of biodegradable waste is no longer permitted - for conformance with pertinent requirements, municipal waste and other biodegradable waste must be pre-treated via thermal or mechanical-biological processes.
All these measures have had strong impact on the formation of NMVOC, PM<sub>2.5</sub>, PM<sub>10</sub> and TSP so that their emissions decreased signifficantly since 1990.

===== Methodology =====
For the estimation of NMVOC, Germany decided against the proposed EF of the EMEP/EEA Guidebook 2019, but instead for the Tier 1 approach of the US-EPA which is also reproduced there (Part B, 5.A, chap. 3.2.2, p. 5; [(EMEPEEA2019>EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019, Copenhagen, 2019)]). According to national experts in the field, the approach of the US-EPA is more likely to produce better data, because the ratio between NMVOC (1.3 %) and CH<sub>4</sub> (98.7 %) in VOC from landfill gas is scientifically sound and assumed to be the very same in Germany. Also, already existing and published data for methane emissions from landfills, derived from the IPCC-FOD Waste Model (see NIR of Germany), can be used. However, with the NIR 2023 some of the emission parameters used to estimate methane emissions have been modified (DOC for food waste, DOC<sub>f</sub> for wood/straw, half-life time for paper and wood/straw) according to the results of research projects initiated for the improvement of the German inventory reporting (Stegmann et al, 2018; S. 172-173, Table 36 [(STEGMANN2018>Stegmann et al, 2018, Überprüfung der methodischen Grundlagen zur Bestimmung der Methanbildung in Deponien, Hamburg/Stuttgart)]). As a result, the methane emissions have changed considerably and thus the related NMVOC-emissions that are reported here.

Emissions for PM<sub>2.5</sub>, PM<sub>10</sub> and TSP, reported under this category, are calculated using the Tier 1 approach of the EMEP/EEA Guidebook 2019, where the emission factors are 0.033 [g/t], 0.219 [g/t] and 0.463 [g/t] (Part B, 5.A, chap. 3.2.2, Table 3-1, p. 5; [(EMEPEEA2019)]).
The EFs are multiplied with the total amount of solid waste (AD) treated in managed above-ground landfills, following the standard equation:

**EM = AD * EF**


==== Activity data ====
Data from 1990 until 2005 are made available for the UBA by the National Statistical Agency by means of a direct data provision (Statistisches Bundesamt, January 2019; [(Statistisches Bundesamt, Data provision by Mail, 14.01.2019; Data are confidential; Wiesbaden)]). Data for 1991+1992 and 1994+1995 are not available and have been interpolated.

From 2006 until today, official statistical data (Statistisches Bundesamt, Fachserie 19, Reihe 1: Abfallentsorgung (Waste management), Table 2.1; [(Statistisches Bundesamt, Fachserie FS 19, Reihe 1: Abfallentsorgung; Wiesbaden; URL: https://www.destatis.de/DE/Publikationen/Thematisch/UmweltstatistischeErhebungen/Abfallwirtschaft/Abfallentsorgung.html)]) are used for the estimation. These data comprise the total amount of solid waste deposited above-ground, meaning, that all mineral wastes (mineral/construction/demolition) are also included. Remaining fractions of these wastes (mineral/construction/demolition) go to underground landfills and therefore do not play a part in dust emissions.

The data are published on a yearly basis with an exception for the actual year of reporting. The activity data for the actual year of reporting are obtained, initially, by carrying the relevant data from the previous year forward, in unchanged form. In the following year, when the actual activity data for the given year becomes available, they replace the data that were carried forward. With regard to emissions from landfills, this procedure has only a very small impact on the total emissions in the relevant current report year.

==== Emission factors ====
See Methodology.

===== Uncertainties =====

The AD from Statistisches Bundesamt usually have an uncertainty of ±3% whereas the uncertainties for the PMs and TSP emission factors, according to the EMEP/EEA Guidebook (Part B, 5.A, chap. 3.2.2, Table 3-1, p. 5), were estimated as:

__Table 1: Uncertainty estimates of PM emission factors__
^ PM<sub>2.5</sub>  |  -99% / +385%  |
^ PM<sub>10</sub>   |  -99% / +379%  |
^ TSP               |  -99% / +377%  |

Due to the fact that for the ratio of NMVOC and CH<sub>4</sub> in VOC from landfill gas no range is given in the EMEP-Guidebook, the overall uncertainty for the emission estimation of NMVOC is estimated by expert judgement to be ± 50%.

===== Recalculations =====

Regular back-calculations are required annually for the previous year, since the waste statistics of the Federal Statistical Office are published with a one-year delay for the data on the quantities and compositions of waste deposited, so that the current reporting year must therefore be estimated. The estimate is replaced in the following year with the then current data.\\


__Table 2: Revised 2021 PM and TSP emissions, in [t]__
|                      ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP      ^
^ current submission   |  1,3078            |  8,6788           |  18,3484  |
^ previous submission  |  1,3628            |  9,0442           |  19,1209  |



<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and for the current year, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>


===== Planned improvements =====
Currently no improvements are planned.
\\
\\
\\
