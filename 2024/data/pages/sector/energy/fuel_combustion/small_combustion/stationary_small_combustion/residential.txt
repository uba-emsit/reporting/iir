====== 1.A.4.b i - Residential: Stationary Combustion ======

===== Short description =====

{{ :sector:energy:fuel_combustion:small_combustion:fireplace.png?nolink&300}}

In source category //1.A.4.b.i. - Other: Residential// emissions from small residential combustion installations are reported.

^  NFR Code                            ^  Method  ^  AD  ^  EF  ^
|  1.A.4.b.i                            |  T2, T3    |  NS  |  CS, D   |
|  {{page>general:Misc:LegendEIT:start}}                        ||||   

\\
^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   ^  Pb   ^  Cd   ^  Hg   ^  As   ^  Cr   ^  Cu   ^  Ni   ^  Se  ^  Zn   ^  PCDD/F  ^  PAHs  ^  HCB  ^  PCBs  ^
^  L/-                                    ^  L/-    ^  L/T             |  -/-             ^  L/T               ^  L/T              ^  L/T  ^  L/-  ^  L/T  |  -/-  |  -/-  ^  L/-  |  -/-  |  -/-  |  -/-  |  -/-  |  NE  |  -/-  ^  L/-     ^  L/T   ^  L/-  |  -/-   |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                                                                                          ||||||||||||||||||||||



===== Methodology =====

==== Activity data ====

For further information on activity data please refer to the [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion |superordinate chapter]] on small stationary combustion.

==== Emission factors ====

For further information on the emission factors applied please refer to the [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion |superordinate chapter]] on small stationary combustion.

__Table 1: Emission factors for domestic combustion installations__
|                       ^  NO<sub>x</sub>  ^  SO<sub>x</sub>  ^  CO     ^  NMVOC  ^  TSP    ^  PM<sub>10</sub>  ^  PM<sub>2.5</sub>  ^  PAH      ^  PCDD/F   ^
|                       ^  [kg/TJ]                                                                                             ||||||^  [mg/TJ]  ^  [µg/TJ]  ^
^ Hard Coal             |  61.1            |  385.5           |  3,422  |  67.0   |  18.5   |  17.6             |  15.7              |  19,215   |      20.8 |
^ Hard Coal Coke        |  40.0            |  458.6           |  5,448  |  11.5   |  16.6   |  15.8             |  14.2              |  32,700   |      45.7 |
^ Hard Coal Briquettes  |  50.4            |  563.5           |  4,875  |  184.1  |  265.4  |  252.8            |  227.3             |  165,858  |      20.2 |
^ Lignite Briquettes    |  87.0            |  421.6           |  2,349  |  158.0  |  79.5   |  76.5             |  68.2              |  148,329  |      24.8 |
^ Natural Wood          |  69.9            |  8.1             |  1,632  |  126.6  |  75.9   |  74.3             |  70.7              |  202,265  |      45.2 |
^ Light Fuel Oil        |  22.1            |  3.3             |  11.8   |  1.5    |  0.9    |  0.9              |  0.9               |  310.0    |       2.2 |
^ Natural Gas           |  20.5            |  0.1             |  13.2   |  0.6    |  0.03   |  0.03             |  0.03              |      3.08 |       2.1 |

TSP and PM emission factors are to a large extend based on measurements without condensed compounds, according to CEN-TS 15883, annex I.

 
===== Trend Discussion for Key Sources =====
The following charts give an overview and assistance for explaining dominant emission trends of selected pollutants.

{{:sector:energy:fuel_combustion:small_combustion:1a4bi_ar.png?700|}}

Annual fluctuations of all fuel types in source category //1.A.4.b.i// depend on heat demand subject to winter temperatures. Between 1990 and 2002 the fuel use changed considerably from coal & lignite to natural gas. The consumption of light heating oil decreased as well. As the activity data for light heating oil is based on the sold amount, it fluctuates due to fuel prices and changing storage amounts. In 2010 and 2013 fuel consumption was particularly high due to the cold winter. The higher fuel consumption in 2014 - 2017 is a result of lower temperatures during the heating period. In 2019 and 2020 the fuel demand increased due to decreasing oil prices, and therefore less in 2021.   

==== Sulfur Oxides & Nitrogen Oxides   - SOx & NOx ====
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_so2.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_so2_2000.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_nox.png?700|}}

SO<sub>2</sub> emissions decrease due to the fuel switch from coal (especially lignite with a high emission factor) to natural gas with a lower emission factor. A further SO<sub>2</sub> reduction from 2008 onwards can be explained by the increasing use of low-sulfur fuel oil. Nowadays  almost exclusively low-sulfur fuel oil is used.
In contrast to SO<sub>2</sub> emissions NO<sub>X</sub> emission trend is less influenced by fuel characteristics but more by combustion conditions. Therefore NO<sub>X</sub> emission values shows lower reduction. During the last years the use of firewood gain influence. 

==== Non-Methane Volatile Organic Compounds & Carbon Monoxide - NMVOC & CO ====
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_nmvoc.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_CO.png?700|}}

Main driver of the NMVOC emission trend is the decreasing lignite consumption. In the residential sector the emission trend is also affected by the increasing use of firewood with high emission factors which levels off the emission reduction. 
The explanation for decreasing carbon monoxide emissions is similar to the trend discussion for SO<sub>2</sub> and NMVOC. Since 1990 the fuel use changed from solid fuels, which causes high CO-emissions, to gaseous fuels, which produce less CO emissions.
 
==== Particulate Matter - PM2.5 & PM10 & TSP ====
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_pm2.5.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_pm10.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_tsp.png?700|}}

The emission trend for PM<sub>2.5</sub>, PM<sub>10</sub>, and TSP are also influenced severely by decreasing coal consumption in small combustion plants, particularly in the period from 1990 to 1994. Since 1995 the emission trend didn't change hardly. Increasing emissions in the last years are caused by the rising wood combustion in residential fire places and stoves.    

==== Persistent Organic Pollutants ====
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_pcddf.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_pah.png?700|}}
{{:sector:energy:fuel_combustion:small_combustion:1a4bi_em_hcb.png?700|}}

The main driver of the POP emission trend are coal and fuelwood combustion. PCDD/F emissions from coal fired furnaces are declining but the effect is retarded by increasing wood consumption. The same influencing variables apply accordingly to the PAH emission trends. The emission trend of HCB shows a high dominance of emissions from wood-burning. Emission factors for HCB are constant from 1990 to 2020. Furthermore, the difference between the EFs for coal and fuelwood is very big. Therefore, the emission trend depends solely on the development of fuelwood consumption. Regarding HCB emissions the inventory is incomplete. This is one of the reasons for the importance of emissions from small combustion plants. In 2010, 2012 and 2013 emissions are particularly high because of the cold winter. 
It's known that in spite of the existing legislation, an unknown quantity of waste wood is illegally burnt. However, it's impossible to ascertain the fuel quantity, since the use of waste wood for heating purposes in small combustion plants is illegal. Therefore all emission factors and emissions refer to the use of untreated wood.

===== Recalculations =====

For the purpose of improving the data quality of the Emission Inventory, National Energy Balances for the years 2003 to 2021 have gone under revisions through fine-tuning of the computational models, consideration of new statistics or re-allocation of activity data, along with other revision mechanisms. These updates led to re-calculations in fuel uses in different sub-categories and in their corresponding emissions. 

 

<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2021**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>
===== Planned improvements =====

There is a running Project on new emission factors for small combustion plants using updated data from the chimney sweepers and new measurement data.