====== 1.A.3.b vi-vii - Road Transport: Automobile Tyre and Brake Wear and Road Abrasion ======

This overview chapter provides information on emissions from automobile tyre and brake wear & road abrasion are reported reported in NFR sub-categories 1.A.3.b vi and 1.A.3.b vii.
These sub-categories are important sources for a) particle emissions and b) emissions of heavy metals, POPs etc. included in these particles.

^  NFR-Code     ^  Name of Category                                                                                                                                      ^
|  1.A.3.b vi   | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:tyre_and_brake_wear| Automobile Tyre and Brake Wear ]]  |
|  1.A.3.b vii  | [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:road_abrasion| Automobile Road Abrasion ]]              |

===== Methodology =====

====Activity data ====

Specific mileage data for all different types of road vehicles are generated within TREMOD (Knörr et al., 2023a) [(KNOERR2023a)]. 
The following table provides an overview of annual mileages. 

__Table 1: Mileage data for road vehicles 1990-2022, in 10<sup>6</sup> kilometers__ 
|                                ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^  2022    ^
^ Passenger Cars                 |  492,280 |  535,524 |  565,345 |  580,076 |  596,532 |  630,995 |  638,156 |  643,711 |  643,198 |  645,829 |  548,971 |  546,127 |  564,042 |
^ Light Duty Vehicles            |   14,259 |   23,294 |   31,541 |   35,938 |   39,226 |   45,330 |   47,457 |   49,698 |   51,918 |   53,657 |   50,983 |   54,572 |   56,132 |
^ Heavy Duty Vehicles            |   40,827 |   54,025 |   62,478 |   60,932 |   61,970 |   66,463 |   68,460 |   69,264 |   70,650 |   70,052 |   67,072 |   69,046 |   68,516 |
| //thereof: Lorries & Trucks//  |   36,657 |   50,109 |   58,440 |   56,788 |   57,728 |   61,548 |   63,061 |   64,704 |   66,084 |   65,435 |   64,128 |   65,876 |   64,744 |
| //thereof: Buses//             |    4,170 |    3,916 |    4,038 |    4,144 |    4,243 |    4,915 |    5,399 |    4,560 |    4,566 |    4,617 |    2,944 |    3,171 |    3,773 |
^ Two-wheelers                   |   15,734 |   12,303 |   15,161 |   15,621 |   15,298 |   14,508 |   14,519 |   14,503 |   14,625 |   14,842 |   14,985 |   13,407 |   14,492 |
| //thereof: Mopeds//            |    5,917 |    3,830 |    4,047 |    4,191 |    4,990 |    4,870 |    4,831 |    4,730 |    4,849 |    4,922 |    5,039 |    4,479 |    4,856 |
| //thereof: Motorcycles//       |    9,817 |    8,473 |   11,113 |   11,429 |   10,308 |    9,638 |    9,687 |    9,773 |    9,776 |    9,920 |    9,946 |    8,928 |    9,636 |
| **TOTAL MILEAGE**              ^  563,099 ^  625,145 ^  674,524 ^  692,567 ^  713,026 ^  757,295 ^  768,591 ^  777,176 ^  780,391 ^  784,380 ^  682,010 ^  683,152 ^  703,182 ^

The following chart illustrates the increase in annual mileage of electric road vehicles in 10<sup>6</sup> kilometers. Despite the exponential growth, only about 1 per cent of annual mileage was "electric" in 2021 (7,000,000,000 of 682,831,000,000 km).
{{ :sector:energy:fuel_combustion:transport:road_transport:1a3b_mileage_electric.png?700 | Annual mileage of electric road vehicles }}

===== Discussion of emission trends =====

<WRAP center round info 80%>
Please see sub-category chapters [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:tyre_and_brake_wear| 1.A.3.b vi - Automobile Tyre and Brake Wear ]] and [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:road_abrasion| 1.A.3.b vii - Automobile Road Abrasion ]].
</WRAP>



===== Recalculations =====

Compared to last year's submission, mileage data has been revised widely due to the availability of additional data sources.
However, total mileage changed only marginally.


__Table 2: Revised total annual mileage data, in 10<sup>6</sup> kilometers___
|                      ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^
^ current submission   |  563,099 |  625,145 |  674,524 |  692,567 |  713,026 |  757,295 |  768,591 |  777,176 |  780,391 |  784,380 |  682,010 |  683,152 |
^ previous submission  |  563,099 |  625,145 |  674,524 |  692,554 |  712,991 |  757,244 |  768,541 |  777,128 |  780,345 |  784,334 |  681,965 |  682,831 |
^ absolute change      |     0.00 |     0.00 |     0.00 |     12.8 |     35.3 |     51.2 |     50.8 |     48.3 |     46.4 |     46.6 |     44.7 |      322 |
^ relative change      |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.01% |    0.01% |    0.01% |    0.01% |    0.01% |    0.01% |    0.05% |

----


[(KNOERR2023a> Knörr et al. (2023a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg & Berlin, 2023. )]