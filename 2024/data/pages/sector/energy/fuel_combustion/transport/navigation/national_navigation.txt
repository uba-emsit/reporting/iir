====== 1.A.3.d ii - National Navigation ======

===== Short description =====

Under category //1.A.3.d ii - National Navigation// emissions from national navigation (both inland and maritime) are reported. 

^  Category Code                          ^  Method      ^  AD     ^  EF        ^
|  1.A.3.d ii                             |  T1, T2, T3  |  NS, M  |  CS, D, M  |
| covering emissions in:                                                     ||||
| **Domestic maritime navigation**        |  T1, T2, T3  |  NS, M  |  CS, D, M  |
| **Domestic inland navigation**          |  T1, T2, T3  |  NS, M  |  CS, D, M  |
|  {{page>general:Misc:LegendEIT:start}}                                     ||||

----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   ^  Pb   ^  Cd   ^  Hg   ^  As   ^  Cr   ^  Cu   ^  Ni   ^  Se   ^  Zn   ^  PCDD/F  ^  B(a)P  ^  B(b)F  ^  B(k)F  ^  I(x)P  ^  PAH1-4  ^  HCB  ^  PCBs  ^
^  L/-                                    |  -/-    |  -/-             |  -/-             ^  L/T               ^  -/T              |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-     |  -/-    |  -/-    |  -/-    |  -/-    |  -/-     |  -/-  |  -/-   |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                                                                                                                                 ||||||||||||||||||||||||||

===== Methodology =====

==== Activity data ====

As described for the over-all sector 1.A.3.d and all other navigational activities in the superordinate chapter, specific fuel consumption data for NFR 1.A.3.d ii is included in the primary fuel deliveries data provided in NEB lines 6 ('International Maritime Bunkers') and 64 ('Coastal and Inland Navigation') [(AGEB2023)]. 

Here, the annual fuel consumption for domestic //maritime// navigation are modelled within [(DEICHNIK2023)] based on AIS data and deduced from NEB lines 6 and 64 respectively, depending on whether or not a certain ship is registered by the International Maritime Organization (IMO). Here, fuels consumed by large, IMO-registered and sea-going ships and vessels are included in NEB line 6 whereas fuels consumed by smaller ships without IMO-registration are included in NEB line 64. After these deductions, the amounts of fuels remaining in NEB 64 are allocated to domestic //inland// navigation.

The small amounts of LNG used almost entirely in ferries are not yet included in the NEB but are estimated directly in the BSH model.

__Table 1: Annual over-all fuel consumption for domestic navigation, in terajoule__
|                   ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^  2022   ^
^ Diesel Oil        |  37,199 |  30,389 |  19,231 |  26,241 |  24,060 |  24,874 |  23,863 |  23,866 |  21,897 |  14,660 |  14,029 |  12,003 |   9,819 |
^ Light Fuel Oil    |         |         |         |         |         |         |         |         |   2,361 |   9,497 |   8,329 |   8,475 |   7,879 |
^ Heavy fuel oil    |   3,103 |   2,186 |   2,382 |   2,054 |   1,810 |     108 |    37.0 |    81.1 |     262 |     394 |     378 |     392 |     237 |
^ LNG               |         |         |         |         |         |    22.0 |    64.4 |    58.8 |     197 |     153 |     276 |     293 |     513 |
^ Gasoline          |     272 |     272 |     272 |     274 |     261 |     265 |     265 |     266 |     262 |     265 |     269 |     273 |     275 |
^ Biogasoline       |         |         |         |    1.90 |    10.1 |    10.7 |    10.8 |    10.8 |    11.3 |    10.9 |    12.0 |    12.9 |    12.8 |
^ LPG               |    7.00 |    7.00 |    7.00 |    7.02 |    7.08 |    7.10 |    7.25 |    7.06 |    7.13 |    7.16 |    7.40 |    7.51 |    7.58 |
| **Ʃ 1.A.3.d ii**  ^  40,582 ^  32,854 ^  21,892 ^  28,577 ^  26,149 ^  25,286 ^  24,248 ^ 24,290  ^ 24,997  ^ 24,988  ^ 23,299  ^ 21,456  ^  18,744 ^

__Table 2: Specific fuel consumption data for domestic maritime and inland navigation, in terajoule__
|                                   ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^  2022   ^
| **NATIONAL MARITIME NAVIGATION**                                                                                                                       ||||||||||||||
^ Diesel Oil                        |   9,484 |   6,828 |   7,367 |   6,399 |   5,690 |   8,980 |   9,335 |   8,960 |   7,084 |         |         |         |         |
^ Light Fuel Oil                    |         |         |         |         |         |         |         |         |   2,361 |   9,497 |   8,329 |   8,475 |   7,879 |
^ Heavy fuel oil                    |   3,103 |   2,186 |   2,382 |   2,054 |   1,810 |     108 |    37.0 |    81.1 |     262 |     394 |     378 |     392 |     237 |
^ LNG                               |         |         |         |         |         |    22.0 |    64.4 |    58.8 |     197 |     153 |     276 |     293 |     513 |
| **NATIONAL INLAND NAVIGATION**                                                                                                                         ||||||||||||||
^ Diesel Oil                        |  27,716 |  23,562 |  11,864 |  19,842 |  18,370 |  15,894 |  14,529 |  14,907 |  14,813 |  14,660 |  14,029 |  12,003 |   9,819 |
^ Gasoline                          |     272 |     272 |     272 |     274 |     261 |     265 |     265 |     266 |     262 |     265 |     269 |     273 |     275 |
^ Biogasoline                       |         |         |         |    1.90 |    10.1 |    10.7 |    10.8 |    10.8 |    11.3 |    10.9 |    12.0 |    12.9 |    12.8 |
^ LPG                               |    7.00 |    7.00 |    7.00 |    7.02 |    7.08 |    7.10 |    7.25 |    7.06 |    7.13 |    7.16 |    7.40 |    7.51 |    7.58 |
| **Ʃ 1.A.3.d ii**                  ^  40,582 ^  32,854 ^  21,892 ^  28,577 ^  26,149 ^  25,286 ^  24,248 ^  24,290 ^  24,997 ^  24,988 ^  23,299 ^  21,456 ^  18,744 ^

{{ :sector:energy:fuel_combustion:transport:navigation:1a3dii_ad.png?702 }}

{{ :sector:energy:fuel_combustion:transport:navigation:1a3dii_ad_lng.png?702 }}

==== Emission factors====

The emission factors applied for **national maritime navigation** are derived from different sources and therefore are of very different quality.

For the main pollutants, country-specific implied values are used, that are based on tier3 EF included in the BSH model [(DEICHNIK2023)] which mainly relate on values from the EMEP/EEA guidebook 2019 [(EMEPEEA2019)]. These modelled IEFs take into account the ship specific information derived from AIS data as well as the mix of fuel-qualities applied depending on the type of ship and the current state of activity.

Here, for **sulphur dioxide** and **particulate matter**, annual values are available representing the impact of fuel sulphur legislation.
In addition, regarding <sub>2</sub>, the increasing operation of so-called scrubbers in order to fullfil emission limits especially within SECA areas is reflected for heavy fuel oil.


__Table 3: Country-specific emission factors applied for fuels used in domestic maritime navigation, in [kg/TJ]__
|                                              ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^ 2015   ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^  2022  ^
| **DIESEL OIL & LIGHT FUEL OIL**<sup>1</sup>                                                                                                          ||||||||||||||
^ NH<sub>3</sub>                               |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |   0.32 |
^ NMVOC                                        |   48.5 |   48.4 |   48.4 |   48.4 |   48.4 |   44.4 |   43.9 |   44.2 |   43.8 |   44.0 |   44.0 |   42.1 |   43.0 |
^ NO<sub>x</sub>                               |  1,101 |  1,101 |  1,101 |  1,101 |  1,101 |  1,184 |  1,183 |  1,189 |  1,200 |  1,199 |  1,169 |  1,194 |  1,181 |
^ SO<sub>2</sub>                               |    466 |    419 |    233 |    186 |   69.8 |   37.2 |   37.2 |   37.2 |   37.2 |   37.2 |   37.2 |   37.2 |   37.2 |
^ BC<sup>2</sup>                               |    110 |   99.1 |   55.0 |   44.0 |   16.5 |   17.4 |   17.7 |   17.7 |   17.3 |   17.5 |   16.8 |   16.9 |   17.1 |
^ PM<sub>2.5</sub>                             |    354 |    320 |    177 |    142 |   53.3 |   56.2 |   57.1 |   57.1 |   55.9 |   56.5 |   54.2 |   54.6 |   55.2 |
^ PM<sub>10</sub>                              |    378 |    342 |    190 |    152 |   57.1 |   60.1 |   61.1 |   61.1 |   59.8 |   60.4 |   58.0 |   58.5 |   59.0 |
^ TSP<sup>3</sup>                              |    378 |    342 |    190 |    152 |   57.1 |   60.1 |   61.1 |   61.1 |   59.8 |   60.4 |   58.0 |   58.5 |   59.0 |
^ CO                                           |    128 |    128 |    128 |    128 |    128 |    140 |    142 |    141 |    139 |    140 |    138 |    140 |    140 |
| **HEAVY FUEL OIL**                                                                                                                                   ||||||||||||||
^ NH<sub>3</sub>                               |   0.33 |   0.33 |   0.33 |   0.33 |   0.33 |   0.34 |   0.34 |   0.34 |   0.34 |   0.34 |   0.34 |   0.34 |   0.34 |
^ NMVOC                                        |   43.0 |   42.8 |   42.9 |   42.9 |   42.8 |   26.1 |   30.2 |   33.7 |   32.5 |   32.7 |   37.4 |   37.5 |   40.7 |
^ NO<sub>x</sub>                               |  1,368 |  1,368 |  1,368 |  1,368 |  1,368 |  1,487 |  1,440 |  1,479 |  1,480 |  1,507 |  1,509 |  1,526 |  1,556 |
^ SO<sub>x</sub>                               |  1,319 |  1,332 |  1,323 |  1,336 |    496 |   48.6 |   49.2 |   48.1 |   45.9 |   46.5 |   48.1 |   47.0 |   47.2 |
^ BC<sup>2</sup>                               |   70.8 |   71.2 |   70.8 |   71.6 |   26.5 |   14.2 |   18.0 |   20.1 |   19.1 |   18.9 |   21.4 |   21.3 |   23.1 |
^ PM<sub>2.5</sub>                             |    590 |    594 |    590 |    596 |    221 |    118 |    150 |    168 |    159 |    158 |    179 |    178 |    192 |
^ PM<sub>10</sub>                              |    649 |    653 |    649 |    656 |    243 |    130 |    165 |    184 |    175 |    173 |    197 |    195 |    211 |
^ TSP<sup>3</sup>                              |    649 |    653 |    649 |    656 |    243 |    130 |    165 |    184 |    175 |    173 |    197 |    195 |    211 |
^ CO                                           |    179 |    179 |    179 |    179 |    179 |    144 |    162 |    157 |    156 |    150 |    151 |    147 |    143 |
| **LIQUEFIED NATURAL GAS (LNG)**                                                                                                                      ||||||||||||||
^ NH<sub>3</sub>                               |  NA                                    |||||  NE                                                            ||||||||
^ NMVOC                                        |  NA                                    |||||   60.2 |   60.2 |   60.2 |   60.2 |   60.2 |   60.2 |   60.2 |   60.2 |
^ NO<sub>x</sub>                               |  NA                                    |||||  NE                                                            ||||||||
^ SO<sub>x</sub>                               |  NA                                    |||||  NE                                                            ||||||||
^ BC<sup>2</sup>                               |  NA                                    |||||  NE                                                            ||||||||
^ PM<sub>2.5</sub>,PM<sub>10</sub>,TSP         |  NA                                    |||||   1.51 |   1.51 |   1.51 |   1.51 |   1.51 |   1.51 |   1.51 |   1.51 |
^ CO                                           |  NA                                    |||||    157 |    157 |    157 |    157 |    157 |    156 |    154 |    154 |
<sup>1</sup> identical EF applied for diesel oil and light fuel oil \\
<sup>2</sup> estimated from f-BCs as provided in [(EMEPEEA2019)]: f-BC (HFO) = 0.12, f-BC (MDO/MGO) = 0.31 as provided in [(EMEPEEA2019)], chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii Navigation, Tables 3-1 & 3-2 \\
<sup>3</sup> ratio of PM<sub>2.5</sub> : PM<sub>10</sub> : TSP derived from the tier1 default EF as provided in [(EMEPEEA2019)], chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii Navigation, Tables 3-1 & 3-2

<WRAP center round info 100%>
For the country-specific emission factors applied for particulate matter, no clear indication is available, whether or not condensables are included.
</WRAP>
 
For main pollutants and particulate matter from **national inland navigation**, modelled emission factors are available from TREMOD (Knörr et al. (2023a)) [(KNOERR2023a)]. 
Here, for //SO<sub>2</sub>,// and //PM//, annual values reflect the impact of fuel-sulphur legislation.

__Table 4: Country-specific emission factors for diesel fuels used in domestic inland navigation, in [kg/TJ]__
|                 ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^ 2015    ^ 2016    ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^  2022   ^
| **DIESEL OIL**                                                                                                                       ||||||||||||||
^ NH<sub>3</sub>  |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |    0.23 |
^ NMVOC           |    96.4 |    87.9 |    77.7 |    72.3 |    66.6 |    61.2 |    60.3 |    59.4 |    58.5 |    58.0 |    57.1 |    56.4 |    55.4 |
^ NO<sub>x</sub>  |   1,327 |   1,331 |   1,336 |   1,289 |   1,231 |   1,175 |   1,164 |   1,152 |   1,141 |   1,135 |   1,124 |   1,115 | 1,093   |
^ SO<sub>x</sub>  |    85.2 |    60.5 |    60.5 |    60.5 |    60.5 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |
^ BC<sup>1</sup>  |    17.5 |    16.0 |    14.1 |    11.8 |    9.17 |    8.14 |    7.99 |    7.83 |    7.67 |    7.58 |    7.44 |    7.31 |    7.12 |
^ PM<sup>2</sup>  |    56.5 |    51.7 |    45.6 |    38.1 |    29.6 |    26.3 |    25.8 |    25.2 |    24.7 |    24.4 |    24.0 |    23.6 |    23.0 |
^ CO              |     417 |     387 |     337 |     299 |     256 |     229 |     225 |     221 |     216 |     213 |     209 |     206 |     203 |
| **GASOLINE**                                                                                                                         ||||||||||||||
^ NH<sub>3</sub>  |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |    0.11 |
^ NMVOC (exh.)    |     952 |    1036 |    1269 |    1373 |    1212 |     895 |     849 |     806 |     770 |     740 |     717 |     701 |     690 |
^ NMVOC (evap.)   |    28.8 |    55.3 |     131 |     164 |     202 |     185 |     183 |     181 |     179 |     177 |     176 |     176 |     176 |
^ NO<sub>x</sub>  |     383 |     375 |     353 |     345 |     337 |     341 |     325 |     299 |     276 |     256 |     237 |     222 |     208 |
^ SO<sub>x</sub>  |    10.1 |    8.27 |    3.22 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |    0.37 |
^ BC<sup>1</sup>  |    2.33 |    2.33 |    2.33 |    2.33 |    2.32 |    2.32 |    2.32 |    2.32 |    2.32 |    2.32 |    2.32 |    2.32 |    2.32 |
^ PM<sup>2</sup>  |    7.50 |    7.50 |    7.50 |    7.50 |    7.50 |    7.49 |    7.49 |    7.49 |    7.49 |    7.49 |    7.49 |    7.49 |    7.49 |
^ CO              |  30,204 |  30,817 |  32,595 |  33,248 |  26,208 |  18,519 |  17,352 |  16,229 |  15,256 |  14,476 |  13,858 |  13,396 |  13,036 |
| **LPG**                                                                                                                              ||||||||||||||
^ NH<sub>3</sub>  |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |    0.21 |
^ NMVOC           |     147 |     147 |     145 |     145 |     145 |     145 |     145 |     145 |     145 |     144 |     141 |     134 |     126 |
^ NO<sub>x</sub>  |   1,346 |   1,342 |   1,325 |   1,325 |   1,325 |   1,325 |   1,325 |   1,325 |   1,325 |   1,316 |   1,284 |   1,225 |   1,144 |
^ SO<sub>x</sub>  |    0.42 |    0.42 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |    0.41 |
^ BC<sup>1</sup>  |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |    0.26 |
^ PM<sup>2</sup>  |    0.85 |    0.85 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |    0.84 |
^ CO              |     114 |     114 |     112 |     112 |     112 |     112 |     112 |     112 |     112 |     112 |     112 |     112 |     112 |
<sup>1</sup> calculated from f-BC as provided in [(EMEPEEA2019)], Chapter: 1.A.3.d.i, 1.A.3.d.ii, 1.A.4.c.iii, Table 3-2: f-BC (MDO/MGO) = 0.31 \\
<sup>2</sup> EF(PM<sub>2.5</sub>) also applied for PM<sub>10</sub> and TSP (assumption: > 99% of TSP from diesel oil combustion consists of PM<sub>2.5</sub>)

<WRAP center round info 100%>
With respect to the emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. (( During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions. ))
</WRAP>

<WRAP center round info 100%>
For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.
</WRAP>

===== Discussion of emission trends =====

__Table 5: Outcome of Key Category Analysis__
|  for: ^  NO<sub>x</sub>  ^  PM<sub>10</sub>  ^  PM<sub>2.5</sub>  ^
|   by: |  L/-             |  -/T              |  L/T               |

For **ammonia**, **NMVOC**, and **nitrogen oxides** as well as **carbon monoxide**, emission trends more or less represent the trend in over-all fuel consumption. 

{{ :sector:energy:fuel_combustion:transport:navigation:em_1a3dii_nh3.png?702 | Annual ammonia emissions }}

{{ :sector:energy:fuel_combustion:transport:navigation:em_1a3dii_nmvoc.png?702 | Annual NMVOC emissions }}

{{ :sector:energy:fuel_combustion:transport:navigation:em_1a3dii_nox.png?702 | Annual nitrogen oxides emissions }}

Nonetheless, for these pollutants, annual emission factors from BSH [(DEICHNIK2023)] and TREMOD [(KNOERR2023a)] have been applied for national //maritime// and //inland// navigation, respectively, reflecting the technical development of the German inland navigation fleet.

Here, the trends in **sulphur dioxide** and **particulate matter** emissions reflect the impact of ongoing fuel-sulphur legislation especially in maritime navigation.

{{ :sector:energy:fuel_combustion:transport:navigation:1a3di_em_so2.png?702 | Annual sulphur oxides emissions }}
{{ :sector:energy:fuel_combustion:transport:navigation:em_1a3dii_pm.png?702 | Annual particulate matter emissions }}

===== Recalculations =====

Compared to the previous submisison, **activity data** were re-estimated in accordance with the revised National Energy Balances (NEB) 2003 to 2021.
As part of this revision, gasoline and LPG applied in inland navigation have been taken into account for the first time within the National Energy Balance.

As the NEB does not provide data on gasoline and LPG before 2003, amounts for 1990 to 2002 were extrapolatded backwards for gap-filling.
In addition, the amounts of //bio//gasoline corresponding to the gasoline inland deliveries was estimated within TREMOD.

Furthermore, but with no effects on emission estimates, diesel oil and light fuel oil used in national maritime navigation are reported seperately now.

__Table 6: Revised fuel consumption data for national maritime and inland navigation, in terajoules__
^                                   ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^
^ **NATIONAL MARITIME NAVIGATION**                                                                                                              |||||||||||||
| **DIESEL OIL**                                                                                                                                |||||||||||||
^ current submission                |   9,484 |   6,828 |   7,367 |   6,399 |   5,690 |   8,980 |   9,335 |   8,960 |   7,084 |       0 |       0 |       0 |
^ previous submission               |   9,484 |   6,828 |   7,367 |   6,399 |   5,690 |   8,980 |   9,335 |   8,960 |   9,445 |   9,497 |   8,339 |   8,475 |
^ absolute change                   |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |  -2,361 |  -9,497 |  -8,339 |  -8,475 |
^ relative change                   |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |  -25.0% |   -100% |   -100% |   -100% |
| **LIGHT FUEL OIL**                                                                                                                            |||||||||||||
^ current submission                |         |         |         |         |         |         |         |         |   2,361 |   9,497 |   8,329 |   8,475 |
^ previous submission               |         |         |         |         |         |         |         |         |  IE     |  IE     |  IE     |  IE     |
^ absolute change                   |         |         |         |         |         |         |         |         |   2,361 |   9,497 |   8,329 |   8,475 |
| **HEAVY FUEL OIL**                                                                                                                            |||||||||||||
^ current submission                |   3,103 |   2,186 |   2,382 |   2,054 |   1,810 |     108 |    37.0 |    81.1 |     262 |     394 |     378 |     392 |
^ previous submission               |   3,103 |   2,186 |   2,382 |   2,054 |   1,810 |     108 |    37.0 |    81.1 |     262 |     394 |     368 |     392 |
^ absolute change                   |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    9.73 |    0.00 |
^ relative change                   |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   2.64% |   0.00% |
| **LNG**                           |         |         |         |         |         |         |         |         |         |         |         |         |
^ current submission                |         |         |         |         |         |    22.0 |    64.4 |    58.8 |     197 |     153 |     276 |     293 |
^ previous submission               |         |         |         |         |         |    22.0 |    64.4 |    58.8 |     197 |     153 |     276 |     293 |
^ absolute change                   |         |         |         |         |         |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |
^ relative change                   |         |         |         |         |         |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |
|                                                                                                                                               |||||||||||||
^ **NATIONAL INLAND NAVIGATION**                                                                                                                |||||||||||||
| **DIESEL OIL**                                                                                                                                |||||||||||||
^ current submission                |  27,716 |  23,562 |  11,864 |  19,842 |  18,370 |  15,894 |  14,529 |  14,907 |  14,813 |  14,660 |  14,029 |  12,003 |
^ previous submission               |  27,716 |  23,562 |  11,864 |  12,851 |  11,182 |  13,321 |  11,131 |  10,150 |  10,619 |  11,259 |  10,076 |  10,481 |
^ absolute change                   |    0.00 |    0.00 |    0.00 |   6,991 |   7,188 |   2,573 |   3,398 |   4,756 |   4,194 |   3,401 |   3,952 |   1,522 |
^ relative change                   |   0.00% |   0.00% |   0.00% |   54.4% |   64.3% |   19.3% |   30.5% |   46.9% |   39.5% |   30.2% |   39.2% |   14.5% |
| **GASOLINE**                                                                                                                                  |||||||||||||
^ current submission                |     272 |     272 |     272 |     274 |     261 |     265 |     265 |     266 |     262 |     265 |     269 |     273 |
^ previous submission               |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |
^ absolute change                   |     272 |     272 |     272 |     274 |     261 |     265 |     265 |     266 |     262 |     265 |     269 |     273 |
| **BIOGASOLINE**                                                                                                                               |||||||||||||
^ current submission                |         |         |         |    1.90 |    10.1 |    10.7 |    10.8 |    10.8 |    11.3 |    10.9 |    12.0 |    12.9 |
^ previous submission               |         |         |         |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |
^ absolute change                   |         |         |         |    1.90 |    10.1 |    10.7 |    10.8 |    10.8 |    11.3 |    10.9 |    12.0 |    12.9 |
| **LPG**                                                                                                                                       |||||||||||||
^ current submission                |    7.00 |    7.00 |    7.00 |    7.02 |    7.08 |    7.10 |    7.25 |    7.06 |    7.13 |    7.16 |    7.40 |    7.51 |
^ previous submission               |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |  NE     |
^ absolute change                   |    7.00 |    7.00 |    7.00 |    7.02 |    7.08 |    7.10 |    7.25 |    7.06 |    7.13 |    7.16 |    7.40 |    7.51 |

__Table 7: Revised over-all fuel consumption data for national navigation, in terajoules__
|                      ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^
^ current submission   |  40,582 |  32,854 |  21,892 |  28,577 |  26,149 |  25,286 |  24,248 |  24,290 |  24,997 |  24,988 |  23,299 |  21,456 |
^ previous submission  |  40,303 |  32,575 |  21,613 |  21,304 |  18,682 |  22,431 |  20,567 |  19,250 |  20,524 |  21,303 |  19,060 |  19,640 |
^ absolute change      |     279 |     279 |     279 |   7,273 |   7,467 |   2,855 |   3,681 |   5,040 |   4,474 |   3,685 |   4,240 |   1,815 |
^ relative change      |   0.69% |   0.86% |   1.29% |   34.1% |   40.0% |   12.7% |   17.9% |   26.2% |   21.8% |   17.3% |   22.2% |   9.24% |


<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2021**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Uncertainties =====

Uncertainty estimates for **activity data** of mobile sources derive from research project FKZ 360 16 023: "Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland" by Knörr et al. (2009) [(KNOERR2009)].

===== Planned improvements ===== 

Besides the **routine revisions of the models** used for maritime and inland navigation, no specific improvements are scheduled.

[(AGEB2023>AGEB, 2023: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; 
https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2030/?wpv-jahresbereich-bilanz=2021-2030, (Aufruf: 12.12.2023), Köln & Berlin, 2023)]
[(BAFA2023>BAFA, 2023: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik 
Deutschland; https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2022_12.xlsx?__blob=publicationFile&v=4, Eschborn, 2023.)]
[(KNOERR2023a> Knörr et al. (2023a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des 
motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg [u.a.]: Ifeu Institut für Energie- und Umweltforschung Heidelberg 
GmbH, Heidelberg & Berlin, 2023.)]
[(DEICHNIK2023> Deichnik (2023): Aktualisierung und Revision des Modells zur Berechnung der spezifischen Verbräuche und Emissionen des von Deutschland ausgehenden Seeverkehrs. from Bundesamts für Seeschifffahrt und Hydrographie (BSH - Federal Maritime and Hydrographic Agency); Hamburg, 2023.)]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook – 2019, Copenhagen, 2019.)]
[(KNOERR2009>Knörr et al. (2009): Knörr, W., Heldstab, J., & Kasser, F.: Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland; final report; URL: https://www.umweltbundesamt.de/sites/default/files/medien/461/publikationen/3937.pdf, FKZ 360 16 023, Heidelberg & Zürich, 2009.)]