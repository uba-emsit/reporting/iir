====== 1.A.2.c - Fuel Combustion Activities in Industries and Construction: Chemicals ======


Energy related emissions from power plants and boiler systems are reported in [[sector:energy:fuel_combustion:industry:other:start |NFR 1.A.2.g viii: Other]] whereas process related emissions are reported in NFR 2.B - Chemical Industry.