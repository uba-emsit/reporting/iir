====== 1.A.2 - Fuel Combustion Activities in Industries and Construction (OVERVIEW) ======

===== Short description =====

In sub-sector **1.A.2 - Fuel Combustion Activities in Industries and Construction** emissions from both stationary and mobile fuel combustion activities in industries and construction are reported within the following sub-categories:

^ NFR-Code ^ Name of Category ^
| **Stationary Combustion in Manufacturing Industries and Construction** ||
| 1.A.2.a | [[sector:energy:fuel_combustion:industry:iron_and_steel|Iron and Steel]] |
| 1.A.2.b | [[sector:energy:fuel_combustion:industry:non-ferrous_metals|Non-ferrous Metals]]  |
| 1.A.2.c | [[sector:energy:fuel_combustion:industry:chemicals|Chemicals]]|
| 1.A.2.d | [[sector:energy:fuel_combustion:industry:pulp_paper_and_print|Pulp, Paper and Print]] |
| 1.A.2.e | [[sector:energy:fuel_combustion:industry:food_processing_beverages_and_tobacco|Food Processing, Beverages and Tobacco]] |
| 1.A.2.f | [[sector:energy:fuel_combustion:industry:non-metallic_minerals|Non-Metallic Minerals]] |
| 1.A.2.g viii | [[sector:energy:fuel_combustion:industry:other:start | Other]] |
| **Mobile Combustion in Manufacturing Industries and Construction** ||
| 1.A.2.g vii | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction|Mobile Combustion in Manufacturing Industries & Construction]] |


The German emission inventory is generally based on the emission behaviour of the plants. Therefore it's necessary to distinguish between process-combustion on the one hand and industrial power plants and boiler systems on the other hand. 
The emission behaviour of power plants and boiler systems of the various industrial sectors is similar. That's why all the emissions from these type of plants were reported under source category 1.A.2.f.i other. 
Whereas the emission behaviour of the different process-combustion systems is individual. A distinction between fuel and process related emissions is usually not possible. Therefore all emissions emissions from process-combustion systems are reported in the corresponding source category of the industry sector NFR 2.