====== 1.B - Fugitive Emissions from fossil fuels ======

During all stages of fuel production and use, from extraction of fossil fuels to their final use, fuel components can escape or be released as fugitive emissions.

While NMVOC, TSP and SO<sub>x</sub> are the most important emissions within the source category //solid fuels//, fugitive emissions of oil and natural gas include substantial amounts of NMVOC and SO<sub>x</sub>.

1.B - "Fugitive emission from fuels" consist of following sub-categories:

^  NFR-Code     ^  Name of category                                             ^
|  **1.B.1**    | [[sector:energy:fugitive:solid_fuels:start|Solid Fuels]]      |
|  **1.B.2.a**  | [[sector:energy:fugitive:oil:start|Oil]]                      |
|  **1.B.2.b**  | [[sector:energy:fugitive:gas:start|Gas]]                      |
|  **1.B.2.c**  | [[sector:energy:fugitive:flaring:start|Venting and Flaring]]  |
|  **1.B.3**    | [[sector:energy:fugitive:geothermal:start|Geothermal Energy]]      |




===== Trends in emissions =====


| {{ :sector:energy:fugitive:co_2024.jpg?600 }}   | {{ :sector:energy:fugitive:nmvoc_2024.jpg?600 }}  |
|  Carbon Monoxide                                |  NMVOC                                            |
|                                                 |                                                   |
| {{ :sector:energy:fugitive:sox_2024.jpg?600 }}  | {{ :sector:energy:fugitive:tsp_2024.jpg?600 }}    |
|  SOx                                            |  Particulate Matter                               |



**Sulphur Dioxide** emissions occur during the production of hard-coal coke. The value of the year 1990 is partly based on the GDR’s emission report, chapter “Produktion” (=production) which has no clear differentiation between mining, transformation and handling of coal. The total emission as reported in the emission report is allocated in the NFR categories 1.B.1 and 2. The split factor is based on estimation of experts.

The apparently steep decline from 2007 to 2008 is the result of a research project in 2010, where new emission factors were determined for coke production for the years 2008.
In sub-category 1.B.2, one main driver of shrinking SO<sub>2</sub> emission is the decreasing amount of flared natural gas. The shrinking emissions are also attributed to the declining emissions from desulphurisation, that are a result of the implementation of modern technology.


**Particulate matter** emissions occur during the transformation of lignite and hard coal. The very steep decline of the emissions in the early 1990s is due to the shrinking production of lignite briquettes (almost 90% in the first five years). The value of the year 1990 is partly based on the GDR’s emission report, chapter “Produktion” (=production) which has no clear differentiation between mining, transformation and handling of lignite. The total emission as reported in the emission report is allocated in the NFR categories 1.B.1 and 2. The split factor is based on estimation of experts.

**NMVOC** emission occur during the production of hard-coal coke. The shrinking emissions are mainly attributed to the hard-coal coke production and the decommissioning of outdated plants. The main sources of NMVOC emissions from total petrol distribution (1.B.2.a.v) were fugitive emissions from handling and transfer (filling/unloading) and container losses (tank breathing). These emissions have decreased by round about 65 % since 1990. The decrease in fugitive emissions during this period is the result of implementation of the Technical Instructions on Air Quality Control (TA-Luft 2002) and of the 20th and 21st Ordinance on the Execution of the Federal Immission Control Act (20. and 21. BImSchV), involving introduction of vapour recovery systems. It is also the result of reduced petrol consumption. 

Currently, about 13 million m<sup>3</sup> of petrol fuels are transported in Germany via railway tank cars. This transport volume entails a maximum of 300,000 handling processes (loading and unloading). Some 5,000 to 6,000 railway tank cars for transport of petrol are in service. Transfer/handling (filling/unloading) and tank losses result in emissions of only 1.4 kt VOC per year. The emissions situation points to the high technical standards that have been attained in railway tank cars and pertinent handling facilities. On the whole, oil consumption is expected to stagnate or decrease. As a result, numbers of oil storage facilities can be expected to decrease as well.

**Carbon monoxide** emissions occur during the production of coke. A trend-reversing issue was the decommissioning of outdated plants in the 1990s. Flaring in oil refineries is the main source for carbon monoxide emission in category 1.B.2. In the early 1990s, emissions from distribution of town gas were also taken into account in calculations. In 1990, the town-gas distribution network accounted for a total of 16 % of the entire gas network. Of that share, 15 % consisted of grey cast iron lines and 84 % consisted of steel and ductile cast iron lines. Since 1997 no town gas has been distributed in Germany's gas mains. Town-gas was the only known source of CO emissions in category 1.B.2.b.


===== Recalculations =====

Recalculations covering the past two years have been carried out as a result of the provisional nature of a number of statistics in this area.  
The reallocation of minor source categories have necessitated recalculations in sub-categories. In addition, it has been determined that the charcoal-production emissions reported to date were being doubly counted, since they were already included in 1.A.1.a.

**Difference between Submission 2024 against 2023**

__Table: Revision of emission estimates, change against submission 2023 in [kt]__

|                    |        ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015  ^  2020  ^  2021  ^
^  PM<sub>2.5</sub>  | 1.B.1  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
| :::                | 1.B.2  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
^  PM<sub>10</sub>   | 1.B.1  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
| :::                | 1.B.2  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
^  TSP               | 1.B.1  |  -4.10 |  -3.32 |  -4.13 |  -4.20 |  -5.12 |  -5.42 |  -3.92 |  -4.33 |
| :::                | 1.B.2  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
^  NMVOC             | 1.B.1  |  -2.11 |  -1.71 |  -2.13 |  -2.17 |  -2.64 |  -2.80 |  -2.02 |  -2.23 |
| :::                | 1.B.2  |   0.10 |   0.13 |   0.14 |   0.15 |   0.16 |   0.16 |   0.17 |   0.21 |
^  Hg                | 1.B.1  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
| :::                | 1.B.2  |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |


<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates reported for Base Year and 2021**, please see the recalculation tables following chapter [[general:recalculations:start|Chapter 8.1 - Recalculations]].
</WRAP>


===== Improvements planned for future submissions =====


  * an ongoing research project estimates emissions from storage and cleaning of tanks for oil and oil products - results are planned to be implemented into the inventory in 2025/26