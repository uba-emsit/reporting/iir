====== 2.B.3 - Adipic Acid Production ======

===== Short description =====

In source category //NFR 2.B.3 - adipic acid production// NO<sub>x</sub> and CO emissions from the production of adipic acid are reported. As there are only three producers of adipic acid, activity data provided by them has to be treated as confidential. Due to this reason, only emissions could be reported.

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.B.3                                  |  T3      |  PS  |  C   |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

^  NO<sub>x</sub>                         |  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  | TSP  |  BC  ^  CO   |  Heavy Metals  |  POPs  |
^  -/-                                    |  NA     |  NA              |  NA              |  NA                |  NA               |  NA  |  NA  ^  -/-  |  NA            |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                 |||||||||||

===== Method =====

As this source category is a key category for N<sub>2</sub>O emissions, plant specific activity data is applied according to the IPCC guidelines that is optained basically via a co-operation agreement with the adipic acid producers.

A single data collection of plant specific NO<sub>x</sub> and CO emissions and related emission factors for one year (2016) was sufficient as the emissions are below the threshold of significance. The derived emission factors are applied to the entire time series and for every plant.

===Activity Data ===

Due to confidentiality concerns, this data is not published (see short description).

===Emission factors===

Due to confidentiality concerns, this data is not published (see short description).

===== Recalculations =====

<WRAP center round info 65%>
With **all input data remaining unrevised**, no recalculations were made compared to the previous submission.
</WRAP>


===== Planned improvements =====

No category-specific improvements are planned.