====== 2.D.3.e - Degreasing ======

===== Short Description =====

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.D.3.e                                |  T2      |  NS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|  NO<sub>x</sub>                         ^  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  |  TSP  |  BC  |  CO  |  Heavy Metals  |  POPs  |
|  NA                                     ^  L/T    |  NA              |  NA              |  NA                |  NA               |  NA   |  NA  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                 |||||||||||

This source category comprises NMVOC emissions from the use of solvents in following processes:
  * **Metal degreasing** 
  * **Electronic component manufacturing** 
  * **Other industrial cleaning** (e.g. precision mechanics, optics, manufacture of watches and clocks)

NMVOC is defined in accordance with the VOC definition found in the EC solvents directive. For purposes of the definition of solvents, the term ‘solvent use’ is also defined in accordance with the EC solvents directive.

===== Method =====
=== General procedure ===

NMVOC emissions are calculated in accordance with a product-consumption-oriented approach. 
In this approach, solvent-based products or solvents are allocated to the source category, and then the relevant NMVOC emissions are calculated from those solvent quantities via specific emission factors. Thus, the use of this method is possible with the following valid input figures for each product group:

  * Quantities of VOC-containing (pre-) products and agents used in the report year,
  * The VOC concentrations in these products (substances and preparations),
  * The relevant application and emission conditions (or the resulting specific emission factor).

The quantity of the solvent-based (pre-)product corresponds to the domestic consumption which is the sum of domestic production plus import minus export.

<WRAP center round info 55%>
EM<sub>NMVOC</sub> = domestic consumption of a certain product ∙ solvent content ∙ EF<sub>product</sub>
</WRAP>

The calculated NMVOC emissions of different product groups for a source category are then aggregated. 
The product / substance quantities used are determined at the product-group level with the help of production and foreign-trade statistics. Where possible, the so-determined domestic-consumption quantities are then further verified via cross-checking with industry statistics.

===== Discussion of emission trends =====
=== General information ===
Since 1990, so the data, NMVOC emissions from use of solvents and solvent-containing products in general have decreased by nearly 60 %. The main emissions reductions have been achieved in the years since 1999. This successful reduction has occurred especially as a result of regulatory provisions such as the 31st Ordinance on the execution of the Federal Immissions Control Act (Ordinance on the limitation of emissions of volatile organic compounds due to the use of organic solvents in certain facilities – 31. BImSchV), the 2nd such ordinance (Ordinance on the limitation of emissions of highly volatile halogenated organic compounds – 2. BImSchV) and the TA Luft.

=== Specific information ===
Until 1999, data of the present source categories 2.D.3.e and f were treated as one source group. From 1990 to 1993 only a rough expert estimation was carried out, which since 1994 in a first step and since 2000 in a second step could be improved by a more detailed data collection that enables to follow the development of source group 2.D.3.e.  Since 2000, the share of this source group accounts for about 5-9 % of total NMVOC emissions from solvent-based products on a very stable niveau. 

 
===== Uncertainties =====

The relative overall uncertainty of emissions caused by applications of this source group is estimated at 50%.

===== Recalculations =====

Routinely, the NMVOC emissions of the last reported year must be actualized in the next reporting cycle as the final data of the foreign trade statistics are regularly only available after the publication of the respective reporting year has been completed. 


<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2021**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>
===== Planned improvements =====
 
At the moment, no category-specific improvements are planned.