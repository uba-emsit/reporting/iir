====== 2.A.3 - Glass Production ======

===== Short description =====
^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.A.3                                  |  T2      |  AS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^ TSP   |  BC  ^  CO   ^  Pb   ^  Cd   |  Hg  ^  As*  ^  Cr*  ^  Cu*  ^  Ni*  ^  Se*  ^  Zn*  |  POPs  |
|  -/-                                    |  -/-    |  -/-             |  -/-             |  -/-               |  -/-              |  -/-  |  NE  |  -/-  |  -/-  |  -/-  |  NA  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                                                                |||||||||||||||||||

\\
Germany's glass industry produces a wide range of different glass types that differ in their chemical composition. 

The national glass production sector includes of the following sub-sectors: 
  * container glass (bottles, jars, drinkware, and bowls), 
  * flat glass (used for windows, glass doors, transparent walls etc.), 
  * domestic glass, 
  * special glass 
and 
  * mineral fibres (glass and stone wool). 

The largest production quantities are found in the sectors of container glass and flat glass. Further processing and treatment of glass and glass objects are not considered.

The glass industry, in particular container glass production, is the main factor in selenium emissions. It accounted for more than 60 % in 1990 and around 45 % in recent years, reaching a peak of almost 80 % in 1995. A complete analysis over all categories is not done (see above '*').
===== Methodology =====

The emissions are calculated via a higher Tier method resembling a Tier 2 method, as the activity rates are tied to specific emission factors for different glass types.

==== Activity data ====

The production figures are taken from the regularly appearing annual reports of the [[https://www.bvglas.de/en/|Federal Association of the German Glass Industry]] (Bundesverband Glasindustrie; BV Glas). "Production" refers to the amount of glass produced, which is considered to be equivalent to the amount of glass melted down. 

==== Emission factors ====

The procedure used to determine emission factors for the various glass types involved and the pertinent emissions is described in detail in reports of two research projects (2008: Report-No. 001264[(UFoPlan FKZ 206 42 300/02: Teilvorhaben 02: „Providing up-to-date emission data for the glass and mineral fiber industry“ downloading via search “UBA-FB 001264” in (https://doku.uba.de ⇒ OPAC ⇒ use parameter ‘Signatur’)], 2021: Texte 45/2021[(ReFoPlan FKZ – 3719 52 1010: „Revision of emission factors for air pollutants in the cement clinker production and glass manufacturing sectors“ downloading via https://www.umweltbundesamt.de/sites/default/files/medien/5750/publikationen/2021-03-18_texte_45-2021_luftschadstoff_glasindustrie.pdf)]). The emission factors were calculated for the various industry sectors. The factors vary over time in keeping with industry monitoring, not only as steady trends, but falling in most cases. The most recently EF are for different glass types the following:

__Table 1: Overview of most recently applied emission factors__
^                   ^  Unit  ^ Container glass  ^ flat glass  ^ domestic glass  ^ special glass  ^ fibre optics  ^ stone wool  ^
^ NO<sub>x</sub>    |  kg/t  |  1.0766          |  1.7708     |  2.8602         |  3.5558        |  0.8          |  1.877      |
^ SO<sub>2</sub>    |  kg/t  |  0.759           |  1.5677     |  0.0599         |  0.1157        |  0.1847       |  2.229      |
^ NMVOC             |  kg/t  |  NA              |  NA         |  NA             |  NA            |  0.6          |  0.657      |
^ CO                |  kg/t  |  0.0732          |  0.0241     |  0.0661         |  0.1195        |  0.06         |  0.185      |
^ NH<sub>3</sub>    |  kg/t  |  0.0026          |  0.0191     |  NA             |  0.0295        |  1.10         |  1.163      |
^ TSP               |  kg/t  |  0.00863         |  0.01681    |  0.015          |  0.00765       |  0.01096      |  0.643      |
^ PM<sub>10</sub>   |  kg/t  |  0.00742         |  0.01429    |  0.0129         |  0.0065        |  0.00932      |  0.0234     |
^ PM<sub>2.5</sub>  |  kg/t  |  0.00483         |  0.00773    |  0.0069         |  0.00352       |  0.00504      |  0.0128     |
^ As                |  g/t   |  0.0279          |  0.0104     |  0.0023         |  0.1143        |  0.0354       |  NE         |
^ Pb                |  g/t   |  0.1237          |  0.0104     |  0.0076         |  0.1158        |  0.1571       |  NE         |
^ Cd                |  g/t   |  0.0032          |  0.0005     |  0              |  0.0028        |  0.0041       |  NE         |
^ Cr                |  g/t   |  0.0186          |  0.0029     |  0.0007         |  0.0148        |  0.0236       |  NE         |
^ Cu                |  g/t   |  0.0035          |  0.02       |  0.0002         |  0.0085        |  0.0056       |  NE         |
^ Ni                |  g/t   |  0.0048          |  0.0061     |  0.0003         |  0.0142        |  0.0061       |  NE         |
^ Se                |  g/t   |  0.2794          |  0.0427     |  0.1273         |  0.0454        |  0.01         |  NE         |

For each glass type the estimated EF are explained in ‘Texte 45/2021’  with an expert votum and uncertainty estimation.

Emissions of BC are not estimated, but there is no evidence from the industry monitoring for this.
===== Trends in emissions =====

Trends in emissions correspond to trends of emission factors and of production development. The resulting trends are not constant, as a result of different EF for various glass types. So emissions of NO<sub>x</sub> and SO<sub>2</sub> couldn't decrease last years due to increased production Level of relevant products.
[{{:sector:ippu:mineral_industry:em_2a3_since_1990.png|**Emission trends in NFR 2.A.3**}}]


===== Recalculations =====

Recalculations were necessary due to correction of a copying mistake of EF of NO<sub>x</sub> from fibre optics. The small changes can be shown as an absolute difference over time as follows:

[{{:sector:ippu:mineral_industry:recalc._2a3_since_1990.png|**Recalculations in NFR 2.A.3**}}]

All minor changes in 2021 were influenced by a AD correction.


=====Planned improvements =====

No further improvements are planned.
