====== 2.C.5 - Lead Production======

===== Short description =====

Within this NFR subcategory, SO<sub>2</sub>, PM<sub>2.5</sub>, PM<sub>10</sub>, TSP, As, Cd, Cu, Hg, Pb, Zn, PCB  and PCDD/F emissions from the production of lead are reported.

^  Category Code                          ^  Method ^  AD  ^  EF     ^
|  2.C.5                                  |  T2     |  AS  |  D, CS  |
|  {{page>general:Misc:LegendEIT:start}}                          ||||

----

|  NO<sub>x</sub>                         |  NMVOC  ^  SO<sub>2</sub>  |  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  |  CO  ^  Pb   ^  Cd   ^  Hg   ^  As   |  Cr  ^  Cu   |  Ni  ^  Se  ^  Zn   ^  PCDD/F  |  PAHs  |  HCB  |
|  NA                                     |  NA     |  -/-             |  NA              |  -/-               |  -/-              |  -/-  |  NE  |  NA  |  -/-  |  -/-  |  -/-  |  -/-  |  NA  |  -/-  |  NA  |  NE  |  -/-  |  -/-     |  NA    |  NA   |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                                                                              |||||||||||||||||||||
===== Method =====

==== Activity data ====

The yearly production figures were taken from the annual statistical report of the German association for non-ferrous metals<sup>**[[#Bibliography| [Lit. 1]]]**</sup>.\\
In 2022 the primary and secondary lead production amounted to 229 kt.
==== Emission factors ====

The emission factor for SO<sub>x</sub> is a tier 1 default value according to the EMEP/EEA air pollutant emission inventory guidebook 2023<sup>**[[#Bibliography| [Lit. 2]]]**</sup> and is supposed to be constant. All other emission factors are tier 2 level factors, have a decreasing trend and were determined in research projects. For heavy metals (HM), the applied emission factors are derived from a research project<sup>**[[#Bibliography| [Lit. 3]]]**</sup>.

__Table 1: emission factors applied for primary and secondary lead production (3 digits rounded off)__
|                   ^  EF (primary)  ^  EF (secondary)      ^  Unit  ^
^ SO<sub>2</sub>    |    2.05               ||  kg/t  |
^ PM<sub>2.5</sub>  |    17.5               ||  g/t  |
^ PM<sub>10</sub>   |    21.3              ||  g/t  |
^ TSP               |    25              ||  g/t  |
^ As                |   49.213   |  41.179   |  mg/t  |
^ Cd                |   62.448   |  19.767   |  mg/t  |
^ Cu                |  162.663   |            |  mg/t  |
^ Hg                |    0.3   |  0.325    |  g/t   |
^ Pb                |    6.028   |  4.506    |  g/t   |
^ Zn                |  664     |  50   |  mg/t  |
^ PCB               |    2.1               ||  mg/t  |
^ PCDD/F            |    0.34               ||  µg/t  |


==== Uncertainties ====

The uncertainties for the production amounts are 5% and for the emission factors not more than 300%. Only for dioxines the upper bond uncertainty is about 900%.


===== Recalculations =====

<WRAP center round info 65%>
With **activity data and emission factors remaining unrevised**, no recalculations were carried out compared to the previous submission.
</WRAP>


===== Planned improvements =====

The emission factor for PCB is still orientated to old emission guidebook values and seems to be to high due to legal terms. The factor will be updated in the next submission 2025.


------
===== Bibliography =====

**Lit.  1:** German association for non-ferrous metals (WirtschaftsVereinigung Metalle): Annual statistical report: https://www.wvmetalle.de \\ 
**Lit.  2:** EMEP/EEA, 2023: EMEP/EEA air pollutant emission inventory guidebook 2023, Copenhagen, 2023. [[https://www.eea.europa.eu/publications/emep-eea-guidebook-2023/part-b-sectoral-guidance-chapters/2-industrial-processes-and-product-use/2-c-metal-production/2-c-5-lead-production-2023/view|https://www.eea.europa.eu]] \\
**Lit.  3:** Ökopol, IER, IZT, IfG: Bereitstellung einer qualitätsgesicherten Datengrundlage für die Emissionsberichterstattung zur Umsetzung von internationalen Luftreinhalte- und Klimaschutzvereinbarungen für ausgewählte Industriebranchen Teilvorhaben 2: NE-Metallindustrie, Kalkindustrie, Gießereien. \\

