====== 2.K - Consumption of POPs and Heavy Metals ======

===== Short description =====

__Former pesticides__: As stated in the chapter on NFR 2.J, POP pesticides (Aldrin, Dieldrin, Chlordane, Toxaphene, Mirex, Endrin, Heptachlor, DDT) listed in Annexes A & B of the Stockholm Convention have not been in use since 1989. 

The PCP ban was enacted in 1989 with the PCP Prohibition Ordinance. 
The use of lindane was severely restricted in the 1980s; there is no longer any approved lindane-containing wood preservative on the market in Germany. 

__HCB__: In the Federal Republic of Germany, HCB-containing pesticides may no longer be used since 1981; in the GDR, the ban has been in force since 1984. More information is given in chapter [[sector:agriculture:agricultural_soils:3df_agriculture_other|3.D.f - Agriculture other including use of pesticides]].

__Dioxins__ and __furans__ are neither produced or applied intentionally but are by-products that can be formed unintentionally in all combustion processes in the presence of chlorine and organic carbon. 

__PAHs__ occur as impurities of other substances or in uncontrolled combustion processes.

//Therefore, no emissions of dioxins and furans, PAHs, and HCB would be reported.//

__PCBs__: Source category 2.K considers PCB emissions from use of polychlorinated biphenyls (PCBs) in transformers, small and large capacitors, anti-corrosive paints and joint sealants. Since 1989, polychlorinated biphenyls (PCBs) may no longer be manufactured and placed on the market in Germany (PCB Prohibition Ordinance 1989, adopted in the Chemicals Prohibition Ordinance 1993). However, due to their long lifetime, PCBs can still enter the environment as longterm or secondary emissions, e.g. through open applications in buildings, use in wall paints, joint sealants, varnishes and applications as flame retardants.  

However, data on open applications in buildings are subject to large uncertainties; in particular, the different amounts of PCBs used in eastern and western Germany and the many application sites (public, private, and industrial buildings) cannot be plausibly quantified. The figures of a research project we have investigated are not aplicable for reporting but for scientific discussion [(See research report (in german only): https://www.umweltbundesamt.de/publikationen/analyse-der-novellierten-nec-richtlinie-bezueglich)].

An emission factor in the EEA/EMEP Guidebook 2023[(EMEP/EEA, 2023: EMEP/EEA air pollutant emission inventory guidebook 2023, Copenhagen, 2023, https://www.eea.europa.eu/publications/emep-eea-guidebook-2023/part-b-sectoral-guidance-chapters/2-industrial-processes-and-product-use/2-k-consumption-of-pops/2-k-consumption-of-pops/view)] is only be reported for PCB. As the calculation simply is linked to the capita disregarding existing prohibitions this emission factor would lead to unjustified high emissions. 

//For this reason and to be consistent with the reporting Guidelines, the notation key NE for PCB  and NA for the other pollutants is used in the NFR tables.
//