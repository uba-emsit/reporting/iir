<hidden onHidden="Click to view Legend" onVisible="Click to hide Legend">
^  L/-  | key source by **L**evel only                                                          |
^  -/T  | key source by **T**rend only                                                          |
^  L/T  | key source by both **L**evel and **T**rend                                            |
|  -/-  | no key source for this pollutant                                                      |
|  IE   | emission of specific pollutant **I**ncluded **E**lsewhere (i.e. in another category)  |
|  NE   | emission of specific pollutant **N**ot **E**stimated (yet)                            |
|  NA   | specific pollutant not emitted from this source or activity = **N**ot **A**pplicable  |
|* |no analysis done |
</hidden>