====== Chapter 10.1 - Point Sources  ======

==== Background ====

Germany, alongside with the European Union (EU) and EU Member States, has signed the //UN-ECE PRTR Protocol//, whereby Germany commits to establish and operate a national Pollutant Release and Transfer Register (PRTR) for public information. //The E-PRTR Regulation (European Regulation (EC) No 166/2006)// and the //German PRTR Act (SchadRegProtAG)// provide the legal basis for this fact. If pollutant threshold values or waste quantities specified in the E-PRTR Ordinance are exceeded, releases of pollutants to air, water and soil, shipments with wastewater, and disposal of hazardous and non-hazardous wastes from certain industrial activities have to be reported annually for the PRTR.
[(SchadstoffeimPRTR2022)]
 
Since December 31, 2016, the new //NEC Directive (2016/2284/EU)// on the reduction of the national emissions of certain atmospheric pollutants came into force, replacing the //Directive (2001/81/EC)// on National Emission Ceilings. The new //NEC Directive (2016/2284/EU)// requires EU Member States to achieve new reduction targets from 2030, which in addition to the previously regulated air pollutants SO<sub>2</sub>, NO<sub>x</sub>, NMVOC and NH<sub>3</sub>, also include reduction targets for particulate matter (PM<sub>2.5</sub>) for the first time. This means that the new directive imposes significantly extended reporting obligations on the EU Member States. For the first time, EU Member States are required to create emission inventories for particulate matter, heavy metals and POPs. The inventories must also include emission projections. Furthermore, emissions from large point sources must be reported and emission data must be spatially itemized. 

For the first time in 2017 and then every four years, EU Member States must report spatially distributed emissions (raster data) with a resolution of 0.1° x 0.1° (longitude-latitude). Large point sources (LPS) must be reported every 4 years, starting in 2017. Reporting is mandatory for agro-industrial and industrial activities whose annual emissions exceed certain thresholds, which correspond to those for emissions reporting under //the E-PRTR Regulation (European Regulation (EC) No 166/2006)//.
[(Ufoplan2019)]

Annex IV of the Directive indicates, that the Member States shall prepare emission inventories "in accordance with the methodologies recognized by the Parties to the //LRTAP Convention (EMEP reporting guidelines)//" and "shall base their reporting on the //Air Pollutant Emissions Inventory (EMEP/EUA Guidelines)// referred to in the Convention."
//The reporting guidelines (UNECE 2015)// define large point sources as follows: 

<WRAP center round box 65%>
"Large point sources (LPS) are defined as facilities whose combined emissions, within the limited identifiable area of the site premises, exceed the pollutant emission thresholds identified in table 1 below."[(Ufoplan2019)] 

The associated Table 1 identifies thresholds, consistent with those identified in the E-PRTR Regulation (2016), for the following pollutants:

  * Sulfur dioxide (SO<sub>2</sub>) 
  * Nitrogen Oxide (NO<sub>x</sub>)
  * Carbon Monoxide (CO)
  * Non-Methane Volatile Organic Compounds (NMVOC) 
  * Ammonia (NH<sub>3</sub>)
  * Particulate Matter ≤2.5µm (PM<sub>2.5</sub>)
  * Particulate Matter ≤10µm (PM<sub>10</sub>)
  * Lead and compounds (Pb)
  * Cadmium and compounds (Cd)
  * Mercury and compounds (Hg)
  * Polycyclic Aromatic Hydrocarbons (PAHs) 
  * PCDD & PCDF (Polychlorinated Dibenzo-Dioxins and -Furans)(as Teq)
  * Hexachlorobenzene (HCB) 
  * Polychlorinated Biphenyls (PCBs) 
</WRAP>

==== Reporting ====

Germany provided its point source data under the LRTAP convention for the first time in 2017. In principle, the German LPS data submission is mainly a copy of the ePRTR data available on both the [[https://www.thru.de/thrude/|national]] and [[https://www.eea.europa.eu/data-and-maps/data/member-states-reporting-art-7-under-the-european-pollutant-release-and-transfer-register-e-prtr-regulation-23|European websites]]. However, as some additional information is missing in the ePRTR dataset, such as stack heights, which are not available at the federal level, some gap filling is needed and explained below.

Please note that the most recent provision of large point source data, submitted in 2021, is based on the 2018 dataset, as ePRTR data for 2019 are not yet available.

<figure>
<caption>**Large point sources for different GNFR sectors for Germany for the year 2018**</caption>
{{ :general:point_sources:lpsgrafik.png?nolink&600 | Large Point Sources for Germany}}
</figure> 
Source of data: https://cdr.eionet.europa.eu/de/un/clrtap/lps/envyikmeq

==== Methodology ====

=== Quality checks ===

Various quality checks of the data were carried out before submission. The data from the ePRTR database was cleaned, processed and missing data was added. Then, the data was transferred into a suitable format (Excel template under LRTAP convention). Before the data could be transformed into the appropriate format for submission, several data preconditions were determined. Furthermore, data quality checks were performed. Below is a list of the quality checks that have been performed.

  * Calculation of the sum of LPS emissions and comparison with the NFR tables
  * Unit mistakes checks 
  * Outlier checks
  * Verification of missing information in the data (such as ID’s, stack heights, GNFR, coordinates, etc.)
  * Checking whether the coordinates are within Germany
  * Checking whether there are duplicates in the coordinates

For example, all point sources identified as outliers in the box plots below were checked individually to be correct:

{{ :general:point_sources:outliers_main.png?nolink&1000 | Outliers main pollutants}}

{{ :general:point_sources:outliers_others.png?nolink&1000 | Outliers other pollutants}}


=== GNFR codes and stack heights ===

The ePRTR dataset includes most of the information needed for LPS reporting. However, both GNFR sectors and stack heights are not included. These point source meta data are instead derived from the PRTR activities given and by researching some important point sources individually. In general, GNFR membership and stack height class have been added according to Table {{ref>StackHeights}} below (mainly based on the Environmental Research Plan of the Federal Ministry for the Environment, Nature Conservation, Building and Nuclear Safety) [(Ufoplan2019)].

<table StackHeights> 
<caption>**Stack heights and further information related to the GNFR sectors **</caption>
^ Activity code from ePRTR  ^ ePRTR description                                                                                                                                                                    ^ GNFR             ^ Stack height class  ^
| 1.a            | Mineral oil and gas refineries                                                                                                                                                        | B_Industry       | 4      |
| 1.b            | Installations for gasification and liquefaction                                                                                                                                       | B_Industry       | 4      |
| 1.c            | Thermal power stations and other combustion installations (>50 MW)                                                                                                                    | A_PublicPower    | 5      |
| 1.d            | Coke ovens                                                                                                                                                                            | B_Industry       | 5      |
| 1.e            | Coal rolling mills                                                                                                                                                                    | B_Industry       | 5      |
| 1.f            | Installations for the manufacture of coal products and solid smokeless fuel                                                                                                           | D_Fugitive       | 4      |
| 2              | Production and processing of metals                                                                                                                                                   | B_Industry       | 5      |
| 2.a            | Metal ore roasting or sintering installations                                                                                                                                         | B_Industry       | 5      |
| 2.b            | Installations for the production of pig iron or steel inc. continuous casting                                                                                                         | B_Industry       | 5      |
| 2.c            | Installations for the processing of ferrous metals                                                                                                                                    | B_Industry       | 5      |
| 2.c.i          | Installations for the processing of ferrous metals: (i) hot-rolling mills                                                                                                             | B_Industry       | 5      |
| 2.c.ii         | Installations for the production of pig iron or steel and processing of ferrous metals (hot-rolling mills and smitheries with hammers)                                                | B_Industry       | 5      |
| 2.d            | Ferrous metal foundries                                                                                                                                                               | B_Industry       | 2      |
| 2.e            | Installations for non-ferrous metals                                                                                                                                                  | B_Industry       | 2      |
| 2.e.i          | Installations for the production of non-ferrous crude metals from ore, concentrates or secondary raw materials by metallurgical, chemical or electrolytic processes                   | B_Industry       | 2      |
| 2.e.ii         | Installations for the smelting, including the alloying, of non-ferrous metals, including recovered products (refining, foundry casting, etc.)                                         | B_Industry       | 2      |
| 2.f            | Installations for surface treatment of metals and plastic materials using an electrolytic or chemical process                                                                         | B_Industry       | 1      |
| 3.a            | Underground mining and related operations; Opencast mining and quarring                                                                                                               | D_Fugitive       | 1      |
| 3.b            | Opencast mining and quarrying                                                                                                                                                         | D_Fugitive       | 1      |
| 3.c            | Installations for the production (see below)                                                                                                                                          | B_Industry       | 2      |
| 3.c.i          | Installations for the production of: (i) cement clinker in rotary kilns, (iii) cement clinker or lime in other furnaces                                                               | B_Industry       | 2      |
| 3.c.ii         | Installations for the production of: (ii) lime in rotary kilns, (iii) cement clinker or lime in other furnaces                                                                        | B_Industry       | 2      |
| 3.c.iii        | Installations for the production of: (i) cement clinker in rotary kilns, (iii) cement clinker or lime in other furnaces                                                               | B_Industry       | 2      |
| 3.d            | Installations for the production of asbestos and the manufacture of asbestos-based products                                                                                           | B_Industry       | 2      |
| 3.e            | Installations for the manufacture of glass, incl. glass fibre                                                                                                                         | B_Industry       | 2      |
| 3.f            | Installations for melting mineral substances, incl. the production of mineral fibres                                                                                                  | B_Industry       | 2      |
| 3.g            | Installations for the manufacture of ceramic products by firing, in particular roofing tiles, bricks, refractory bricks, tiles, stoneware or porcelain                                | B_Industry       | 2      |
| 4.a            | Chemical installations for the production on an industrial scale of basic organic chemicals                                                                                           | B_Industry       | 1      |
| 4.a.i          | Chemical installations for the production on an industrial scale of basic organic chemicals: simple hydrocarbons (linear or cyclic, saturated or unsaturated, aliphatic or aromatic)  | B_Industry       | 1      |
| 4.a.ii         | Chemical installations for the production on an industrial scale of basic organic chemicals: oxygen-containing hydrocarbons                                                           | B_Industry       | 1      |
| 4.a.iii        | Chemical installations for the production on an industrial scale of basic organic chemicals: sulphurous hydrocarbons                                                                  | B_Industry       | 1      |
| 4.a.iv         | Chemical installations for the production on an industrial scale of basic organic chemicals: nitrogenous hydrocarbons                                                                 | B_Industry       | 1      |
| 4.a.ix         | Chemical installations for the production on an industrial scale of basic organic chemicals: synthetic rubbers                                                                        | B_Industry       | 1      |
| 4.a.vi         | Chemical installations for the production on an industrial scale of basic organic chemicals: halogenic hydrocarbons                                                                   | B_Industry       | 1      |
| 4.a.vii        | Chemical installations for the production on an industrial scale of basic organic chemicals: organometallic compounds                                                                 | B_Industry       | 1      |
| 4.a.viii       | Chemical installations for the production on an industrial scale of basic organic chemicals: basic plastic material (polymers, syntetic fibers and cellulose-based fibers)            | B_Industry       | 1      |
| 4.a.x          | Chemical installations for the production on an industrial scale of basic organic chemicals: dyes and pigments                                                                        | B_Industry       | 1      |
| 4.a.xi         | Chemical installations for the production on an industrial scale of basic organic chemicals: surface-active agents and surfactants                                                    | B_Industry       | 1      |
| 4.b            | Chemical installations for the production on an industrial scale of basic inorganic chemicals                                                                                         | B_Industry       | 1      |
| 4.b.i          | Chemical installations for the production on an industrial scale of basic inorganic chemicals: gases                                                                                  | B_Industry       | 1      |
| 4.b.ii         | Chemical installations for the production on an industrial scale of basic inorganic chemicals: acids                                                                                  | B_Industry       | 1      |
| 4.b.iii        | Chemical installations for the production on an industrial scale of basic inorganic chemicals: bases                                                                                  | B_Industry       | 1      |
| 4.b.iv         | Chemical installations for the production on an industrial scale of basic inorganic chemicals: salts                                                                                  | B_Industry       | 1      |
| 4.b.v          | Chemical installations for the production on an industrial scale of basic inorganic chemicals: non-metals, metal-oxides or other inorganic compounds                                  | B_Industry       | 1      |
| 4.c            | Chemical installations for the production on an industrial scale of fertilisers                                                                                                       | B_Industry       | 1      |
| 4.d            | Installations using a chemical or biological process for the production on an industrial scale of basic plant health products and of biocides                                         | B_Industry       | 1      |
| 4.e            | Installations using a chemical or biological process for the production on an industrial scale of basic pharmaceutical products                                                       | B_Industry       | 1      |
| 4.f            | Installations for the production on an industrial scale of explosives and pyrotechnic products                                                                                        | B_Industry       | 1      |
| 5.a            | Installations for the disposal or recovery of hazardous waste                                                                                                                         | J_Waste          | 3      |
| 5.b            | Installations for the incineration of non-hazardous waste                                                                                                                             | J_Waste          | 2      |
| 5.c            | Installations for the disposal of non-hazardous waste                                                                                                                                 | J_Waste          | 1      |
| 5.d            | Landfills                                                                                                                                                                             | J_Waste          | 1      |
| 5.e            | Installations for the disposal or recycling of animal carcasses and animal waste                                                                                                      | J_Waste          | 2      |
| 5.f            | Urban waste-water treatment plants                                                                                                                                                    | J_Waste          | 1      |
| 5.g            | Independently operated industrial wastewater treatment plants                                                                                                                         | J_Waste          | 1      |
| 6              | Paper and wood producing plants                                                                                                                                                       | B_Industry       | 2      |
| 6.a            | Industrial plants for the production of pulp from timber or similar fibrous materials                                                                                                 | B_Industry       | 2      |
| 6.b            | Industrial plants for the production of paper and board and other primary wood products                                                                                               | B_Industry       | 2      |
| 6.c            | Industrial plants for the preservation of wood and wood products with chemicals                                                                                                       | E_Solvents       | 2      |
| 7.a            | Installations for the intensive rearing of poultry or pigs                                                                                                                            | K_AgriLivestock  | 1      |
| 7.a.i          | Installations for the intensive rearing of poultry or pigs: with 40 000 places for poultry                                                                                            | K_AgriLivestock  | 1      |
| 7.a.ii         | Installations for the intensive rearing of poultry or pigs: with 2 000 places for production pigs (over 30 kg)                                                                        | K_AgriLivestock  | 1      |
| 7.a.iii        | Installations for the intensive rearing of poultry or pigs: with 750 places for sows                                                                                                  | K_AgriLivestock  | 1      |
| 8.a            | Slaughterhouses                                                                                                                                                                       | B_Industry       | 1      |
| 8.b            | Treatment and processing intended for the production of food and beverage products                                                                                                    | B_Industry       | 1      |
| 8.b.i          | Treatment and processesing intended for the production of food and beverage products from: animal raw materials other than milk                                                       | B_Industry       | 1      |
| 8.b.ii         | Treatment and processesing intended for the production of food and beverage products from: vegetable raw materials                                                                    | B_Industry       | 1      |
| 8.c            | Treatment and processing of milk                                                                                                                                                      | B_Industry       | 1      |
| 9.a            | Plants for the pre-treatment (operations such as washing, bleaching, mercerisation) or dyeing of fibres or textiles                                                                   | E_Solvents       | 1      |
| 9.b            | Plants for the tanning of hides and skins                                                                                                                                             | E_Solvents       | 1      |
| 9.c            | Installations for the surface treatment of substances, objects or products using organic solvents                                                                                     | E_Solvents       | 1      |
| 9.d            | Installations for the production of carbon (hard-burnt coal) or electrographite by means of incineration or graphitisation                                                            | B_Industry       | 2      |
| 9.e            | Installations for the building of, and painting or removal of paint from ships                                                                                                        | E_Solvents       | 1      |
</table>

==== References ====

[(SchadstoffeimPRTR2022>Pollutants of the PRTR - Situation in Germany - Reporting years 2007 - 2022, Umweltbundesamt, 2024 [[https://thru.de/wp-content/uploads/2024/08/Pollutants_of_the_PRTR-Situation_in_Germany-Reporting_years_2007-2022_V2.pdf|External Link]] )]

[(Ufoplan2019>Analyse der novellierten NEC-Richtlinie bezüglich der erweiterten Anforderungen an die Berichterstattung von Schadstoffemissionen in die Luft - Umweltforschungsplan des Bundesministeriums für Umwelt, Naturschutz, Bau und Reaktorsicherheit, im Auftrag des Umweltbundesamtes, 2019 [[https://www.bmu.de/fileadmin/Daten_BMU/Pools/Forschungsdatenbank/fkz_3717_51_1010_nec_richtlinie_bf.pdf|External Link]] )]



 

