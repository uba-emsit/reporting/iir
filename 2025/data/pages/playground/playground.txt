====== PlayGround (VORLAGE FÜR EIN NFR-SPEZIFISCHES KAPITEL) ======

<WRAP center round important 100%>
SOFERN MÖGLICH, BITTE KEINE SEITEN UNTERHALB DER NFR-STRUKTUR ANLEGEN. VIELMEHR SOLLTE DER DETAILLIERUNGSSGRAD DER NFR-TABELLEN HIER ALS GRUNDLAGE DIENEN. 

SOLLTE DENNOCH EINE FEINERE UNTERTEILUNG NÖTIG SEIN (z.B. bei unterscheidlichen Produkten innerhalb einer "Sammelkategorie"), KANN DIESE KOMFORTABEL DURCH NUTZUNG VON SEITEN-INTERNEN VERWEISEN, DIE DAS SPRINGEN ZU EINZELNEN ABSÄTZEN ERLAUBEN, REALISIERT WERDEN.

So können z.B. Methodiken, AD und EF für mehrere Unterkategorien unterhalb des Detailllevels der NFR-Tabellen separat beschrieben werden, für übergreifende Aspekte wie Trenddiskussion, Rückrechnungen, Unsicherheiten, Verbesserungspläne und ggf. FAQ kann auf entsprechende übergreifende Absätze am Ende derselben Seite verwiesen werden:

[[#Recalculations]]; [[#Uncertainties]]; [[#FAQs]]
</WRAP>

===== Short description =====

In category //?.?.? - ?????//, emissions from ..... are reported. 

<WRAP center round important 100%>
KOPIERVORLAGE SCHADSTOFF-KÜRZEL: 
NO<sub>x</sub>, NMVOC, SO<sub>2</sub>, NH<sub>3</sub>, PM<sub>2.5</sub>, PM<sub>10</sub>, TSP, BC, CO, Pb, Cd, Hg, As, Cr, Cu, Ni, Se, Zn, PCDD/F, PAH, B[a]P, B[b]F, B[k]F, I[x]P, HCB, PCBs
</WRAP>

^  NFR Code                               ^  Method           ^  AD                    ^  EF           ^
|  ?.?.?.?                                |  T1 T2 T3 C CS M  |  NS RS IS PS M AS Q C  |  D CS PS M C  |
|  {{page>general:Misc:LegendEIT:start}}                                                            ||||

^ NO<sub>x</sub>                          ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^ TSP   ^  BC  ^  CO   ^  Pb   ^  Cd   ^  Hg   |  As  ^  Cr  ^  Cu  ^  Ni  |  Se  ^  Zn  |  PCDD/F  ^  B(a)P  ^  B(b)F  ^  B(k)F  ^  I(x)P  ^  PAH1-4  |  HCB  |  PCBs  |
^  L/T                                    ^  L/-    ^  -/T             |  NE              |  -/-               |  -/-              |  -/-  |  NR  |  -/-  |  -/-  |  -/-  |  -/-  |  NA  |  NE  |  NE  |  NE  |  NA  |  NE  |  NA      |  -/-    |  -/-    |  -/-    |  IE     |  -/-     |  NA   |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                                                                                                                          ||||||||||||||||||||||||||


^ NO<sub>x</sub>                          ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^ TSP   ^  BC  ^  CO   ^  Heavy Metals  ^  POPs  ^
^  L/T                                    ^  L/-    ^  -/T             |  NE              |  -/-               |  -/-              |  -/-  |  NR  |  -/-  |  NA            |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                  |||||||||||


| <WRAP center round important 100%>           TATSÄCHLICH NICHT EMITTIERTE SCHADSTOFFE KÖNNEN GERN AUCH AUS DER LEISTE GELÖSCHT WERDEN. DAZU EINFACH ENTSPRECHENDE SPALTEN ENTFERNEN. ALTERNATIV KANN JEWEILS AUCH "NA" EINGETRAGEN WERDEN.     

----


IM FALLE VON SCHADSTOFFEN, DIE __NOCH NICHT__ BERICHTET WERDEN, SOLLTE ENSTPRECHEND "NE" (ODER, IM FALLE VON BC, GGF. AUCH "NR") EINGETRAGEN WERDEN.     </WRAP>                                                                                                                                                                                                                                                        |||||||||||||||||||||||||||


Germany's railway sector is undergoing a long-term modernisation process, aimed at making electricity the main energy source for rail transports. Use of electricity, instead of diesel fuel, to power locomotives has been continually increased, and electricity now provides 80% of all railway traction power. Railways' power stations for generation of traction current are allocated to the stationary component of the energy sector (1.A.1.a) and are not included in the further description that follows here. In energy input for trains of German railways, diesel fuel is the only energy source that plays a significant role apart from electric power. 

===== Methodology =====

=== Activity Data ===

Federal Statistical Office of Germany

Basically, total inland deliveries of //diesel oil// are available from the National Energy Balances (NEBs) (AGEB, 2022) [(AGEB2022)]. This data is based upon sales data of the Association of the German Petroleum Industry (MWV) [(MWV2021)]. As a recent revision of MWV data on diesel oil sales for the years 2005 to 2009 has not yet been adopted to the respective NEBs, this original MWV data has been used for this five years. 

Data on the consumption of biodiesel in railways is provided in the NEBs as well, from 2004 onward. But as the NEBs do not provide a solid time series regarding most recent years, the data used for the inventory is estimated based on the prescribed shares of biodiesel to be added to diesel oil. 

Small quantities of //solid fuels// are used for historical steam engines vehicles operated mostly for tourism and exhibition purposes. Official fuel delivery data are available for lignite, through 2002, and for hard coal, through 2000, from the NEBs. In order to complete these time series, studies were carried out in 2012 [(HEDEL2012)], 2016 [(ILLICHMANN2016)] and 2021 [(HASENBALG2021)]. During these studies, questionaires were provided to any known operator of historical steam engines in Germany. Here, due to limited data archiving, nearly complete data could only be gained for years as of 2005. For earlier years, in order to achieve a solid time series, conservative gap filling was applied. 

__Table 1: Annual activity data__

|             ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^
^ Activity 1  |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |
^ Activity 2  |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |


<WRAP center round important 100%>
HIER DARF GERN MIT GRAFIKEN GEARBEITET WERDEN.

IDEALERWEISE GERN EINHEITLICH MIT SÄULENDIAGRAMMEN AB BASISJAHR: 

HIER GEHTS ZUR VORLAGE IM CORPORATE DESIGN DES UBA: [[http://ubanet/websites/PB2/Layout-Publikationen/CorporateDesign/Diagramme%20und%20Tabellen/Forms/AllItems.aspx]]  (DATEI UBA_DIAGRAMM_CALIBRI_2017.xlsx)															
</WRAP>


=== Emission factors ===

<WRAP center round important 100%>
BEI FÜR ALLE EF IDENTISCHEN EINHEITEN, DIE EINHEIT BITTE IN [] IM TABELLENTITEL ANGEBEN. BEI VARIIERENDEN EINHEITEN BITTE SEPARATE SPALTE EINFÜGEN.
</WRAP>

__Table 2: Annual emission factors, in [?]__
|              ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^ 2019  ^ 2020  ^  2021  ^
^ Pollutant 1  |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ Pollutant 2  |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ Pollutant 3  |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |


__Table 2: Annual product-specific emission factors, in [?]__
|              ^  1990       ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^ 2019  ^ 2020  ^  2021  ^
|              |  Product 1                                                                                                                        ||||||||||||||||
^ Pollutant 1  |             |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ Pollutant 2  |             |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ Pollutant 3  |             |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
|              |  Product 2                                                                                                                        ||||||||||||||||
^ Pollutant 1  |             |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ Pollutant 2  |             |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ Pollutant 3  |             |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |

VORLAGE FORMEL: 
<WRAP center round info 55%>
EM<sub>?</sub> = AD ∙ SF ∙ EF<sub>?</sub>
∙ + - ≠ ≈ ≤ ≥ ÷ ± ∫ √ ∆ 
</WRAP>

ODER:

<m> M_{C} = 0.45 * A  * B * beta </m>


where:

  *  0.45 = average fraction of carbon in fuel wood

  * A = forest area burnt in [m²]

  * B = mean above-ground biomass of fuel material per unit area in [kg/m²]

  * β = burning efficiency (fraction burnt) of the above-ground biomass
 
<WRAP center round important 100%>
weitere Synthax siehe: [[https://www.dokuwiki.org/plugin:mathpublish:syntax]]
</WRAP>


===== Discussion of emission trends =====

<WRAP center round important 100%>
BITTE MINDESTENS FÜR KEY CATEGORIES; DARÜBER HINAUS FREIWILLIG.
</WRAP>

 
__Table 3: Outcome of Key Category Analysis__
|  for: ^  TSP    ^  PM<sub>10</sub>  ^  PM<sub>2.5</sub>  ^
|   by: |  Level  |  L/-              |  L/-               |


<WRAP center round important 100%>
HIER DARF UND SOLL BITTE MIT GRAFIKEN GEARBEITET WERDEN.

IM FALLE EINER KEY CATEGORY NACH DEM TREND MÖGLICHST MIT LINIENDIAGRAMM AB BASISJAHR ODER 2005.
IM FALLE EINER KEY CATEGORY NACH DEM LEVEL GERN MIT SÄULEN ODER TORTEN, JEWEILS FÜR BASISJAHR UND AKTUELLES BERICHTSJAHR (t-2). 

HIER GEHTS ZUR VORLAGE IM CORPORATE DESIGN DES UBA: [[http://ubanet/websites/PB2/Layout-Publikationen/CorporateDesign/Diagramme%20und%20Tabellen/Forms/AllItems.aspx]]  (DATEI UBA_DIAGRAMM_CALIBRI_2017.xlsx)															
</WRAP>

Trend charts for relevant pollutants (please click to enlarge):

{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_nox.png?125 |annual nitrogen oxides emissions from railways}}
{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_so2.png?125 |annual sulphur oxides emissions from railways}}
{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_nmvoc.png?125 |annual NMVOC oxides emissions from railways}}
{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_nh3.png?125 |annual ammonia oxides emissions from railways}}
{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_pm.png?125|annual particulate matter oxides emissions from railways}}
{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_pm10.png?125 |annual pm10 emissions from railways}}
{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_cu.png?125 |annual copper emissions from railways}}


{{ :sector:energy:fuel_combustion:transport:railways:1a3c_em_pm10.png?600 |annual pm10 emissions from railways}}
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

----

{{:sector:energy:fuel_combustion:transport:railways:1a3c_em_pm10.png?600 |annual pm10 emissions from railways }}
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

----

{{ :sector:energy:fuel_combustion:transport:railways:1a3c_em_pm10.png?600|annual pm10 emissions from railways }}
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
----


===== Recalculations =====

OPTIONEN:

<WRAP center round info 65%>
With **all input data remaining unrevised**, no recalculations were made compared to the previous submission.
</WRAP>

ODER: 

__Table 4: Revised activity data for year...__
|                      ^  activity 1  ^  activity 2  ^
^ current submission   |  10,782      |  896         |
^ previous submission  |  10,145      |  843         |
^ absolute change      |  637         |  52.9        |
^ relative change      |  6.28%       |  6.27%       |

__Table 4b: Revised activity data time series__
|                      ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^ 2019  ^ 2020  ^  2021  ^
| Activity 1           |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ current submission   |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ previous submission  |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ absolute change      |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ relative change      |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %    |  %    |  %     |
| Activity 2           |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ current submission   |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ previous submission  |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ absolute change      |        |        |        |        |        |        |        |        |        |        |        |        |        |       |       |        |
^ relative change      |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %    |  %    |  %     |


__Table 5: Revised emission factors__
|                      ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  |     |
| Pollutant 1                                                                                                                                  ||||||||||||||||     |
^ current submission   |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |     |
^ previous submission  |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |     |
^ absolute change      |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |     |
^ relative change      |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %  |
| Pollutant 2                                                                                                                                  ||||||||||||||||     |
^ current submission   |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |     |
^ previous submission  |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |     |
^ absolute change      |        |        |        |        |        |        |        |        |        |        |        |        |        |        |        |     |
^ relative change      |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %     |  %  |

<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and //[hier bitte das Jahr t-2 einsetzen!]//**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Uncertainties =====


===== Planned improvements ===== 

(wesentliche) OPTIONEN:

Currently, no source-specific improvements are planned.

Currently, there are no plans for source-specific enhancements beyond routine model appending. 

As part of the next annual reporting,...

As a result of the ongoing review process, the inventory compiler plans to....

 
===== FAQs ===== 
(Freiwillig, nur falls relevant und sinnvoll. Ansonsten Punkt bitte weglassen.)

UND HIER NOCH EIN PAAR WIEDERKEHRENDE QUELLEN.

AGEB, 2023: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2030/?wpv-jahresbereich-bilanz=2021-2030, (Aufruf: 12.12.2023), Köln & Berlin, 2023)

BAFA, 2023: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2022_12.xlsx?__blob=publicationFile&v=4, Eschborn, 2023.)

Knörr et al. (2023b): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Aktualisierung des Modells TREMOD-Mobile Machinery (TREMOD MM) 2023, Heidelberg, 2023.)

EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook – 2023, https://www.eea.europa.eu//publications/emep-eea-guidebook-2023, Copenhagen, 2023.)