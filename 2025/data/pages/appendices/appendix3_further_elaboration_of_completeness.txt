====== Appendix 3 ======

Appendix 3 provides a further elaboration of completeness, uses of NE & IE and (potential) sources of air pollutant emissions excluded (where relevant).