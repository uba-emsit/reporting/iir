====== 2.D.3.d - Coating Application ======

===== Short Description =====

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.D.3.d                                |  T2      |  NS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|  NO<sub>x</sub>                         ^  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  |  TSP  |  BC  |  CO  |  Heavy Metals  |  POPs  |
|  NA                                     ^  L/T    |  NA              |  NA              |  NA                |  NA               |  NA   |  NA  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                 |||||||||||

This source category comprises NMVOC emissions from the use of solvent-based products of three major sub-categories: Decorative coating applications, industrial coating applications and other non-industrial paint application
The following product groups are taken into consideration:
== i)	Decorative coating applications: ==
  * **Application of paints and lacquers in Car repairing**
  * ***Professional application of paints and lacquers for Construction and Building** (emulsion paints for indoor application; silicate exterior paints; synthetic resin plasters / silicate; varnishes; primers and protection coatings; other coatings)
  * **Do-it-yourself application of paints and lacquers for Building** (emulsion paints for indoor application; silicate exterior paints; synthetic resin plasters / silicate; varnishes; primers and protection coatings; other coatings)
  * **Application of paints and lacquers for Wood surfaces** (wooden interiors, carpentry)

== ii)	Industrial coating applications ==
  * **Application of paints and lacquers for Manufacture of cars** (primers, fillers, top coat / clear lacquers)
  * **Application of paints and lacquers for Car Repairing of commercial vehicles**
  * **Application of paints and lacquers for Coil Coating**
  * **Application of paints and lacquers for Boat Building**
  * **Application of paints and lacquers for Wood surfaces** (furniture)
  * **Other industrial paint application** (such as paint spray, electrical appliances, mechanical engineering, automotive accessories, metal goods, wire enamel, synthetic materials, paper/foil)

== iii)	Other non-industrial paint application (marking paints, corrosion protection, other) ==

‘NMVOC’ is defined in accordance with the VOC definition found in the EC solvents directive. For purposes of the definition of solvents, the term ‘solvent use’ is also defined in accordance with the EC solvents directive. 

===== Method =====

== General procedure ==
NMVOC emissions are calculated in accordance with a product-consumption-oriented approach. 
In this approach, solvent-based products or solvents are allocated to the source category, and then the relevant NMVOC emissions are calculated from those solvent quantities via specific emission factors. Thus, the use of this method is possible with the following valid input figures for each product group:
  * Quantities of VOC-containing (pre-) products and agents used in the report year,
  * The VOC concentrations in these products (substances and preparations),
  * The relevant application and emission conditions (or the resulting specific emission factor).

The quantity of the solvent-based (pre-)product corresponds to the domestic consumption which is the sum of domestic production plus import minus export.

<WRAP center round info 55%>
EM<sub>NMVOC</sub> = domestic consumption of a certain product ∙ solvent content ∙ EF<sub>product</sub>
</WRAP>

The calculated NMVOC emissions of different product groups for a source category are then aggregated. 
The product / substance quantities used are determined at the product-group level with the help of production and foreign-trade statistics. Where possible, the so-determined domestic-consumption quantities are then further verified via cross-checking with industry statistics.

== Specific information ==
An emission factor of 95% was allocated to all open applications (e. g. all decorative coating applications). For installation-related industrial applications specific emission factors were assessed and applied.

===== Discussion of emission trends =====
== General Information ==

Since 1990, so the data, NMVOC emissions from use of solvents and solvent-containing products in general have decreased by nearly 60 %. The main emissions reductions have been achieved in the years since 1999. This successful reduction has occurred especially as a result of regulatory provisions such as the 31st Ordinance on the execution of the Federal Immissions Control Act (Ordinance on the limitation of emissions of volatile organic compounds due to the use of organic solvents in certain facilities – 31. BImSchV), the 2nd such ordinance (Ordinance on the limitation of emissions of highly volatile halogenated organic compounds – 2. BImSchV) and the TA Luft.

== Specific information ==

Since 1990, data of source category 2.D.3.d are recorded. Since 2000, a more detailed data collection procedure enables to follow the development of different applications, which altogether account for 35 - 47 % of total NMVOC emissions from solvent-based products.

For category 2D3d coating applications, there is a significant dip in the time series in 1994 that may relate to an over-estimation for the years until 1994. This difference between 1993 and 1994 has to be mainly linked to the enhancement of the emission calculation method as from 1996. Since then national production and foreign trade statistics has been used for the calculation of product and solvent consumption instead of expert judgements. However, a recalculation could only be done backwards to the year 1994 due to the unavailability of production and foreign trade statistics in the necessary differentiation before (German reunification).

The clear decrease in the NMVOC emissions in 2.D.3d has ended in 2015. The following seven applications caused major emissions and developments in category 2.D.3d: 
Wooden furniture, Mechanical engineering, Varnishes DIY, Varnishes professional, Wooden interiors, Manufacture of cars and Corrosion protection 
 
After the reunification of East and West Germany, the paints and coating application industries had economically good years. The emissions from 1990 to 1993 stayed on a high level. After 1993, this economic hype ended. In consequence, also the emissions declined.
A major reason for the decrease of overall emissions in this source category is the fulfillment of the Decopaint-Directive according to maximum solvent contents. The German "Blauer Engel" ("Blue Angel") environmental quality seal supported this development by certifying a range of products, including low-solvent paints and lacquers. 

===== Uncertainties =====

Relative Uncertainties for emissions for each product were obtained by error propagation and refer to the 95% confidence interval. 
  * __Domestic Consumption:__ For all values based on the official statistics, a relative uncertainty of ±10% for lacquers and of ±15% for thinners was applied. 
  * __Solvent content:__ For each product, a relative error at ±15% for lacquers was applied, but not exceeding 100% or falling below 0%.
  * __Emission factors:__ For each product, a relative error at ±15% was applied, but not exceeding 100% or falling below 0%. 
Hence the overall uncertainty of emissions caused by application of products of this source group is at least 40%.

===== Recalculations =====

Routinely the NMVOC emissions of the last reported year must be actualized in the next reporting cycle as the final data of the foreign trade statistics are regularly only available after the publication of the respective reporting year has been completed. \\

<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2021**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements ===== 
At the moment, no category-specific improvements are planned.