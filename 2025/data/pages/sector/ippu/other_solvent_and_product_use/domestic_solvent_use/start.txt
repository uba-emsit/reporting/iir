====== 2.D.3.a - Domestic Solvent Use, including Fungicides ======

===== Short description =====

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.D.3.a                                |  T2      |  NS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|  NO<sub>x</sub>                         ^  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  |  TSP  |  BC  |  CO  |  Heavy Metals  |  POPs  |
|  NA                                     ^  L/-    |  NA              |  NA              |  NA                |  NA               |  NA   |  NA  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                 |||||||||||

The following product groups are taken into consideration:

== i)	Domestic solvent use ==
  * **Soaps**
  * **Laundry detergents, dishwashing detergents and cleaning products** (fabric softeners; universal detergents; washing agents; auxiliary washing preparations; dishwashing liquids; detergents for dishwashers; floor detergents; carpet shampooers; car cleaning shampoos; glass cleaners; WC cleaners)
  * **Care products for footwear, leather articles, furniture, floors and cars**
  * **Polishing agents** (for metal)
  * **Deodorizers** (for rooms and others)
  * **Perfumes** (including after shaves; eau de toilette, perfumes)
  * **Cosmetic and make-up preparations** (make-up; hand care products; nail care products; pedicure products; face cleanser;  suntan lotions; face and body care products and others)
  * **Shampoos and hair care products** (shampoos; preparations for permanent waving or straightening; hair sprays; lotions and brilliantines; toning shampoos; hair colouring products; hair bleaching and other)
  * **Other personal care products** (shaving creams; personal deodorants and antiperspirants; bath essences; depilatories, deodorants, preparations for intimate hygiene and other)
  * **Antifreeze agents for cars**

== ii)	Domestic use of pharmaceutical products ==

‘NMVOC’ is defined in accordance with the VOC definition found in the EC solvents directive. For purposes of the definition of solvents, the term ‘solvent use’ is also defined in accordance with the EC solvents directive.
 
===== Method =====

== General procedure ==
NMVOC emissions are calculated in accordance with a product-consumption-oriented approach. 

In this approach, solvent-based products or solvents are allocated to the source category, and then the relevant NMVOC emissions are calculated from those solvent quantities via specific emission factors. Thus, the use of this method is possible with the following valid input figures for each product group:

  * Quantities of VOC-containing (pre-) products and agents used in the report year,
  * The VOC concentrations in these products (substances and preparations),
  * The relevant application and emission conditions (or the resulting specific emission factor).


The quantity of the solvent-based (pre-)product corresponds to the domestic consumption which is the sum of domestic production plus import minus export.


<WRAP center round info 60%>
NMVOC Emission = domestic consumption of a certain product * solvent content * specific emission factor
</WRAP>

The calculated NMVOC emissions of different product groups for a source category are then aggregated. 

The product / substance quantities used are determined at the product-group level with the help of production and foreign-trade statistics. Where possible, the so-determined domestic-consumption quantities are then further verified via cross-checking with industry statistics.

== Specific information ==
Calculation of domestic consumption was based on:
  * the German production statistics and external trade statistics for the subgroup “Domestic solvent use”. 
  * turnover values of pharmaceuticals produced in Germany for the subgroup “Domestic use of pharmaceutical products”, 


Solvent contents for this product group corresponds to personal information from industrial associations and German literature[(Berner, P.: Maßnahmen zur Minderung der Emissionen flüchtiger organischer Verbindungen aus der Lackanwendung - Vergleich zwischen Abluftreinigung und primären Maßnahmen am Beispiel Baden-Württembergs, Stuttgart: Institut für Energiewirtschaft und Rationelle Energieanwendung, Universität Stuttgart, Forschungsbericht Band 42, 1996 - Dissertation, Stuttgart, 1996)].
   
For alcohol-based cleaning detergents an emission factor of 3% was assumed for calculations [(Wooley, J., Nazaroff, W.N., Hodgon, A.T.: Release of ethanol to the atmosphere during use of consumer cleaning products, J. Air Waste Manage. Assoc. 40, 1114-1120, Berkeley, California, 1990.)].
For all other products of this source category (e.g. hair spray, after shave, perfumes), an emission factor of 95% was applied.

===== Discussion of emission trends =====

=== General information ===

Since 1990, so the data, NMVOC emissions from use of solvents and solvent-containing products in general have decreased by nearly 60%. The main emissions reductions have been achieved in the years since 1999. This successful reduction has occurred especially as a result of regulatory provisions such as the 31st Ordinance on the execution of the Federal Immissions Control Act (Ordinance on the limitation of emissions of volatile organic compounds due to the use of organic solvents in certain facilities – 31. BImSchV) [(31. BImSchV: Ordinance on the limitation of emissions of volatile organic compounds due to the use of organic solvents in certain facilities; https://www.gesetze-im-internet.de/bimschv_31/index.html)], the 2nd such ordinance (Ordinance on the limitation of emissions of highly volatile halogenated organic compounds – 2. BImSchV) [(2. BImSchV: Ordinance on the limitation of emissions of highly volatile halogenated organic compounds; https://www.gesetze-im-internet.de/bimschv_2_1990/index.html)] and the TA Luft.

=== Specific information ===

Until 1999, data of the present source categories 2.D.3.a, 2.D.3.h and 2.D.3.i were treated as one source group. Since 2000, a more detailed data collection enables to follow the development of source group 2.D.3.a, which accounts for about 10 to 17 per cent of total NMVOC emissions from solvent-based products.
For more than 20 years there have not been observed a decreasing trend. 

The following four product groups account for 44 to 69 per cent of 2.D.3.a emissions: 
  * hair sprays, 
  * antifreeze agents for cars, 
  * eau de toilette 
  * pharmaceutical products.

As emission factors and solvent contents largely remained robust since 2000, domestic consumption of products caused visible changes in NMVOC emissions. For instance, the annual amount of used ‘antifreeze agents for cars’ primarily depends on the weather situation of the specific year. As antifreeze agents have one of the highest domestic consumption values of the category ‘domestic solvent use’, they significantly effect the final value of emitted NMVOC of this category. The increase in emissions in 2010 can be explained by a long-lasting cold season.
For few product groups, such as personal deodorants, antiperspirants and car surface protectants, domestic consumption and hence NMVOC emissions show a declining trend compared to 2005. 
However, for many product groups, such as soaps, hair sprays, eau de toilette and pharmaceutical products domestic consumption and emissions increased for in the same period.

===== Uncertainties =====

Uncertainties for emissions for each product were obtained by error propagation and refer to the 95% confidence interval. 

For the majority of activity data (domestic consumption) based on official statistics, a relative uncertainty of ±10% has been applied with the complex value for ‘antifreeze agents for cars’ with ±20% being the only exception.

Regarding the __solvent content__, for each product a relative error at ±15% was applied, but not exceeding 100% or falling below 0%.

As for __emission factors__, for each product a relative error at ±15% was applied, but not exceeding 100% or falling below 0% again with the exception for ‘antifreeze agents for cars’ at ±25% based on expert estimation.

Hence the overall uncertainty of emissions caused by application of products of this source group is 40% with exception of ‘antifreeze agents for cars’ at 60%.

===== Recalculations =====

Routinely the NMVOC emissions of the last reported year must be updated in the next reporting cycle in accordance with the then final data of the foreign trade statistics.


<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2021**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

At the moment, no category-specific improvements are planned.
