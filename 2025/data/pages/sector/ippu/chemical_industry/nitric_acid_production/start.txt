====== 2.B.2 - Nitric Acid Production ======

===== Short description =====


^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.B.2                                  |  T2      |  PS  |  D   |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

^  NO<sub>x</sub>                         |  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  | TSP  |  BC  |  CO  |  Heavy Metals  |  POPs  |
^  -/-                                    |  NA     |  NA              |  NA              |  NA                |  NA               |  NA  |  NA  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                |||||||||||

During the production of nitric acid (HNO<sub>3</sub>), nitrogen oxide is produced unintentionally in a secondary reaction during the catalytic oxidation of ammonia (NH<sub>3</sub>). HNO<sub>3</sub> production occurs in two process stages:
  * Oxidation of NH<sub>3</sub> to NO and
  * Conversion of NO to NO<sub>2</sub> and absorption in H<sub>2</sub>O.


Details of the process are outlined below:

{{ :sector:ippu:chemical_industry:nitric_acid_production:nitricacidproduction.png?nolink&200|}}

**Catalytic oxidation of ammonia**

A mixture of ammonia and air at a ratio of 1:9 is oxidised, in the presence of a platinum catalyst alloyed with rhodium and/or palladium, at a temperature of between 800 and 950 °C. The reaction according to the Oswald process is as follows:
 
4 NH<sub>3</sub> + 5 O<sub>2</sub> --> 4 NO + 6 H<sub>2</sub>O

Simultaneously, nitrogen, nitrous oxide and water are formed by the following undesired secondary reactions:

4 NH<sub>3</sub> + 3 O<sub>2</sub> --> 2 N<sub>2</sub> + 6 H<sub>2</sub>O

4 NH<sub>3</sub> + 4 O<sub>2</sub> --> 2 N<sub>2</sub>O + 6 H<sub>2</sub>O

All three oxidation reactions are exothermic. Heat may be recovered to produce steam for the process and for export to other plants and/or to preheat the residual gas. The reaction water is condensed in a cooling condenser, during the cooling of the reaction gases, and is then conveyed into the absorption column.

===== Method =====
In Germany, there are currently nine nitric acid plants.
==== Activity data ====

As this source category is a key category for N<sub>2</sub>O, plant specific activity data is collected here according to the IPCC guidelines.

This data is made available basically via a co-operation agreement with the nitric acid producers and the IVA (Industrieverband Agrar). As the data provided by the producers has to be treated as confidential, it is anonymised by the IVA before submitting it to the UBA, with one producer as exception who is delivering its data directly to the UBA. After checking this specific data, it is merged with that provided by the IVA.

According to the IVA, catalytic reduction is used as an abatement method in some of the plants. 

==== Emission factors ====

Different T2 default NO<sub>x</sub> emission factors based on different technology types and abatement systems are used from the EEA Emission Inventory Guidebook 2019 (EF for medium and high pressure processes and for catalytic reduction of low, medium and high pressure process)[(EEA, Oct 2019: : EMEP/EEA air pollutant emission inventory guidebook 2019, Part B: sectoral guidance chapters, 2.B Chemical industry: pp.21-23, Table 3.11, Table 3.12 and Table 3.14.)]. The applied emissions factors are listed in **Table 1**.

__Table 1: Tier 2 emission factors of NO<sub>x</sub> for source category 2.B.2 Nitric acid production, in [kg/t]__
^  EF   ^  Process                                                    ^
|  7.5  | medium pressure process                                     |
|  3    | high pressure process                                       |
|  0.4  | low, medium and high pressure process, catalytic reduction  |


===== Recalculations =====

<WRAP center round info 65%>
With **all input data remaining unrevised**, no recalculations were made compared to the previous submission.
</WRAP>


===== Planned improvements =====

No category-specific improvements are planned. 



------

