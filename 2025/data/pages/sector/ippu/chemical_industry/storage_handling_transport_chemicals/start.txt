====== 2.B.10.b - Storage, Handling and Transport of Chemical Products ======

^ Category Code  ^  Method                                                                           ||||^  AD                                        ||||^  EF                              |||||
| 2.B.10.b       |  T2                                                                               |||||  NS                                        |||||  CS                              |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO  ^  Pb  ^  Cd  ^  Hg  ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -               |  -/-    |  -               |  -               |  -                 |  -                |  -    |  -   |  -   |  -   |  -   |  -   |  -     |  -    |  -    |
{{page>general:Misc:LegendEIT:start}}
\\
=====Short description=====
Emissions from storage cover all refinery products. According to the EMEP guidebook, fuel-related emissions are reported under 1.B.2. (see Chapter 3., 1.B.2a Oil ). Emissions from other mineral oil products that are not used as fuel (like naphtha, methanol etc.) are reported separately here.
=====Method=====
A distinction of mineral oil products is only made between fuels and naphtha. Based on the individual annual amount for these two subcategories, a split factor is calculated. 
====Activity data====
The annual production of naphtha through the time series is listed in **Table 1** below.

__Table 1: Annual naphtha production, in [kt]__
^ 1990  |  11546.09  |
^ 1991  |  12566.84  |
^ 1992  |  12705.24  |
^ 1993  |  12986.79  |
^ 1994  |  13393.21  |
^ 1995  |  13369.77  |
^ 1996  |  13430.44  |
^ 1997  |  15070.53  |
^ 1998  |  15959.62  |
^ 1999  |  15810.00  |
^ 2000  |  16091.47  |
^ 2001  |  16736.24  |
^ 2002  |  16660.01  |
^ 2003  |  16981.74  |
^ 2004  |  17895.30  |
^ 2005  |  18024.31  |
^ 2006  |  17016.65  |
^ 2007  |  16708.99  |
^ 2008  |  15744.92  |
^ 2009  |  15236.77  |
^ 2010  |  16610.69  |
^ 2011  |  15708.84  |
^ 2012  |  15770.00  |
^ 2013  |  16213.82  |
^ 2014  |  17065.99  |
^ 2015  |  16331.02  |
^ 2016  |  15797.92  |
^ 2017  |  15605.03  |
^ 2018  |  11439.19  |
^ 2019  |  11263.72  |
^ 2020  |  11804.49  |
^ 2021  |  13686.27  |
^ 2022  |  12669.02  |
^ 2023  |  10580.86  |
====Emission factors====
The emission factor used for NMVOC was determined by evaluating emission declarations from refineries for the period 2004 to 2016, in the framework of a research project (Bender & von Müller, 2019)[(Bender, M., & von Müller, G. (2019). Emissionsfaktoren zu Raffinerien für die nationale Emissionsberichterstattung (FKZ 3716 41 107 0).)]. Since no data is available for earlier years, the emission factor obtained this way is applied for all years as of 1990.

__Table 2: Emission factor of NMVOC from storage of petroleum products, in [g/m<sup>3</sup>]__
^                                                                                         ^  EF   ^
| Storage of liquid petroleum products in tank-storage facilities outside of refineries   |  100  |
| Storage of gaseous petroleum products in tank-storage facilities outside of refineries  |  500  |

=====Recalculations======
No recalculations have been carried out compared to last year's submission.

<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2020**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

=====Planned improvements=====
An ongoing research project estimates emissions from storage and cleaning of tanks for oil and oil products - results are planned to be implemented into the inventory in 2025/26.