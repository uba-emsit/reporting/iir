====== 2.C.7.b - Nickel Production ======

In subcategory //NFR 2.C.7.b - Nickel production// the TSP, SO<sub>2</sub> and Ni emissions from nickel mining are reported. Reporting only covers the year 1990 because in 1991 nickel mining stopped in Germany.

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.C.7.b                                |  T1      |  AS  |  D   |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|              |  NO<sub>x</sub>                         |  NMVOC  ^  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  ^  TSP  |  BC  |  CO  |  Pb  |  Cd  |  Hg  |  As  |  Cu  |  Cr  ^  Ni   |  Se  |  Zn  | PCDD/F  |  PAHs  |  HCB  |
| 1990-1991:   |  NE                                     |  NE     |  -/-             |  NE              |  NR                |  NR               |  -/-  |  NR  |  NE  |  NE  |  NE  |  NE  |  NE  |  NE  |  NE  |  -/-  |  NE  |  NE  |  NE     |  NE    |  NE   |
| as of 1992:  |  NO                                                                                                                                                                                                                           |||||||||||||||||||||
|              |  {{page>general:Misc:Legendkca:start}}                                                                                                                                                                                        |||||||||||||||||||||

===== Method =====

=== Activity data ===

The yearly production figure was taken from the annual statistical report of the US Geological Survey (USGS) for non-ferrous metals<sup>**[[#Bibliography| [Lit. 1]]]**</sup>.


=== Emission factors ===

The emission factors are the default values according to the EMEP/EEA air pollutant emission inventory guidebook 2023<sup>**[[#Bibliography| [Lit. 2]]]**</sup>.

__Table 1: applied Tier1 emission factors, in [kg/t]__

|                 ^  EF     ^
^ SO<sub>2</sub>  |  18     |
^ TSP             |  0.3    |
^ Ni              |  0.025  |

=== Uncertainties ===

===== Recalculations =====

<WRAP center round info 60%>
There is no primary nickel production in Germany. Therefore it is **not necessary to update** the activity data and emission factors.
</WRAP>

===== Planned improvements =====

There are no category-specific improvements planned.


[(USGS> US Geological Survey - https://www.usgs.gov/)]
[(EEA> EMEP/EEA air pollutant emission inventory guidebook 2019; https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/2-industrial-processes/2-c-metal-production/2-c-7-b-nickel/view)]

 \\ 
 \\ 
------
===== Bibliography =====

**Lit.  1:** US Geological Survey https://www.usgs.gov \\
**Lit.  2:** EMEP/EEA air pollutant emission inventory guidebook 2023 [[https://www.eea.europa.eu/publications/emep-eea-guidebook-2023/part-b-sectoral-guidance-chapters/2-industrial-processes-and-product-use/2-c-metal-production/2-c-7-b-nickel/view|https://www.eea.europa.eu]]
