====== 2.A.5.b - Construction and Demolition ======

===== Short description =====

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.A.5.b                                |  T1/T2   |  NS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|  NO<sub>x</sub>                         |  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^ TSP   |  BC  |  CO  |  Heavy Metals  |  POPs  |
|  NA                                     |  NA     |  NA              |  NA              |  -/-               ^  L/-              ^  L/-  |  NA  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                 |||||||||||


\\
With respect to particle emissions, construction is the second main emissions source in the Mineral industries.

===== Methodology =====

Since the last update of the UNECE Guidebook, a Tier 1 method is applied to estimate particulate matter emissions. The T1 GB method is used by us with various adaptations to national conditions, so this is already higher tier, perhaps as T1/T2.

The approach for uncontrolled fugitive emissions for this source category was adapted for national circumstances within a research Project (Umweltbundesamt, 2016) [(Umweltbundesamt, 2016: Development of Methods for the Generation of Emission Data for Air Pollutants from Building Activity and Construction Zones, Dessau-Roßlau, 2016, https://research.ebsco.com/linkprocessor/plink?id=46c9c9e5-c6f9-3229-b7af-6585eb409115)], partly considered exiting control techniques. As a result, the information of the statistics is combined with modified default emission factors for TSP and PM.

==== Activity data ====

Activity data are determined taking into account figures for various construction activities. Data is based on production statistics (national statistics). According to the method used, figures of area of land affected by construction activities per building were concluded from statistical data and multiplied with emission factors, as explained below. The common uncertainty of 3% for national statistics could be increased as a result of this  calculation, but the effect is not estimated at the moment.

==== Emission factors ====

The emission factors used are results of Adaptation of UNECE-Defaults (EEA, 2016) [(EEA2023>EEA, 2023: EEA Report No 03/2023 EMEP EEA air pollutant emission inventory guidebook 2023, Copenhagen, 2023; https://www.eea.europa.eu/publications/emep-eea-guidebook-2023/part-b-sectoral-guidance-chapters/2-industrial-processes-and-product-use/2-a-mineral-products/2-a-5-b-construction/view)].

__Table 1: Overview of apllied emission factors, in [kg / m<sup>2</sup> * y, for roads in tons / km<sup>2</sup> * y]__
^ Kind of building              ^  Pollutant         ^  EF value  ^  EF trend  ^
| single and two-family houses  |  TSP               |  0.0638    |  constant  |
| :::                           |  PM<sub>10</sub>   |  0.0191    |  constant  |
| :::                           |  PM<sub>2.5</sub>  |  0.0019    |  constant  |
| apartment buildings           |  TSP               |  0.329     |  constant  |
| :::                           |  PM<sub>10</sub>   |  0.099     |  constant  |
| :::                           |  PM<sub>2.5</sub>  |  0.0099    |  constant  |
| non-residential               |  TSP               |  0.631     |  constant  |
| :::                           |  PM<sub>10</sub>   |  0.189     |  constant  |
| :::                           |  PM<sub>2.5</sub>  |  0.0189    |  constant  |
| roads                         |  TSP               |  1,674     |  constant  |
| :::                           |  PM<sub>10</sub>   |  502       |  constant  |
| :::                           |  PM<sub>2.5</sub>  |  50.2      |  constant  |

Several further assumptions were necessary to use the formula of the Guidebook:
| EM = EF * B * f * m     ||

The EF is adapted with Moisture Level Correction factor and Silt Content Correction factor in all cases, both 0.20 and 2.22. The assumption about the duration of the construction activity uses the Default values (EEA, 2023)[(EEA2023)]:
^ Type of building                                   ^^  Estimated duration (year)      ^^
| Construction of houses (single and two family)     ||  0.5 (6 months)                 ||
| Construction of apartments (all types)             ||  0.75 (9 months)                ||
| Non-residential construction                       ||  0.83 (10 months)               ||
| Road construction                                  ||  1 (12 months)                  ||

AD is a result of multiplying B the number of houses constructed and f the conversion factor.

===== Trends in emissions =====
All trends in emissions as product of EF and AD correspond to trends of construction activities.

[{{:sector:ippu:mineral_industry:EM_2A5b_since_1990.PNG|**Emission trends in NFR 2.A.5.b**}}]

Notes to PCB emissions of old buildings are given in chapter [[sector:ippu:pops_and_hm_consumption:start|2.K - Consumption of POPs and Heavy Metals]].

===== Recalculations =====

<WRAP center round info 65%>
With **all input data remaining unrevised**, no recalculations were made compared to the previous submission.
</WRAP>


=====Planned improvements =====
At the moment, no category-specific improvements are planned.

===== FAQs =====
//**Where can I find emissions estimation of demolition activities?**// - Demolishing without any significant new construction is not covered and there are no other emission factors available for demolition activities only. Nevertheless you can find Information about emissions from 
[[sector:waste:other_waste:building_and_car_fires:start|5.E.2 - Other Waste: Building Fires]]. 

//**Why do German EFs differ from EEA defaults?**// - It has to do with the default 50% reduction for non-residential buildings and roads (as a result of wetting unpaved temporary roads) that is assumed in the calculations for Germany. This is also already accounted for in the EPA emission factors. It is a result of a control measure that is nearly always taken but in principle optional. In the Guidebook a 50% reduction is advised.

