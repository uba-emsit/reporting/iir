====== 2.I - Wood Processing ======

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  2.I                                    |  T1      |  NS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO  ^  Heavy Metals  ^  POPs  ^
|  NA                                     |  -/-    |  NA              |  NA              |  -/-               |  -/-              |  -/-  |  NE  |  IE  |  NA            |  NA    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                 |||||||||||

This industrial sector essentially includes the production of chipboards. It's of minor meaning with view on emissions.

Chipboards is made from wood chips with added binders under the influence of pressure and heat. The main source of NMVOC emissions are the wood chips used, from which NMVOCs are emitted during drying due to the effect of heat. NMVOC can also be emitted from the wood and the binder during the pressing process. Chipboards are produced in about 20 plants in Germany. The chipboard industry is dominated by larger companies.

=====Activity data=====

The activity data are taken from the national statistics [(Federal Statistical Office, reporting numbers until 2018: 1621 13 131; 1621 13 133; 1621 13 163; 1621 13 500, reporting numbers from 2019: 162112001, 162112002, 162112003, 162113160, 162114190, 162114500, converted and summarised in tonnes)], but must be converted from volume to mass data for further use.

__Table 1: Produced amounts, in 10<sup>6</sup> metric tonnes [t]__
^  2014   ^  2015   ^  2016  ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^  2022   ^
|  4.446  |  4.402  |  4.56  |  4.703  |  4.322  |  4.489  |  4.431  |  4.776  |  4.366  |

=====Emissions factors=====

The emission factors of 0.9 kg/t for NMVOC and 0.3 kg/t for PM were estimated on the basis of expert judgements.

===== Recalculations =====

<WRAP center round info 65%>
With **activity data and emission factors remaining unrevised**, no recalculations were carried out compared to the previous submission.
</WRAP>
