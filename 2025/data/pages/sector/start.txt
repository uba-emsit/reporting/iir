====== SECTROR CHAPTERS - OVERVIEW ======

The folling table provides a comprehensive list of the separate sector chapters included in this Informative Inventory Report.

^ [[Sector:Energy:Start| NFR 1 - ENERGY ]]                                                                                                                                                                 ^
| **//[[Sector:Energy:Fuel Combustion:Start| 1.A - FUEL COMBUSTION ACTIVITIES]]//**                                                                                                                        |
| **[[Sector:Energy:Fuel Combustion:Energy Industries:Start| 1.A.1 - Energy Industries]]**                                                                                                                 |
| [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| 1.A.1.a - Public electricity and heat production]]                                                             |
| [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| 1.A.1.b - Petroleum refining]]                                                                                                     |
| [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| 1.A.1.c - Manufacture of solid fuels and other energy industries]]                             |
| **[[Sector:Energy:Fuel Combustion:Industry:Start| 1.A.2 - Fuel Combustion Activities in Industries and Construction]]**                                                                                  |
| [[sector:energy:fuel_combustion:industry:iron_and_steel| 1.A.2.a - Stationary combustion in manufacturing industries and construction: Iron and Steel ]]                                                 |
| [[sector:energy:fuel_combustion:industry:non-ferrous_metals| 1.A.2.b - Stationary combustion in manufacturing industries and construction: Non-ferrous Metals ]]                                         |
| [[sector:energy:fuel_combustion:industry:chemicals| 1.A.2.c - Stationary combustion in manufacturing industries and construction: Chemicals ]]                                                           |
| [[sector:energy:fuel_combustion:industry:pulp_paper_and_print| 1.A.2.d - Stationary combustion in manufacturing industries and construction: Pulp, Paper and Print ]]                                    |
| [[sector:energy:fuel_combustion:industry:food_processing_beverages_and_tobacco| 1.A.2.e - Stationary combustion in manufacturing industries and construction: Food Processing, Beverages and Tobacco ]]  |
| [[sector:energy:fuel_combustion:industry:non-metallic_minerals| 1.A.2.f - Stationary combustion in manufacturing industries and construction: Non-Metallic Minerals ]]                                   |
| //[[sector:energy:fuel_combustion:industry:other:start| 1.A.2.g viii - Stationary Combustion in Manufacturing Industries & Construction: Other  ]]//                                                     |
| //[[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| 1.A.2.g vii -  Mobile Combustion in Manufacturing Industries & Construction ]]//                  |
| **[[Sector:Energy:Fuel Combustion:Transport:Start| 1.A.3 - Transport]]**                                                                                                                                 |
| [[sector:energy:fuel_combustion:transport:civil_aviation:start| 1.A.3.a - Civil Aviation ]]                                                                                                              |
| //[[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| 1.A.3.a i (i) - International Civil Aviation: LTO ]]//                                                    |
| //[[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| 1.A.3.a ii (i) - Domestic Civil Aviation: LTO ]]//                                                             |
| //[[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_Cruise| 1.A.3.a i (ii) - International Civil Aviation: Cruise ]]//                                             |
| //[[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_Cruise| 1.A.3.a ii (ii) - Domestic Civil Aviation: Cruise ]]//                                                      |
| [[sector:energy:fuel_combustion:transport:road_transport:start| 1.A.3.b - Road Transport ]]                                                                                                              |
| //[[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:start| 1.A.3.bi-iv - Emissions from Fuel Combustion in Road Vehicles (Overview)]]//           |
| //[[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| 1.A.3.b i - Road Transport: Passenger Cars ]]//                               |
| //[[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| 1.A.3.b ii - Road Transport: Light duty vehicles ]]//                    |
| //[[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| 1.A.3.b iii - Road Transport: Heavy duty vehicles ]]//                   |
| //[[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| 1.A.3.b iv - Road Transport: Mopeds & Motorcycles ]]//                |
| //[[sector:energy:fuel_combustion:transport:road_transport:fugitive_emissions_from_gasoline_evaporation| 1.A.3.b v - Gasoline Evaporation ]]//                                                           |
| //[[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:start| 1.A.3.b vi-vii - Emissions from Wear and Abrasion in Road Transport (Overview) ]]//              |
| //[[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:tyre_and_brake_wear|1.A.3.b vi - Road Transport: Tyre and Brake Wear ]]//                               |
| //[[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:road_abrasion| 1.A.3.b vii - Road Transport: Road Abrasion ]]//                                         |
| [[sector:energy:fuel_combustion:transport:railways:start| 1.A.3.c - Railways ]]                                                                                                                          |
| [[sector:energy:fuel_combustion:transport:navigation:start| 1.A.3.d - Navigation ]]                                                                                                                      |
| //[[sector:energy:fuel_combustion:transport:navigation:international_maritime_navigation| 1.A.3.d i - International Maritime Navigation ]]//                                                             |
| //[[sector:energy:fuel_combustion:transport:navigation:international_inland_waterways| 1.A.3.d i (ii) - International Inland Navigation ]]//                                                             |
| //[[sector:energy:fuel_combustion:transport:navigation:national_navigation| 1.A.3.d ii - National Navigation ]]//                                                                                        |
| [[sector:energy:fuel_combustion:transport:other_transport:start| 1.A.3.e - Other Transport ]]                                                                                                            |
| //[[sector:energy:fuel_combustion:transport:other_transport:pipeline_transport| 1.A.3.e i - Pipeline Transport ]]//                                                                                      |
| **[[sector:energy:fuel_combustion:small_combustion:start|1.A.4 - Small Combustion]]**                                                                                                                    |
| //[[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| 1.A.4.a i - Commercial and Institutional - Stationary Combustion ]]//                           |
| //[[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| 1.A.4.b i - Residential - Stationary Combustion ]]//                                                         |
| //[[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| 1.A.4.c i - Agriculture, Forestry, Fishing - Stationary Combustion ]]//                     |
| //[[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| 1.A.4.a ii - Commercial / Institutional: Mobile ]]//                                            |
| //[[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| 1.A.4.b ii - Residential: Household and Gardening: Mobile ]]//                                                   |
| //[[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| 1.A.4.c ii - Agriculture/Forestry/Fishing: Off-road Vehicles and Other Machinery ]]//               |
| //[[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| 1.A.4.c iii - Agriculture/Forestry/Fishing: National Fishing ]]//                                                    |
| **[[sector:energy:fuel_combustion:other_including_military:start|1.A.5 - Other (including Military)]]**                                                                                                  |
| [[sector:energy:fuel_combustion:other_including_military:stationary_fuel_combustion_in_military_facilities| 1.A.5.a - Other: Stationary (including Military)]]                                           |
| [[sector:energy:fuel_combustion:other_including_military:military_transport| 1.A.5.b - Other: Mobile (including Military) ]]                                                                             |
| **//[[Sector:Energy:Fugitive:Start| 1.B - FUGITIVE EMISSIONS FROM FUELS]]//**                                                                                                                            |
| [[Sector:Energy:Fugitive:Solid Fuels:Start| 1.B.1 - Solid Fuels]]                                                                                                                                        |
| [[sector:energy:fugitive:oil:start| 1.B.2.a - Oil ]]                                                                                                                                                     |
| [[sector:energy:fugitive:gas:start| 1.B.2.b - Natural Gas ]]                                                                                                                                             |
| [[sector:energy:fugitive:flaring:start| 1.B.2.c - Flaring ]]                                                                                                                                             |
|                                                                                                                                                                                                          |
^ [[Sector:IPPU:Start| NFR 2 - INDUSTRIAL PROCESSES & PRODUCT USE (IPPU)]]                                                                                                                                 ^
| **//[[Sector:IPPU:Mineral_Industry:Start| 2.A - MINERAL INDUSTRY - Overview ]]//**                                                                                                                       |
| [[Sector:IPPU:Mineral_Industry:Cement_Production:Start| 2.A.1 - Cement Production]]                                                                                                                      |
| [[Sector:IPPU:Mineral_Industry:Lime_Production:Start| 2.A.2 - Lime Production]]                                                                                                                          |
| [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| 2.A.3 - Glass Production]]                                                                                                                        |
| [[Sector:IPPU:Mineral_Industry:Quarrying_Mining:Start| 2.A.5.a - Quarrying and Mining of Minerals other than Coal]]                                                                                      |
| [[Sector:IPPU:Mineral_Industry:Construction_Demolition:Start| 2.A.5.b - Construction and Demolition]]                                                                                                    |
| [[Sector:IPPU:Mineral_Industry:Storage_Handling_Transport_Minerals:Start| 2.A.5.c - Storage, Handling and Transport of Mineral Products]]                                                                |
| [[Sector:IPPU:Mineral_Industry:Other_Mineral_Products:Start| 2.A.6 - Other Mineral Products]]                                                                                                            |
| //**[[Sector:IPPU:Chemical_Industry:Start| 2.B - CHEMICAL INDUSTRY ]]**//                                                                                                                                |
| [[Sector:IPPU:Chemical_Industry:Ammonia_Production:Start| 2.B.1 - Ammonia Production]]                                                                                                                   |
| [[Sector:IPPU:Chemical_Industry:Nitric_Acid_Production:Start| 2.B.2 - Nitric Acid Production]]                                                                                                           |
| [[Sector:IPPU:Chemical_Industry:Adipic_Acid_Production:Start| 2.B.3 - Adipic Acid Production]]                                                                                                           |
| [[Sector:IPPU:Chemical_Industry:Carbide_Production:Start| 2.B.5 - Carbide Production]]                                                                                                                   |
| [[Sector:IPPU:Chemical_Industry:Titanium_Dioxide Production:Start| 2.B.6 - Titanium Dioxide Production]]                                                                                                 |
| [[Sector:IPPU:Chemical_Industry:Soda_Ash_Production:Start| 2.B.7 - Soda Ash Production]]                                                                                                                 |
| [[Sector:IPPU:Chemical_Industry:Other:Start| 2.B.10.a - Other]]                                                                                                                                          |
| [[Sector:IPPU:Chemical_Industry:Storage_Handling_Transport_Chemicals:Start| 2.B.10.b - Storage, Handling and Transport of Chemical Products]]                                                            |
| //**[[Sector:IPPU:Metal_Production:Start| 2.C - METAL PRODUCTION ]]**//                                                                                                                                  |
| [[Sector:IPPU:Metal_Production:Iron_And_Steel_Production:Start| 2.C.1 - Iron and Steel Production]]                                                                                                      |
| [[Sector:IPPU:Metal_Production:Ferroalloys_Production:Start| 2.C.2 - Ferroalloys Production]]                                                                                                            |
| [[Sector:IPPU:Metal_Production:Aluminum_Production:Start| 2.C.3 - Aluminum Production]]                                                                                                                  |
| [[Sector:IPPU:Metal_Production:Magnesium_Production:Start| 2.C.4 - Magnesium Production]]                                                                                                                |
| [[Sector:IPPU:Metal_Production:Lead_Production:Start| 2.C.5 - Lead Production]]                                                                                                                          |
| [[Sector:IPPU:Metal_Production:Zinc_Production:Start| 2.C.6 - Zinc Production]]                                                                                                                          |
| [[Sector:IPPU:Metal_Production:Copper_Production:Start| 2.C.7.a - Copper Production]]                                                                                                                    |
| [[Sector:IPPU:Metal_Production:Nickel_Production:Start| 2.C.7.b - Nickel Production]]                                                                                                                    |
| [[Sector:IPPU:Metal_Production:Other_Metal_Production:Start| 2.C.7.c - Other Metal_Production]]                                                                                                          |
| [[Sector:IPPU:Metal_Production:Storage_Handling_Transport_Metals:Start| 2.C.7.d - Storage, Handling and Transport of Metal Products]]                                                                    |
| //**[[Sector:IPPU:Other_Solvent_And_Product_Use:Start| 2.D - OTHER SOLVENT & PRODUCT USE]]**//                                                                                                           |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Domestic_Solvent_Use:Start| 2.D.3.a - Domestic Solvent Use including fungicides]]                                                                            |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Road_Paving:Start| 2.D.3.b - Road Paving with Asphalt]]                                                                                                      |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Asphalt_Roofing:Start| 2.D.3.c - Asphalt Roofing]]                                                                                                           |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Coating_Applications:Start| 2.D.3.d - Coating Applications]]                                                                                                 |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Degreasing:Start| 2.D.3.e - Degreasing]]                                                                                                                     |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Dry_Cleaning:Start| 2.D.3.f - Dry Cleaning]]                                                                                                                 |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Chemical_Products:Start| 2.D.3.g - Chemical Products]]                                                                                                       |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Printing:Start| 2.D.3.h - Printing]]                                                                                                                         |
| [[Sector:IPPU:Other_Solvent_And_Product_Use:Other_Solvent_Use:Start| 2.D.3.i - Other Solvent Use]]                                                                                                       |
| **//[[Sector:IPPU:Other_Product_Use:Start| 2.G - OTHER PRODUCT USE]]//**                                                                                                                                 |
| [[Sector:IPPU:Other_Product_Use:Fireworks:Start| 2.G.4 - Use of Fireworks]]                                                                                                                              |
| [[Sector:IPPU:Other_Product_Use:Tobacco:Start| 2.G.4 - Use of Tobacco]]                                                                                                                                  |
| [[Sector:IPPU:Other_Product_Use:Charcoal:Start| 2.G.4 - Charcoal]]                                                                                                                                       |
| **//[[Sector:IPPU:Pulp_Paper_Food:Start| 2.H - Other (Pulp & Paper, Food)]]//**                                                                                                                          |
| [[Sector:IPPU:Pulp_Paper_Food:Pulp_And_Paper_Industry:Start| 2.H.1 - Pulp and Paper Industry]]                                                                                                           |
| [[Sector:IPPU:Pulp_Paper_Food:Food_And_Beverages:Start| 2.H.2 - Food and Beverages Industry]]                                                                                                            |
| [[Sector:IPPU:Pulp_Paper_Food:Other_Industrial_Processes:Start| 2.H.3 - Other Industrial Processes]]                                                                                                     |
| //**[[Sector:IPPU:Wood_Processing:Start| 2.I - Wood Processing]]**//                                                                                                                                     |
| **//[[Sector:IPPU:POP_Production:Start| 2.J - Production of POPs]]//**                                                                                                                                   |
| **//[[sector:ippu:pops_and_hm_consumption:start| 2.K - Consumption of POPs and Heavy Metals]]//**                                                                                                        |
| **//[[Sector:IPPU:Bulk Products:Start| 2.L - Other Production, Consumption, Storage, Transportation or Handling of Bulk Products]]//**                                                                   |
| [[Sector:IPPU:Bulk_Products:Handling_of_Bulk_Products:Start| 2.L(a) - Handling of Bulk Products]]                                                                                                        |
| [[Sector:IPPU:Bulk_Products:Diffuse_Emissions_from_Industrial_Establishments:Start| 2.L(b) - Diffuse Emissions From Industrial Establishments]]                                                          |
|                                                                                                                                                                                                          |
^ [[Sector:Agriculture:Start| NFR 3 - AGRICULTURE ]]                                                                                                                                                       ^
| [[Sector:Agriculture:Manure_Management:Start| 3.B - Manure Management]]                                                                                                                                  |
| [[Sector:Agriculture:Agricultural_Soils:Start| 3.D - Agricultural Soils]]                                                                                                                                |
| [[Sector:Agriculture:Field_Burning:Start| 3.F - Field Burning Of Agricultural Residues]]                                                                                                                 |
| [[Sector:Agriculture:Agricultural_Other:Start| 3.I - Agricultural: Other]]                                                                                                                               |
|                                                                                                                                                                                                          |
^ [[Sector:Waste:Start| NFR 5 - WASTE ]]                                                                                                                                                                   ^
|                                                                                                                                                                                                          |
| [[Sector:Waste:Biological_Treatment_Solid_Waste_Disposal:Start| 5.A - Biological Treatment of Waste - Solid Waste Disposal on Land]]                                                                     |
| [[Sector:Waste:Biological_Treatment_Composting:Start| 5.B.1 - Biological treatment of waste - Composting]]                                                                                               |
| [[Sector:Waste:Biological_Treatment_Anaerobic_Digestion_At_Biogas_Facilities:Start| 5.B.2 - Biological treatment of waste - Anaerobic digestion at biogas facilities]]                                   |
| [[Sector:Waste:Cremation:Start| 5.C.1.b.v - Cremation]]                                                                                                                                                  |
| [[Sector:Waste:Open_Burning:Start| 5.C.2 - Open Burning of Waste]]                                                                                                                                       |
| [[Sector:Waste:Domestic_And_Commercial_Wastewater_Handling:Start| 5.D.1 - Domestic & Commercial Wastewater Handling]]                                                                                    |
| [[Sector:Waste:Industrial_Wastewater_Handling:Start| 5.D.2 - Industrial Wastewater Handling]]                                                                                                            |
| [[sector:waste:other_waste:mechanical-biological_treatment:start|5.E.1 - Other Waste: Mechanical-biological Treatment of Waste]]                                                                         |
| [[Sector:Waste:Other_Waste:Building_And_Car_Fires:Start| 5.E.2 - Building and Car Fires]]                                                                                                                |
|                                                                                                                                                                                                          |
^ [[sector:other_and_natural_sources:other_sources| NFR 6 - OTHER SOURCES]]                                                                                                                                ^
| [[sector:other_and_natural_sources:other_sources:human_sweating&breathing| 6.A.1 - Emissions from human sweating and breathing]]                                                                         |
| [[sector:other_and_natural_sources:other_sources:pets| 6.A.2 - Ammonia emissions from pets]]                                                                                                             |
^                                                                                                                                                                                                          ^
^ [[sector:other_and_natural_sources:other_sources| NFR 11 - NATURAL SOURCES ]]                                                                                                                            ^
| [[sector:other_and_natural_sources:natural_sources:forest_fires| 11.B - Forest Fires]]                                                                                                                   |


                                                                                                                                                                                                                          