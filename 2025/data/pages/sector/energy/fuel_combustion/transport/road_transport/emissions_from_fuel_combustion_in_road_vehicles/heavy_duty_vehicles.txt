====== 1.A.3.b iii - Transport: Road Transport: Heavy Duty Vehicles and Buses ======

===== Short description =====

In sub-category //1.A.3.b iii - Road Transport: Heavy Duty Vehicles and Buses// emissions from fuel combustion in trucks, lorries, buses etc. are reported. 

{{ :sector:energy:fuel_combustion:transport:bus.png?nolink&300}}

^  Category Code                          ^  Method  ^  AD     ^  EF        ^
|  1.A.3.b iii                            |  T1, T3  |  NS, M  |  CS, M, D  |
|  {{page>general:Misc:LegendEIT:start}}                                 ||||



----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   ^  Pb   ^  Cd   ^  Hg   ^  As   ^  Cr   ^  Cu   ^  Ni   ^  Se   ^  Zn   ^  PCDD/F  ^  B(a)P  ^  B(b)F  ^  B(k)F  ^  I(x)P  ^  PAH1-4  ^  HCB  ^  PCBs  ^
^  L/T                                    |  -/-    |  -/-             |  -/-             ^  L/T               ^  L/T              |  -/-  ^  L/T  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-     |  -/-    |  -/-    |  -/-    |  -/-    |  -/-     |  NE   |  -/-   |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                                                                                                                                 ||||||||||||||||||||||||||

===== Methodology =====

==== Activity data ====


Specific consumption data for heavy-duty vehicles (trucks and lorries) and buses are generated within TREMOD [(KNOERR2023a)]. - The following tables provide an overview of annual amounts of fuels consumed by these vehicles in Germany.


__Table 1: Annual fuel consumption of trucks and lorries, in terajoules__
|                                ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^  2022    ^
| **thereof: Buses**                                                                                                                                               ||||||||||||||
^ Diesel oil                     |   54,436 |   46,012 |   45,864 |   34,647 |   42,817 |   43,298 |   44,344 |   45,118 |   48,219 |   46,769 |   32,612 |   31,487 |   35,722 |
^ Biodiesel                      |          |       72 |      508 |    2,416 |    3,349 |    2,405 |    2,403 |    2,459 |    2,721 |    2,653 |    2,667 |    2,208 |    2,482 |
^ Natural Gas (CNG & LNG)        |          |          |          |    1,144 |    2,060 |      952 |      805 |      497 |      545 |      338 |      291 |      248 |      169 |
^ Biomethane                     |          |          |          |          |       65 |      257 |      274 |      303 |      241 |      339 |      322 |      234 |      207 |
^ Petroleum                      |          |      610 |      414 |          |          |          |          |          |          |          |          |          |          |
| **Ʃ Buses**                    ^   54,436 ^   46,694 ^   46,786 ^   38,206 ^   48,291 ^   46,913 ^   47,827 ^   48,378 ^   51,726 ^   50,099 ^   35,892 ^   34,177 ^   38,582 ^
| **thereof: Trucks & Lorries**                                                                                                                                    ||||||||||||||
^ Diesel oil                     |  382,343 |  507,393 |  590,498 |  419,069 |  494,618 |  495,912 |  500,514 |  530,155 |  578,748 |  548,376 |  566,485 |  525,417 |  496,843 |
^ Biodiesel                      |          |      792 |    6,542 |   29,218 |   38,688 |   27,548 |   27,129 |   28,898 |   32,655 |   31,106 |   46,335 |   36,845 |   34,526 |
^ Natural Gas (CNG & LNG)        |          |          |          |          |          |      210 |      177 |      135 |      217 |      421 |    1,163 |    2,131 |    2,029 |
^ Biomethane                     |          |          |          |          |          |       57 |       60 |       82 |       96 |      421 |    1,284 |    2,017 |    2,486 |
| **Ʃ Trucks & Lorries**         ^  382,343 ^  508,185 ^  597,040 ^  448,287 ^  533,306 ^  523,727 ^  527,879 ^  559,270 ^  611,717 ^  580,324 ^  615,267 ^  566,410 ^  535,884 ^
| **HDVs over-all**                                                                                                                                                ||||||||||||||
^ Diesel oil                     |  436,779 |  553,405 |  636,362 |  453,716 |  537,434 |  539,210 |  544,857 |  575,273 |  626,967 |  595,144 |  599,097 |  556,904 |  532,565 |
^ Biodiesel                      |          |      863 |    7,050 |   31,633 |   42,037 |   29,953 |   29,532 |   31,357 |   35,376 |   33,759 |   49,002 |   39,053 |   37,009 |
^ Natural Gas (CNG & LNG)        |          |          |          |    1,144 |    2,060 |    1,162 |      982 |      632 |      763 |      760 |    1,454 |    2,379 |    2,198 |
^ Biomethane                     |          |          |          |          |       65 |      314 |      334 |      385 |      337 |      760 |    1,606 |    2,252 |    2,693 |
^ Petroleum                      |          |      610 |      414 |          |          |          |          |          |          |          |          |          |          |
| **Ʃ 1.A.3.b iii**              ^  436,779 ^  554,878 ^  643,825 ^  486,493 ^  581,597 ^  570,639 ^  575,706 ^  607,647 ^  663,443 ^  630,423 ^  651,159 ^  600,587 ^  574,465 ^
source: TREMOD [(KNOERR2023a)]

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_ad_diesel.png?701 |  Annual consumption of diesel fuels }}
{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_ad_gas.png?702 |  Annual consumption of gaseous fuels }}

__Table 2: Annual mileage of electric buses and trucks, as of 2012, in [km]__
|           ^  2012       ^  2013       ^  2014       ^  2015       ^  2016       ^  2017       ^  2018        ^  2019        ^  2020       ^  2021       ^  2022       ^
^ e-buses   |   8,487,329 |   9,845,760 |  10,876,484 |  12,152,006 |  16,751,955 |  22,652,366 |   44,265,774 |   73,852,059 |  17,202,225 |  18,705,389 |  46,436,326 |
^ e-trucks  |  16,622,028 |  17,425,009 |  19,612,927 |  26,890,851 |  44,461,550 |  70,219,861 |  103,916,921 |  132,048,817 |  53,621,255 |  53,399,717 |  78,383,254 |

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_ad_el.png?703 | Development of mileage of eletric heavy-duty vehicles}}

<WRAP center round info 100%>
For more information on mileage and abrasion-related emisisons, please refer to sub-chapters on emissions from [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:start | tyre & brake wear and road abrasion]].
</WRAP>

==== Emission factors ====

The majority of emission factors for exhaust emissions from road transport are taken from the 'Handbook Emission Factors for Road Transport' (HBEFA, versions 4.1 and 4.2) [(KELLER2019)], [(NOTTER2022)] where they are provided on a tier3 level mostly and processed within the TREMOD software used by the party [(KNOERR2023a)]. 

However, it is not possible to present these tier3 values in a comprehendible way here. 

<WRAP center round info 100%>
With respect to the country-specific emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. ((During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions.))
</WRAP>


For exhaust-emissions of heavy metals (other then lead from leaded gasoline) and PAH, default emission factors from the 2019 EMEP Guidebook (EMEP/EEA, 2019) [(EMEPEEA2019)] have been applied, whereas regarding PCDD/F, a tier1 EF from (Rentz et al., 2008) [(RENTZ2008)] is used instead.

__Table 3: tier1 emission factors applied for exhaust heavy emissions of heavy metals and POPs__
|                              |  **Pb**       |  **Cd**  |  **Hg**  |  **As**  |  **Cr**  |  **Cu**  |  **Ni**  |  **Se**  |  **Zn**  |  **B[a]P**     |  **B[b]F**  |  **B[k]F**  |  **I[...]P**  |  **PAH 1-4**  |  **PCDD/F**   |
|                              |   **[g/TJ]**                                                                                  |||||||||   **[mg/TJ]**                                                          |||||  **[µg/km]**  |
^ Diesel oil                   |         0.012 |    0.001 |    0.123 |    0.002 |    0.198 |    0.133 |    0.005 |    0.002 |    0.419 |            498 |         521 |         275 |           493 |         1,788 |               |
^ Biodiesel                    |         0.013 |    0.001 |    0.142 |    0.003 |    0.228 |    0.153 |    0.005 |    0.003 |    0.483 |            575 |         601 |         317 |           569 |         2,062 |               |
^ CNG & Biogas                 |  NE           |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE            |  NE         |  NE         |  NE           |  NE           |               |
^ Petroleum                    |  NE           |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE            |  NE         |  NE         |  NE           |  NE           |               |
^ all fuels: buses                                                                                                                                                                     ||||||||||||||               |      0.000019 |
^ all fuels: trucks & lorries                                                                                                                                                          ||||||||||||||               |      0.000016 |

{{ :sector:energy:fuel_combustion:transport:truck.png?nolink&300}}

===== Discussion of emission trends =====

__Table 4: Outcome of Key Category Analysis__
|  for: ^  NO<sub>x</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  BC   ^
|   by: |  Level / Trend   |  L/T               |  L/T              |  L/T  |

==== Nitrogen oxides====

Until 2005, NO<sub>x</sub> emissions followed mileage and fuel consumption. Since 2006, in contrast to nearly unchanged fuel consumption, emissions have decreased due to controlled catalytic-converter use and engine improvements resulting from continual tightening of emissions laws.  

__Table 5: EURO norms and their effect on limit values of NO<sub>x</sub> emissions from diesel heavy-duty vehicles, in [g/kWh]__
^  pre-Euro  ^  Euro I  ^  Euro II  ^  Euro III  ^  Euro IV  ^  Euro V  ^  Euro VI[(EURLex2009)]  ^
|  14.4      |  8 / 9   |  7        |  5         |  3.5      |  2       |  0.4 / 0.46             |

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_nox.png?701 | Annual nitrogen oxides emissions }}

====Non-methane volatile organic compounds (NMVOC) and carbon monoxide ====

Since 1990, exhaust emissions of **NMVOC**  and **carbon monoxide** have decreased sharply due to catalytic-converter use and engine improvements resulting from ongoing tightening of emissions laws and improved fuel quality. 

__Table 6: EURO norms and their effect on limit values of CO emissions from diesel heavy-duty vehicles, in [g/kWh]__
^  pre-Euro  ^  Euro I     ^  Euro II  ^  Euro III  ^  Euro IV  ^  Euro V  ^  Euro VI[(EURLex2009)]  ^
|  11.2      |  4.5 / 4.9  |  4        |  2.1       |  1.5      |  1.5     |  1.5      |

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_nmvoc.png?701 |Annual NMVOC emissions }}
{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_co.png?701 |Annual carbon monoxide emissions }}

==== Ammonia and sulphur dioxide ====

As for the entire road transport sector, the trends for **sulphur dioxide** (SO<sub>2</sub>) and **ammonia** (NH<sub>3</sub>) exhaust emissions from heavy duty vehicles show charcteristics different from those shown above: Here, the strong dependence on increasing fuel qualities (sulphur content) leads to an cascaded downward trend of SO<sub>2</sub> emissions , influenced only slightly by increases in fuel consumption and mileage. For **ammonia** emissions the increasing use of catalytic converters in gasoline driven cars in the 1990s lead to a steep increase whereas both the technical development of the converters and the ongoing shift from gasoline to diesel cars resulted in decreasing emissions in the following years.

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_so2.png?701 | Annual sulphur oxides emissions }}

====Particulate matter & Black carbon====

As for all reported exhaust PM emissions from mobile diesel vehicles the Party assumes that nearly all particles emitted are within the PM<sub>2.5</sub> range, resulting in similar emission values for PM<sub>2.5</sub>, PM<sub>10</sub>, and TSP.

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_pm.png?701 | Annual particulate matter emissions from heavy-duty vehicles and buses.}}

===== Recalculations =====

Compared to submission 2024, recalculations in **activity data** result mainly from a revision of the underlying National Energy Balance (NEB) for 2022 as well as the underlying model for 2022 and 2023. 

__Table 7: Revised fuel consumption data, in terajoules__
|                              ^  2003    ^  2004    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^  2022  ^
| **DIESEL OIL**                                                                                                                             ||||||||||||        |
^ current Submission           |  489,987 |  469,978 |  453,716 |  537,434 |  539,210 |  544,857 |  575,273 |  626,967 |  595,144 |  599,097 |  556,904 |        |
^ previous Submission          |  533,902 |  523,808 |  492,626 |  553,922 |  591,195 |  598,967 |  602,876 |  587,927 |  600,602 |  567,318 |  553,414 |        |
^ absolute change              |  -43,915 |  -53,830 |  -38,910 |  -16,488 |  -51,985 |  -54,110 |  -27,603 |   39,039 |   -5,458 |   31,779 |    3,490 |        |
^ relative change              |   -8.23% |   -10.3% |   -7.90% |   -2.98% |   -8.79% |   -9.03% |   -4.58% |    6.64% |   -0.91% |    5.60% |    0.63% |        |
| **BIODIESEL**                                                                                                                              ||||||||||||        |
^ current Submission           |   14,175 |   17,624 |   31,633 |   42,037 |   29,953 |   29,532 |   31,357 |   35,376 |   33,759 |   49,002 |   39,053 |        |
^ previous Submission          |   14,828 |   18,340 |   32,920 |   42,467 |   32,339 |   31,754 |   32,184 |   34,175 |   34,209 |   47,125 |   38,446 |        |
^ absolute change              |     -653 |     -716 |   -1,287 |     -430 |   -2,386 |   -2,222 |     -827 |    1,201 |     -450 |    1,878 |      606 |        |
^ relative change              |   -4.40% |   -3.91% |   -3.91% |   -1.01% |   -7.38% |   -7.00% |   -2.57% |    3.51% |   -1.32% |    3.98% |    1.58% |        |
| **NATURAL GAS** (CNG & LNG)                                                                                                                ||||||||||||        |
^ current Submission           |      910 |    1,014 |    1,144 |    2,060 |    1,162 |      982 |      632 |      763 |      760 |    1,454 |    2,379 |        |
^ previous Submission          |        0 |        0 |    1,147 |    2,141 |    1,900 |    1,466 |    1,441 |    1,312 |    2,041 |    3,769 |    6,473 |        |
^ absolute change              |      910 |    1,014 |    -3,81 |    -80,8 |     -738 |     -484 |     -808 |     -549 |   -1,281 |   -2,315 |   -4,094 |        |
^ relative change              |          |          | -0.33%   |   -3.77% |   -38.8% |   -33.0% |   -56.1% |   -41.9% |   -62.8% |   -61.4% |   -63.2% |        |
| **BIOMETHANE**                                                                                                                             ||||||||||||        |
^ current Submission           |          |          |          |     65.4 |      314 |      334 |      385 |      337 |      760 |    1.606 |    2.252 |        |
^ previous Submission          |          |          |          |        0 |      312 |      330 |      369 |      301 |      492 |      677 |      682 |        |
^ absolute change              |          |          |          |     65.4 |     2.61 |     4.25 |     15.7 |     35.8 |      268 |      928 |    1,569 |        |
^ relative change              |          |          |          |          |    0.84% |    1.29% |    4.25% |    11.9% |    54.4% |     137% |     230% |        |
| **NFR 1.A.3.b iii TOTAL**                                                                                                                  ||||||||||||        |
^ current Submission           |  505,072 |  488,616 |  486,493 |  581,597 |  570,639 |  575,706 |  607,647 |  663,443 |  630,423 |  651,159 |  600,587 |        |
^ previous Submission          |  548,729 |  542,148 |  526,693 |  598,531 |  625,745 |  632,517 |  636,870 |  623,716 |  637,344 |  618,890 |  599,016 |        |
^ absolute change              |  -43,658 |  -53,532 |  -40,201 |  -16,934 |  -55,105 |  -56,812 |  -29,223 |   39,727 |   -6,921 |   32,269 |    1,572 |        |
^ relative change              |   -7.96% |   -9.87% |   -7.63% |   -2.83% |   -8.81% |   -8.98% |   -4.59% |    6.37% |   -1.09% |    5.21% |    0.26% |        |
\\

Due to the variety of highly specific tier3 **emission factors** applied, it is not possible to display any changes in these data sets in a comprehendible way.

<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2022**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

Besides a routine revision of the underlying model, no specific improvements are planned.


[(AGEB2023>AGEB, 2023: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; 
https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2030/?wpv-jahresbereich-bilanz=2021-2030, (Aufruf: 12.12.2023), Köln & Berlin, 2023)]
[(BAFA2023>BAFA, 2023: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik 
Deutschland; https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2022_12.xlsx?__blob=publicationFile&v=4, Eschborn, 2023.)]
[(KNOERR2023a> Knörr et al. (2023a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des 
motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg [u.a.]: Ifeu Institut für Energie- und Umweltforschung Heidelberg 
GmbH, Heidelberg & Berlin, 2023.)]
[(KELLER2019> Keller et al. (2019): Keller, M., Hausberger, S., Matzer, C., Wüthrich, P., & Notter, B.: Handbook Emission Factors for Road Transport, version 4.1 (Handbuch Emissionsfaktoren des Straßenverkehrs 4.1) URL: https://assets-global.website-files.com/6207922a2acc01004530a67e/625e8c74c30e26e022b319c8_HBEFA41_Development_Report.pdf - Dokumentation, Bern, 2019. )]
[(NOTTER2022> Notter et al. (2022): Notter, B., Cox, B., Hausberger, S., Matzer, S., Weller, K., Dippold, M., Politschnig, N., Lipp, S. (IVT TU Graz), Allekotte, M., Knörr, W. (ifeu), André, M. (IFSTTAR), Gagnepain, L. (ADEME), Hult, C., Jerksjö, M. (IVL): Handbook Emission Factors for Road Transport, version 4.2 (Handbuch Emissionsfaktoren des Straßenverkehrs 4.2) URL: https://assets-global.website-files.com/6207922a2acc01004530a67e/6217584903e9f9b63093c8c0_HBEFA42_Update_Documentation.pdf - Dokumentation, Bern, 2022. )]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019; https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-3-b-i/view; Copenhagen, 2019.)]
[(RENTZ2008> Rentz et al., 2008: Nationaler Durchführungsplan unter dem Stockholmer Abkommen zu persistenten organischen Schadstoffen (POPs), im Auftrag des Umweltbundesamtes, FKZ 205 67 444, UBA Texte | 01/2008, January 2008 - https://www.umweltbundesamt.de/en/publikationen/nationaler-durchfuehrungsplan-unter-stockholmer )]
[(EURLex2009> EUR-Lex, 2009: REGULATION (EC) No 595/2009 OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 18 June 2009 on type-approval of motor vehicles and engines with respect to emissions from heavy duty vehicles (Euro VI) and on access to vehicle repair and maintenance information and amending Regulation (EC) No 715/2007 and Directive 2007/46/EC and repealing Directives 80/1269/EEC, 2005/55/EC and 2005/78/EC - https://data.europa.eu/eli/reg/2009/595/oj )]