====== 1.A.3.a - Transport: Civil Aviation ======

===== Short description =====

^  NFR-Code                                          ^  Name of Category                          ^  Method   ^  AD  ^  EF  ^  Key Category Analysis  ^
| 1.A.3.a                                            | Civil Aviation                             |  //see sub-category details//           ||||
| //consisting of / including source categories//                                                                                           |||||| 
| **LTO-range: Included in National Totals**                                                                                                ||||||
| 1.A.3.a i (i)                                      | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto|International Civil Aviation - LTO]]     |  //see sub-category details//           ||||
| 1.A.3.a ii (i)                                     | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto|Domestic Civil Aviation - LTO ]]         |  //see sub-category details//           ||||
| **Cruise phase: Not included in National Totals**                                                                                         ||||||
| 1.A.3.a i (ii)                                     | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_Cruise| International Civil Aviation - Cruise]]  |  //see sub-category details//           ||||
| 1.A.3.a ii (ii)                                    | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_Cruise| Domestic Civil Aviation - Cruise]]       |  //see sub-category details//           ||||


Air transports differ significantly from land and water transports with respect to emissions production. In air transports, fuels are burned under atmospheric conditions that a) differ markedly from those prevailing at ground level and b) can vary widely. 

{{  :sector:energy:fuel_combustion:transport:aircraft.png?nolink&300}}

The main factors that influence the combustion process in this sector include atmospheric pressure, environmental temperature and humidity – all of which are factors that vary considerably with altitude.

In category 1.A.3.a - Civil Aviation the emissions from both national (domestic) and international civil aviation are reported with separate acquisition of flight phases LTO (Landing/Take-off: 0-3,000 feet) and Cruise (above 3,000 feet) where only emissions from LTO from both national and international flights have to be included in the national totals.

Emissions from military aircraft are not included in this category but are reported under military airborne combustion in NFR sub-category 1.A.5.b ii.

Country specifics:
The use of aviation gasoline is assumed to take place within the LTO-range of domestic flights only (below 3,000 feet). This assumption is a compromise due to a lack of further information and data.

===== Methodology =====

NOTE: Data available from Eurocontrol via the European Environment Agency (EEA) is not being used for inventory compilation. Nonetheless, depending on its timeliness, it is taken into account for verification purposes.

Estimation of aircraft emissions has been carried out using a tier 3a approach, i.e. under consideration of the annual distances flown by different types of aircraft, deviated into domestic and international flights, also considering the different flight stages LTO cycle (Landing/Take-off cycle, i.e. aircraft movements below 3,000 feet or about 915 meters of altitude) and cruise.

Essential for emissions reporting is the separation of domestic and international air traffic. This happens using a so-called split factor representing the ratio of fuel consumption for national flights and the over-all consumption.

For determination of this ratio, results from TREMOD AV (TRansport Emissions MODel AViation) have been used, based on the great circle distances flown by the different types of aircraft (Allekotte et al. (2024) [(ALLEKOTTE2024)] & Gores (2024) [(GORES2024)]. Here, the ratio is calculated on the basis of statistics on numbers of national and international flights departing from German airports provided by the Federal Statistical Office (Statistisches Bundesamt).

For further dividing kerosene consumption onto flight stages LTO and cruise, again results calculated within the TREMOD AV data base based on data provided by the Federal Statistical Office have been used.

Emissions are being estimated by multiplying the kerosene consumption of the flight stage with specific emission factors (EF). Here, emissions of SO<sub>2</sub> and H<sub>2</sub>O are independent from the method used, depending only on the quantity and qualities of the fuel used. In contrast, emissions of NO<sub>x</sub>, NMVOC, and CO strongly depend on the types of engines, flight elevations, flight stage, etc. and can be estimated more precisely with higher tiers. The emission factors for NO<sub>x</sub>, CO, and NMVOC are therefore computed within TREMOD AV.

The aviation gasoline (avgas) used is not added to the annual kerosene consumptions but reported separately. As proposed in (IPCC, 2006a) [(IPCC2006a)], emissions caused by the incineration of avgas are calculated using adapted EF and calorific values following a tier1 approach. Here, a split into national and international shares is not necessary as avgas is supposed to only being used in smaller aircraft operating on domestic routes and within the LTO range. - This conservative assumption leads to a slight overestimation of national emissions.1

For further information on AD (entire time series), EF, key sources, and recalculations see sub-chapters linked above.

==== Activity Data ====

Emissions estimation is mainly based on consumption data for jet kerosene and aviation gasoline as provided in the national Energy Balances (AGEB, 2024) [(AGEB2024)]. For very recent years with no AGEB data available (Normally the last year of the period reported.) data provided by the Federal Office of Economics and Export Control (BAFA) [(BAFA2024)] is being used.

Table 1: Sources for 1.A.3.a activity data
| through 1994               | **AGEB** - National Energy Balance, line 76: 'Luftverkehr'                   |
| from 1995                  | **AGEB** - National Energy Balance, line 63: 'Luftverkehr'                   |
| recent years / comparison  | **BAFA** - Official oil data, table 7j: 'An die Luftfahrt' + 'An Sonstige'*  |
* to achieve consistency with AGEB data, amounts given for deliveries 'to Aviation' ('An die Luftfahrt') and 'to Others' ('An Sonstige') have to be added
(see FAQs for more information)

__Table 2: Total inland fuel deliveries to civil aviation, in terajoules__
^            ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^  2022    ^  2023    ^
^  Kerosene  |  193,329 |  233,437 |  297,258 |  343,828 |  361,751 |  361,651 |  389,024 |  425,140 |  437,203 |  434,490 |  199,931 |  257,520 |  384,975 |  402,121 |
^ Avgas      |    2,438 |    1,142 |    1,120 |      698 |      568 |      570 |      419 |      415 |      401 |      328 |      214 |      159 |      175 |      130 |
^ 1.A.3.a    |  195,767 |  234,579 |  298,378 |  344,526 |  362,319 |  362,221 |  389,443 |  425,555 |  437,604 |  434,818 |  200,145 |  257,679 |  385,151 |  402,251 |

source: Working Group on Energy Balances (AGEB): National Energy Balances (AGEB, 2024) [(AGEB2024)]

For the present purposes, kerosene-consumption figures from NEB and BAFA statistics have to be broken down by national (= domestic) and international flights:
Here, the split has been calculated on the basis of statistics on numbers of national and international flights departing from German airports provided by the Federal Statistical Office (Statistisches Bundesamt) within TREMOD AV [(ALLEKOTTE2024)].

__Table 3: Ratios for calculating the shares of fuels used in 1.A.3.a ii - Domestic and 1.A.3.a i - International Civil Aviation, in %__
|                                               ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^  2022  ^  2023  ^
| **1.A.3.a ii - Civil domestic aviation**                                                                                                                      |||||||||||||||
^ Kerosene                                      |   15.3 |   12.8 |   11.0 |   8.80 |   8.35 |   7.63 |   7.14 |   6.34 |   6.14 |   6.38 |   6.18 |   3.70 |   3.61 |   3.65 |
^ Avgas                                         |   86.0 |   85.9 |   86.1 |   86.2 |   86.3 |   86.2 |   97.3 |   97.0 |   96.5 |   96.2 |   97.5 |   92.8 |   92.8 |   79.2 |
| **1.A.3.a i - Civil international aviation**                                                                                                                  |||||||||||||||
^ Kerosene                                      |   84.7 |   87.2 |   89.0 |   91.2 |   91.6 |   92.4 |   92.9 |   93.7 |   93.9 |   93.6 |   93.8 |   96.3 |   96.4 |   96.4 |
^ Avgas                                         |   14.0 |   14.1 |   13.9 |   13.8 |   13.7 |   13.8 |   2.66 |   3.00 |   3.53 |   3.80 |   2.54 |   7.22 |   7.20 |   20.8 |

__Table 4: Resulting annual shares of jet kerosene and avgas used in 1.A.3.a ii - Domestic and 1.A.3.a i - International Civil Aviation, in terajoules__
|                                           ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^  2022    ^  2023    ^
| 1.A.3.a ii - Civil domestic aviation                                                                                                                                                  |||||||||||||||
^ Kerosene                                  |   29,501 |   29,989 |   32,746 |   30,260 |   30,210 |   27,605 |   27,783 |   26,935 |   26,843 |   27,739 |   12,354 |    9,541 |   13,914 |   14,676 |
^ Avgas                                     |    2,098 |      981 |      964 |      601 |      490 |      491 |      407 |      403 |      386 |      316 |      209 |      147 |      163 |      103 |
| 1.A.3.a i - Civil international aviation                                                                                                                                              |||||||||||||||
^ Kerosene                                  |  163,828 |  203,448 |  264,512 |  313,568 |  331,542 |  334,046 |  361,241 |  398,205 |  410,360 |  406,750 |  187,577 |  247,979 |  371,062 |  387,445 |
^ Avgas                                     |      340 |      161 |      156 |     96.3 |     78.0 |     78.6 |     11.1 |     12.5 |     14.1 |     12.5 |     5.43 |     11.5 |     12.6 |     27.0 |
| 1.A.3.a - OVER-ALL                                                                                                                                                                    |||||||||||||||
^ Kerosene                                  |  193,329 |  233,437 |  297,258 |  343,828 |  361,751 |  361,651 |  389,024 |  425,140 |  437,203 |  434,490 |  199,931 |  257,520 |  384,975 |  402,121 |
^ Avgas                                     |    2,438 |    1,142 |    1,120 |      698 |      568 |      570 |      419 |      415 |      401 |      328 |      214 |      159 |      175 |      130 |

The deviation of the kerosene consumed onto the two flight stages LTO and cruise again has been carried based on TREMOD AV estimations allowing the export of kerosene consumption during LTO for both domestic and international flights.

__Table 5: Annual shares of LTO phase in domestic and international civil aviation, in %__
|             ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015  ^ 2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^  2022  ^  2023  ^
^ 1.A.3.a i   |  7.55  |  8.25  |  7.42  |  7.13  |  7.59  |   8.37 |  8.01 |   7.44 |   7.43 |   7.71 |   7.25 |   6.08 |   6.14 |   6.53 |
^ 1.A.3.a ii  |  30.0  |  29.3  |  27.8  |  27.5  |  27.5  |   27.6 |  28.0 |   28.1 |   28.3 |   28.1 |   27.5 |   32.9 |   31.8 |   28.5 |
source: number of domestic and international flights as provided by the Federal Statistical Office (Destatis, 2024) [(DESTATIS2024)], compiled and computed within [(ALLEKOTTE2024)] and [(GORES2024)]
a assumption: all aircraft using aviation gasoline are operated within the LTO-range below 3,000 feet and only for domestic flights

Cruise consumption is then calculated as the difference between Total Consumption minus LTO Consumption.


==== Emission factors ====

===Kerosene===

Emissions have been calculated for each flight phase, based on the respective emission factors. Therefore, the EF used have been taken from a wide range of different sources.
In contrast to earlier submissions, the emissions of  NO<sub>x</sub>, CO und HC are based on aircraft-specific EF deposited within TREMOD AV. With this very detailed estimations average EF are being formed which are than used for emissions reporting.

The EF provided with the current submission represent annual average EF for the entire fleet, calculated as implied EF from the emissions computed within TREMOD AV and therefore differ from the values used in the past. 

**Sulphur dioxide (SO<sub>2</sub>)** emissions depend directly on the kerosene's sulphur content which varies regionally as well as seasonally. The EF used by Eurocontrol of 0.84 kg SO<sub>2</sub>/t kerosene lies between the values used for German inventory for 1990 to 1994 (1.08 to 1.03 kg SO<sub>2</sub>/t) and from 1995 (0.4 kg SO<sub>2</sub>/t).
In IPCC 2006b [(IPCC2006)] with 1 kg SO<sub>2</sub>/t kerosene value comes very close to the old inventory values provided, based on a sulfur content of 0.05 % of weight. Following current information of the expert committee for the standardization of mineral oil and fuels (Fachausschuss für Mineralöl-und Brennstoffnormung, FAM), the common value for sulphur content of kerosene in Germany is about 0.01% of weight, i.e. one fifth of the IPCC data. In IIR 2009, a sulfur content of 0.021 weight% have been used, based on measurements from 1998 (Döpelheuer (2002)) [(DOEPELHEUER2002)]. 

As an EF decreasing due to improved production procedures and stricter critical levels seems plausible, for this report a constant decline between the annual values of 1.08 g SO<sub>2</sub>/kg for 1990, 0.4 g for 1998 and 0.2 g for 2009 has been assumed. Thereby, an exhaustive conversion of the sulfur into suflur dioxide is expected. - Due to the EF depending directly on the S content of the kerosene, one annual EF is used for both flight stages.

**Nitrogen oxide (NO<sub>x</sub>)**, **carbon monoxide (CO)** and **hydrocarbons (HC)** emissions were estimated using IEF calculated within TREMOD AV, based upon more specific (depending on type of aircraft, flight stage) EF mostly taken from the EMEP-EEA data base. For 2009, 40 % of over-all starts (about 70 % of total kilometres flown) had to be linked with adapted EF as it was not possible to directly or even indirectly (via similar types of aircraft) allocate the aircraft used here. Therefore, regression analysis had to be carried out, estimating EF via emission functions that calculate an EF for the respective type of engine depending on the particular take-off weight. 

As a basis for these functions the EF of types of aircraft with given EF have been used (see: Knörr et al. (2023c)) [(KNOERR2023c)]. From the trend of the emissions calculated within TREMOD AV, annual average EF for the entire fleet have been formed, which have then been used for reporting. Hence, the EF differ widely from those used in earlier submissions. 

**Ammonia (NH<sub>3</sub>)** emissions were estimated using an EF of 0.173 g/kg kerosene for both flight stages (UBA, 2009) [(UBA2009)].

The EFs for **non-methane volatile organic compounds (NMVOC)** were calculated as the difference between the EF for over-all hydrocarbons (HC) and the EF for methane (CH<sub>4</sub>).

**Particulate Matter**
Within the IPCC EF data base, there are no default data provided for emissions of particulate matter (TSP, PM<sub>10</sub>, and PM<sub>2.5</sub>). Therefore, the EF for dust (**T**otal **S**uspended **P**articulate Matter – **TSP**) are taken over from Corinair (2006) [(CORINAIR2006)], giving specific values for an average fleet and for the two flight stages in table 8.2: For national flights 0.7 kg TSP/LTO and 0.2 kg TSP/t kerosene and 0.15 kg TSP/LTO and 0.2 kg TSP/t kerosene for international flights. Following this table, a kerosene consumption per LTO cycle of 825 kg for national and 1,617 kg for international flights have been assumed and the EF for the LTO stage have been estimated.

The EF for **water vapor (H<sub>2</sub>O)** provided by Eurocontrol (2004) is about 1,230g H<sub>2</sub>O / kg kerosene, whereas in Corinair (2006) [(CORINAIR2006)] 1,237g H<sub>2</sub>O /kg is assumed. Based on the stoichiometric assumptions mentioned above a EF(CO<sub>2</sub>) of 1.24 kg H<sub>2</sub>O/kg can be derived. To reduce the number of sources for EF, here, the Corinair value has been used for both flight stages and for both national and international flights.

As for **polycyclic aromatic hydrocarbons** (PAH), tier1 EF from (EMEP/EEA, 2019) [(EMEPEEA2023)] have been apllied here. As the EMEP guidebook does not provide original EF for jet kerosene, values provided for gasoline in road transport have been used here as a proxy and will be replaced by more appropriate data as soon as this is available.

The conversion of EF representing emissions per kilo fuel combusted [kg pollutant/kg kerosene] into energy related EF [kg pollutant/TJ energy] has been carried out using a net calorific value of 43,000 kJ/kg.

=== Aviation gasoline ===

For aviation gasoline (avgas) a deviation onto LTO and cruise is assumed to be unnecessary. Therefore, there are no such specific EF used here. As for kerosene, the EF for **NO<sub>x</sub>**, **CO** and **HC** have been taken from the calculations carried out within TREMOD AV. Here, for calculating aircraft specific NO<sub>x</sub>, CO, and HC emissions corresponding EF from the EMEP-EEA data base have been used that have than been divided by the annual avgas consumption to form annual average EF for emission reporting.

With respect to fuel characteristics, there are no big differences between avgas and gasoline used in passenger cars (PC). Therefore, specific **sulphur dioxide (SO<sub>2</sub>)** emissions from PC gasoline can be carried forward to avgas. - Following the expert committee for the standardization of mineral oil and fuels (FAM), the critical value of sulfur content for gasoline sold at gas stations is 10 mg/kg, i.e. 0,001 % of weight - or one tenth of the kerosene value. Therefore, the EF used for avgas equals the EF used for kerosene reduced by 90 %.

There are different sorts of avgas sold with different **lead (Pb)** contents. As an exact annual ration of the sorts sold is not available, the most common type of avgas (AvGas 100 LL (Low Lead)) with a lead content of 0.56 g/l is set as an approximation. This value lies slightly below the value of 0.6 g/l as proposed in the EMEP Guidebook 2009. – For estimating lead emissions here the value provided for AvGas 100 LL has been converted into an EF of about 0.75 g lead/kg avgas using a density of 0.75 kg/l.

The **EF(TSP)** were calculated from the lead content of AvGas 100 LL by multiplication with a factor 1.6 as used for leaded gasoline in road transport in the TREMOD system.

For **NMVOC**, an EF from the Revised IPCC Guidelines 1996 (pages I 42 and 40) [(IPCC1996a)], [(IPCC1996b)], have been used.

All other EF are not available specifically for small aircraft and therefore have been equalized with the EF used for kerosene, national, cruise.

The conversion of the EF from [kg emission/kg avgas consumed] into [kg emission/TJ energy converted] has been carried out using a net calorific value of 44,300 kJ/kg.

> **NOTE:** For the country-specific emission factors applied for particulate matter, no clear indication is available, whether or not condensables are included.  

> For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.

=====Recalculations=====

With the total kerosene inland deliveries remainig unchanged within the National Energy Balances, the domestic share of total kerosene consumption was revised only for 2022 and based on slightly revised background data for this specific year computed within the underlying model, TREMOD Aviation.

__Table 7: Revised percental shares of kerosene used for domestic flights, in %__
|                                                      ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^  2022  ^
| % of **JET KEROSENE** allocated to domestic flights                                                                                                          ||||||||||||||
^ current submission                                   |   15.3 |   12.8 |   11.0 |   8.80 |   8.35 |   7.63 |   7.14 |   6.34 |   6.14 |   6.38 |   6.18 |   3.70 |  3.614 |
^ previous submission                                  |   15.3 |   12.8 |   11.0 |   8.80 |   8.35 |   7.63 |   7.14 |   6.34 |   6.14 |   6.38 |   6.18 |   3.70 |  3.613 |
^ absolute change                                      |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |  0.001 |
^ relative change                                      |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.03% |
| % of **AVGAS**  allocated to domestic flights                                                                                                                ||||||||||||||
^ current submission                                   |   86.0 |   85.9 |   86.1 |   86.2 |   86.3 |   86.2 |   97.3 |   97.0 |   96.5 |   96.2 |   97.5 |   92.8 |   92.8 |
^ previous submission                                  |   86.0 |   85.9 |   86.1 |   86.2 |   86.3 |   86.2 |   97.3 |   97.0 |   96.5 |   96.2 |   97.5 |   92.8 |   92.8 |
^ absolute change                                      |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |   0.00 |
^ relative change                                      |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |  0.00% |

As a result, the amounts of jet kerosene allocated to NFR sub-categories //1.A.3.a i// and //1.A.3.a ii// and for 2022 had to be revised accordingly.
Here, the activity data allocated to NFR //1.A.3.a i// was reduced by the same amount by which the activity data allocated to NFR //1.A.3.a ii// was increased:

__Table 8: Revised amounts of fuel allocated to international (1.A.3.a i) and domestic (1.A.3.a ii) flights, in terajoules__
|                                               ^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^  2022    ^
| **1.A.3.a i - CIVIL INTERNATIONAL AVIATION**                                                                                                                                    ||||||||||||||
| **JET KEROSENE**                                                                                                                                                                ||||||||||||||
^ current submission                            |  163,828 |  203,448 |  264,512 |  313,568 |  331,542 |  334,046 |  361,241 |  398,205 |  410,360 |  406,750 |  187,577 |  247,979 |  371,062 |
^ previous submission                           |  163,828 |  203,448 |  264,512 |  313,568 |  331,542 |  334,046 |  361,241 |  398,205 |  410,360 |  406,750 |  187,577 |  247,979 |  371,066 |
^ absolute change                               |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |    -4.64 |
^ relative change                               |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |  -0.001% |
| **AVGAS**                                                                                                                                                                       ||||||||||||||
^ current submission                            |      340 |      161 |      156 |     96.3 |     78.0 |     78.6 |     11.1 |     12.5 |     14.1 |     12.5 |     5.43 |     11.5 |     12.6 |
^ previous submission                           |      340 |      161 |      156 |     96.3 |     78.0 |     78.6 |     11.1 |     12.5 |     14.1 |     12.5 |     5.43 |     11.5 |     12.6 |
^ absolute change                               |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |
^ relative change                               |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |
|                                               |          |          |          |          |          |          |          |          |          |          |          |          |          |
| **1.A.3.a ii - CIVIL DOMESTIC AVIATION**                                                                                                                                        ||||||||||||||
| **JET KEROSENE**                                                                                                                                                                ||||||||||||||
^ current submission                            |   29,501 |   29,989 |   32,746 |   30,260 |   30,210 |   27,605 |   27,783 |   26,935 |   26,843 |  27,739  |   12,354 |    9,541 |   13,914 |
^ previous submission                           |   29,501 |   29,989 |   32,746 |   30,260 |   30,210 |   27,605 |   27,783 |   26,935 |   26,843 |  27,739  |   12,354 |    9,541 |   13,909 |
^ absolute change                               |     0.00 |     0.00 |     0.00 |     0.00 |    -0.01 |    -0.01 |     0.00 |    -0.01 |     0.00 |     0.00 |    -0.01 |    -0.01 |     4.63 |
^ relative change                               |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.03% |
| **AVGAS**                                                                                                                                                                       ||||||||||||||
^ current submission                            |    2,098 |      981 |      964 |      601 |      490 |      491 |      407 |      403 |      386 |      316 |      209 |      147 |      163 |
^ previous submission                           |    2,098 |      981 |      964 |      601 |      490 |      491 |      407 |      403 |      386 |      316 |      209 |      147 |      163 |
^ absolute change                               |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |     0.01 |     0.00 |     0.00 |     0.00 |     0.00 |     0.00 |    -0.01 |
^ relative change                               |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |    0.00% |

<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2022**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

Besides the routine revision of the underlying model, no specific improvements are planned.

===== Uncertainties =====

Information on uncertainties is provided here with most data representing expert judgement from the research project mentioned above.

For estimating uncertainties, the partial uncertainties (U<sub>1</sub> to U<sub>n</sub>) of the components incorporated in emission calculations have to be quantified.
By additive linking of the squared partial uncertainties the overall uncertainty (U<sub>total</sub>) can then be estimated (IPCC, 2000) [(IPCC2000)].

The uncertainties given here have been evaluated for all time series and flight stages as average values.
Estimating the overall uncertainty has been carried out as shown in the table below. In the very left column the components of the uncertainty estimations are listed with their partial uncertainties given in the next column. The next columns show the data linked to estimate the different overall uncertainties which themselves represent partial uncertainties for higher aggregated data and so on.

As an example, the uncertainty of the kerosene consumptions for domestic flights divided by flight stages (LTO and cruise) has been calculated from the partial uncertainty of the over-all kerosene consumption for domestic flights and the partial uncertainty of the LTO-cruise-split. Here, the split is based on the number of flights provided by the Federal Statistical Office and assumptions on the composition of the fleet. The overall uncertainties of both fuel consumption during LTO and cruise itself then represent a partial uncertainty within the estimation of the uncertainties of emissions.

Several partial uncertainties are based on assumptions. For example, the uncertainty given for the entire time series of the split factor domestic:international flights is an average value:  
For the years 1990 to 2002 data is based upon estimations carried out within TREMOD AV which themselves are based on data from the Federal Statistical Office and EF from the EMEP-EEA data base. For 2003 to 2011 data from Eurocontrol are being used, that are calculated within ANCAT. Comparing results from the ANCAT model with actual consumption data show aberrations of ±12 %. Here, data calculated with AEM 3 model would have an uncertainty of only 3 to 5 % (EUROCONTROL 2006) [(EUROCONTROL2006)].

As no uncertainty estimates were carried out for ammonia and particulate matter within the above-mentioned project, values from the PAREST research project mentioned for most over mobile sources were used. Here, the final report has not yet been published.


===== FAQs =====

**Whereby does the party justify the adding-up of the two amounts given in BAFA table 7j as deliveries 'An die Luftfahrt' and 'An Sonstige' ?**

For mineral oils, German National Energy Balances (NEBs) - amongst other sources - are based on BAFA data on the amounts delivered to different sectors. A comparison with consumption data from AGEB and BAFA shows that data from NEB line 76 /63: 'Luftverkehr' equates to the amount added from both columns in BAFA table 7j.

**On which basis does the party estimate the reported lead emissions from aviation gasoline?**

assumption by party: aviation gasoline = AvGas 100 LL
(AvGas 100 LL is the predominant sort of aviation gasoline in Western Europe)1
lead content of AvGas 100 LL: 0.56 g lead/liter (as tetra ethyl lead)2

The applied procedure is similar to the one used for calculating lead emissions from leaded gasoline used in road transport. (There, in contrast to aviation gasoline, the lead content constantly declined resulting in a ban of leaded gasoline in 1997.)

**On which basis does the party estimate the reported TSP emissions from aviation gasoline?**

The TSP emissions calculated depend directly on the reported lead emissions: The emission factor for TSP is 1.6 times the emission factor used for lead: EF(TSP) = 1.6 x EF(Pb).
The applied procedure is similar to the one used for calculating TSP emissions from leaded gasoline used in road transport.

[(AGEB2024>AGEB, 2024: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; 
https://ag-energiebilanzen.de/wp-content/uploads/2024/03/EBD22e.xlsx, (Aufruf: 04.12.2024), Köln & Berlin, 2024)]
[(BAFA2024>BAFA, 2024: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2023_12.xlsx?__blob=publicationFile&v=2, Eschborn, 2024.)]
[(KNOERR2012> Knörr, W., Schacht, A., & Gores, S. (2012): Entwicklung eines eigenständigen Modells zur Berechnung des Flugverkehrs (TREMOD-AV): Endbericht. Endbericht zum F+E-Vorhaben 360 16 029, 
URL: https://www.umweltbundesamt.de/publikationen/entwicklung-eines-modells-zur-berechnung; Berlin & Heidelberg, 2012.)]
[(ALLEKOTTE2024> Allekotte et al. (2024): TREMOD Aviation (TREMOD AV) - Revision des Modells zur Berechnung des Flugverkehrs (TREMOD-AV). Heidelberg, 
Berlin: Ifeu Institut für Energie- und Umweltforschung Heidelberg GmbH & Öko-Institut e.V., Berlin & Heidelberg, 2024.)]
[(GORES2024> Gores (2024): Inventartool zum deutschen Flugverkehrsinventar 1990-2023, im Rahmen der Aktualisierung des Moduls TREMOD-AV im Transportemissionsmodell TREMOD, Berlin, 2024.)]
[(EMEPEEA2023> EMEP/EEA, 2023: EMEP/EEA air pollutant emission inventory guidebook 2023, https://www.eea.europa.eu/publications/emep-eea-guidebook-2023/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-3-a-aviation-2023/view; Copenhagen, 2023.)]
[(EUROCONTROL2020> Eurocontrol (2020): Advanced emission model (AEM); https://www.eurocontrol.int/model/advanced-emission-model; 2020.)]
[(IPCC2006a> IPCC (2006b): Intergovernmental Panel on Climate Change: IPCC emission factor data base; URL: http://www.ipcc-nggip.iges.or.jp/EFDB/main.php)]
[(DOEPELHEUER2002> Döpelheuer (2002): Anwendungsorientierte Verfahren zur Bestimmung von CO, HC und Ruß aus Luftfahrttriebwerken, Dissertationsschrift des DLR, Institut für Antriebstechnik, Köln, 2002.)]
[(CORINAIR2006> CORINAIR, 2006 - EMEP/CORINAIR Emission Inventory Guidebook - 2006, EEA technical report No. 11/2006; Dezember 2006, Kopenhagen, 2006 URL: http://www.eea.europa.eu/publications/EMEPCORINAIR4 )]
[(IPCC1996a> Revised 1996 IPCC Guidelines, Volume 3: Reference Manual, Chapter I: Energy; URL: http://www.ipcc-nggip.iges.or.jp/public/gl/guidelin/ch1ref2.pdf, p. I.40 )]
[(IPCC1996b> Revised 1996 IPCC Guidelines, Volume 3: Reference Manual, Chapter I: Energy; http://www.ipcc-nggip.iges.or.jp/public/gl/guidelin/ch1ref3.pdf, p. I.42 )]
[(IPCC2000> IPCC, 2000: Intergovernmental Panel on Climate Change, Good Practice Guidance and Uncertainty Management in National Greenhouse Gas Inventories, IPCC Secretariat, 16th Session, Montreal, 1-8 May 2000, URL: http://www.ipcc-nggip.iges.or.jp/public/gp/english/ )]
[(EUROCONTROL2006> EUROCONTROL, 2006 – The Advanced Emission Model (AEM3) - Validation Report, Jelinek, F., Carlier, S., Smith, J., EEC Report EEC/SEE/2004/004, Brüssel 2004 URL: http://www.eurocontrol.int/eec/public/standard_page/DOC_Report_2004_016.html http://www.eurocontrol.int/eec/public/standard_page/DOC_Report_2006_030.html )]