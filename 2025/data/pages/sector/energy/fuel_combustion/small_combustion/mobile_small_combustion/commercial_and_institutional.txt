====== 1.A.4.a ii - Commercial / Institutional: Mobile ======

===== Short description =====

In //NFR 1.A.4.a ii - Commercial/institutional: Mobile// fuel combustion activities and emissions from non-road diesel and LPG-driven (forklifters) vehicles used in the commercial and institutional sector are taken into account.

^  Category Code                          ^  Method  ^  AD     ^  EF        ^
|  1.A.4.a ii                             |  T1, T2  |  NS, M  |  CS, D, M  |
|  {{page>general:Misc:LegendEIT:start}}                                 ||||

----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   ^  Pb   ^  Cd   ^  Hg   ^  As   ^  Cr   ^  Cu   ^  Ni   ^  Se   ^  Zn   ^  PCDD/F  ^  B(a)P  ^  B(b)F  ^  B(k)F  ^  I(x)P  ^  PAH1-4  |  HCB  |  PCBs  |
|  -/-                                    |  -/-    |  -/-             |  -/-             |  -/-               |  -/-              |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-     |  -/-    |  -/-    |  -/-    |  -/-    |  -/-     |  NE   |  NE    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                                                                                                                                 ||||||||||||||||||||||||||

===== Methodology =====

==== Activity data ====

Sector-specific **diesel** consumption data are included in the primary fuel-delivery data available from NEB line 67: 'Commercial, trade, services and other consumers' (AGEB, 2024) [(AGEB2024)].

__Table 1: Sources for primary fuel-deliveries data__
|| through 1994 || NEB line 79: 'Households and small consumers' ||
|| as of 1995 || NEB line 67: 'Commercial, trade, services and other consumers' ||
\\

Following the deduction of diesel oil inputs for military vehicles as provided in (BAFA, 2024) [(BAFA2024)], the remaining amounts of diesel oil are apportioned onto off-road construction vehicles (NFR 1.A.2.g vii) and off-road vehicles in commercial/institutional use (1.A.4.a ii) as well as agriculture and forestry (1.A.4.c ii) based upon annual shares derived from (Knörr et al. (2024b)) [(KNOERR2024b)]  (cf. superordinate chapter).

__Table 2: Annual contribution of NFR 1.A.4.a ii to the over-all amounts of diesel oil provided in NEB line 67__
^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021  ^  2022  ^  2023  ^
|  5.98%  |  3.95%  |  3.67%  |  3.66%  |  5.07%  |  5.40%  |  5.41%  |  5.37%  |  4.83%  |  4.93%  |  4.91%  |  4.58% |  4.30% | 4.00%  |
source: TREMOD MM [(KNOERR2024b)]

As the NEB does not distinguish into specific biofuels, consumption data for biodiesel are calculated by applying Germany's official annual shares of biodiesel blended to fossil diesel oil. 

In contrast, for **LPG**-driven forklifters, specific consumption data is modelled by Knörr et al. (2024) [(KNOERR2024b)]. These amounts are then subtracted from the over-all amount available from NEB line 67 to estimate the amount of LPG used in stationary combustion.

__Table 3: Annual fuel consumption, in terajoules__
|                   ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015   ^  2016   ^  2017   ^  2018  ^  2019  ^  2020  ^  2021  ^  2022  ^  2023  ^
^ Diesel Oil        |  6,696 |  3,868 |  3,487 |  3,001 |  4,501 |   5,579 |   5,686 |   5,714 |  4,985 |  5,209 |  5,122 |  4,866 |  4,397 |  4,029 |
^ Biodiesel         |        |        |        |    200 |    352 |     310 |     308 |     312 |    281 |    295 |    419 |    341 |    305 |    299 |
^ LPG               |  2,787 |  3,450 |  4,261 |  4,533 |  4,629 |   4,256 |   4,336 |   4,301 |  4,264 |  4,213 |  4,139 |  4,063 |  3,987 |  3,912 |
| **Ʃ 1.A.4.a ii**  ^  9,482 ^  7,318 ^  7,749 ^  7,733 ^  9,482 ^  10,144 ^  10,330 ^  10,327 ^  9,530 ^  9,718 ^  9,680 ^  9,270 ^  8,689 ^  8,239 ^

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4aii_ad.png?702 | Annual fuel consumption }}

==== Emission factors ====

The emission factors used here are of rather different quality:
Basically, for all **main pollutants**, **carbon monoxide** and **particulate matter**, annual IEF modelled within TREMOD MM are used, representing the sector's vehicle-fleet composition, the development of mitigation technologies and the effect of fuel-quality legislation. 

As no such specific EF are available for biofuels, the values used for diesel oil are applied to biodiesel, too.

__Table 4: Annual country-specific emission factors from TREMOD MM, in kg/TJ__
|                               ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^  2022  ^  2023  ^
| **Diesel fuels**<sup>1</sup>                                                                                                                  |||||||||||||||
^ NH<sub>3</sub>                |   0.15 |   0.16 |   0.16 |   0.16 |   0.16 |   0.17 |   0.17 |   0.17 |   0.17 |   0.17 |   0.17 |   0.17 |   0.17 |   0.17 |
^ NMVOC                         |    247 |    223 |    197 |    139 |   93.0 |   58.5 |   53.3 |   48.9 |   45.3 |   42.0 |   38.8 |   35.4 |   31.8 |   28.4 |
^ NO<sub>x</sub>                |    999 |  1.025 |  1.003 |    833 |    633 |    476 |    451 |    426 |    404 |    384 |    364 |    344 |    324 |    304 |
^ SO<sub>x</sub>                |   79.6 |   60.5 |   14.0 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |
^ BC<sup>3</sup>                |    107 |   88.6 |   74.4 |   55.3 |   42.2 |   32.1 |   30.0 |   28.3 |   26.8 |   25.5 |   24.0 |   22.3 |   20.4 |   18.3 |
^ PM<sup>2</sup>                |    194 |    161 |    134 |   93.6 |   64.4 |   43.0 |   39.5 |   36.7 |   34.5 |   32.6 |   30.6 |   28.4 |   26.0 |   23.6 |
^ CO                            |    856 |    795 |    725 |    560 |    429 |    321 |    301 |    283 |    266 |    250 |    233 |    215 |    195 |    176 |
| **LPG**                                                                                                                                       |||||||||||||||
^ NH<sub>3</sub>                |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |   0.21 |
^ NMVOC                         |    147 |    147 |    145 |    145 |    145 |    145 |    145 |    145 |    145 |    144 |    141 |    134 |    126 |    116 |
^ NO<sub>x</sub>                |  1,346 |  1,342 |  1,325 |  1,325 |  1,325 |  1,325 |  1,325 |  1,325 |  1,325 |  1,316 |  1,284 |  1,225 |  1,144 |  1,056 |
^ SO<sub>x</sub>                |   0.42 |   0.42 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |   0.41 |
^ BC<sup>3</sup>                |   0.13 |   0.13 |   0.13 |   0.13 |   0.13 |   0.13 |   0.13 |   0.13 |   0.13 |   0.12 |   0.12 |  11.6% |   0.11 |   0.10 |
^ PM<sup>2</sup>                |   0.85 |   0.85 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |   0.84 |
^ CO                            |    114 |    114 |    112 |    112 |    112 |    112 |    112 |    112 |    112 |    112 |    112 |    112 |    112 |    112 |
<sup>1</sup> due to lack of better information: similar EF are applied for fossil and biofuels \\
<sup>2</sup> EF(PM<sub>2.5</sub>) also applied for PM<sub>10</sub> and TSP (assumption: > 99% of TSP consists of PM<sub>2.5</sub>) \\
<sup>3</sup> estimated via a f-BCs as provided in [(EMEPEEA2023)], Chapter 1.A.2.g vii, 1.A.4.a ii, b ii, c ii, 1.A.5.b i - Non-road, note to Table 3-1: Tier 1 emission factors for off-road machinery 

<WRAP center round info 100%>
With respect to the emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. ((During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions.))
</WRAP>

<WRAP center round info 100%>
For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources. - Here, for lead (Pb) from leaded gasoline and corresponding TSP emissions, additional emissions have been calculated from 1990 to 1997 based upon contry-specific emission factors from TREMOD MM.
</WRAP>

===== Discussion of emission trends =====

>  **NFR 1.A.4.a ii** is no key source.

==== Unregulated pollutants ====


For all unregulated pollutants, emission trends directly follow the trend in fuel consumption.

==== Regulated pollutants ====

===Nitrogen oxides and Sulphur dioxide===

For all regulated pollutants, emission trends follow not only the trend in fuel consumption but also reflect the impact of fuel-quality and exhaust-emission legislation.
Here, emissions of sulphur oxides follow the step-by-step reduction of sulphur contents in liquid fuels, resulting in a reduction of over 99% since 1990. 

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4aii_em_sox.png?702 | Annual sulphur oxides emissions }}

===Ammonia===

Ammonia emissions are driven by the consumption of LPG with its comparably high emission factor.

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4aii_em_nh3.png?702 | Annual ammonia emissions }}

===NMVOC===

Emissions oif NMVOC are again driven by the consumption of LPG with its comparably high emission factor. Here, the ongoing downward trend results from the decrease in the emission factor applied for diesel fuels.
{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4aii_em_nmvoc.png?702 | Annual NMVOC emissions }}
====Particulate matter & Black carbon====

Over-all PM emissions are by far dominated by emissions from diesel oil combustion with the falling trend basically following the decline in fuel consumption between 2000 and 2005. 
Nonetheless, the decrease of the over-all emission trend was and still is amplified by the expanding use of particle filters especially to eliminate soot emissions.

Additional contributors such as the impact of TSP emissions from the use of leaded gasoline (until 1997) have no significant effect onto over-all emission estimates.

Here, as the EF(BC) are estimated via fractions provided in the 2019 EMEP Guidebook [(EMEPEEA2019)], black carbon emissions follow the corresponding emissions of PM<sub>2.5</sub>.

{{ :sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:1a4aii_em_pm.png?702 | Annual particulate-matter emissions }}

===== Recalculations =====

Compared to the previous submission, recalcultaions in primary activity data (PAD) result from the revision of the National Energy Balance 2022 and the percental shares as compiled in TREMOD MM [(KNOERR2024b)].

__Table 5: Revised percental shares__
|                      ^  1990   ^  1995   ^  2000  ^  2005  ^  2010  ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^  2022   ^
^ current submission   |   0.060 |   0.040 |  0.037 |  0.037 |  0.051 |   0.054 |   0.054 |   0.054 |   0.048 |   0.049 |   0.049 |   0.046 |   0.043 |
^ previous submission  |   0.061 |   0.040 |  0.037 |  0.037 |  0.051 |   0.058 |   0.060 |   0.061 |   0.056 |   0.058 |   0.057 |   0.056 |   0.051 |
^ absolute change      |  -0.001 |   0.000 |  0.000 |  0.000 |  0.000 |  -0.004 |  -0.006 |  -0.007 |  -0.008 |  -0.008 |  -0.008 |  -0.010 |  -0.008 |
^ relative change      |  -2.16% |  -0.70% |  0.00% |  0.00% |  0.35% |  -6.29% |  -10.1% |  -11.5% |  -13.7% |  -14.6% |  -14.1% |  -18.6% |  -15.9% |
\\

Source-specific activity data have been revised accordingly:

__Table 6: Revised source-specific activity data, in terajoules [TJ]__
|                      ^  1990   ^  1995   ^  2000  ^  2005  ^  2010  ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^  2022   ^
| **DIESEL OIL**                                                                                                                         ||||||||||||||
^ current submission   |   6,696 |   3,868 |  3,487 |  3,001 |  4,501 |   5,579 |   5,686 |   5,714 |   4,985 |   5,209 |   5,122 |   4,866 |   4,397 |
^ previous submission  |   6,844 |   3,895 |  3,487 |  3,001 |  4,485 |   5,953 |   6,324 |   6,458 |   5,778 |   6,099 |   5,960 |   5,979 |   5,239 |
^ absolute change      |    -148 |   -27,3 |   0,00 |   0,00 |   15,8 |    -375 |    -639 |    -743 |    -793 |    -889 |    -838 |  -1,114 |    -843 |
^ relative change      |  -2.16% |  -0.70% |  0.00% |  0.00% |  0.35% |  -6.29% |  -10.1% |  -11.5% |  -13.7% |  -14.6% |  -14.1% |  -18.6% |  -16.1% |
| **BIODIESEL**                                                                                                                          ||||||||||||||
^ current submission   |         |         |        |    200 |    352 |     310 |     308 |     312 |     281 |     295 |     419 |     341 |     305 |
^ previous submission  |         |         |        |    200 |    351 |     331 |     343 |     352 |     326 |     346 |     487 |     419 |     364 |
^ absolute change      |         |         |        |   0.00 |   1.23 |   -20.8 |   -34.6 |   -40.5 |   -44.7 |   -50.5 |   -68.5 |   -78.1 |   -59.1 |
^ relative change      |         |         |        |  0.00% |  0.35% |  -6.29% |  -10.1% |  -11.5% |  -13.7% |  -14.6% |  -14.1% |  -18.6% |  -16.2% |
| **LPG**                                                                                                                                ||||||||||||||
^ current submission   |   2,787 |   3,450 |  4,261 |  4,533 |  4,629 |   4,256 |   4,336 |   4,301 |   4,264 |   4,213 |   4,139 |   4,063 |   3,987 |
^ previous submission  |   2,787 |   3,450 |  4,261 |  4,533 |  4,629 |   4,256 |   4,336 |   4,301 |   4,264 |   4,213 |   4,139 |   4,063 |   3,987 |
^ absolute change      |    0.00 |    0.00 |   0.00 |   0.00 |   0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |    0.00 |
^ relative change      |   0.00% |   0.00% |  0.00% |  0.00% |  0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |   0.00% |
| **Ʃ 1.A.4.a ii**                                                                                                                       ||||||||||||||
^ current submission   |   9,482 |   7,318 |  7,749 |  7,733 |  9,482 |  10,144 |  10,330 |  10,327 |   9,530 |   9,718 |   9,680 |   9,270 |   8,689 |
^ previous submission  |   9,630 |   7,345 |  7,749 |  7,733 |  9,465 |  10,540 |  11,004 |  11,110 |  10,368 |  10,658 |  10,586 |  10,462 |   9,591 |
^ absolute change      |    -148 |   -27.3 |   0.00 |   0.00 |   17.0 |    -395 |    -673 |    -784 |    -838 |    -940 |    -906 |  -1,192 |    -902 |
^ relative change      |  -1.54% |  -0.37% |  0.00% |  0.00% |  0.18% |  -3.75% |  -6.12% |  -7.06% |  -8.08% |  -8.82% |  -8.56% |  -11.4% |  -9.40% |

<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2022**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Uncertainties =====

Uncertainty estimates for **activity data** of mobile sources derive from research project FKZ 360 16 023: "Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland" by (Knörr et al. (2009)) [(KNOERR2009)].

Uncertainty estimates for **emission factors** were compiled during the PAREST research project. Here, the final report has not yet been published.

===== Planned improvements =====

Besides the annual **routine revision** of **TREMOD MM**, no specific improvements are planned.

===== FAQs =====

**//Why are similar EF applied for estimating exhaust heavy metal emissions from both fossil and biofuels?//**

The EF provided in [(EMEPEEA2019)] represent summatory values for (i) the fuel's and (ii) the lubricant's heavy-metal content as well as (iii) engine wear. Here, there might be no heavy metal contained in biofuels. But since the specific shares of (i), (ii) and (iii) cannot be separated, and since the contributions of lubricant and engine wear might be dominant, the same emission factors are applied to biodiesel and bioethanol.


[(AGEB2024>AGEB, 2024: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; 
https://ag-energiebilanzen.de/wp-content/uploads/2024/03/EBD22e.xlsx, (Aufruf: 04.12.2024), Köln & Berlin, 2024)]
[(BAFA2024>BAFA, 2024: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2023_12.xlsx?__blob=publicationFile&v=2, Eschborn, 2024.)]
[(KNOERR2024b>Knörr et al. (2024b): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Aktualisierung des Modells TREMOD-Mobile Machinery (TREMOD MM) 2024, Heidelberg, 2024.)]
[(EMEPEEA2023>EMEP/EEA, 2023: EMEP/EEA air pollutant emission inventory guidebook – 2023, https://www.eea.europa.eu//publications/emep-eea-guidebook-2023, Copenhagen, 2023.)]
[(KNOERR2009>Knörr et al. (2009): Knörr, W., Heldstab, J., & Kasser, F.: Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland; final report; URL: https://www.umweltbundesamt.de/sites/default/files/medien/461/publikationen/3937.pdf, FKZ 360 16 023, Heidelberg & Zürich, 2009.)]