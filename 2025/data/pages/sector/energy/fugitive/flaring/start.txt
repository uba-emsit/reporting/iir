====== 1.B.2.c - Venting and Flaring ======

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  1.B.2.c                                |  T2      |  AS  |  CS  |
|  {{page>general:Misc:LegendEIT:start}}                        ||||
\\

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  |  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   |  Pb  |  Cd  ^  Hg   |  Additional HM  |  POPs  |
|  -/-                                    |  -/-    |  -/-             |  NA              |  -/-               |  -/-              |  -/-  |  -/-  |  -/-  |  NA  |  NA  |  -/-  |  NA             |  NA    |
|  {{page>general:Misc:LegendKCA:start}}                                                                                                                                                       ||||||||||||||
\\

Pursuant to general requirements of the Technical Instructions on Air Quality Control TA Luft (2002), gases, steam, hydrogen and hydrogen sulphide released from pressure valves and venting equipment must be collected in a gas-collection system. Wherever possible, gases so collected are burned in process combustion. Where such use is not possible, the gases are piped to a flare. Flares used for flaring of such gases must fulfill at least the requirements for flares for combustion of gases from operational disruptions and from safety valves. For refineries and other types of plants in categories 1.B.2, flares are indispensable safety components. In crude-oil refining, excessive pressures can build up in process systems, for various reasons. 

Such excessive pressures have to be reduced via safety valves, to prevent tanks and pipelines from bursting. Safety valves release relevant products into pipelines that lead to flares. Flares carry out controlled burning of gases released via excessive pressures. When in place, flare-gas recovery systems liquify the majority of such gases and return them to refining processes or to refinery combustion systems. In the process, more than 99 % of the hydrocarbons in the gases are converted to CO<sub>2</sub> and H<sub>2</sub>O. When a plant has such systems in operation, its flarehead will seldom show more than a small pilot flame.

__Table 1: Activity data applied for 1.B.2.c__
^                             ^  Unit           ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2015  ^  2020  ^  2022  ^
| Flared natural gas          | millions of m³  |     36 |     33 |     36 |   18.7 |   12.1 |   10.5 |   14.1 |   10.4 |
| Refined crude-oil quantity  | millions of t   |    107 |   96.5 |  107.6 |  114.6 |   95.4 |   93.4 |   84.0 |   90.0 |

Flaring takes place in extraction and pumping systems and at refineries. In refineries, flaring operations are subdivided into regular operations and start-up / shut-down operations in connection with disruptions. 

__Table 2: Emission factors applied for flaring emissions in natural gas extraction, in [kg/ 1000 m<sup>3</sup>]__
|         ^  Value  ^
^  NMVOC  |  0.005  |
^  NO<sub>x</sub>    |  1.269  |
^  SO<sub>2</sub>    |  8.885  |
^  CO     |  0.726  |

__Table 3: Emission factors applied for flaring emissions at petroleum production facilities__
|       ^  Unit  ^  Value  ^
^  NO<sub>x</sub>  |  kg/t  |  0.008  |
^  SO<sub>2</sub>  |  kg/t  |  0.010  |
^  CO   |  g/t   |  0.1    |

__Table 4: Emission factors applied for flaring emissions at at refineries: normal flaring operations__
|         ^  Unit   ^  Value  ^
^  NMVOC  |  kg/m³  |  0.004  |
^  CO     |  kg/m³  |  0.001  |
^  SO<sub>2</sub>    |  kg/m³  |  0.003  |
^  NO<sub>x</sub>    |  g/m³   |  0.4    |

__Table 5: Emission factors applied for flaring emissions at at refineries: disruptions of flaring operations, in [kg/t]__
|         ^  Value  ^
^  NMVOC  |  0.001  |
^  CO     |  0.001  |
^  SO<sub>2</sub>    |  0.007  |
^  NO<sub>x</sub>    |  0.004  |

The emission factors have been derived from the 2004 and 2008 emissions declarations Theloke et al. 2013 [(THELOKE2013)]. In 2019, they were updated for CH<sub>4</sub>, N<sub>2</sub>O, CO, NMVOC, NO<sub>x</sub> and SO<sub>2</sub>, on the basis of Bender & von Müller, 2019 [(BENDER2019)].

Venting emissions are taken into account in category 1.B.2.b.iii. The SO₂ emissions are obtained from the activity data for the flared natural gas (Table 178) and an emission factor of 0.140 kg / 1,000 m³, a factor which takes account of an average H₂S content of 5 % by volume.
The emission factors are determined on the basis of emissions reports, crude-oil-refining capacity and total capacity utilisation at German refineries. The guide for this work consists of the evaluation assessment of Theloke et al. (2013) [(THELOKE2013)].

===== Recalculations =====

<WRAP center round info 60%>
For more details please refer to the super-ordinate chapter [[sector:energy:fugitive:start|1.B - Fugitive Emissions from fossil fuels]]
</WRAP>
===== Planned improvements =====

Currently no improvements are planned. 

===== References =====


[(THELOKE2013>Theloke, J., Kampffmeyer, T., Kugler, U., Friedrich, R., Schilling, S., Wolf, L., & Springwald, T. (2013). Ermittlung von Emissionsfaktoren und Aktivitätsraten im Bereich IPCC (1996) 1.B.2.a. i-vi - Diffuse Emissionen aus Mineralöl und Mineralölprodukten (Förderkennzeichen 360 16 033). Stuttgart.)]
[(BENDER2019>Bender, M., & von Müller, G. (2019). Konsolidierung der Treibhausgasemissionsberechnungen unter der 2. Verpflichtungsperiode des Kyoto-Protokolls und der neuen Klimaschutz-Berichterstattungs-pflichten an die EU (FKZ 3716 41 107 0).)]
