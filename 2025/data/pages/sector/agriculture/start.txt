====== Chapter 5 - NFR 3 - Agriculture (OVERVIEW) ======


^  NFR-Code   ^  Name of Category                                                                       ^
|  3.B         | [[Sector:Agriculture:Manure_Management:Start| 3.B Manure Management]]                   |
|  3.D         | [[Sector:Agriculture:Agricultural_Soils:Start| 3.D Agricultural Soils]]                 |
|  3.F         | [[Sector:Agriculture:Field_Burning:Start | 3.F Field Burning Of Agricultural Residues]]  |
|  3.I         | [[Sector:Agriculture:Agricultural_Other:Start | 3.I Agricultural: Other]]                |

===== Short description =====


Emissions occurring in the agricultural sector in Germany derive from manure management (NFR 3.B), agricultural soils (NFR 3.D) and agriculture other (NFR 3.I).
Germany does not report emissions in category field burning (NFR 3.F) (key note: NO), because burning of agricultural residues is prohibited by law (see Rösemann et al., 2025)((Rösemann, C., Vos, C., Haenel, H.-D., Dämmgen, U., Döring, U., Wulf, S., Eurich-Menden, B., Freibauer, A., Döhler, H., Schreiner, C., Osterburg, B., Fuß,R. (2025) Calculations of gaseous and particulate emissions from German agriculture 1990 – 2023 : Report on methods and data (RMD) Submission 2024. www.eminv-agriculture.de)).

The pollutants reported are:

  *     ammonia (NH<sub>3</sub>),
  *     nitric oxides (NO<sub>x</sub>),
  *     volatile organic compounds (NMVOC),
  *     particulate matter (PM<sub>2.5</sub>, PM<sub>10</sub> and TSP) and
  *     hexachlorobenzene (HCB).

No heavy metal emissions are reported.

The calculations for the present IIR 2024 were finished before the release of the EMEP (2023) guidebook. Therefore, methodological changes in the EMP (2023) guidebook were not considered for the present submission.

In 2023 the agricultural sector emitted 527.0 Gg of NH<sub>3</sub>, 99.5  Gg of NO<sub>x</sub>, 301.3 Gg of NMVOC, 61.0 Gg of TSP, 35.4 Gg of PM<sub>10</sub> and 5.4 Gg of PM<sub>2.5</sub> and 0.30 kg HCB. The trend from 1990 onwards is shown in the graph below. The sharp decrease of emissions from 1990 to 1991 is due to a reduction of livestock population in the New Länder (former GDR) following the German reunification. The increase of NH<sub>3</sub> emissions since 2005 is mostly due to the expansion of anaerobic digestion of energy crops, especially the application of the digestion residues. This emission source also affects NO<sub>x</sub> emissions. The decrease of NH<sub>3</sub> emissions since 2015 is mostly due to a decline in the amounts of mineral fertilizer sold and stricter regulations concerning application of urea fertilizers, as well as declining livestock numbers Further details concerning trends can be found in Rösemann et al. (2025) chapter “Emissions results submission 2025”. 

As depicted in the diagram below, in 2023 92.6 % of Germany’s total NH<sub>3</sub> emissions derived from the agricultural sector, while nitric oxides reported as NO<sub>x</sub> contributed 11.8 % and NMVOC 30.9 % to the total NO<sub>x</sub> and NMVOC emissions of Germany. Regarding the emissions of PM<sub>2.5</sub>, PM<sub>10</sub> and TSP the agricultural sector contributed 7.0 % (PM<sub>2.5</sub>), 19.5 % (PM<sub>10</sub>) and 18.3 % (TSP) to the national particle emissions. HCB emissions of pesticide use contributed 7.6 % to the total German emissions. 

====Mitigation measures====
The agricultural inventory model can represent several abatement measures for emissions of NH<sub>3</sub> and particles. The measures comprise: 

  * changes in animal numbers and amount of applied fertilizers 

  * air scrubbing techniques: yearly updated data on frequencies of air scrubbing facilities and the removal efficiency are provided by KTBL (Kuratorium für Technik und Bauwesen in der Landwirtschaft / Association for Technology and Structures in Agriculture) and also based on the agricultural census 2020. The average removal efficiency of NH<sub>3</sub> is 80 % for swine and 70 % for poultry, while for TSP and PM<sub>10</sub> the rates are set to 90 % and for PM<sub>2.5</sub> to 70 % for both animal categories. For swine two types of air scrubbers are distinguished: first class systems that remove both NH<sub>3</sub> and particles, and second class systems that remove only particles reliably and have an ammonia removal efficiency of 20%.

  * reduced raw protein content in feeding of fattening pigs: the German animal nutrition association (DVT, Deutscher Verband Tiernahrung e.V.) provides data on the raw protein content of fattening pig feed, therefore enabling the inventory to depict the changes in N-excretions over the time series. The time series is calibrated using data from official and representative surveys conducted by the Federal Statistical Office.

  * reduced raw protein content in feeding and feed conversion rates of broilers: the German animal nutrition association (DVT, Deutscher Verband Tiernahrung e.V.) provides data on the raw protein content of fattening broiler feed, and feed conversion rates of broilers. This makes it possible to model the changes in N-excretions over the timeseries.

  * low emission spreading techniques of manure: official agricultural censuses survey the prevalence of different manure spreading techniques and how fast organic fertilizers are incorporated into the soil. Germany uses distinct emission factors for different methods, techniques and incorporation durations.

  * covering of slurry storage: agricultural censuses survey the prevalence of different slurry covers. Germany uses distinct emission factors for the different covers. 

  * use of urease inhibitors: for urea fertilizer the German fertilizer ordinance prescribes the use of urease inhibitors or the direct incorporation into the soil from 2020 onwards. The NH<sub>3</sub> emission factor for urea fertilizers is therefore reduced by 70% from 2020 onwards for the direct incorporation, according to Bittman et al. (2014, Table 15)((Bittman, S., Dedina, M., Howard C.M., Oenema, O., Sutton, M.A., (eds) (2014): Options for Ammonia Mitigation. Guidance from the UNECE task Force on Reactive Nitrogen. Centre for Ecology and Hydrology, Edinburgh, UK)). For the use of urease inhibitors the NH<sub>3</sub> emission factor is reduced by 60% from 2020 onwards, see Rösemann et al. (2025), Chapter 5.2.1.2.

For NO<sub>x</sub> and NMVOC no mitigation measures are included. 




===== Reasons for recalculations =====


(see [[general:recalculations:start|Chapter 8.1 - Recalculations]])

The following list summarizes the most important reasons for recalculations. Recalculations result from improvements in input data and methodologies (for details see Rösemann et al. (2025), Chapter 1.3). 

  
  - Mineral fertilizers: The NH3 emission factors for mineral fertilizers in EMEP (2023) were used for the first time in the present submission.
  - Crop residues: NH3 emissions from crop residues are calculated for the first time according to the tier 2 method from EMEP (2023). To facilitate this, cover crops are now included in the inventory data. Cover crops are also a newly reported source of PM emissions from soils
 - Cattle and pigs: improved emission factors for cattle and pig housing were introduced based on national research projects. 
  -  Horses: The animal numbers were corrected in all years and all districts by a factor of 2.75 to include horses which are kept outside of agricultural holdings and not counted by agricultural census. All horses are reported under 3.B as permitted by EMEP (2023) Section 4.2. 
  -  Transformation processes in storage: The previous assumption for untreated slurry that 10 % of the TAN entering storage is converted to Norg (immobilization) was dropped because no sufficient scientific evidence could be found for this.
  - Manure Management emission factors for N2O, NOx and N2:  N2O emission factors were adopted from the IPCC Refinement (2019). The N2O EF for solid manure storage doubles, which then also applies to the corresponding NOx and N2 EFs.
  - Dairy cows: Milk yield and slaughter weights for 2022 have been slightly corrected in the official statistics. 
  - Heifers: 2022 slaughter weights have been slightly corrected in the official statistics. 
  - Male beef cattle: In some years, slaughter ages and slaughter weights have been updated in the HIT database. 
  - Numbers of laying hens, pullets and broilers were corrected in the years before 2013. The numbers are higher than in earlier submissions.. 
  - Sows: For several federal states, confidential data for number of piglets per sow and year has been replaced. 
  - Fattening pigs: for several federal confidential data for growth rates, start weights and final weights has been replaced. For Saxony and Saxony-Anhalt (no more recent data available than 2016 or 2017) the corresponding data from the neighboring federal state of Thuringia was adopted instead of keeping the last known value as was previously the case.
  - Poultry: amounts of straw were corrected for all poultry categories
  - Broilers: Update of the national gross production of broiler meat in 2022. 
  - Laying hens and pullets: due to new weight data for laying hens for 2021, the starting and final weights of laying hens have been recalculated for the entire timeseries. Since the initial weight of the laying hens corresponds to the final weight of the pullets, this also has (small) effects on the energy requirements and excretion of the pullets.
  - Animal numbers horses, poultry, goats for 2021 and 2022: The previously extrapolated animal numbers for 2021 and 2022 have been replaced by interpolated animal numbers, as new figures from the agricultural structure survey are available for 2023.
  - Application of inorganic fertilizers: The mitigating factor for urea emissions if applied with urease inhibitor (since the year 2020) was reduced from 70 % to 60 %. Correction of amounts applied in some years before 2008 due to a mistake in calculation of the mean value of the three years going into the moving average.
  - Application of sewage sludge: Replacement of extrapolated activity data in 2022 with data from the Federal Statistical Office and corrections of activity data for  years after 2006.
  - Anaerobic digestion: Update of activity data as of 2013 concerning gastight storage data.  This applies to both digested energy crops and digested animal manure. 
  - Imported manure: The amounts of imported manure from the Netherlands have been updated for years after 2017.
  - Compost and digested waste: input data for 2022 has been updated.
  - Distribution data 1990 – 1999: RAUMIS distribution data is only available for the years 1991, 1995 and 1999. The data for the intervening years (1992 – 1994 and 1996 – 1998 have now been generated by using linear interpolation (1990 = 1991).  



===== Visual overview =====

__Emission trends for main pollutants in //NFR 3 - Agriculture//:__
{{ :sector:iir_nfr3.png?nolink&direct&700 |NFR 3 emission trends per category }}
{{ :sector:iir_nfr3_from_2005.png?nolink&direct&700 |NFR 3 emission trends per category, from 2005 }}

__Contribution of NFRs 1 to 6 to the National Totals, for 2021__
{{ :sector:mainpollutants_sharesnfrs_incl_transport.png?direct&direct&700 | Percental contributions of NFRs 1 to 6 to the National Totals}}

===== Specific QA/QC procedures for the agriculture sector=====

Numerous input data were checked for errors resulting from erroneous transfer between data sources and the tabular database used for emission calculations. The German IEFs and other data used for the emission calculations were compared with EMEP default values and data of other countries (see Vos et al., 2024). Changes of data and methodologies are documented in detail (see Vos et al. 2024, Chapter 1.3). 

A comprehensive review of the emission calculations was carried out by comparisons with the results of Submission 2023 and by plausibility checks. 

Once emission calculations with the German inventory model Py-GAS-EM are completed for a specific submission, activity data (AD) and implied emission factors (IEFs) are transferred to the CSE database (Central System of Emissions) to be used to calculate the respective emissions within the CSE. These CSE emission results are then cross-checked with the emission results obtained by Py-GAS-EM. 

Furthermore, in addition to UNFCCC, UNECE and NEC reviews, the Py-GAS-EM model is continuously validated by experts of KTBL (Kuratorium für Technik und Bauwesen in der Landwirtschaft, Association for Technology and Structures in Agriculture) and the EAGER group (European Agricultural Gaseous Emissions Inventory Researchers Network). 
