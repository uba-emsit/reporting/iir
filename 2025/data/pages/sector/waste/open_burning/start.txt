====== 5.C.2 - Open Burning of Waste ======

^  Category Code                          ^  Method  ^  AD  ^  EF     ^
|  5.C.2                                  |  CS      |  Q   |  D, CS  |
|  {{page>general:Misc:LegendEIT:start}}                           ||||

----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   ^  Pb   ^  Cd   ^  Hg  ^  As  ^  Cr  ^  Cu  ^  Ni  ^  Se  ^  Zn  ^  PCDD/F  ^  B(a)P  ^  B(b)F  ^  B(k)F  ^  I(x)P  ^  PAH1-4  ^  HCB  ^  PCBs  ^
|  -/-                                    |  -/-    |  -/-             |  NE              |  -/-               |  -/-              |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  NE  |  NE  |  NE  |  NE  |  NE  |  NE  |  NE  |  -/-     |  -/-    |  -/-    |  -/-    |  -/-    |  -/-     |  NE   |  NE    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                                                                                                                          ||||||||||||||||||||||||||

Within NFR sub-category 5.C.2 - Open Burning of Waste, the German emissions inventory provides only emissions from allowed bonfires and from other wooden materials burnt outdoors. Emissions from bonfires are key source for PM<sub>2.5</sub> and PM<sub>10</sub>, but in principle of minor priority due to discontinuous appearance.

Please see chapter regarding farming/plantation waste: [[sector:agriculture:field_burning:start|3.F - Field burning of agricultural residues]] - this is banned by law in Germany. So there is no gap of reporting.

Emissions from open burning of wood and green waste for traditional purposes, so-called bonfires such as Easter fires, are reported model-based. In addition to biogenic carbon dioxide, emissions of NO<sub>x</sub>, SO<sub>2</sub>, CO, NMVOC, particulate matter (PM<sub>2.5</sub>, PM<sub>10</sub> and TSP), Polycyclic Aromatic Hydrocarbons (PAHs) and Heavy Metals are covered so far.
\\

=====Method=====

For developing of a estimation frame a survey regarding the number of such bonfires was carried out by an expert work [(Wagner & Steinmetzer, 2018: Jörg Wagner, Sonja Steinmetzer, INTECUS GmbH Abfallwirtschaft und umweltintegratives Management: Erhebung der Größen und Zusammensetzung von Brauchtums- und Lagerfeuern durch kommunale Befragungen; URL: https://www.umweltbundesamt.de/sites/default/files/medien/1410/publikationen/2018-02-19_texte_11-2018_lager-brauchtumsfeuer.pdf; UBA-Texte 11/2018)]. As the result, questionnaires from municipalities and statistical projections for Germany for the year 2016 were checked. The project has shown a declining trend since 1990. On the basis of expert judgement, a further reduction of emissions in the future is expected.

As discussed on Review 2020 regarding all relevant sources: A comparison shows that the volume of bonfires is significantly higher than the volume of campfires. In terms of number, however, the two types of fires are similar. Due to the large fluctuations of the minimum/maximum values, the median was proposed in study.
In our view the estimation of bonfires emissions is conservative and completly.
====Activity data====

Activity data for this category are based on data from a step by step calculation: After the evaluation of the questionaires an extrapolation of the volume and the number of bonfires was made for Germany. For the years since 2019, it became visible that, in addition to the model-based continuous decrease in activities, special aspects must be taken into account: Because of the restrictions on public activities during the pandemic, modeling of less traditional events was searched for. 

Two types of fires were already classified in the expert project: camp fires in the more private sector and, most importantly, Easter Fires in the more public sector. The calculations are now considered separately and the camp fires are modeled with a continued steady decline. 

Here, Easter fires follow an approach about general percentage decreases and additionally in 2019 five percentage points decrease corresponding to various cancels due to forest fire risk. In 2020 and 2021, an additional 70 percent decrease was modeled due to cancellations for pandemic response (but no complete cancellation in Germany because there were exceptions and follow-up events). For 2022 no restrictions were modelled, only the known slight decreasing trend.
The following values are the result of evaluation:

__Table 1: Total annual mass of bonfires, in metric tonnes [t]__
^  1990    ^  1995    ^  2000    ^  2005    ^  2010    ^  2015    ^  2020    ^  2021    ^ 2022  ^
|  431,394 |  414,276 |  397,157 |  380,038 |  362,919 |  345,800 |  135,170 | 134.297  |   324.915  |

====Emission factors====

As discussed on Review 2020 regarding EF used and referenced: We use different EF from different references suitable for the burning of wooden wastes. We consider both fresh wood (garden and park waste) and dry wood (without coatings etc.). We have tried to find relevant parallels, for example because of the burning of fresh wood with regard to forest fires. But the most EF are from GB 2023 for 5.C.2, evaluated and corrected in use, as shown in the following table:

^                   ^  value  ^  unit   ^ Current reference                                                                                                                                                                                                                                                                ^
^ CO                |  48.8   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ NO<sub>x</sub>    |  1.38   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ SO<sub>2</sub>    |  0.03   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ NMVOC             |  1.47   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ TSP               |  4.31   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ PM<sub>10</sub>   |  4.13   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ PM<sub>2.5</sub>  |  3.76   |  kg/ t  | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ BC                |  1,05   |  kg/ t  | GB 2023 5.C.2, table 3-2 (28% of PM2.5)                                                                                                                                                                                                                                          |
^ PCDD/F            |  10.0   |  µg/ t  | GB 2023 5.C.2, table 3-1                                                                                                                                                                                                                                                         |
^ PAH               |  3.39   |  g/ t   | sum of single compounts                                                                                                                                                                                                                                                          |
^ B[a]P             |  1.3    |  g/ t   | IIR Ireland  [(FurtherProject>(EF is referenced to a former research project called 'Use of charcoal, tobacco etc.'. This was a literature research, which is only available via UBA library in German. The EF is relating wood burning as it was documented in Ireland's IIR)]  |
^ B[b]F             |  1.5    |  g/ t   | IIR Ireland  [(FurtherProject)]                                                                                                                                                                                                                                                  |
^ B[k]F             |  0.5    |  g/ t   | IIR Ireland  [(FurtherProject)]                                                                                                                                                                                                                                                  |
^ I[...]P           |  0.09   |  g/ t   | IIR Ireland [(FurtherProject)]                                                                                                                                                                                                                                                   |
^ Pb                |  0.32   |  g/ t   | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |
^ Cd                |  0.13   |  g/ t   | GB 2023 5.C.2, table 3-2                                                                                                                                                                                                                                                         |

\\
===== Trends in emissions =====

All trends in emissions correspond to trends of AD. No rising trends are identifiable in the long term.

[{{:sector:waste:em_5c_bon_since_1990.png|**Emission trends of bonfires**}}]
===== Recalculations =====

Recalculations were necessary due to corrected emission factors. The significant changes can be shown as an absolute difference over time as follows:

[{{:sector:waste:recalculation_5c2_since_1990.png|**Recalculations in NFR 5.C.2**}}]

<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2021**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

