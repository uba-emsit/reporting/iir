====== 5.D.1 - Domestic & Commercial Wastewater Handling  ======

===== Short description =====

^  Category Code                          ^  Method  ^  AD  ^  EF  ^
|  5.D.1                                  |  T1      |  NS  |  D   |
|  {{page>general:Misc:LegendEIT:start}}                        ||||

----

|  NO<sub>x</sub>                         ^  NMVOC  |  SO<sub>2</sub>  |  NH<sub>3</sub>  |  PM<sub>2.5</sub>  |  PM<sub>10</sub>  |  TSP  |  BC  |  CO  |  Heavy Metals  |  POPs  |
|  NA                                     ^  -/-    |  NA              |  NA              |  NA                |  NA               |  NA   |  NA  |  NA  |  NA            |  NA    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                 |||||||||||

In category **5.D.1**, __NMVOC emissions__ from domestic and commercial wastewater handling are reported. The domestic section is covered by wastewaters of municipal origin (large centralised plants; ranging from 1,000 up to >100,000 resident values). The commercial section is covered by industrial and commercial wastewaters, co-treated in municipal wwt-plants.

According to national experts, dry toilets (including latrines) do not play a role in sewage treatment in Germany because they are not in compliance with the legislation and thus do not constitute a procedure of orderly wastewater disposal. Due to that reason NH<sub>3</sub> emissions cannot be estimated and the notation key is set to NA.

§ 55 of the German water resources act (german: Wasserhaushaltsgesetz, WHG) demands the assuring of the general wellbeing in order of the wastewater disposal (german: gemeinwohlverträgliche Abwasserentsorgung). To ensure this requirement the water regarding laws of the several federal states of Germany (e.g. § 46 Abs. 1 WG BW; Art. 34 BayWG) obligate to the transfer of wastewater from the citizen to the public authorities or to assigned companies (german: Überlassungspflicht). The details are described in municipal bylaws which for the most cases obligate to the connection to the municipal wastewater infrastructure (german: Anschluss- und Benutzungszwang). Exceptions are possible but most likely realised in form of septic tanks or drainless cesspools.

We assume that if there are very little exceptions for dry-toilets on a municipal level, that those are demanded to be separating toilets, as urine and faeces would be collected separately. Because of the necessary contact between urine and faeces to build ammonia from urea (contained in urine) by hydrolysis through urease (enzyme, contained in faeces) and the assumed very little number of exceptions, there are no assessable emissions of ammonia. 

The superior federal law (WHG) described above was redesigned and implemented in its current form in the year 2009 following the reform of federalism (german: Föderalismusreform) and to implement requirements from the 2000/60/EC Water Framework Directive. The regulation has been described by the laws of the federal states before this time but latest with the implementation of the requirements of the 91/271/EEC directive concerning urban waste water treatment (e.g. BayROkAbwV).


===== Method =====
Emissions reported under this category are calculated using the Tier 1 approach of the EMEP/EEA Guidebook 2019, where the emission factor (EF) is 15 mg/m³ wastewater (Part B, 5.D, chap. 3.2.2, Table 3-1, p. 7 [(EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019, Copenhagen, 2019)]). This EF is multiplied with the total amount of wastewater (AD) treated in domestic and commercial wwt-plants, following the equation:\\
\\ 
<WRAP center round info 25%>
**Emissions <sub>(NMVOC)</sub> = AD x EF** (ibid., chap. 3.2.1)
</WRAP>


==== Activity data ====

Total volumes of treated municipal wastewater are derived by the German statistical agency (Statistisches Bundesamt, Fachserie 19, Reihe 2.1.2 [(Statistisches Bundesamt, Fachserie 19, Reihe 2.1.2)]). The data source is published on a three-year basis with new data only for the respective year of the update. The availability of the data starts in 1991 with an exception for the following update, which was for 1995. Missing data are inter- or extrapolated.

==== Emisson factors ====
See method

===== Uncertainties =====
The AD from Statistisches Bundesamt have an uncertainty of ±3% (normal distribution) whereas the uncertainty for the EF, due to its range (5/50 mg/m³), is -70 / +210 % and the distribution lognormal.

===== Recalculations =====

Recalculations were not necessary.


<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates** for Base Year and for the current year, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====
Currently no improvements are planned.
\\
\\
\\