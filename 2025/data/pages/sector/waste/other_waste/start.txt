====== 5.E - Other Waste (please specify in IIR)  ======

Under NFR category **5.E - Other Waste**, Germany so far reports __greenhouse gas emissions from the mechanical biological treatment (MBT) of waste__ as well as __air-pollutant emissions from building and car fires__.

^  Category Code                                                                              ^  Method                    ^  AD  ^  EF  ^
| 5.E - Other Waste                                                                           | see sub-category details               |||
| //consisting of / including source categories://                                                                                    ||||
| 5.E.1 - Mechanical Biological Treatment (MBT)                                               |  NA  (GHG emissions only)              |||
| 5.E.2 - [[Sector:Waste:Other_Waste:Building_And_Car_Fires:Start| Building and Car Fires ]]  |  D                         |  NS  |  D   |
|  {{page>general:Misc:LegendEIT:start}}                                                                                              ||||

----

^  NO<sub>x</sub>                         ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO  ^  Pb   ^  Cd   ^  Hg   ^  As   ^  Cr   ^  Cu   ^  Ni  ^  Se  ^  Zn  ^  PCDD/F  ^  PAHs  ^  HCB  ^ PCBs  ^
|  NE                                     |  NE     |  NE              |  NE              ^  L/-               ^  L/-              |  -/-  |  -/-  |  NE  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  -/-  |  NE  |  NE  |  NE  ^  L/-     |  NE    |  NE   | NE    |
|  {{page>general:Misc:Legendkca:start}}                                                                                                                                                                                                      ||||||||||||||||||||||