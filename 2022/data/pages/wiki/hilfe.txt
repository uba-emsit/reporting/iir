====== Hilfeseite für Autoren ======

===== Dokuwiki - was ist anders, was ist zu beachten =====
Gegenüber der bisherigen Plattform auf Wikidot.com nutzt der neue NIR die Plattform DokuWiki. Diese unterscheidet sich auch abseits der technischen Aspekte von der Insellösung Wikidot.com.

==== Login & Passwörter ====
Zugangsdaten sollten alle bekommen haben, wenn nicht einfach fragen. Bitte ändert das Passwort so schnell wie möglich.
<WRAP Tip>Das Wiki-System versendet aus Sicherheitsgründen keine E-Mails, d.h. die Funktion Passwort zurücksetzen funktioniert nicht. Bitte kontaktiert die zuständigen KollegInnen.</WRAP>

==== Änderungen ====
Da das System keine Mails verschicken kann experimentiere ich gerade mit einem Plugin dass für beliebige Namespace-Ebenen und Nutzer die letzten Änderungen zeigt.

[[iir:changes_all_nfr| Changes for all NFR-Codes (under construction)]]

==== Aussehen ====
Wir ihr auf Anhieb seht hat der neue IIR ein ganz anderes Design, aber auch komplett neue Funktionen.

Links ist (je nach Bildschirmauflösung ein- oder augegklappt) eine **Navigation** zu finden (der sog. [[:sidebar|Sidebar]]), diese ermöglicht ein zielgenaues Ansteuern der Unterkapitel. Darunter findet ihr unter dem Punkt **//Site Tools//** auch weitere Möglichkeiten das Wiki zu navigieren bzw. die letzten Änderungen anzusehen.

**Ein- und Ausloggen** ist wie bisher oben rechts zu finden (aber auch im //Sidebar// links unter //User Tools//), wenn ihr eingeloggt seid seht ihr dort auch euren Benutzernamen und die Einstellungen.

Aber auch die einzelnen **Seiten** sehen etwas anders aus als im alten Wiki. Alle Seiten mit Überschriften generieren (so man es nicht ausschaltet) ein Inhaltsverzeichnis, dieses ist oben rechts unter //Table of Contents// per Mausklick aufklappbar.
Über der Seite seht ihr neben dem kleinen Haus auch die Position der Seite in der Struktur des Wiki (dem sog. Namespace).

Rechts neben der Seite befindet sich eine Spalte mit Optionen, dort findet ihr alle Funktionen zur gesamten Seite (bearbeiten, exportieren usw.). Wer genau hin guckt erkennt auch nach jedem durch Überschriften definierte Abschnitt einen kleinen Stift rechts, damit kann ein einzelner Abschnitt bearbeitet werden, der Rest bleibt dann unangetastet. An Tabellen seht ihr auch eine kleine Fahne unten mit der Funktion //EDIT//, damit könnt ihr Tabellen in einem separaten Editor bearbeiten.

==== Syntax ====
DokuWiki hat eine etwas andere Art Text während der Bearbeitung zu formatieren. Dazu gibt es eine allgemeine Hilfeseite direkt im Wiki: [[wiki:syntax|Syntax]].
Für besondere Formatierungen gibt es Befehls-Erweiterungen, die wichtigsten findet ihr auch in der [[wiki:syntax#syntax_plugins|Syntax-Hilfeseite am Ende unter Syntax Plugins]], die Links dort führen auf die externen Hilfeseiten zu den Funktionen.

<WRAP Tip> Erweiterungen deren Syntax ggf. für euch interessant sind:
  * [[http://dokuwiki.org/plugin:include|Include]] - damit kann man die Seiten oder teile von Seiten in andere Seite einbinden, z.B. sinnvoll wenn man Elemente öfter als einmal verwenden möchte
  * [[http://www.dokuwiki.org/plugin:gallery|Gallery]] - damit kann man Grafiken in einer kleinen Galerie zusammenfassen statt sie direkt einzubetten
  * [[http://www.dokuwiki.org/plugin:hidden|Hidden]] - damit lassen sich einzelne Elemente ausklappbar gestalten (z.B. Legenden)
  * [[https://www.dokuwiki.org/plugin:imagebox|ImageBox]] - einfach eckige Klammern um beliebige Medien setzen und die Bezeichnung wird unter das Objekt gesetzt und ein grauer Kasten um Medium & Text gesetzt (wie bei Wikipedia).
  * [[https://www.dokuwiki.org/plugin:wrap|Wrap]] - Wem die vorgegebenen Layout-Funktionen nicht ausreichen der findet mit Wrap einen Baukasten der Größe eines Baumarktes.
  * [[https://www.dokuwiki.org/plugin:mathpublish|Math]] - wie der Name sagt kann man hier mathematische Gleichungen erstellen, das ist nicht unbedingt einfach aber sehr elegant.
</WRAP>

==== Namespaces - Struktur des WIKI ====
Im Gegensatz zu Wikidot nutzt dieses Wiki einen Namespace, ein Art Ordnerstruktur in der die einzelnen Inhalte (Seiten, Medien) hirarchisch abgelegt sind.
Diese Struktur bildet sich teilwise selbstständig wenn ihr z.B. neue Seiten anlegt, aber sie wird auch generiert wenn man beim Setzen von internen Links die Struktur angibt. Gebt ihr bei einem internen Link keine extra Struktur an so verbleibt ihr in der Struktur der Seite von der ihr aus verlinkt.

Dies seht ihr z.B. auf der Seite [[:sidebar|Sidebar]], dort habe ich die Ebenen bereits im Link vorgeplant, sobald die Seite //Start// erstellt wird bildet sich die Struktur //General > Key Categories// :
   [[General:Key Categories:Start | 1.5 Key Categories]]

<WRAP Tip>Inhalte & Strukturen lassen sich auch nachträglich mit dem Werkzeug //Move// in der Struktur verscheiben. Bitte meldet euch falls dies notwendig wird</WRAP>
 


==== Komfort-Funktionen ====
Wie schon angesprochen bietet die Plattform DokuWiki eine etwas komfortablere Arbeitsumgebung als Wikidot.
Hier noch mal zusammengefasst:
  * Bearbeiten einzelner Abschnitte einer Seite (kleiner Stift recht nach einem durch eine Überschrift definierten Abschnitt).
  * Viel einfacheres Bearbeiten von Tabellen im Tabellen-Editor, inkl. Copy & Paste der Daten aus Excel-, Word- oder HTML-Tabellen. 
  * Komfortables Einfügen von internen Links mit Hilfe eines Auswahl-Assistenten (via Linksymbol ∞ in der Menüzeile über dem Editor-Fenster).
  * Die Hierarchische Struktur des Wiki erübrigt das manuelle Benennen von übergeordneten Seiten (breadcrumbs).
  * Anständiger Medienmanager (links in der Navigation unter Site Tools > media Manager) der alle Medien des ganzen Wiki zentral & hierarchisch aufführt. Diese sind dort dann auch einfach austauschbar.



===== Migration von Wikidot.com =====
Da die Syntax der beiden Wikisysteme sehr unterschiedlich ist bringt es wenig den Quelltext der alten Wikiseite in das neue System zu kopieren.
daher hier ein Vorgehen wie ich es bereits erprobt habe.

==== Texte ====
Einfacher ist es den Inhalt der HTML-Seite per Copy & Paste als reinen Text in die leere Seite im neuen Wiki zu kopieren und die Formatierungen (Überschriften, Aufzählungen etc.) händisch nach zu pflegen.

==== Medien ====
Viele Medien werden jedes Jahr aktualisiert, für diese braucht ja keine Migration stattzufinden, es reicht die aktualisierte Datei ins neue Wiki hochzuladen.
Für zu migrierende Medien sind diese im alten Wiki herunterzuladen und dann bei der Bearbeitung über das Menü (Bilderrahmen - //Add images and other files//) im Medienmanager hochzuladen.

==== Tabellen ====
Hier hat sich bei mir bewährt eine leere Tabelle über das Menü anzulegen, die notwendige Anzahl Zeilen & Spalten einzufügen, ggf. zusammengefasste Zellen anzulegen so dass ein Copy & Paste-Vorgang aus Excel-, Word- oder HTML-Tabellen einfach möglich ist. Das erfordert ggf. beim ersten mal etwas Übung, die Aktualisierung später ist dann aber komfortabel möglich.

<WRAP Tip>Im Zweifel gerne nachfragen, geben gerne Hilfestellung.</WRAP>

===== Erstellung neuer Inhalte =====
Neue Seiten, Bilder oder Tabellen lassen sich dank Komfort-Funktionen recht einfach erstellen.
==== Neue Seiten ====
Wie auch im alten Wiki ist die Erstellung neuer Seiten am einfachsten wenn auf einer Seite (in gleicher Hierarchiestufe) ein interner Link auf die zu erstellende Seite gesetzt wird. Diesen roten Link klickt man an und kann gleich loslegen. Dabei ist es sinnvoll die neuen Seiten so zu benennen dass sie sich in die vorgegebene Struktur (siehe Ebenen im Sidebar) einpasst. 
==== Neue Tabellen ====
Im Editor fügt man eine neue Tabelle über das Symbol {{:wiki:insert_table.png?nolink|}} //Insert a new table// ein. Dann erscheint der Tabellen-Editor in dem ihr einige Formatierungsoptionen im Menü und eine Reihe von weiteren Funktionen wie das Einfügen oder Löschen von Zellen im Kontextmenü (per Rechtsklick auf zellen) habt.
Eine wichtige Funktion ist die oberste im Kontextmenü: //**H** Toggle Header State// die einstellt ob die ausgewählten Zellen Kopfzeile sind (fett und leicht grau hinterlegt). Damit markiert ihr bitte die Zeilen (bzw. Spalten) die die Datenkategorien & Einheiten enthalten.

==== Neue Medien ====
Neue Medien könnt ihr im Editor-Menü mit Klick auf das entsprechende Symbol (grünes Bild mit braunem Bilderrahmen) hochladen oder auswählen.
Das erscheinende Fenster ist der Mediamamanger, dort könnt ihr auswählen wo die Datei stehen soll (im Zweifel nichts ändern, voreingestellt ist dies immer die Hierarchieebene der bearbeiteten Seite), Optionen zum hochladen und im Anschluss umbenennen der Datei wenn nötig.

<WRAP Tip>Wenn ihr Medien habt die ihr an mehreren Stellen benutzt empfiehlt sich ggf. diese zusammen an einem Ort in der Hierarchie abzulegen, z.B. eine Ebene höher als die Artikel die diese einbinden.</WRAP>
==== Neue Quellen ====
Für Quellen bietet das Wiki eine ähnliche Herangehensweise wie bisher:
Die Erweiterung [[https://www.dokuwiki.org/plugin:refnotes|RefNotes]] bietet die Möglichkeit die Quellen einfach mit entsprechender Syntax im Text zu benennen, jeder Eintrag wird dann eine Fußnote die unter dem Text steht und auch erscheint wenn man über der Fußnote mit der Maus verweilt. Dies ist ein gutes Vorgehen wenn man wenig Quellen zitiert und diese auch nicht mehrfach zitiert werden.

Mehr Optionen findet ihr auf der [[https://www.dokuwiki.org/plugin:refnotes:syntax|Hilfeseite zur RefNotes-Syntax]].

Eine einfache Aufwertung bieten z.B. **[[https://www.dokuwiki.org/plugin:refnotes:syntax#named_notes|Named Notes]]**, d.h. man gibt jedem Zitat einen Namen und kann die Einträge dann mehrfach zitieren.

<WRAP Tip> Weitere Optionen bei großen Literaturbeständen die ggf. auf mehreren Seiten genutzt werden sollen sind auch möglich, ebenso wie eine Kategorisierung über Namespaces. Da müsstet ihr euch aber erst mal selbst reinfuchsen. </WRAP>
===== Bearbeitung von Inhalten =====
Die Bearbeitung funktioniert prinzipiell wie die Neuerstellung.
**Seiten** könnt ihr wie gesagt abschnittsweise bearbeiten, dies erhöht v.a. auf langen Seiten die Übersicht.
**Tabellen** könnt ihr aus der normalen Ansicht über das Fähnchen EDIT direkt bearbeiten.
**Medien** lassen sich auch komfortabel über den Medienmanager bearbeiten, diesen findet ihr wie gesagt im Sidebar unter Site Tools.

Änderungen in der **Namespace-Struktur** der angelegten Inhalte können nur Admins vornehmen, dafür gibt es ein Werkzeug welches auch die Links zwischen den Inhalten korrigiert.



