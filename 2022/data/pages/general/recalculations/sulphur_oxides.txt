====== Recalculations - Sulphur Oxides ======

**Revision of NO<sub>x</sub> emissions over time:**

{{ :general:recalculations:SO2_absolut.png?700 }}
{{ :general:recalculations:SO2_relativ.png?700 }}

The changes in the **National Total** reported for **1990 (-14.4 kt or -0.26 %)** result almost entirely from revised emission estimates in NFRs **1.A.3.d ii** (-13.80 kt), **2.B.6** (+1.73 kt) and **2.B.10.a** (-2.58 kt) together with less significant changes throughout NFRs 1 and 2.

Here, the strongest percental change occurs for **1.A.3.d ii with minus 56 %**.

__Table 1: Changes in emission estimates for 1990__
^                      ^  Submission 2021  ^  Submission 2022  ^  Difference             |^  Reasoning                                                                                       ^
^ NFR Sector           ^  [kt]                                              ||^ relative  ^  see description and reasoning in:                                                               ^
^ NATIONAL TOTAL       ^  5.474,42         ^  5.460,02         ^  -14,40      ^  -0,26%   ^ sub-category chapters                                                                            ^
^ NFR 1 - Energy       ^  5.297,93         ^  5.284,38         ^  -13,55      ^  -0,26%   ^ sub-category chapters                                                                            ^
| 1.A.3.a ii(i)        |  0,18             |  0,18             |  0,00        |  -0,03%   | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]  |
| 1.A.3.c              |  7,73             |  8,24             |  0,51        |  6,56%    | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                |
| 1.A.3.d ii           |  24,67            |  10,87            |  -13,80      |  -55,94%  | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                |
| 1.A.4.c iii          |  0,36             |  0,19             |  -0,18       |  -48,74%  | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| here ]]        |
| 1.A.5.b              |  3,55             |  3,47             |  -0,08       |  -2,27%   | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]             |
^ NFR 2  - IPPU        ^  176,39           ^  175,54           ^  -0,85       ^  -0,48%   ^ sub-category chapters                                                                            ^
| 2.B.6                |  C                |  1,73             |  1,73        |           | [[Sector:IPPU:Chemical_Industry:Titanium_Dioxide Production:Start| here ]]                       |
| 2.B.10.a             |  56,50            |  53,93            |  -2,58       |  -4,56%   | [[Sector:IPPU:Chemical_Industry:Other:Start| here ]]                                             |
^ NFR 3 - Agriculture  ^  NA                                                           |||^                                                                                                  ^
^ NFR 5 - Waste        ^  0,11             ^  0,11             ^  0,00        ^  0,00%    ^                                                                                                  ^
^ NFR 6 - Other        ^  NA                                                           |||^                                                                                                  ^


Changes in the **National Total** reported for **2019 ( -3.84 kt or -1.46 %)** result mainly from revisions in **NFR sub-categories 1.A.1.c, 1.A.2.g viii and 2.A.3** together with less relevant changes throughout NFRs 1, 2 and 5.

__Table 2: Changes in emission estimates for 2019__
^                      ^  Submission 2021  ^  Submission 2022  ^  Difference              ^^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [kt]                                              ^^^  relative  ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  263,53           ^  259,70           ^  -3,84       ^  -1,46%    ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  195,50           ^  193,90           ^  -1,59       ^  -0,81%    ^ sub-category chapters                                                                                                                     ^
| 1.A.1.a              |  99,43            |  99,78            |  0,35        |  0,36%     | [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| here ]]                                         |
| 1.A.1.b              |  32,80            |  32,91            |  0,11        |  0,34%     | [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| here ]]                                                             |
| 1.A.1.c              |  8,86             |  7,73             |  -1,13       |  -12,72%   | [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| here ]]                         |
| 1.A.2.a              |  4,73             |  4,87             |  0,14        |  2,96%     | [[sector:energy:fuel_combustion:industry:iron_and_steel| here ]]                                                                          |
| 1.A.2.b              |  0,382            |  0,378            |  -0,004      |  -1,05%    | [[sector:energy:fuel_combustion:industry:non-ferrous_metals| here ]]                                                                      |
| 1.A.2.e              |  0,79             |  0,77             |  -0,01       |  -1,64%    | [[sector:energy:fuel_combustion:industry:food_processing_beverages_and_tobacco| here  ]]                                                  |
| 1.A.2.g vii          |  0,01824          |  0,01825          |  0,00001     |  0,05%     | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.2.g viii         |  27,27            |  25,67            |  -1,59       |  -5,84%    | [[sector:energy:fuel_combustion:industry:other:start| here  ]]                                                                            |
| 1.A.3.a i(i)         |  0,65             |  0,65             |  0,01        |  1,10%     | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0,165            |  0,166            |  0,001       |  0,64%     | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  0,517            |  0,516            |  -0,001      |  -0,13%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0,062            |  0,061            |  -0,001      |  -1,06%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0,2419           |  0,2416           |  -0,0003     |  -0,13%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0,0070           |  0,0069           |  -0,0001     |  -1,10%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.c              |  0,225            |  0,239            |  0,014       |  6,45%     | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                                                         |
| 1.A.3.d ii           |  0,61             |  0,38             |  -0,23       |  -37,88%   | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a i            |  0,90             |  1,24             |  0,34        |  37,36%    | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:commercial_institutional| here ]]                            |
| 1.A.4.a ii           |  0,003990         |  0,003994         |  0,000004    |  0,10%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]                            |
| 1.A.4.b i            |  9,47             |  10,41            |  0,93        |  9,84%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  0,00155          |  0,00162          |  0,00007     |  4,50%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]                                             |
| 1.A.4.c i            |  2,32             |  1,82             |  -0,50       |  -21,61%   | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:agriculture_forestry_fishery| here ]]                        |
| 1.A.4.c ii           |  0,02161          |  0,02164          |  0,00003     |  0,12%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]                                |
| 1.A.4.c iii          |  0,02             |  0,01             |  -0,01       |  -45,52%   | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| here ]]                                                 |
| 1.A.5.a              |  0,011            |  0,012            |  0,001       |  7,94%     | [[sector:energy:fuel_combustion:other_including_military:stationary_fuel_combustion_in_military_facilities| here ]]                       |
| 1.A.5.b              |  0,04             |  0,02             |  -0,02       |  -39,13%   | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]                                                      |
| 1.B.1.b              |  0,800            |  0,804            |  0,004       |  0,48%     | [[Sector:Energy:Fugitive:Solid Fuels:Start| here ]]                                                                                       |
| 1.B.2.a iv           |  4,2097           |  4,2104           |  0,0006      |  0,01%     |                                                                                                                                           |
| 1.B.2.c              |  0,9525           |  0,9526           |  0,0001      |  0,01%     |                                                                                                                                           |
^ NFR 2  - IPPU        ^  67,90            ^  65,66            ^  -2,24       ^  -3,30%    ^ sub-category chapters                                                                                                                     ^
| 2.A.3                |  9,88             |  7,64             |  -2,24       |  -22,70%   | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                                                            |
| 2.A.6                |  1,2034           |  1,2039           |  0,0005      |  0,04%     | [[Sector:IPPU:Mineral_Industry:Other_Mineral_Products:Start| here ]]                                                                      |
| 2.B.6                |  C                |  0,95             |  0,95        |            | [[Sector:IPPU:Chemical_Industry:Titanium_Dioxide Production:Start| here ]]                                                                |
| 2.B.10.a             |  10,91            |  9,96             |  -0,95       |  -8,75%    | [[Sector:IPPU:Chemical_Industry:Other:Start| here ]]                                                                                      |
| 2.H.1                |  0,620            |  0,619            |  -0,002      |  -0,27%    | [[Sector:IPPU:Pulp_Paper_Food:Pulp_And_Paper_Industry:Start| here ]]                                                                      |
^ NFR 3 - Agriculture  ^  NA                                                            |||^                                                                                                                                           ^
^ NFR 5 - Waste        ^  0,137            ^  0,138            ^  0,001       ^  0,66%     ^ sub-category chapters                                                                                                                     ^
| 5.C.2                |  0,063            |  0,064            |  0,001       |  1,44%     | [[Sector:Waste:Open_Burning:Start| here ]]                                                                                                |
^ NFR 6 - Other        ^  NA                                                            |||^                                                                                                                                           ^