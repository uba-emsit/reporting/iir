====== Recalculations - Cadmium (Cd) ======

The marginal changes within the **National Total** reported for **1990 (-0.0004 t | -0.001 %)** resultfrom revisions throughout NFRs 1 and 2.

Here, the strongest percental change occurs for NFR **1.A.4.c iii** with minus 50 %.

__Table 1: Changes of emission estimates 1990__
^                          ^  Submission 2021  ^  Submission 2022  ^  Difference               |^  Reasoning                                                                                            ^
^ NFR Sector               ^  [t]                                                ||^  relative  ^  see description and reasoning in:                                                                    ^
^ NATIONAL TOTAL           ^  29,1013          ^  29,1010          ^  -0,0004      ^  -0,001%   ^ sub-category chapters                                                                                 ^
^ **NFR 1 - Energy**       ^  12,7616          ^  12,7591          ^  -0,003       ^  -0,02%    ^ sub-category chapters                                                                                 ^
| 1.A.3.a i(i)             |  0,00000012       |  0,00000008       |  -0,00000004  |  -33,33%   | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]  |
| 1.A.3.a ii(i)            |  0,0000017        |  0,0000011        |  -0,0000006   |  -33,33%   | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]       |
| 1.A.3.c                  |  0,011            |  0,013            |  0,002        |  14,84%    | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                     |
| 1.A.3.d ii               |  0,014            |  0,010            |  -0,004       |  -28,84%   | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                     |
| 1.A.4.b ii               |  0,0033           |  0,0034           |  0,0001       |  2,79%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]         |
| 1.A.4.c iii              |  0,0002           |  0,0001           |  -0,0001      |  -50,70%   | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| here ]]             |
| 1.A.5.b                  |  0,00035          |  0,00028          |  -0,00007     |  -18,87%   | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]                  |
^ **NFR 2  - IPPU**        ^  16,265           ^  16,267           ^  0,002        ^  0,01%     ^ sub-category chapters                                                                                 ^
| 2.A.3                    |  0,029            |  0,033            |  0,004        |  13,24%    | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                        |
| 2.D.3.g                  |  NA               |  0,000009         |  0,000009     |            | [[Sector:IPPU:Other_Solvent_And_Product_Use:Chemical_Products:Start| here ]]                          |
| 2.G                      |  1,078            |  1,077            |  -0,002       |  -0,15%    | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                        |
^ **NFR 3 - Agriculture**  ^  NA                                                             |||^                                                                                                       ^
^ **NFR 5 - Waste**        ^  0,075            ^  0,075            ^  0,00         ^  0,00%     ^                                                                                                       ^
^ **NFR 6 - Other**        ^  NA                                                             |||^                                                                                                       ^

The changes within the **National Total** reported for **2019 (-0.15 t | -1.36 %)** are dominated by changes in **NFR 2.A.3 with -0.16 t** together with less significant revisions throughout NFRs 1, 2 and 5.

The most significant percental changes occur for **NFRs 1.A.3.a i(i) and 1.A.3.a ii(i) with minus 84 %**.

__Table 1: Changes of emission estimates 2019__
^                      ^ Submission 2021  ^ Submission 2022  ^  Difference               ^^ Reasoning                                                                                                                                 ^
^ NFR Sector           ^  [t]                                              ^^^  relative  ^ see description and reasoning in:                                                                                                         ^
^ NATIONAL TOTAL       ^  10,85           ^  10,70           ^  -0,15        ^  -1,36%    ^ sub-category chapters                                                                                                                     ^
^ NFR 1 - Energy       ^  2,91            ^  2,89            ^  -0,01        ^  -0,51%    ^ sub-category chapters                                                                                                                     ^
| 1.A.1.a              |  0,76            |  0,76            |  0,01         |  0,96%     | [[sector:energy:fuel_combustion:energy_industries:public_electricity_and_heat_production| here ]]                                         |
| 1.A.1.b              |  1,05            |  1,02            |  -0,03        |  -3,15%    | [[sector:energy:fuel_combustion:energy_industries:petroleum_refining| here ]]                                                             |
| 1.A.1.c              |  0,0088          |  0,0086          |  -0,0003      |  -2,85%    | [[sector:energy:fuel_combustion:energy_industries:manufacture_of_solid_fuels_and_other_energy_industries| here ]]                         |
| 1.A.2.g vii          |  0,0000682       |  0,0000680       |  -0,0000002   |  -0,24%    | [[sector:energy:fuel_combustion:industry:mobile_combustion_in_manufacturing_industries_construction| here ]]                              |
| 1.A.2.g viii         |  0,10            |  0,11            |  0,01         |  11,76%    | [[sector:energy:fuel_combustion:industry:other:start| here  ]]                                                                            |
| 1.A.3.a i(i)         |  0,00000005      |  0,00000001      |  -0,00000004  |  -83,52%   | [[sector:energy:fuel_combustion:transport:civil_aviation:international_civil_aviation_-_lto| here ]]                                      |
| 1.A.3.a ii(i)        |  0,0000011       |  0,0000002       |  -0,0000009   |  -83,69%   | [[sector:energy:fuel_combustion:transport:civil_aviation:domestic_civil_aviation_-_lto| here ]]                                           |
| 1.A.3.b i            |  0,004042        |  0,004044        |  0,000002     |  0,05%     | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars| here ]]          |
| 1.A.3.b ii           |  0,000223        |  0,000220        |  -0,000002    |  -1,10%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles| here ]]     |
| 1.A.3.b iii          |  0,000782        |  0,000781        |  -0,000001    |  -0,15%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles| here ]]     |
| 1.A.3.b iv           |  0,000086        |  0,000085        |  -0,000001    |  -1,10%    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles| here ]]  |
| 1.A.3.c              |  0,0036          |  0,0040          |  0,0004       |  10,34%    | [[sector:energy:fuel_combustion:transport:railways:start| here ]]                                                                         |
| 1.A.3.d ii           |  0,0052          |  0,0050          |  -0,0002      |  -3,24%    | [[sector:energy:fuel_combustion:transport:navigation:national_navigation| here ]]                                                         |
| 1.A.4.a ii           |  0,00000710      |  0,00000712      |  0,00000001   |  0,18%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| here ]]                            |
| 1.A.4.b i            |  0,5551          |  0,5552          |  0,0001       |  0,02%     | [[sector:energy:fuel_combustion:small_combustion:stationary_small_combustion:residential| here ]]                                         |
| 1.A.4.b ii           |  0,00201         |  0,00204         |  0,00004      |  1,87%     | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| here ]]                                             |
| 1.A.4.c ii           |  0,0039          |  0,0038          |  -0,0001      |  -1,80%    | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| here ]]                                |
| 1.A.4.c iii          |  0,000119        |  0,000075        |  -0,000044    |  -37,07%   | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| here ]]                                                 |
| 1.A.5.b              |  0,00014         |  0,00005         |  -0,00009     |  -64,99%   | [[sector:energy:fuel_combustion:other_including_military:military_transport| here ]]                                                      |
^ NFR 2  - IPPU        ^  7,880           ^  7,746           ^  -0,13        ^  -1,70%    ^ sub-category chapters                                                                                                                     ^
| 2.A.3                |  0,18            |  0,02            |  -0,16        |  -90,03%   | [[Sector:IPPU:Mineral_Industry:Glass_Production:Start| here ]]                                                                            |
| 2.D.3.g              |  NA              |  0,00001         |  0,00001      |            | [[Sector:IPPU:Other_Solvent_And_Product_Use:Chemical_Products:Start| here ]]                                                              |
| 2.G                  |  0,83            |  0,85            |  0,02         |  2,96%     | [[Sector:IPPU:Other_Product_Use:Start| here ]]                                                                                            |
^ NFR 3 - Agriculture  ^  NA                                                           ^^^^                                                                                                                                           ^
^ NFR 5 - Waste        ^  0,0606          ^  0,0611          ^  0,0006       ^  0,98%     ^ sub-category chapters                                                                                                                     ^
| 5.C.2                |  0,0409          |  0,0415          |  0,0006       |  1,44%     | [[Sector:Waste:Open_Burning:Start| here ]]                                                                                                |
^ NFR 6 - Other        ^  NA                                                           |||^                                                                                                                                           ^