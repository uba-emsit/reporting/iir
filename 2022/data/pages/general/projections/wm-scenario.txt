=== Measures that have already been implemented or measures whose implementation has been decided are assigned to the WM scenario. ===


**Reductions in large combustion plants through implementation of the 13<sup>th</sup> and 17<sup>th</sup> BImSchV as well as minimum requirements of recent BAT conclusions:**

Measures for large combustion plants (LCP) that have already been implemented through the 13<sup>th</sup> and 17<sup>th</sup> BImSchV or do have future reduction effects from the existing regulations as well as minimum requirements of recent BAT conclusions are considered as WM scenario. With the judgment of 27 January 2021 - T-699/17 - by the ECJ, the complete implementation of these assumptions is fraught with uncertainties. At the time the emission projections were drawn up, the jurisdiction had not yet been made. For this reason, the measures are included in WM scenario. The measures affect time series from the NFR sectors 1.A.1, 1.A.2 and 1.A.3 and lead to a reduction in the emission factors. Potential mitigation effects emerge from BAT conclusions according to Directive 2010/75/EU. If the current submission 2020 shows that the emissions in the time series are already below the upper ends of the specified emission ranges and thus the statutory maximum limit values will be fulfilled, these will be updated unchanged. In the case of time series above the upper range, the maximum permitted limit values are used as a result of the measure in the sense of a conservative estimation and the emission factors of the pollutants for each source group are recalculated.

The calculations always follow the same procedure. Important elements are the specific limit values of the 13<sup>th</sup> and 17<sup>th</sup> BImSchV as well as the distribution of the plants according to their rated thermal input (RTI) in megawatts (MW). In addition, it is assumed that all new and existing plants correspond at least to the standard of the upper range of the associated BAT conclusions. The lower emission factor out if both calculations is than compared with the reference value. If the recalculated emission factor of the source category under consideration is greater than the current reference value, the reference value from the 2020 submission will be updated unchanged. If the reference value is larger, the new value is set and projected.

According to expert estimates, the plant inventory is split as in Table 2 according to the RTI (in MW). These (cumulative) proportions are necessary for the calculation of the mean values in relation to the upper range of limit values for each source category and pollutant.

__Table 2: Proportionate inventory of LCPs according to their power range__ 
^  RTI in MW  ^  Proportion  ^ 
|  <100	      |   4.5 %	     |			
|  100-300    |   14.5 %     |		
|  300-1000   |   68 %       | 	
|  >1000      |   13 %       | 	
|<sub>The limit values of LCP are set according to their power ranges. The table shows the estimated proportion of LCP in Germany in relation to the RTI provided.</sub>||

__Example 1__

The concrete procedure is illustrated using the example of NO<sub>X</sub> emissions from the use of raw lignite as fuel for heat generation in public district heating plants. 

The specific limit values for lignite can be found in Commission Implementing Decision (EU) 2017/1442 BAT 20. With a reference oxygen of 6 per cent, the plants are differentiated according to size and specified with the limit value in mg / Nm<sup>3</sup>. The limit values are converted into kg / TJ using the specific conversion factor of 2.40 (see Table 1). The calculated limit value is therefore averaged for each plant size, taking into account the number of plants, and thus, the estimated value for the necessary NO<sub>X</sub> emission factor for compliance with the limit value is calculated in accordance with the BAT conclusions. The necessary data can be found in Table 3. This shows the plants subdivision according to their RTI with the assigned limit values in mg / Nm<sup>3</sup> and kg / TJ. 

__Table 3: Emission limit values (yearly averages) when using raw lignite in existing plants__ 
^  Plant size according to RTI in MW  ^  max limit value in mg/m<sup>3</sup>  ^  max limit value in kg/TJ  ^  Proportion  ^
| 		<100	  	      |		270           		 | 	112.70	  	    |	4.5 %  	 |
| 		100-300		      |		180	       		 |	75.13	  	    |  14.5 %  	 |
| 		>300		      |		175	       		 |	73.04     	    |	81 %   	 |
|<sub>The LCP emission limit values for the use of raw lignite are regulated in (EU) 2017/1442 BAT 20. There are separate limit values for each RTI of the plant. The upper range is shown here as a limit value for existing plants as yearly averages in mg / Nm<sup>3</sup> and kg / TJ.</sub>|||| 

The emission factor is calculated in (1).

    (1) emission factor (lignite) = 112.70 kg/TJ * 4.5% + 75.13 kg/TJ * 14.5% + 73.04 kg/TJ * 81% = 75.13 kg/TJ

The comparison with the current submission 2020 shows that the calculated emission factor (75.13 kg / TJ) is lower than that of the reference value from 2018 (76.8 kg / TJ). Thus from 2020 onwards the emission factor will be replaced by the new value and used for the projection. 

This procedure is analogous for the evaluation of all source groups and pollutants.  

__Example 2__

According to the Commission Implementing Decision (EU) 2017/1442 of 31 July 2017 on Conclusions on Best Available Techniques (BAT) according to Directive 2010/75/EU of the European Parliament and of the Council for large combustion plants, the maximum permissible pollutant emission for NO<sub>X</sub> while using heavy fuel oil as fuel in plants with more than 1500 operating hours per year is 300 mg / Nm<sup>3</sup> as yearly average for existing plants. Thus, the maximum emission quantity is applicable law and is below the inventory emission factor for the reference year 2018 and therefore assigned to the WM scenario. Affected time series are assigned to the NFR sector 1.A.1.b in which the emission factors are reduced. Since a reduction is not expected until 2025, the emission factor of the source categories corresponds to the current value of the 2020 submission. The emission factors from 2025 onwards result from assuming the upper range of the BAT conclusion (300 mg / Nm<sup>3</sup>) as the maximum permitted emission and thus set it as emission factor. The value is converted into kg / TJ according to the specific flue gas volume of heavy fuel oil (see Table 1).

After the conversion, a projected NO<sub>X</sub> emission factor of 88.5 kg / TJ results as indicated in equation (2).

    (2) emission factor (heavy fuel oil) = 300 mg/Nm^3 / 3.39 = 88.5 kg/TJ.

__Special features of the evaluation of the emission factors__

When using liquid fuels in LCP, the specific conversion factor of 3.39 (see Table 1) is used for the assessment of NO<sub>X</sub> emissions, analogous to heavy fuel oil, when using “other mineral oil products”.

When evaluating NO<sub>X</sub> emissions from the use of refinery gas, a distinction must be made between electricity and heat generation, as the limit values differ. A maximum limit value of 100 mg / Nm<sup>3</sup> applies to electricity generation, whereas plant size-specific limit values (200 mg / Nm<sup>3</sup> for plants <300 MW and 100 mg / Nm<sup>3</sup> for plants >300 MW) must be taken into account for heat generation. 

When calculating the SO<sub>2</sub> emissions from source group “Mitverbrennung in öffentlichen Fernheizwerken” and “Mitverbrennung in öffentlichen Kraftwerken”, a clear distinction is made between existing plants and new plants. The emission factor of the existing plants is estimated at 78.44 kg / TJ and adopted for 2020. It is assumed that by 2030 all plants will correspond to the latest technology and will therefore be adopted from 2030 onwards with the limit value for new plants, estimated at 61.81 kg / TJ. Furthermore, it is assumed that a linear / continuous renewal takes place, so that the mean value from 2020 and 2030 is calculated for 2025 and assumed as the emission factor (70.13 kg / TJ).


**Reduction in large combustion plants burning lignite through the coal phase-out:**

The German Coal Phase-Out Law (“Kohleausstiegsgesetz”) from August 2020 stipulates to gradually phase out coal power plants burning lignite until 31 December 2038. The activity rates and emission factors of public heating and thermal power plants for NO<sub>X</sub> are therefore reassessed.

The starting point for the evaluation of the activity rates as a result of the phase-out is the current total RTI. According to the official phase-out path of the Federal Ministry for Economic Affairs and Energy((https://www.bmwk.de/Redaktion/DE/Downloads/S-T/stilllegungspfad-braunkohle.html)) and assuming that from 2039 onwards (after the shutdown of the last blocks) no more RTI will be provided, the RTI per district counted back for the years 2020, 2025, 2030 and 2035 are shown in Table 4. As a result, the reduction in the observed periods from 2020 to 2035 can be calculated in absolute and relative values for each district.

__Table 4: Decommissioning path of the districts according to RTI in the years 2018 to 2039__ 
^ District	  ^ RTI 2018 in MW ^ RTI 2020 in MW ^ 		^ RTI 2025 in MW ^ 	    ^ RTI 2030 in MW ^   	^ RTI 2035 in MW ^ 		^ RTI 2039 in MW ^
^ Lausitz	  | 	6292	   ^ 	  6000	    |       0	^ 	 6000    |   -3000  ^ 	  3000	     | 	   0	^     3000	 |     -3000	^ 	0	 |
^ Central Germany | 	2650	   ^ 	  2650	    | 	    0	^ 	 2650    | 	 0  ^ 	  2650       | 	-900	^     1750	 |     -1750	^ 	0	 |
^ Rhineland	  | 	8985	   ^  	  8520      |	-2820	^ 	 5700    |   -2700  ^ 	  3000       | 	   0	^     3000	 |     -3000	^ 	0	 |
^ Total		  |	17927	   ^ 	 17170	    | 		^	14350    | 	    ^ 	  8650       | 		^     7750	 | 	        ^  	0        |
|<sub>Current RTI of the individual districts as well as the overall RTI according to the decommissioning path in the course of the coal phase-out in Germany.</sub>|||||||||||

The total emissions per district for the years 2020 to 2035 are now calculated from the relative values of the years under review and the total emissions from the 2020 submission (1123133.92 kt NO<sub>X</sub>). The calculation of the value for the Lausitz district in 2020 is shown in (3) - (4) as an example.

According to the values in Table 4 the relative value of the Lausitz district is calculated as:

    (3) RTI proportion (Lausitz in 2020) = (6000 MW)/(17170 MW) = 0.35.

The total emission for the area in 2020 results from the total emission from the submission and the calculated share of the Lausitz district:

    (4) total NOx-emission (Lausitz in 2020) = 1123133.92 kt * 0.35 = 392475.45 kt.   

In addition, the distribution of the electricity and heat generation per district is necessary for the estimation of the activity rates. For this purpose, the share of NO<sub>X</sub> emissions is divided into the two energy generation processes per district and the activity rates from the time series of the 2020 submission are averaged over the years 1995 to 2018. This results in a share of electricity or heat energy for the reference value from 2018.

Finally, the activity rates of the individual districts for the years 2020 to 2035 result from the product of the calculated share of the reference value from 2018 for electricity or heat generation and the total emissions of the districts for the year under consideration. The activity rates of the Helmstedt and Hesse districts will be updated with 0 for electricity and heat generation from 2020, since the phase-out has already been completed here.

When calculating the NO<sub>X</sub> emission factors as a result of the phase-out, the areas of Central Germany, Lausitz and Rhineland are considered separately. The Helmstedt and Hesse districts are not included in the analysis as explained above. The individual districts will be subdivided into their existing power plants. For each power plant, the total activity rate and the emission factors for NO<sub>X</sub> for the years 2004 to 2018 in TJ or kg / TJ from the 2020 submission are adopted as data basis. In order to take into account fluctuations in the activity rates and emission factors, the activity rates and emission factors are averaged over the years. In addition, the mean value for all power plants in a district is calculated for the formation of the emission factor by weighting according to their activity. Hence, each district is assigned an implied emission factor for the years 2020 to 2035 according to its phase-out path.

With the shutdown of the last block of a power plant, this plant is considered to be shut down and from this point in time it is no longer included in the calculation of the emission factor. This applies to the Schkopau power plants (Central Germany district) from 2035 onwards, Jänschwalde, Boxberg III (both: Lausitz district) and Weißweiler (Rhineland district) from 2030 onwards.

In the case of Boxberg IV in the Lausitz district, the time series will only be taken into account from 2013 onwards, as Unit R started continuous operation on 16 February 2012, initially on a test basis and finally officially as the last unit in October 2012, meaning that the Boxberg IV power plant will only have reliable data from 2013 onwards.

**Reduction in small combustion installations through the 1<sup>st</sup> BImSchV**

The amendment of the 1<sup>st</sup> BImSchV in 2010 by further tightening the emission limit values for NO<sub>X</sub> in oil-fired small combustion installations in § 6 (1) to 110 mg / kWh for installations up to 120 kW, 120 mg / kWh for installations between 120 kW and 400 kW and 185 mg / kWh for installations above 400 kW results in approximately 30.1, 33.3 and 51.4 kg / TJ as emission factors. In dependence of a weighting based on the size class distribution in different sectors, that were determined in the UBA project PAREST((https://www.umweltbundesamt.de/sites/default/files/medien/461/publikationen/texte_48_2013_appelhans_e010_komplett_0_0.pdf)), leading to averaged implied emission factors shown in Table 5, relevant for time series from the NFR sectors 1.A.4 and 1.A.5.a. The implied emission factors of the four relevant source groups are given in the project for 2020 in kg / TJ and are kept constant for the projection in 2025 and 2030. Table 5 shows the values from PAREST for 2020 and the reference value from the 2020 submission for each source category and fuel.  

__Table 5: Emission factors for oil-fired small combustion installations (SCI)__
^ Source Group 					         ^ Fuel 	      ^ NO<sub>X</sub> EF 2018 in kg/TJ (Submission 2020) ^ NO<sub>X</sub> EF 2020 in kg/TJ (PAREST) ^
| Heat generation in SCI of the households  		 | Light heating oil  |	 		41.77	 	       		  |		31.8 	     		     |
| Heat generation in SCI in agriculture and horticulture | Light heating oil  |			43.65	               		  |		39.6 	       		     |
| Heat generation in SCI of the military services 	 | Light heating oil  |			43.65	               		  |		39.7 	       		     |
| Heat generation in SCI of the other small consumers    | Light heating oil  |			43.65	               	 	  |		39.6 	       		     |

Reductions of dust emissions from small combustion installations are achieved in the NFR sectors 1.A.4 and 1.A.5 through the implementation of the 1<sup>st</sup> BImSchV. The calculation of the future emission factors is based on the projection of the "Energiewende" scenario (EWS) from Tebert et al. (2016)((Tebert, C., Volz, F., Töfke, K. (2016): Development and update of emission factors for the National Inventory regarding small and medium-size combustion plants of households and small consumers, on behalf of the German Environment Agency (UBA), Project-Nr. 3712 42 313 2)), while the current underlying projection is containing a greater use of solid biomass in 2030 than the EWS. The developments in the area of small combustion systems, in particular the development of fuel use and the existing plant inventory, are difficult to assess and are fraught with uncertainties. According to expert assessments, with an increase of solid biomass use the implied emission factor will further decrease as the share of newer and cleaner installations will go up. Therefore, the projected implied emission factors based on the EWS used here are expected to be conservative. 

The report by Tebert et al. (2016) as well as the appendix show the fractions of fuel consumption in small combustion installations according to plant type and output range in absolute and relative sizes for 2030. In addition, a distinction is made between households (“Haushalte” (HH)) and commerce, trade, services (“Gewerbe, Handel, Dienstleistung” (GHD)). In Table 6, the dust emission factors for 2030 for HH and GHD are given in kg / TJ per type of installation and the relative share in TJ is shown. All types of installations are weighted with the associated emission factor and finally the weighted mean for HH and GHD in kg / TJ for 2030 can be calculated and taken over into the projection for dust.

__Table 6: Share of fuel used in small combustion installations in 2030 in EWS __
^ plant type 			             				^ HH-EF in kg/TJ ^	GHD-EF in kg/TJ ^ Proportion of HH in TJ in % ^ Proportion of GHD in TJ in % ^
| slow-burning stoves		             				| 	    86	      | 	86	| 		0.8	      | 	 0.0		     |
| tiled stoves			             				| 	    97        | 	97	| 	       12.3	      | 	 1.7                 |
| fireplaces with open combustion chamber    				|          132        | 	132	| 		8.7	      | 	 0.3                 |
| fireplaces with closed combustion chamber  				| 	    32        | 	32	| 	       35.1	      | 	 2.0                 |
| pellet stoves			  	     				| 	    21	      | 	21	| 		7.5	      | 	 0.4                 |
| split log boilers (manually-stoked) (4-25 MW) 			| 	    41	      | 	41	| 		4.9	      | 	 1.0                 |
| split log boilers (manually-stoked) (25-50 MW)			| 	    13        | 	13	| 	       13.0	      | 	 2.7                 |
| split log boilers (manually-stoked) (> 50 MW)				| 	    17        | 	17	| 		8.4	      | 	 1.7                 |
| wood chip boiler (4-25 MW) 						| 	    	      | 	12	| 		 	      | 	 0.5                 |
| wood chip boiler (25-50 MW)						|                     | 	31	| 		 	      | 	16.1                 |
| wood chip boiler (>50 MW)  						| 	              | 	20	| 		 	      | 	14.4                 |
| pellet boilers (4-25 MW)	  	     				| 	    14        | 	14	| 		6.8	      | 	 0.4                 |
| pellet boilers (25-50 MW)	  	     				| 	    13        | 	13	| 		1.0           | 	 0.1                 |
| pellet boilers (>50 MW)	  	     				| 	    14        | 	14	| 		1.1           | 	 0.1                 |
| bathroom boilers     		  	     				| 	    51        | 	51	| 		0.0           | 	 0.0                 |
| cooking stoves                       	     				| 	    41        | 	41	| 		0.4           | 	 0.2                 |
| manually-stoked heating boilers (commercial, incl. residual wood)	| 	              | 	18	| 		              | 	19.8                 |
| injection furnaces	  	  	     				| 	              | 	35	| 		              | 	13.4                 |
| underfeeding furnaces		  	     				| 	              | 	27	| 		              | 	15.7                 |
| pre-boiler furnaces	 	  	     				| 	              |		21	| 		  	      |          9.5                 |
^ Weighted mean EWS		  	     				| 	  43.44       |       26.44	| 		              | 	                     |

For the years 2020 and 2025, the emission factors were calculated using the reference value from the 2020 submission in such a way that a linear reduction in dust emissions takes place.

The emission factors of the source groups “Wärmeerzeugung in KFA der Landwirtschaft und Gärtnereien” and “Wärmeerzeugung in KFA der militärischen Dienststellen” result in the year 2030 from the same ratio as to “Wärmeerzeugung in KFA der Haushalte” in the reference year 2018. This is shown as an example for the case of “Wärmeerzeugung in KFA der Landwirtschaft und Gärtnereien” in (5).

    (5) emission factor (""Wärmeerzeugung in KFA der Landwirtschaft und Gärtnereien") = (43.44 kg/TJ / 75.93 kg/TJ) * 84.24 kg/TJ = 48.19 kg/TJ


**Reduction in industrial processes through low-dust filter technology in sinter plants:**

The assumed potential for reducing dust emissions from sinter plants is taken from the final report of the UBA project Luft 2030 (Jörß et al., 2014((Jörß, W., Emele, L., Scheffler, M., Cook, V., Theloke, J., Thiruchittampalam, B., Dünnebeil, F., Knörr, W., Heidt, C., Jozwicka, M., Kuenen, J.J.P., Denier van der Gon, H.A.C., Visschedijk, A.J.H., van Gijlswijk, R.N., Osterburg, B., Laggner, B., Stern, R., Handke, V. (2014): Luftqualität 2020/2030: Weiterentwicklung von Prognosen für Luftschadstoffe unter Berücksichtigung von Klimastrategien, on behalf oft he German Envrionment Agency (UBA), Project-Nr. 3710 43 219, UBA-Texte 35/2014, https://www.umweltbundesamt.de/publikationen/luftqualitaet-20202030-weiterentwicklung-von))), where measure P 009 results in dust emissions of less than 10 mg / Nm<sup>3</sup> due to better filter technology. It is assumed that only half of the potential from the LUFT 2030 project will be reached in average. Thus, the emission factors for PM<sub>2.5</sub> and PM<sub>10</sub> result from the mean value of the current submission 2020 and the emission factor from the LUFT 2030 project at 50 per cent each. The affected time series are assigned to the NFR sector 2.C.1. This technology also causes new split factors for the calculation of PM<sub>2.5</sub> and PM<sub>10</sub>. Therefore, the split factor for PM<sub>10</sub> is taken from the LUFT 2030 project, too.

The emission factor for dust is calculated by dividing the given sizes of the emission factor for PM<sub>10</sub> by the split factor for PM<sub>10</sub>. Consequently, the split factor for PM<sub>2.5</sub> can be calculated by dividing the emission factor for PM<sub>2.5</sub> by the emission factor for dust.

These calculated factors (emission factor dust and the split factors for PM<sub>2.5</sub> and PM<sub>10</sub>) for the recorded emission sources are used for the projection and transferred to the database.


**Reduction in industrial processes resulting from updated emissions factors in the nitric acid production:**

The NO<sub>X</sub> emissions from nitric acid production in Germany are preliminary reassessed by updating the emission factors. This reduction concerns the time series of the NFR sector 2.B.2. In 2020, an internal query was carried out among the relevant companies on the NO<sub>X</sub> emission factors, in which almost all producers participated. As a result, there was a certain variability of the emission factors, both with regard to the data quality and the absolute measured values. In the overall picture, however, it became clear that all plants had an emission factor of less than 2 kg NO<sub>X</sub> per tonne of nitric acid and thus fell well below the constant inventory emission factor of 10 kg per tonne. The industry association and industry experts assume that the non-participating systems will not exceed the stated value. As a result, the conservative assessment of the experts is taken into account in the emission projection and the emission factor is updated to 2 kg / t for the years 2020 to 2035. A comprehensive update of the inventory is planned for 2021.


**Reduction in medium combustion plants through implementation of the 44<sup>th</sup> BImSchV:**

The general conditions for the calculation of the pollutant emissions from medium combustion plants (MCP), gas turbines and combustion engine plants are regulated by the 44<sup>th</sup> BImSchV and are therefore part of the WM scenario. The underlying limit values of the emission calculation are taken from the 44<sup>th</sup> BImSchV (March 2020). The measure leads to a reduction in the emission factors of the affected time series from the NFR sector 1.A.1 to 1.A.5.

The data basis for the calculation is the submission 2020. The source categories are reassessed separately according to the pollutants and the relevant fuel inputs. The expected service life of the plants (in years) is taken into account (see Table 8) as well as a distinction is made between old and new plants and the RTI of the plants in MW (see Table 7). Table 7 shows the plant split for the various fuel uses taking into account the RTI.

__Table 7: Proportional plant split of the MCP according to fuel consumption and RTI__
^ Plant split according to fuel consumption ^  RTI in MW  ^  Proportion  ^
^ Biomass			    	    | 	 1-5	|   6.5 %    |
|		:::			    | 	5-20	|  17.7 %    |
|		:::		            |  20-50	|  75.8 %    |
^ Lignite				    |   1-20	|  95.8 %    |
| 		:::			    |  20-50	|   4.2 %    | 
^ Hard coal				    |   1-20	|  90.2 %    |
|		:::			    |  20-50	|   9.8 %    |
^ Heavy fuel				    |   5-20	|  68.0 %    |
|	       :::                          |  20-50	|  32.0 %    |
|<sub>The limit values of the MCP are specified in the 44<sup>th</sup> BImSchV according to their performance ranges. The table shows the estimated proportion of MCP in Germany in relation to the RTI provided and the fuel input used.</sub>|||

__Table 8: Expected service life of MCP according to type of plant, pollutant and fuel use__ 
|							^  Expected average service life 	^
^ Combustion plants - solid fuels			|	20 years 			|				    
^ Combustion plants – liquid and gaseous fuels		|	15 years 			| 
^ gas and steam turbines (GuD) and gas turbines (GT)	|	22 years 			| 
^ internal combustion engines - biogas			|	 5 years			| 
^ internal combustion engines – other fuels		|	10 years			|

The new emission factors are always calculated according to the same pattern. The limit values of the 44<sup>th</sup> BImSchV are weighted for each power range of the plants and calculated for old and new plants. Assuming that a constant rate of existing plants is renewed or upgraded annually, the weighting of the limit values for new plants for the projections in 2025, 2030 or 2035 is increased or, depending on the expected service life of the plant, only the limit values for new plants are taken into account.

If the current emission factor from the 2020 submission undercuts the calculated value, the current reference value is updated because it is already below the upper range according to the 44<sup>th</sup> BImSchV and thus complies with the maximum limit values. The recalculated values for the time series are adopted and the maximum permitted limit value is assigned to time series when the current emission factor is above the upper range.

__Example:__

The exact procedure is exemplified by the example of NO<sub>X</sub> emission factors when using solid biomass as fuel. The procedure is in principle the same for all pollutants and fuels.

The basis for the calculation is the maximum amount of NO<sub>X</sub> emissions permitted in the 44<sup>th</sup> BImSchV §10 (4) and (15) when using solid biomass (other solid biomass) as fuel (see Table 9). After conversion with the specific conversion factor of 2.39 (see Table 1), the limit values for old and new plants are available in kg / TJ. Table 9 shows the limit values for solid biomass according to the performance range for old and new plants in mg / Nm<sup>3</sup> and kg / TJ.

__Table 9: Limit values for solid biomass in MCP according to the power range for old and new plants__
^ Fuel					^ Plant		^  Limit value according to 44<sup>th</sup> BImSchV in mg/Nm<sup>3</sup>   ^^^ Limit value in kg/TJ    ^^^
| 	::: 				| ::: 		|  Power range in MW				      		|||  Power range in MW	      ||| 
| 	::: 				| ::: 		|  1-5	|  >5	|  >20						|   1-5	   |    >5   |   >20    | 
^ Solid biomass (other solid biomass) 	| existing	| 	  600  ||  370						|        250.4      ||  154.43  | 
^ Solid biomass (other solid biomass)	| new		|  370	|  300	|  200						|   154.4  |  125.2  |   83.5	| 
|<sup>Limit values for solid biomass in MCP for old and new plants according to the 44th BImSchV in mg / Nm^3 and kg / TJ.</sup>||||||||

It is assumed that the service life of the plant is 20 years (see Table 8). In addition, it is assumed that an annual renewal of the plant will be implemented after the 44<sup>th</sup> BImSchV comes into force in 2019 and that the limit values for new plants getting greater weight each year. 

According to the assumption in 2025 (6 years after the regulation came into force) there is a proportion of 6/20 which fulfil the requirements of new plants and 14/20 which adhere to the limit values of old plants. In 2030, eleven years after the 44<sup>th</sup> BImSchV was introduced, the proportion of new plants is 11/20 compared to 9/20 old plants. After 16 years, the limit value for new plants is included in the calculation with 16/20.

Taking into account the plants proportions per size measured in RTI in WM (see Table 9), a new emission factor of 153.01 kg / TJ for 2025 results, as shown in (6).

    (6) emission factor (solid biomass in 2025) = 14/20 * {(6.5% + 17.7%) * 250.4 kg/TJ + 75.8% * 154.4 kg/TJ} + 6/20 * {6.5% * 154.4 kg/TJ + 17.7% * 125.2 kg/TJ + 75.8% * 83.5 kg/TJ} = 153.01 kg/TJ.

Since the reference value from the 2020 submission (137.5 kg / TJ) is already below the calculated limit, it will be updated for the year 2025. The procedure for calculating the emission factor in 2030 is identical and is shown in (7).

    (7) emission factor (solid biomass in 2030) = 9/20 * {(6.5% + 17.7%) * 250.4 kg/TJ + 75.8% * 154.4 kg/TJ} + 11/20 * {6.5% * 154.4 kg/TJ + 17.7% * 125.2 kg/TJ + 75.8% * 83.5 kg/TJ} = 132.46 kg/TJ

In 2030 the newly calculated limit value will be below the reference value, so that this is adopted as the new NO<sub>X</sub> emission factor.

__Special Feature:__

When calculating the NO<sub>X</sub> emission factors when using lignite and hard coal as fuel, the plant split is only differentiated according to the RTI of less than 20 MW and greater than 20 MW. The limit values given in the 44<sup>th</sup> BImSchV are differentiated according to 1-5 MW, 5-20 MW and more than 20 MW. As a result, the assumption was made that the plant split between 1-5 MW and 5-20 MW in equal proportions would be valued with a factor of 0.5. 

According to the 44<sup>th</sup> BImSchV § 16, the emission limit values for combustion engines will only apply from 1 January 2025 on, so that the assumption of the partial renewal of plants will only apply from 2025 on. As a result, the reference values from the 2020 submission will be updated as the emission factor for the source categories concerned for 2020 and 2025.

The source groups of “Wärmeerzeugung in TA Luft-Anlagen der Landwirtschaft und Gärtnereien“ (for SO<sub>2</sub> and NO<sub>X</sub>) as well as “Wärmeerzeugung in Pfanzenölmotoren der übrigen Kleinverbraucher” (for NO<sub>X</sub>) were assessed separately. In some cases, their data series have a strong deviation in the emission factors in the reference scenario compared to the remaining source groups with the fuel use of other liquid fuels and would distort the data assessment.

**Reduction in agriculture in the updated Thünen-Baseline-Projection:**

The starting point for the WAM scenario were not the emissions from 2005, but the probable emissions that result from the updated Thünen baseline for the year 2030((Thuenen-Report 82 (2020): Thünen-Baseline 2020 – 2030: Agrarökonomische Projektionen für Deutschland, https://www.thuenen.de/media/publikationen/thuenen-report/Thuenen_Report_82.pdf)). For the NAPCP 2019 this was the Thünen baseline 2017-2027((Thuenen-Report 56 (2018): Thünen-Baseline 2017 – 2027: Agrarökonomische Projektionen für Deutschland, https://www.thuenen.de/media/publikationen/thuenen-report/Thuenen-Report_56.pdf)), which was extrapolated for the year 2030. According to this old baseline, agricultural NH<sub>3</sub> emissions in 2030 were expected to be 542 kt NH<sub>3</sub>. 

Since the methods for calculating emissions improve or change between inventory submissions and these changes also have an impact on previous years, the NH<sub>3</sub> quantities to be reduced when converted to absolute figures are variable and depend on the respective underlying inventory submission.

According to the updated baseline 2020-2030, however, agricultural emissions for 2030 are only 485 kt NH<sub>3</sub>. This large difference between the two baseline emissions (a reduction by 57 kt NH<sub>3</sub>) is explained by the following differences between the two baselines:

   - Reduction of the amount of energy crops for fermentation to about half the amount of the old baseline (effect: -22 kt NH<sub>3</sub> emissions).
   - Reduction of the amount of mineral fertilizer applied (from 1772 kt N to 1655 kt N) and a lower proportion of urea in the amount applied (effect: -19 kt NH<sub>3</sub> emissions).
   - Due to the new Fertilizing Ordinance (DÜV 2020), liquid manure (except for leachate) and poultry manure applied to uncultivated arable land must be incorporated within 1 hour. In addition, a decline in the prevalence of the "broad-cast" technology was projected (effect: approximately -15 kt NH<sub>3</sub> emissions).
   - There are also slight differences in the new baseline in the forecasted number of animals and animal performance. Additionally, there were changes in the emission models (see previous IIRs). This strongly interacts with the previous point, so that it is not possible to determine a more specific reduction effect of the new Fertilizing Ordinance. (Overall, the effects of 3. and 4. add up to approx. - 16 kt NH<sub>3</sub> emissions.)

Information on the underlying assumptions for projecting activity data is also provided in Thuenen-Report 84((Thuenen-Report 84 (2021): Calculations of gaseous and particulate emissions from German agriculture 1990 – 2019, Report on methods and data (RMD) Submission 2021, https://www.thuenen.de/media/publikationen/thuenen-report/Thuenen_Report_84.pdf)) on pages 74 to 75 in chapter 3.3.7 as well as in table 3.9. Although Thünen-Report 84 matches emission inventory submission 2021, the numbers in 2025 and 2030 are the same as used for the projections based on emission inventory submission 2020 described here. For all other data the activity data of the year 2018 are used with the exception of area and yield data for horticultural crops, strawberries, cereals for whole plant harvesting, yield data for grassland and the input data for liming. For these data the mean values for the years 2016 to 2018 were used.

In total, according to the updated baseline, only 57 kt of NH<sub>3</sub> need to be additionally reduced by agriculture.  

The results as presented at the top of the page have been widely circulated and discussed with sector experts from industry, science and public authorities.
