====== 1.A.3 - Transport (OVERVIEW) ======

===== Short description =====

Sub-sector **1.A.3 - Transport** includes emissions from fuel combustion activities as well as abrasive emission and fugitive emissions within the following sub-categories:

^  NFR-Code  ^  Name of Category                                                                    ^
|  1.A.3.a   |  [[sector:energy:fuel_combustion:transport:civil_aviation:start|Civil Aviation]]     |
|  1.A.3.b   |  [[sector:energy:fuel_combustion:transport:road_transport:start|Road Transport]]     |
|  1.A.3.c   |  [[sector:energy:fuel_combustion:transport:railways:start|Railways ]]                |
|  1.A.3.d   |  [[sector:energy:fuel_combustion:transport:navigation:start|Navigation]]             |
|  1.A.3.e   |  [[sector:energy:fuel_combustion:transport:other_transport:start| Other Transport]]  |