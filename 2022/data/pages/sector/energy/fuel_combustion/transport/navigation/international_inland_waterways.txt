<WRAP left round info 100%>
At the moment, Germany does not report any emissions from this sub-category.
</WRAP>


Primary fuel deliveries data available from the National Energy Balance (NEB), from the BAFA Official Mineral Oil Data or other statistiocs does not allow a differentiation into national and international inland navigation on German inland waterways. Therefore, for the time beeing, all activity data is allocated to [[sector:energy:fuel_combustion:transport:navigation:national_navigation|1.A.3.d ii - National Navigation]] and here to the sub-sector of //1.A.3.d ii (b) - National Inland Navigation//.