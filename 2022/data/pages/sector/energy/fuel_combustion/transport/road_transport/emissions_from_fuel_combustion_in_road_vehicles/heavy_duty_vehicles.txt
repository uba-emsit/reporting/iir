====== 1.A.3.b iii - Transport: Road Transport: Heavy Duty Vehicles and Buses ======

===== Short description =====
In sub-category //1.A.3.b iii - Road Transport: Heavy Duty Vehicles and Buses// emissions from fuel combustion in trucks, lorries, buses etc. are reported. 


^ Category Code  ^  Method                                                                           ||||^  AD                                           ||||^  EF                                     |||||
| 1.A.3.b iii    |  T1, T3                                                                           |||||  NS, M                                        |||||  CS, M, D                               |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^  CO   ^  PB   ^  Cd        ^  Hg   ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  L/T             |  -/-    |  -/-             |  -/-             |  L/T               |  L/T              |  -/-  |  L/T  |  -/-  |  -/-  |  -/-       |  -/-  |  -/-   |  -/-  |  -    |

{{page>general:Misc:LegendEIT:start}}

===== Methodology =====

==== Activity data ====

Specific consumption data for heavy-duty vehicles (trucks and lorries) and buses are generated within TREMOD [(KNOERR2021a)]. - The following tables provide an overview of annual amounts of fuels consumed by these vehicles in Germany.

{{  :sector:energy:fuel_combustion:transport:bus.png?nolink&300}}  
{{  sector:energy:fuel_combustion:transport:truck.png?nolink&300 }}

__Table 1: Annual fuel consumption of trucks and lorries, in terajoules__
|                             ^  1990    ^  1995    ^  2000    ^  2005    ^  2006    ^  2007    ^  2008    ^  2009    ^  2010    ^  2011    ^  2012    ^  2013    ^  2014    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^
| **HDVs: Buses**                                                                                                                                                         |||||||||||||||          |          |          |          |          |
^ Diesel oil                  |   57,322 |   49,060 |   49,204 |   39,890 |   39,400 |   37,316 |   38,776 |   42,093 |   44,448 |   44,080 |   47,336 |   48,611 |   46,147 |   49,428 |   51,353 |   50,519 |   48,606 |   50,088 |   38,954 |
^ Biodiesel                   |        0 |       77 |      545 |    2,666 |    4,750 |    4,993 |    3,863 |    3,401 |    3,408 |    3,076 |    3,341 |    2,883 |    2,836 |    2,704 |    2,722 |    2,697 |    2,825 |    2,853 |    3,236 |
^ CNG                         |        0 |        0 |        0 |    1,183 |    1,682 |    1,965 |    2,072 |    2,255 |    2,200 |    2,011 |    2,157 |    1,720 |    1,663 |    1,591 |    1,213 |    1,150 |      955 |    1,004 |    1,152 |
^ Biogas                      |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |      308 |      340 |      419 |      268 |      285 |      318 |      257 |      408 |      529 |
^ Petroleum                   |        0 |      610 |      414 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |
| **Ʃ Buses**                 ^   57,322 ^   49,137 ^   49,750 ^   43,739 ^   45,832 ^   44,274 ^   44,711 ^   47,748 ^   50,056 ^   49,167 ^   53,142 ^   53,555 ^   51,065 ^   53,991 ^   55,574 ^   54,684 ^   52,644 ^   54,353 ^   43,871 ^
| **HDVs: Trucks & Lorries**                                                                                                                                              |||||||||||||||          |          |          |          |          |
^ Diesel oil                  |  401,801 |  541,198 |  631,094 |  486,181 |  508,342 |  492,112 |  496,382 |  483,129 |  526,568 |  527,325 |  550,954 |  566,780 |  539,718 |  559,069 |  564,622 |  568,615 |  553,447 |  562,904 |  537,774 |
^ Biodiesel                   |        0 |      844 |    6,991 |   32,490 |   61,291 |   65,841 |   49,454 |   39,031 |   40,370 |   36,793 |   38,884 |   33,613 |   33,166 |   30,581 |   29,933 |   30,355 |   32,171 |   32,062 |   44,671 |
^ CNG                         |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |      395 |      338 |      316 |      275 |      194 |      187 |      170 |      222 |      348 |
^ Biogas                      |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |       56 |       67 |       80 |       46 |       46 |       52 |       46 |       90 |      160 |
| **Ʃ Trucks & Lorries**      ^  401,801 ^  542,043 ^  638,086 ^  518,670 ^  569,633 ^  557,953 ^  545,836 ^  522,160 ^  566,938 ^  564,118 ^  590,290 ^  600,798 ^  573,279 ^  589,972 ^  594,796 ^  599,209 ^  585,834 ^  595,278 ^  582,953 ^
| **HDVs over all**                                                                                                                                                       |||||||||||||||          |          |          |          |          |
^ Diesel oil                  |  459,124 |  590,259 |  680,299 |  526,071 |  547,742 |  529,429 |  535,158 |  525,222 |  571,016 |  571,405 |  598,290 |  615,392 |  585,865 |  608,497 |  615,976 |  619,134 |  602,053 |  612,992 |  576,728 |
^ Biodiesel                   |        0 |      921 |    7,537 |   35,155 |   66,042 |   70,833 |   53,317 |   42,432 |   43,778 |   39,869 |   42,225 |   36,496 |   36,002 |   33,285 |   32,656 |   33,052 |   34,996 |   34,915 |   47,907 |
^ CNG                         |        0 |        0 |        0 |    1,183 |    1,682 |    1,965 |    2,072 |    2,255 |    2,200 |    2,011 |    2,552 |    2,058 |    1,979 |    1,866 |    1,407 |    1,337 |    1,125 |    1,226 |    1,500 |
^ Biogas                      |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |      365 |      407 |      499 |      315 |      331 |      370 |      303 |      498 |      688 |
^ Petroleum                   |        0 |      610 |      414 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |        0 |
| **Ʃ 1.A.3.b iii**           ^  459,124 ^  591,179 ^  687,835 ^  562,409 ^  615,465 ^  602,227 ^  590,547 ^  569,908 ^  616,994 ^  613,284 ^  643,432 ^  654,353 ^  624,344 ^  643,963 ^  650,370 ^  653,893 ^  638,477 ^  649,631 ^  626,824 ^
source: TREMOD [(KNOERR2021a)]

<WRAP center round info 60%>
For information on mileage, please refer to sub-chapters on emissions from [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:start | tyre & brake wear and road abrasion]].
</WRAP>

==== Emission factors ====

The majority of emission factors for exhaust emissions from road transport are taken from the 'Handbook Emission Factors for Road Transport' (HBEFA, version 4.1) [(KELLER2017)] where they are provided on a tier3 level mostly and processed within the TREMOD software used by the party [(KNOERR2021a)]. 

However, it is not possible to present these tier3 values in a comprehendible way here. 

<WRAP center round info 100%>
With respect to the country-specific emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. ((During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions.))
</WRAP>

For heavy-metal (other then lead from leaded gasoline) and PAH exhaust-emissions, default emission factors from the 2019 EMEP Guidebook (EMEP/EEA, 2019) [(EMEPEEA2019)] have been applied.
Regarding PCDD/F, tier1 EF from (Rentz et al., 2008) [(RENTZ2008)] are used instead.

__Table 2: tier1 EF derived from default values__
|                              |  **Pb**       |  **Cd**  |  **Hg**  |  **As**  |  **Cr**  |  **Cu**  |  **Ni**  |  **Se**  |  **Zn**  |  **B[a]P**     |  **B[b]F**  |  **B[k]F**  |  **I[...]P**  |  **PAH 1-4**  |  **PCDD/F**   |
|                              |   **[g/TJ]**                                                                                  |||||||||   **[mg/TJ]**                                                          |||||  **[µg/km]**  |
^ Diesel oil                   |         0.012 |    0.001 |    0.123 |    0.002 |    0.198 |    0.133 |    0.005 |    0.002 |    0.419 |            498 |         521 |         275 |           493 |         1,788 |               |
^ Biodiesel                    |         0.013 |    0.001 |    0.142 |    0.003 |    0.228 |    0.153 |    0.005 |    0.003 |    0.483 |            575 |         601 |         317 |           569 |         2,062 |               |
^ CNG & Biogas                 |  NE           |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE            |  NE         |  NE         |  NE           |  NE           |               |
^ Petroleum                    |  NE           |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE            |  NE         |  NE         |  NE           |  NE           |               |
^ all fuels: buses                                                                                                                                                                     ||||||||||||||               |      0.000019 |
^ all fuels: trucks & lorries                                                                                                                                                          ||||||||||||||               |      0.000016 |

===== Discussion of emission trends =====

__Table 3: Outcome of Key Category Analysis__
|  for: ^  NO<sub>x</sub>  ^  BC   ^  PM<sub>10</sub>  ^  PM<sub>2.5</sub>  ^
|   by: |  L/T             |  L/T  |  L/T              |  L/T               |

==== Nitrogen oxides====

Until 2005, NO<sub>x</sub> emissions followed mileage and fuel consumption. Since 2006, in contrast to nearly unchanged fuel consumption, emissions have decreased due to controlled catalytic-converter use and engine improvements resulting from continual tightening of emissions laws.  

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_nox.png?700 |Annual nitrogen oxides emissions }} }}

====Non-methane volatile organic compounds (NMVOC) and carbon monoxide ====

Since 1990, exhaust emissions of **NMVOC**  and **carbon monoxide** have decreased sharply due to catalytic-converter use and engine improvements resulting from ongoing tightening of emissions laws and improved fuel quality. 

{{:sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_nmvoc.png?700|Annual NMVOC emissions }}
{{:sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_co.png?700|Annual carbon monoxide emissions }}

==== Ammonia and sulphur dioxide ====

As for the entire road transport sector, the trends for **sulphur dioxide** (SO<sub>2</sub>) and **ammonia** (NH<sub>3</sub>) exhaust emissions from heavy duty vehicles show charcteristics different from those shown above: Here, the strong dependence on increasing fuel qualities (sulphur content) leads to an cascaded downward trend of SO<sub>2</sub> emissions , influenced only slightly by increases in fuel consumption and mileage. For **ammonia** emissions the increasing use of catalytic converters in gasoline driven cars in the 1990s lead to a steep increase whereas both the technical development of the converters and the ongoing shift from gasoline to diesel cars resulted in decreasing emissions in the following years.

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_so2.png?700 |Annual sulphur oxides emissions }}

====Particulate matter & Black carbon====

As for all reported exhaust PM emissions from mobile diesel vehicles the Party assumes that nearly all particles emitted are within the PM<sub>2.5</sub> range, resulting in similar emission values for PM<sub>2.5</sub>, PM<sub>10</sub>, and TSP.

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3biii_em_pm.png?700 }}

===== Recalculations =====

Compared to submission 2020, recalculations were carried out due to a routine revision of the TREMOD software and the revision of several National Energy Balances (NEB). 

Here, **activity data** were revised within TREMOD due to the provision of the final NEB 2019.

Furthermore, significant re-allocations of consumption shares between the different vehicle types and classes were conducted, effecting the entire time series but with the 1.A.3.b consumptipon totals remaining unaltered.

__Table 4: Revised fuel consumption data, in terajoules__
|                             ^  1990    ^  1995    ^  2000    ^  2005    ^  2006    ^  2007    ^  2008    ^  2009    ^  2010    ^  2011    ^  2012    ^  2013    ^  2014    ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^
| **DIESEL OIL**                                                                                                                                                                                        ||||||||||||||||||          |
^ Submission 2022             |  459.124 |  590.259 |  680.299 |  526.071 |  547.742 |  529.429 |  535.158 |  525.222 |  571.016 |  571.405 |  598.290 |  615.392 |  585.865 |  608.497 |  615.976 |  619.134 |  602.053 |  612.992 |
^ Submission 2021             |  459.124 |  590.259 |  680.299 |  526.071 |  547.742 |  529.429 |  535.158 |  525.220 |  571.008 |  571.389 |  598.264 |  615.361 |  585.893 |  608.417 |  615.537 |  618.619 |  601.836 |  613.898 |
^ absolute change             |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     2,07 |     8,16 |     16,2 |     26,4 |     30,7 |    -28,5 |     79,2 |      439 |      515 |      217 |     -906 |
^ relative change             |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,01% |    0,07% |    0,08% |    0,04% |   -0,15% |
| **BIODIESEL**                                                                                                                                                                                         ||||||||||||||||||          |
^ Submission 2022             |          |      921 |    7.537 |   35.155 |   66.042 |   70.833 |   53.317 |   42.432 |   43.778 |   39.869 |   42.225 |   36.496 |   36.002 |   33.285 |   32.656 |   33.052 |   34.996 |   34.915 |
^ Submission 2021             |          |      921 |    7.537 |   35.155 |   66.042 |   70.833 |   53.317 |   42.432 |   43.777 |   39.867 |   42.223 |   36.494 |   36.004 |   33.281 |   32.633 |   33.025 |   34.984 |   34.954 |
^ absolute change             |          |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,17 |     0,63 |     1,13 |     1,86 |     1,82 |    -1,75 |     4,33 |     23,3 |     27,5 |     12,6 |    -39,5 |
^ relative change             |          |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,01% |    0,07% |    0,08% |    0,04% |   -0,11% |
| **CNG**                                                                                                                                                                                               ||||||||||||||||||          |
^ Submission 2022             |          |          |          |    1.183 |    1.682 |    1.965 |    2.072 |    2.255 |    2.200 |    2.011 |    2.552 |    2.058 |    1.979 |    1.866 |    1.407 |    1.337 |    1.125 |    1.226 |
^ Submission 2021             |          |          |          |    1.183 |    1.682 |    1.965 |    2.072 |    2.255 |    2.200 |    2.011 |    2.552 |    2.057 |    1.967 |    1.846 |    1.390 |    1.173 |    1.104 |    1.060 |
^ absolute change             |          |          |          |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,02 |     1,10 |     12,0 |     20,3 |     17,1 |      165 |     21,4 |      166 |
^ relative change             |          |          |          |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,05% |    0,61% |    1,10% |    1,23% |   14,04% |    1,94% |    15,7% |
| **BIOGAS**                                                                                                                                                                                            ||||||||||||||||||          |
^ Submission 2022             |          |          |          |          |          |          |          |          |          |          |      365 |      407 |      499 |      315 |      331 |      370 |      303 |      498 |
^ Submission 2021             |          |          |          |          |          |          |          |          |          |          |      365 |      407 |      496 |      311 |      327 |      365 |      297 |      485 |
^ absolute change             |          |          |          |          |          |          |          |          |          |          |     0,00 |     0,22 |     3,03 |     3,43 |     4,03 |     4,98 |     5,77 |     13,7 |
^ relative change             |          |          |          |          |          |          |          |          |          |          |    0,00% |    0,05% |    0,61% |    1,10% |    1,23% |    1,37% |    1,94% |    2,83% |
| **PETROLEUM**               |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |
^ Submission 2022             |          |      610 |      414 |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |
^ Submission 2021             |          |      610 |      414 |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |
^ absolute change             |          |     0,00 |     0,00 |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |
^ relative change             |          |    0,00% |    0,00% |          |          |          |          |          |          |          |          |          |          |          |          |          |          |          |
| **TOTAL FUEL CONSUMPTION**                                                                                                                                                                            ||||||||||||||||||          |
^ Submission 2022             |  459.124 |  591.789 |  688.249 |  562.409 |  615.465 |  602.227 |  590.547 |  569.908 |  616.994 |  613.284 |  643.541 |  654.470 |  624.469 |  644.101 |  650.529 |  654.098 |  638.776 |  650.539 |
^ Submission 2021             |  459.124 |  591.789 |  688.249 |  562.409 |  615.465 |  602.227 |  590.547 |  569.906 |  616.985 |  613.267 |  643.403 |  654.319 |  624.360 |  643.855 |  649.887 |  653.181 |  638.221 |  650.396 |
^ absolute change             |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     0,00 |     2,24 |     8,79 |     17,4 |      138 |      151 |      110 |      245 |      642 |      916 |      556 |      143 |
^ relative change             |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,00% |    0,02% |    0,02% |    0,02% |    0,04% |    0,10% |    0,14% |    0,09% |    0,02% |

Due to the variety of highly specific tier3 **emission factors** applied, it is not possible to display any changes in these data sets in a comprehendible way.

<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2019**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

Besides a routine revision of the underlying model, no specific improvements are planned.

===== FAQs =====

[(KNOERR2021a> Knörr et al. (2021a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg & Berlin, 2021. )]
[(KELLER2017> Keller et al. (2017): Keller, M., Hausberger, S., Matzer, C., Wüthrich, P., & Notter, B.: Handbook Emission Factors for Road Transport, version 4.1 (Handbuch Emissionsfaktoren des Straßenverkehrs 4.1) URL: http://www.hbefa.net/e/index.html - Dokumentation, Bern, 2017. )]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019; https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-3-b-i/view; Copenhagen, 2019.)]
[(RENTZ2008> Rentz et al., 2008: Nationaler Durchführungsplan unter dem Stockholmer Abkommen zu persistenten organischen Schadstoffen (POPs), im Auftrag des Umweltbundesamtes, FKZ 205 67 444, UBA Texte | 01/2008, January 2008 - URL: http://www.umweltbundesamt.de/en/publikationen/nationaler-durchfuehrungsplan-unter-stockholmer )]