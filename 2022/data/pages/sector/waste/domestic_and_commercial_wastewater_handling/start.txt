====== 5.D.1 - Domestic & Commercial Wastewater Handling  ======

===== Short description =====

^ Category Code  ^  Method                                                                           ||||^  AD                                        ||||^  EF                              |||||
| 5.D.1          |  T1                                                                               |||||  NS                                        |||||  D                               |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO  ^  Pb  ^  Cd  ^  Hg  ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -               |  -/-    |  -               |  -               |  -                 |  -                |  -    |  -   |  -   |  -   |  -   |  -   |  -     |  -    |  -    |
 {{page>general:Misc:LegendEIT:start}}
\\

In category **5.D.1**, __NMVOC emissions__ from domestic and commercial wastewater handling are reported. The domestic section is covered by wastewaters of municipal origin (large centralised plants; ranging from 1000 up to >100.000 resident values). The commercial section is covered by industrial and commercial wastewaters, co-treated in municipal wwt-plants.

===== Method =====
Emissions reported under this category are calculated using the Tier 1 approach of the EMEP/EEA Guidebook 2019, where the emission factor (EF) is 15 mg/m³ wastewater (Part B, 5.D, chap. 3.2.2, Table 3-1, p. 7 [(EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019, Copenhagen, 2019)]). This EF is multiplied with the total amount of wastewater (AD) treated in domestic and commercial wwt-plants, following the equation:\\
\\ 
<WRAP center round info 25%>
**Emissions <sub>(NMVOC)</sub> = AD x EF** (ibid., chap. 3.2.1)
</WRAP>


==== Activity data ====

Total volumes of treated municipal wastewater are derived by the German statistical agency (Statistisches Bundesamt, Fachserie 19, Reihe 2.1.2 [(Statistisches Bundesamt, Fachserie 19, Reihe 2.1.2)]). The data source is published on a three-year basis with new data only for the respective year of the update. The availability of the data starts in 1991 with an exception for the following update, which was for 1995. Missing data are inter- or extrapolated

==== Emisson factors ====
See method

===== Uncertainties =====
The AD from Statistisches Bundesamt have an uncertainty of ±3% (normal distribution) whereas the uncertainty for the EF, due to its range (5/50 mg/m³), is -70 / +210 % and the distribution lognormal.

===== Recalculations =====

Recalculations were not necessary.

<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2019**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====
Currently no improvements are planned.
\\
\\
\\