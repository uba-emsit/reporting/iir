====== 2.B.3 - Adipic Acid Production ======

===== Short description =====
In source category //NFR 2.B.3 - adipic acid production// NO<sub>x</sub> and CO emissions from the production of adipic acid are reported. As there are only three producers of adipic acid, data provided by them has to be treated as confidential. Due to this reason, only emissions could be reported.

^ Category Code  ^  Method                                                                           ||||^  AD                                         ||||^  EF                              |||||
| 2.B.3          |  T3                                                                               |||||  PS                                         |||||  C                               |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO   ^  Pb  ^  Cd  ^  Hg  ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -/-             |  -      |  -               |  -               |  -                 |  -                |  -    |  -   |  -/-  |  -   |  -   |  -   |  -     |  -    |  -    |

{{page>general:Misc:LegendEIT:start}} \\


===== Method =====

As this source category is a key category for N<sub>2</sub>O, plant specific activity data is applied here according to the IPCC guidelines. This data is made available basically via a co-operation agreement with the adipic acid producers.
A single data collection of plant specific NO<sub>x</sub> and CO emissions and related emission factors of one year (2016) was sufficient as the emissions are below the threshold of significance. These emission factors are applied to the whole time series for every plant.

===Activity Data ===
Due to confidentiality concerns, this data is not made public (see short description).

===Emission factors===
Due to confidentiality concerns, this data is not made public (see short description).
===== Recalculations =====

No recalculations were necessary.

<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2019**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>
===== Planned improvements =====
No category-specific improvements are planned.