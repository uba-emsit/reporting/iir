====== 2.C.7.c - Other Metal Production ======

===== Short description =====
In source category //NFR C.7.c - Other Metal Production// thermal galvanisation is reported and the main pollutants are PM.

^ Category Code  ^  Method                                                                           ||||^  AD                                        ||||^  EF                                  |||||
| 2.C.7.c        |  T1                                                                               |||||  AS                                        |||||  PS, CS                              |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO  ^  Pb  ^  Cd      ^  Hg  ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -               |  -      |  -               |  -               |  -/-               |  -/-              |  -/-  |  -   |  -   |  -   |  -       |  -   |  -     |  -    |  -    |

{{page>general:Misc:LegendEIT:start}} \\


===== Method =====

=== Activity data ===

The yearly production figures were provided by the German association "Industrieverband Feuerverzinken e.V" (IFV) back to year 2010 <sup>**[[#Bibliography| [Lit. 1]]]**</sup>.
The figures are based on annual queries of its member companies. As the market share is about 45% the IFV extrapolates the total values for Germany on this basis.

=== Emission factors ===

The emission factor for TSP was determined on the basis of data supplied for the FMP (Ferrous Metals Processing) BREF review. \\
The split factors originate from <sup>**[[#Bibliography| [Lit. 2]]]**</sup>

As produced amounts are confidantial no emission factors could be published.
===== Recalculations =====

<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2019**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>
===== Planned improvements =====

No improvements are planned.

 \\
 \\ 
------
===== Bibliography =====

**Lit.  1:** Industrieverband Feuerverzinken e.V. (IFV), https://www.feuerverzinken.com/ \\
**Lit.  2:** UBA, 2007: Jörß, Wolfram; Handke, Volker; Lambrecht, Udo and Dünnebeil, Frank (2007): Emissionen und Maßnahmenanalyse Feinstaub 2000 – 2020. UBA-TEXTE Nr. 38/2007. Dessau-Roßlau: Umweltbundesamt. URL: https://www.umweltbundesamt.de/publikationen/emissionen-massnahmenanalyse-feinstaub-2000-2020. \\ 