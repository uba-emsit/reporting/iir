====== 2.C - Metal Production (OVERVIEW) ======

Metal production comprises the following categories and sub-categories:

^ 2.C Metal Production ^
|                      |
| [[Sector:IPPU:Metal_Production:Iron_And_Steel_Production:Start | 2.C.1 Iron and Steel Production]]|
| [[Sector:IPPU:Metal_Production:Ferroalloys_Production:Start | 2.C.2 Ferroalloys Production]]|
| [[Sector:IPPU:Metal_Production:Aluminum_Production:Start | 2.C.3 Aluminium Production]]|
| [[Sector:IPPU:Metal_Production:Magnesium_Production:Start | 2.C.4 Magnesium Production]]|
| [[Sector:IPPU:Metal_Production:Lead_Production:Start | 2.C.5 Lead Production]]|
| [[Sector:IPPU:Metal_Production:Zinc_Production:Start | 2.C.6 Zinc Production]]|
| [[Sector:IPPU:Metal_Production:Copper_Production:Start | 2.C.7.a Copper Production]]|
| [[Sector:IPPU:Metal_Production:Nickel_Production:Start | 2.C.7.b Nickel Production]]|
| [[Sector:IPPU:Metal_Production:Other_Metal_Production:Start | 2.C.7.c Other Metal Production]]|
| [[Sector:IPPU:Metal_Production:Storage_Handling_Transport_Metals:Start | 2.C.7.d Storage, Handling and Transport of Metal Products]]|

In the CSE data base, the subcategory **NFR 2.C.1 - Iron and Steel production** includes sinter production, pig-iron production, oxygen steel production, electric steel production, hot and cold rolling and iron and steel castings. \\
The subcategories **NFR 2.C.2 - Production of Ferroalloys**, **2.C.5 - Lead production**, **2.C.6 - Zinc production** and **2.C.7.a - Copper production** are listed directly as such in the CSE. \\ **NFR 2.C.3 - Aluminium production** is subdivided into __primary__ aluminium and __remelted__ aluminium. \\ 
The subcategory **NFR 2.C.7.c - Other metal production** includes thermal galvanisation.