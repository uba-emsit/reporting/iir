====== 2.C.7.a - Copper Production ======

===== Short description =====


Within this NFR subcategory, SO<sub>2</sub>, PM<sub>2.5</sub>, PM<sub>10</sub>, TSP, PCDD/F, HCB, As, Cd, Cu, Hg, and Pb emissions from the production of copper are reported.

^ Category Code  ^  Method                                                                           ||||^  AD                                         ||||^  EF                                  |||||
| 2.C.7.a        |  T1                                                                               |||||  AS                                         |||||  D, CS                               |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO  ^  Pb   ^  Cd     ^  Hg   ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -               |  -      |  -/-             |  -               |  -/-               |  -/-              |  -/-  |  -   |  -   |  -/-  |  L/-    |  -/-  |  -/-   |  -    |  -/-  |
===== Method =====

=== Activity data ===

The yearly production figures were taken from the annual statistical report of the German association for non-ferrous metals<sup>**[[#Bibliography| [Lit. 1]]]**</sup>.

=== Emission factors ===

The emission factors are either default values according to the 2019 EMEP/EEA air pollutant emission inventory guidebook<sup>**[[#Bibliography| [Lit. 2]]]**</sup> or determined in several research projects or from companies environmental reports <sup>**[[#Bibliography| [Lit. 3]]]**</sup>.

__Table 1: Tier 1 emission factors applied for entire time series (primary and secondary copper production)__
^  Pollutant  ^  EF  ^  Unit  ^  Source                                                             ^
| HCB         | 1    | mg/t   | Emission guidebook 2019 <sup>**[[#Bibliography| [Lit. 2]]]**</sup>  |
| PCDD/F      | 2.9  | µg/t   | Emission guidebook 2019 <sup>**[[#Bibliography| [Lit. 2]]]**</sup>  |

__Table 2: Emission factors applied for primary copper production in 2019__
^  Pollutant 	^  EF 	^  Unit  ^  Source  ^
| TSP  		        | 0.09  | kg/t   | Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |
| PM<sub>10</sub>  	| 0.0765 | kg/t | Calculated from Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |
| PM<sub>2.5</sub>  	| 0.063  | kg/t | Calculated from Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |
| SO<sub>2</sub>  	| 3.6 	 | kg/t | Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |
| As  		        | 0.8 	| g/t | Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |
| Cd  		        | 15 	| g/t | Emission guidebook 2019 <sup>**[[#Bibliography| [Lit. 2]]]**</sup>|
| Cu  		        | 13.8 	| g/t | Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |
| Hg  		        | 0.031 | g/t | Emission guidebook 2019 <sup>**[[#Bibliography| [Lit. 2]]]**</sup> |
| Pb  		        | 3.1 	| g/t | Aurubis <sup>**[[#Bibliography| [Lit. 4]]]**</sup> |

__Table 3: Emission factors applied for secondary copper production in 2019__
^  Pollutant 	^  EF 	^  Unit  ^  Source  ^
| TSP  		| 0.100 | kg/t | PAREST <sup>**[[#Bibliography| [Lit. 5]]]**</sup> |
| PM<sub>10</sub>  	| 0.085 | kg/t | PAREST <sup>**[[#Bibliography| [Lit. 5]]]**</sup>  |
| PM<sub>2.5</sub>  	| 0.07 	| kg/t | PAREST <sup>**[[#Bibliography| [Lit. 5]]]**</sup> |
| SO<sub>2</sub>  	| 3.0 	| kg/t | Emission guidebook 2019 <sup>**[[#Bibliography| [Lit. 2]]]**</sup> |
| As  		        | 2 	| g/t  | Emission guidebook 2019 <sup>**[[#Bibliography| [Lit. 2]]]**</sup> |
| Cd  		     | 486.428 	| mg/t | NE-G-K <sup>**[[#Bibliography| [Lit. 3]]]**</sup> |
| Cu  		  | 46,088.62 	| mg/t | NE-G-K <sup>**[[#Bibliography| [Lit. 3]]]**</sup> |
| Hg  		       | 2.644  | mg/t | NE-G-K <sup>**[[#Bibliography| [Lit. 3]]]**</sup> |
| Pb  		  | 21,977.15 	| mg/t | NE-G-K <sup>**[[#Bibliography| [Lit. 3]]]**</sup> |


===== Recalculations =====

<WRAP center round info 60%>
For pollutant-specific **information on recalculated emission estimates for Base Year and 2019**, please see the pollutant specific recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>


===== Planned improvements =====
No category specific improvements are planned.

 \\ 
 \\ 

------
===== Bibliography =====

**Lit.  1:**  German association for non-ferrous metals (WirtschaftsVereinigung Metalle): Annual statistical report: https://www.wvmetalle.de \\
**Lit.  2:** EMEP/EEA air pollutant emission inventory guidebook 2019, https://www.eea.europa.eu/publications/emep-eea-guidebook-2019\\
**Lit.  3:** Ökopol, IER, IZT, IfG: Bereitstellung einer qualitätsgesicherten Datengrundlage für die Emissionsberichterstattung zur Umsetzung von internationalen Luftreinhalte- und Klimaschutzvereinbarungen für ausgewählte Industriebranchen \\Teilvorhaben 2: NE-Metallindustrie, Kalkindustrie, Gießereien. \\
**Lit.  4:** Aurubis, Umwelterklärung 2020. 2020, Aurubis AG, https://www.aurubis.com/verantwortung/kennzahlen-und-berichterstattung \\
**Lit.  5:**  PAREST, UBA Texte | 48/2013, https://www.umweltbundesamt.de/publikationen/beschreibung-minderungsmassnahmen-im-projekt-parest \\