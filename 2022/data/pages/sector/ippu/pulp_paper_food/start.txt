====== 2.H - Other: Pulp & Paper, Food (OVERVIEW) ======

Within NFR category 2.H - Other, emissions from the production of pulp & paper as well as food & beverages are reported.

| [[Sector:IPPU:Pulp_Paper_Food:Pulp_And_Paper_Industry:Start | 2.H.1 Pulp and Paper Industry]]|
| [[Sector:IPPU:Pulp_Paper_Food:Food_And_Beverages:Start | 2.H.2 Food and Beverages Industry]]|
| [[Sector:IPPU:Pulp_Paper_Food:Other_Industrial_Processes:Start | 2.H.3 Other Industrial Processes]]|


