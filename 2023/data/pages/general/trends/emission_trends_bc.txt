====== Emission Trends BC ======

Germany reports Black Carbon (BC) emissions for all years from 2000 onward. The main sources are transport as well as mobile and stationary combustion. Germany uses the EMEP/EEA 2016 Guidebook to estimate BC emissions, augmented by some country specific emission factors, i.e. split factors for the BC portion of PM<sub>2.5</sub>, in particular in road transport. The following figure provides an overview on the sources and their respective contribution to the German national total.


{{ :general:trends:iir_black_carbon_details_2000.png?direct&800 |BC emissions in the year 2000}}
{{ :general:trends:iir_black_carbon_details_2021.png?direct&800 |BC emissions in the latest year}}
===== Main drivers =====

Between 2000 and 2021, **Total Black Carbon emissions dropped by 74%**. 

The main drivers are the **transport emissions (NFR 1.A.3)** with 73% of total 2000 emissions, and a 84% reduction between 2000 and 2021. 
Over the entire time series, 90% of the transport emissions come from **Road Transport (NFR 1.A.3.b)**. The overlying trend towards more diesel cars in the German fleet slowed the decrease in emission over this period (see figure below).

18% of the 2000 total emissions result from **Other Sectors (NFR 1.A.4)**, mostly from residential stationary combustion and mobile sources therein, with a 39% reduction between 2000 and 2021.

__Table: Black Carbon emissions 1990-2021, in kilotonnes [kt]__

^                                                                                                            |||||||||||||^  Trend: latest compared to                                                                  ||
^  2000                  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^  2000                                            ^  last years                               ^
|                     38 |     31 |     23 |     21 |     19 |     19 |     17 |     16 |     15 |     14 |     12 |     12 |     10 |     10 |  {{:general:trends:down_green.png|down}} -74.0%  |  {{:general:trends:down_green.png|down}}  |

{{ :general:trends:iir_bc_trend.png?direct&800 | trend of BC emissions, by sector }}
