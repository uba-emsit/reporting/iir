====== Chapter 1.6 - QA/QC and Verification Methods ======
{{  :general:quality_verification:qaqc1.png?nolink&400}}
===== The Quality System for Emission Inventories and it´s contribution to UNECE-CLRTAP reporting =====
The German Quality System for Emission Inventories (QSE) originally was designed to serve the purposes of emission reporting under the UN Framework Convention on Climate Change (UNFCCC). It takes account of provisions of the 2006 IPCC (Intergovernmental Panel on Climate Change) Guidelines - General Guidance and Reporting (GGR), of national circumstances in Germany and of the internal structures and procedures of the German Environment Agency (UBA), the reporting institution.\\
Thus it can be assumed that by adopting the IPCC Guidelines, the UNECE-CLRTAP seriously facilitated the task of developing a QC/QA-System for the reporting of air pollutants. It is likely that all QSE procedures are flexible enough to be able to routinely incorporate these demands in the future as well.\\
The QSE is designed to cover all participants of the National System on Emissions (NaSE) which are mostly identical to those responsible for the reporting of air pollutants. Within the German German Environment Agency, the appliance of the QSE and it’s procedures has been made mandatory for UNFCCC reporting by an internal directive (UBA-Hausanordnung 11/2005). The adoption of the QSE´s approach will be useful for CLRTAP-reporting too but its implications have to be considered carefully (mostly due to human resources), which is why the appliance of QSE procedures is partly outstanding. Currently few parts are used for reporting under CLRTAP, (see below, “Current State”).

The requirements pertaining to a system for quality control and quality assurance (QC/QA system) and to measures for quality control and quality assurance are defined primarily by Chapter 6 of the 2006 IPCC GL GGR.         \\
From those provisions, the German Environment Agency derived "General requirements pertaining to quality control and quality assurance in connection with greenhouse-gas-emissions reporting". It is assumed that these requirements completely apply for CLRTAP-reporting too. Therefore they are briefly given in the following to depict the necessary though partly outstanding trail of action for the development of a consistent QC/QA-System under UNECE-CLRTAP.

===== Main demands =====
The 2006 IPCC Guidelines require that QC/QA systems be introduced with the aim of enhancing transparency, consistency, comparability, completeness, accuracy and timeliness and, especially, that such inventories fulfill requirements pertaining to "good inventory practice". A QC/QA system comprises the following:

 •	An agency responsible for coordinating QC/QA activities\\ •	Development and implementation of a QC/QA plan\\ •	General QC procedures\\ •	Source-category-specific QC procedures\\ •	QA procedures\\ •	Verification activities\\ •	Reporting procedures\\ •	Documentation and archiving procedures  

==== Agency responsible for coordinating QC/QA activities ====
A Single National Entity (national coordinating agency), is responsible for the QC/QA system. The German Single National Entity is established in the German Environment Agency (UBA). In executing its function, it is good practice to establish the position of a coordinator for the Quality System to be developed. A QC/QA coordinator has responsibility for ensuring that a relevant QC/QA system is developed and implemented. Such implementation should be suitably institutionalised – for example, by means of an in-house directive or association agreement.

==== QC/QA plan ====
The purpose of a QC/QA plan is to ensure that QC/QA measures are properly organised and executed. It includes a description of all required QC/QA measures and a schedule for implementation of such measures. It also defines the primary emphasis of such measures.\\
Good practice calls for establishing a QC/QA plan and then reviewing and updating it each year after the latest inventory has been prepared.
On the basis of the results of annual inventory review, the results of QC/QA measures and other informations of which it is aware, the Single National Entity aims to prepare an improvement plan for the entire inventory. On this basis, in turn, it will then derive proposals for a binding inventory plan for the next year to be reported.

==== General quality control ====
Pursuant to the definition used by the 2006 IPCC GL (Chapter 6.1 GGR), quality control (QC) comprises a system of routine specialised measures for measuring and checking the quality of inventories in preparation.\\
Requirements pertaining to general (formerly so called Tier-1) QC procedures can be derived from the requirements mentioned in Chapter 6.6, especially Table 6.1 which includes a complete list of general QC measures.\\
Required quality controls and their results should be recorded and not all quality controls have to be carried out on an annual basis. It should be ensured that all source categories undergo detailed quality control at least periodically.

==== Source-category-specific quality control ====
In addition to undergoing general procedures, particularly relevant source categories (such as key sources), available resources presupposed, should undergo category specific (formerly so called Tier 2) quality control with regard to determination of activity rates, emissions and uncertainties (cf. Chapter 6.7; GGR). The chapters of the 2006 IPCC GL that pertain to the various individual source categories (Vol 2-5) include additional information relative to source-category-specific QC measures. Such guidelines should be considered in preparation of a QC/QA plan.

==== Quality assurance procedures ====
While the primary aim of quality control is to ensure that methods are correctly applied, the primary purpose of quality assurance is to examine methods and data as such and improve or correct them as necessary.\\
Pursuant to the relevant IPCC definition (Chapter 6.1; GGR), measures for quality assurance (QA) are based "on a planned system of reviews by persons who are not directly involved in preparing the inventory. Such reviews – which are best carried out by independent third parties – should be applied to completed inventories, after QC procedures have been carried out.\\
The required instrument for quality assurance is the annual "basic" peer review, though additional audits on "strategic points of the inventory" should also be conducted.

==== Verification activities ====
According to the GGR-Chapter 6.10 "verification activities" refer to emission estimates only. Verification activities for emission factors and activity data are subsumed under the headword and chapter "quality control", which is a somewhat confusing approach. Nevertheless, and by leaving this peculiarity aside, verification, next to QC, is undoubtedly one key to ensure confidence and reliability of the estimates, corresponding emission factors and activity data.

==== Reporting procedures ====
The Single National Entity is responsible for initiating, coordinating and globally organising reporting. Provision of data and reports by third parties must conform to applicable requirements pertaining to the scope, form and scheduling for such provision.

==== Documentation and Archiving ====
As a general requirement, all data and information used for inventory calculation must be documented (i.e. recorded) and archived, for each report year. The purpose of such documentation (i.e. recording) is to make it possible to completely reconstruct all emissions calculations after the fact. The general requirements pertaining to documentation and archiving for the entire process of preparation of the inventory are described in Chapter 6.10.1 of the 2006 IPCC GL GGR.\\
Consequently, **data providers** have the obligation to keep records information relative to data they supply to the German Environment Agency, for purposes of inventory calculations.\\
The types of quality control, the dates on which those measures were carried out, the pertinent results, and the corrections and modifications triggered by quality control measures should be recorded by the institution supplying the pertinent data.\\
**Providers of emissions calculations** have obligations to record the calculation methods and the rationale for their appliance. Calculation models, -files and -software, as well as the assumptions and criteria for the appliance of activity data and emission factors and their references have to be archived.
Moreoverthis quality assurance (QA) and confidentiality issues for data secrecy have to be archived.

===== Current state =====
Information regarding the German Environment Agency's current organisational measures for implementing the requirements given above is provided in the following.

==== Structural organisation - Role concept ====

Within the QSE framework, a concept for a start-up organisation was developed that defines binding responsibilities inside the German Environment Agency, for implementation of the necessary QC and QA measures. The purpose of defining roles and responsibilities is to facilitate the effective information exchange and the directive-conformal execution of QC and QA. The following roles are also used for reporting under CLRTAP (but are not yet established on a mandatory basis).

<WRAP center round info 60%>
**Specialised expert at the operational level (FV)**\\
  * Source specific expert\\
  * Tasks: collection and entry (into the CSE) of activity data (AD)
</WRAP>

<WRAP center round info 60%>
**Quality control manager (QKV)**\\
  * superior of the FV\\
  * Tasks: checking and approving of activity data
</WRAP>

<WRAP center round info 60%>
**Specialized contact person (source-category specific, FAP)**\\
  * Member of the Single National Entity\\
  * Tasks: Checking of AD, collection of emission factors; data entry (into CSE) & calculation; preparation of texts; facilitation of specialised and technical support (inventory work and reporting).
</WRAP>

<WRAP center round info 60%>
**Report coordinator (IIRK)**\\
  * Member of the Single National Entity\\
  * Tasks: Coordination of textual work. Establishing the Framework for preparation of the IIR from the various relevant contributions.
</WRAP>


<WRAP center round info 60%>
**CSE Coordinator (ZSEK)**\\
  * Member of the Single National Entity\\
  * Tasks: Ensuring the integrity of databases; emissions reporting and data aggregation into reporting formats.
</WRAP>


==== Current workflow management organisation ====
The workflow specified by the QSE is currently also used for activity data and additionally for emission factors in the agriculture sector (but not yet established on a mandatory basis). Moreoverthis parts of the adopted design are additionally altered to serve the purposes of CLRTAP-reporting in the present state. For example FV are currently responsible for activity data collection, entry and calculation and not also for collecting emission factors as is with the UNFCCC-Reporting.
{{  :general:quality_verification:grafik_qcqa_roles-responsibilities.png?700  }}
Figure: QSE – Roles, responsibilities and workflow

==== General and Key-Source-category-specific quality control (QC) ====
The general and Key-category-specific quality control of the QSE is already in force for Activity Data. This is due to the fact that all AD are the same as those used for UNFCCC-Reporting (except for the Handling of Bulk Products, because these AD aren’t used under UNFCCC). The quality of AD is supervised by the appliance of QC-Checks. The appliance of these QC-Checks (via special QSE-Checklists) is mandatory for all partcipants. The QSE-Checklists cover the set of general QC measures given by Table 6.1 of the 2006 IPCC GL GGR plus additional category-specific measures for key-source-category-specific quality control.\\
The QSE-Checklist are carried out annually throughout the whole inventory regardles if key-category or not.

==== Quality assurance (QA) ====
Data, Methods and Estimates are generally derived by Staff (FV) who are not member of the Single National Entity (see "Structural organisation - Role concept"). The activities to be performed by FV, concerning quality issues, are related to QC only.

QA-tasks are to be performed by the following roles, starting with the QKV (internal QA). The tasks are given within the already mentioned QSE-Checklists. They build up on another and rely on reviewing and checking of finalized estimates and data. Each participant has it's own set of QA-Checks.\\
The FAP, as a member of the Single National Entity, is the first role that is not directly involved in preparing the AD (external QA). The "basic annual peer review" is conducted by this role. QA-independence is guaranteed henceforward.

The QA-Checks are carried out annually throughout the whole inventory. More intense peer reviews are undertaken when the need arises, performed by national experts and in most cases conducted by means of an national workshop.\\
The QSE´s QA-procedures are already in force for Activity Data. This is due to the fact that all AD are the same as those used for UNFCCC-Reporting (except for the Handling of Bulk Products, because these AD aren’t used under UNFCCC).
 