====== 2.C.3 - Aluminium Production ======

===== Short description =====

^ Category Code  ^  Method                                                                           ||||^  AD                                          ||||^  EF                                 |||||
| 2.C.3          |  T2, T3                                                                           |||||  AS                                          |||||  D, CS                              |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO   ^  Pb   ^  Cd     ^  Hg  ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -/-             |  -      |  -/-             |  -               |  -/-               |  -/-              |  -/-  |  -   |  L/-  |  -/-  |  -/-    |  -/- |  -/-   |  -/-  |  L/T  |

The category //2.C.3 - Aluminium production// is subdivided into primary aluminium and secondary aluminium production. 

In Germany, primary aluminium is produced in electrolytic furnaces with pre-baked anodes. The pre-baked anodes are produced in separate anode production plants, where petroleum coke and tar pitch are mixed together and subsequently baked. This process produces PAH emissions. 

Secondary aluminium is produced in several different furnace types. Emissions can vary according to different scrap qualities. The use of hexachloroethane for degassing during refining operations of secondary aluminium production has been prohibited by law in Germany since 2002, resulting in an omission of the source for HCB. For the period between 1990 and 2001, however, no data on national emissions of HCB in secondary aluminium industries is available. In order to be able to calculate these HCB emissions, the default emission factor for HCB was used.


===== Method =====

=== Activity data ===

The production figures of each year were taken from the annual statistical report of the German association for non-ferrous metals <sup>**[[#Bibliography| [Lit. 1]]]**</sup>. 

The total quantity of waste gas incurred per tonne of aluminium during the production of primary aluminium was multiplied by an average concentration value formed from several individual figures coming from different plants. The values are weighted appropiately and then used to derive the average concentration value. 

=== Emission factors ===

The emission factors are either default values according to the 2019 EMEP/EEA air pollutant emission inventory guidebook <sup>**[[#Bibliography| [Lit. 2]]]**</sup> or determined in research projects <sup>**[[#Bibliography| [Lit. 3]]]**</sup>. 
The emission factors also make allowance for fugitive emission sources, such as emissions via hall roofs. \\

The emission figures used for CO are the results of emission measurements within the context of investment projects. \\

The emission factors for SO<sub>2</sub> are calculated from the specific anode consumption. The anodes consist of petrol coke. This material has a specific sulphur concentration of about 1.2 %, from which an SO<sub>2</sub> emission factor of 10.4 kg/t Al can be calculated. The average anode consumption is 430 kg of petrol coke per tonne of aluminium.

The following tables show some process-related emission factors.

__Table 1: Emission factors applied for anode production__

^  Pollutant  ^  Activity / Process  ^  EF  ^  Unit  ^  Trend   ^
| PAH         | anode production     | 300  | mg/t   | falling  |

__Table 2: Emission factors applied for primary aluminium production__

^  Pollutant  ^  Activity / Process  ^  EF  ^  Unit  ^  Trend  ^
| CO | primary aluminium | 180 | kg/t | constant |
| SO<sub>2</sub> | primary aluminium | 7.341 | kg/t | constant |
| TSP | primary aluminium | 0.83 | kg/t | falling |
| PM<sub>10</sub> | primary aluminium | 0.7055 | kg/t | falling |
| PM<sub>2.5</sub> | primary aluminium | 0.581 | kg/t | falling |
| Cd | primary aluminium | 0.15 | g/t | constant |
| Ni | primary aluminium | 0.162 | g/t | falling |
| Zn | primary aluminium | 10 | g/t | constant |
| NO<sub>x</sub> | primary aluminium | 1 | kg/t | constant |

Nitrogen oxide emissions essentially arise from the nitrogen content of the fuels in combustion processes. At temperatures above 1,000 ° C, Nitrogen oxide can also form from nitrogen in the air. Another source of NO<sub>x</sub> is the electrolysis in primary aluminium production due to the presence of nitrogen in the anode, which can be oxidized to NO<sub>x</sub>. All these emission sources resulting from energy-related processes are included in 1.A.2.b. It is not known whether other sources of NO<sub>x</sub> have quantitative effects. In order not to miss process-related NO<sub>x</sub> emissions, the standard emission factor is also used. Germany is following recommendations provided by the Expert Review Team for the NECD Review 2017.

__Table 3: Emission factors applied for secondary aluminium production__

^  Pollutant             ^  Activity / Process  ^  EF      ^  Unit  ^  Trend    ^
| TSP                    | resmelted aluminium  | 0.055    | kg/t   | constant  |
| PM<sub>10</sub>        | resmelted aluminium  | 0.047    | kg/t   | constant  |
| PM<sub>2.5</sub>       | resmelted aluminium  | 0.03843  | kg/t   | constant  |
| Cd                     | resmelted aluminium  | 7        | mg/t   | constant  |
| Cu                     | resmelted aluminium  | 8.411    | mg/t   | constant  |
| Hg                     | resmelted aluminium  | 1.7      | mg/t   | constant  |
| Pb                     | resmelted aluminium  | 4.452    | mg/t   | constant  |
| Zn                     | resmelted aluminium  | 4        | g/t    | constant  |
| HCB (years 1990-2001)  | resmelted aluminium  | 5        | g/t    | constant  |

===== Recalculations =====

<WRAP center round info 60%>
With **activity data and emission factors remaining unrevised**, no recalculations were carried out compared to Submission 2022.
</WRAP>



===== Planned improvements =====

The emission factor for CO will be adjusted to typical operating parameters mentioned in the technical guideline VDI 2286 "Emission //control - Electrolytic aluminium reduction process"//<sup>**[[#Bibliography| [Lit. 4]]]**</sup> for the next submission 2024. With that adjustment the emission factor will be harmonized with the standard emission factor of the emission guidebook 2019 lowering the CO emissions.

===== Bibliography =====

** Lit. 1:**  German association for non-ferrous metals (WirtschaftsVereinigung Metalle): Annual statistical report: https://www.wvmetalle.de \\
** Lit. 2:** EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019, Copenhagen, 2019. https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/2-industrial-processes/2-c-metal-production/2-c-3-aluminium-production/view \\
** Lit. 3:** Ökopol, IER, IZT, IfG: Bereitstellung einer qualitätsgesicherten Datengrundlage für die Emissionsberichterstattung zur Umsetzung von internationalen Luftreinhalte- und Klimaschutzvereinbarungen für ausgewählte Industriebranchen Teilvorhaben 2: NE-Metallindustrie, Kalkindustrie, Gießereien. \\
** Lit. 4:** VDI 2286 Blatt 1:2013-08 Emissionsminderung Aluminiumschmelzflusselektrolyse (Emission control; Electrolytic aluminium reduction process). Berlin: Beuth Verlag \\

