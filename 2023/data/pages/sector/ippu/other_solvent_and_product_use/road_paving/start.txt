====== 2.D.3.b - Road Paving  ======

===== Short description =====

^ Category Code  ^  Method                                                                           ||||^  AD                                        ||||^  EF                              |||||
| 2.D.3.b        |  T1                                                                               |||||  AS                                        |||||  CS                              |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC  ^  CO  ^  Pb  ^  Cd  ^  Hg  ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  -/-             |  -/-    |  -/-             |  -               |  -/-               |  -/-              |  -/-  |  -   |  -   |  -   |  -   |  -   |  -     |  -    |  -    |

{{page>general:Misc:LegendEIT:start}}
\\

Currently, the report tables list produced quantities of mixed asphalt products (from stationary installations only) and NMVOC, NOx and SO2 emissions caused of this. Only emissions from asphalt production are reported. Figures relative to emissions released during laying of asphalt have not been examined.

===== Method =====
=== Activity data ===
The applicable quantity of mixed asphalt products produced (activity rate) has been taken from communications of the Deutscher Asphaltverband (DAV; German asphalt association). In total about 660 asphalt-mixing plants produce most recently 38 Million tonnes of hot-mix for road paving [(https://www.asphalt.de/themen/aktuelles/)].

=== Emission factors ===
Emission factors have been determined country-specifically, pursuant to Tier 2. For determination of emission factors for emissions measurements from over 400 asphalt-mixing plants, made during the period 1989 through 2000, were used. The majority of the emissions occur during drying of pertinent mineral substances. Almost all of the NMVOC emissions originate in the organic raw materials used, and they are released primarily in parallel-drum operation, as well as from mixers and loading areas. On average, about 50% of the NOx and SO<sub>x</sub> involved come from the mineral substances (proportional process emissions). CO emissions are calculated solely in connection with fuel inputs.

__Table 1: Overview of applied emission factors for production of mixed asphalt, in [kg/t]__
|                   ^  EF value  ^  EF trend  ^
^ NMVOC             |  0.030     |  constant  |
^ NO<sub>x</sub>    |  0.015     |  constant  |
^ SO<sub>x</sub>    |  0.030     |  constant  |
^ TSP               |  0.006     |  constant  |
^ PM<sub>10</sub>   |  0.0057    |  constant  |
^ PM<sub>2.5</sub>  |  0.003     |  constant  |
\\
===== Trends in emissions =====

All trends in emissions correspond to trends of production amount. No rising trends are to identify.
[{{:sector:ippu:other_product_use:em_2d3b_since_1990.png|**Emission trends of road paving**}}]

===== Recalculations =====

<WRAP center round info 60%>
With **activity data and emission factors remaining unrevised**, no recalculations were carried out compared to Submission 2022.
</WRAP>

===== Planned improvements =====
At the moment, no category-specific improvements are planned.