====== 1.A.3.b i - iv - Emissions from fuel combustion in Road Vehicles (OVERVIEW) ======

===== Short description =====

This overview chapter provides information on the emissions from fuel combustion activities in road transport sub-categories //1.A.3.b i, ii, iii, and iv//. 

^  NFR-Code    ^  Name of Category                                                                                        ^
| 1.A.3.b i    | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:passenger_cars | Passenger Cars]]                 |
| 1.A.3.b ii   | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:light_duty_vehicles | Light Duty Vehicles ]]     |
| 1.A.3.b iii  | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:heavy_duty_vehicles | Heavy Duty Vehicles ]]      |
| 1.A.3.b iv   | [[sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:mopeds_and_motorcycles | Mopeds & Motorcycles ]]  |

===== Methodology =====

==== Activity data ====

Basically, total inland fuel deliveries are available from the National Energy Balances (NEBs) (AGEB, 2022) [(AGEB2022)], line 62: Straßenverkehr (Road Transport) as compiled by the Association of the German Petroleum Industry (MWV) [(MWV2021)]. 

Based upon these primary activity data, specific consumption data for the different types of road vehicles are generated within TREMOD [(KNOERR2022a)]. 

<WRAP center round info 60%>
For further details see main chapter [[sector:energy:fuel_combustion:transport:road_transport:start | 1.A.3.b - Road Transport ]] as wells as the sub-category chapters linked above.
</WRAP>


==== Emission factors ====

The majority of emissions factors for exhaust emissions from road transport are taken from the 'Handbook Emission Factors for Road Transport' (HBEFA, version 4.1) [(KELLER2017)] where they are provided on a tier3 level mostly and processed within the TREMOD software used by the party [(KNOERR2021a)]. 

<WRAP center round info 100%>
With respect to the emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. ((During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions.))
</WRAP>

As it is not possible to present these tier3 values in a comprehendible way, the NFR sub-chapters linked above provide sets of fuel-specific implied emission factors instead.

For heavy-metal (other then lead from leaded gasoline) and PAH exhaust-emissions, default emission factors from (EMEP/EEA, 2019) [(EMEPEEA2019)] have been applied.
Regarding PCDD/F, tier1 EF from (Rentz et al., 2008) [(RENTZ2008)] are used instead.

<WRAP center round info 100%>
For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.
</WRAP>

===== Trends of exhaust emissions from road transport vehicles =====


For **ammonia emissions**, the increasing use of catalytic converters in gasoline driven cars in the 1990s lead to a steep increase whereas both the technical development of the converters and the ongoing shift from gasoline to diesel cars resulted in decreasing emissions in the following years.

The observed trends for **NO<sub>x</sub>, NMVOC and CO emissions** represent the changes in legislatory emission limits and the regarding implementation of mitigation technologies.
The following table provides an overview of the implemetation of Euro norms in Germany.

__Table 1: Overview of Euro norms adn their implementation in Germany__
|                                                 ^  Type Approval for new vehicle types            ^  Type Approval for new vehicles              ^  Testing Cycle  ^
^ PASSENGER CARS & LIGHT-DUYT VEHICLES: DIESEL                                                                                                                    ||||
^ Euro Norm 1                                     |                             since 01. Juli 1992 |                        since 01. Januar 1993 |  NEFZ           |
^ Euro 2                                          |                           since 01. Januar 1996 |                        since 01. Januar 1997 |  NEFZ           |
^ Euro 3                                          |                           since 01. Januar 2000 |                        since 01. Januar 2001 |  NEFZ           |
^ Euro 4                                          |                           since 01. Januar 2005 |                        since 01. Januar 2006 |  NEFZ           |
^ Euro 5a[(EURLex2007)]                                         |                        since 01. September 2009 |                        since 01. Januar 2011 |  NEFZ           |
^ Euro 5b                                         |                        since 01. September 2011 |                        since 01. Januar 2013 |  NEFZ           |
^ Euro 6b[(EURLex2007)]                                         |                        since 01. September 2014 |                     since 01. September 2015 |  NEFZ           |
^ Euro 6c                                        |                        since 01. September 2017 |                     since 01. September 2018 |  WLTC           |
^ Euro 6d-Temp                                    |                        since 01. September 2017 |                     since 01. September 2019 |  WLTC           |
^ Euro 6d                                         |                          Freiwillige Einstufung |                       Freiwillige Einstufung |  WLTC           |
^ Euro 6d-ISC-FCM                                 |  since 01.01.2020 (36AP) / 10.01.2021 (36AQ-AR) |  Ab 01.01.2021 (36AP) / 01.01.2022 (36AQ-AR) |  WLTC           |
^ PASSENGER CARS & LIGHT-DUYT VEHICLES: GASOLINE                                                                                                                  ||||
^ Abgasnorm Euro 1                                |                             since 01. Juli 1992 |                        since 01. Januar 1993 |  NEFZ           |
^ Euro 2                                          |                           since 01. Januar 1996 |                        since 01. Januar 1997 |  NEFZ           |
^ Euro 3                                          |                           since 01. Januar 2000 |                        since 01. Januar 2001 |  NEFZ           |
^ Euro 4                                          |                           since 01. Januar 2005 |                        since 01. Januar 2006 |  NEFZ           |
^ Euro 5a [(EURLex2007)]                                         |                        since 01. September 2009 |                        since 01. Januar 2011 |  NEFZ           |
^ Euro 6b [(EURLex2007)]                                        |                        since 01. September 2014 |                     since 01. September 2015 |  NEFZ           |
^ Euro 6c                                         |                        since 01. September 2017 |                     since 01. September 2018 |  WLTC           |
^ Euro 6d-Temp                                    |                        since 01. September 2017 |                     since 01. September 2019 |  WLTC           |
^ Euro 6d                                         |                          Freiwillige Einstufung |                       Freiwillige Einstufung |  WLTC           |
^ Euro 6d-ISC-FCM                                 |  since 01.01.2020 (36AP) / 10.01.2021 (36AQ-AR) |  Ab 01.01.2021 (36AP) / 01.01.2022 (36AQ-AR) |  WLTC           |
^ MOPEDS                                                                                                                                                          ||||
^ Euro 1                                          |                             since 17. Juni 1999 |                                              |  ECE R47        |
^ Euro 2                                          |                             since 17. Juni 2002 |                                              |  ECE R47        |
^ Euro 4                                          |                           since 01. Januar 2017 |                        since 01. Januar 2018 |  ECE R47        |
^ Euro 5                                          |                           since 01. Januar 2020 |                        since 01. Januar 2021 |  WMTC           |
^ MOTORCYCLES                                                                                                                                                     ||||
^ Euro 1                                          |                             since 17. Juni 1999 |                                              |  ECE R47        |
^ Euro 2                                          |                             since 17. Juni 2002 |                                              |  ECE R47        |
^ BUSES & TRUCKS                                                                                                                                                  ||||
^ Euro I                                          |                                      01. Jan 92 |                                              |  ESC R-49       |
^ Euro II                                         |                                      01. Okt 96 |                                              |  ESC R-49       |
^ Euro III                                        |                                      01. Okt 00 |                                              |  ESC&ELR, ETC   |
^ Euro IV                                         |                                      01. Okt 05 |                                              |  ESC&ELR, ETC   |
^ Euro V                                          |                                      01. Okt 08 |                                              |  ESC&ELR, ETC   |
^ Euro VI [(EURLex2009)]                          |                                      01. Okt 13 |                                              |  WHTC, WHSC     |

Trends for **sulphur dioxide** show charcteristics very different from those shown above. 
Here, the strong dependence on increasing fuel qualities leads to an cascaded downward trend of **SO<sub>2</sub> emissions**, influenced only slightly by increases in fuel consumption and mileage. 

The following table provides the development of sulphur contents over the years for Old (OGL) and New German Länder (NGL) and Germany (GER).

__Table 2: Development of fuel sulphur contents in Germany__
^  Area covered       ^  Year(s) covered  ^  Gasoline  ^  Diesel oil  ^
| EAST GERMANY (DDR)  |  until 1988       |    500 ppm |    6,000 ppm |
| :::                 |  1989-1990        |    500 ppm |    6,000 ppm |
| WEST GERMANY (BRD)  |  until 1984       |    250 ppm |    2,700 ppm |
| :::                 |  1985             | :::        |    2,500 ppm |
| :::                 |  1986             | :::        |    2,100 ppm |
| :::                 |  1987             | :::        | :::          |
| :::                 |  1988             | :::        |    1,700 ppm |
| :::                 |  1989             | :::        | :::          |
| :::                 |  1990             |    220 ppm | :::          |
| GERMANY             |  1991             |    220 ppm |    1,300 ppm |
| :::                 |  1992             | :::        | :::          |
| :::                 |  1993             | :::        | :::          |
| :::                 |  1994             | :::        | :::          |
| :::                 |  1995             |    180 ppm | :::          |
| :::                 |  1996             | :::        |      600 ppm |
| :::                 |  1997             | :::        |      400 ppm |
| :::                 |  1998-2000        |     70 ppm |      300 ppm |
| :::                 |  2001             |     55 ppm |      250 ppm |
| :::                 |  2002             |     25 ppm |       40 ppm |
| :::                 |  since 2003       |      8 ppm |        8 ppm |

For **exhaust particulate matter emissions** from diesel road vehicles, the party assumes that nearly all particles emitted are within the PM<sub>2.5</sub> range, resulting in similar emission values for PM<sub>2.5</sub>, PM<sub>10</sub>, and TSP.
Excumptions from this assumption can be observed for gasoline road vehicles for the years until 1997 when **additional TSP emissions** resulted **from the use of leaded gasoline** that was banned in 1997.
Furthermore, **black carbon** emissions are estimated via implied emission factors derived from fractions of PM as provided in [(EMEPEEA2019)].

For **Heavy Metals** and **PAHs**, emissions are calculated with tier1 default EF from [(EMEPEEA2019)] resulting in trends that simply reflect the annual fuel consumption.

{{sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:emep2019_1.a.3.bi-iv_table3-86_hm_from_engine_wear.png?nolink&700}}

Here, the only excumption are **lead emissions from leaded gasoline** that was in use until 1996 with lead contents provided in the table below:

__Table 3: Development of gasoline's lead content in Germany__
^  Area covered       ^  Year(s) covered  ^  Lead content     ^
| EAST GERMANY (GDR)  |  1989-1990        |  126 mg/l         |
| WEST GERMANY (BRD)  |  1990             |  42 mg/l          |
| GERMANY             |  1991             |  29 mg/l          |
| :::                 |  1992             |  20 mg/l          |
| :::                 |  1993             |  16 mg/l          |
| :::                 |  1994             |  11 mg/l          |
| :::                 |  1995             |  8 mg/l           |
| :::                 |  1996             |  4 mg/l           |
| :::                 |  since 1997       |  0 mg/l (banned)  |

===== Recalculations =====

Recalculations of exhaust-emissions are mainly based on annual routine revisions of the underlying TREMOD model. For more information, please see the specific chapters linked above.

[(AGEB2022>AGEB, 2022: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2020/?wpv-jahresbereich-bilanz=2011-2020, (Aufruf: 23.11.2022), Köln & Berlin, 2022)]
[(BAFA2022>BAFA, 2022: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2021_12.xlsx;jsessionid=80E1FD32B36918F682608C03FDE79257.1_cid381?__blob=publicationFile&v=5, Eschborn, 2022.)]
[(MWV2021> MWV (2021): Association of the German Petroleum Industry (Mineralölwirtschaftsverband, MWV): Annual Report 2019, page 65, Table 'Sektoraler Verbrauch von Dieselkraftstoff 2012-2019'; URL: https://www.mwv.de/wp-content/uploads/2020/09/MWV_Mineraloelwirtschaftsverband-e.V.-Jahresbericht-2020-Webversion.pdf, Berlin, 2020.)]
[(KNOERR2022a> Knörr et al. (2022a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg & Berlin, 2022. )]
[(KELLER2017> Keller et al. (2017): Keller, M., Hausberger, S., Matzer, C., Wüthrich, P., & Notter, B.: Handbook Emission Factors for Road Transport, version 4.1 (Handbuch Emissionsfaktoren des Straßenverkehrs 4.1) URL: http://www.hbefa.net/e/index.html - Dokumentation, Bern, 2017. )]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019; https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-3-b-i/view; Copenhagen, 2019.)]
[(RENTZ2008> Rentz et al., 2008: Nationaler Durchführungsplan unter dem Stockholmer Abkommen zu persistenten organischen Schadstoffen (POPs), im Auftrag des Umweltbundesamtes, FKZ 205 67 444, UBA Texte | 01/2008, January 2008 - URL: http://www.umweltbundesamt.de/en/publikationen/nationaler-durchfuehrungsplan-unter-stockholmer )]
[(EURLex2007> EUR-Lex, 2007: REGULATION (EC) No 715/2007 OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 20 June 2007 on type approval of motor vehicles with respect to emissions from light passenger and commercial vehicles (Euro 5 and Euro 6) and on access to vehicle repair and maintenance information - https://data.europa.eu/eli/reg/2007/715/oj )]
[(EURLex2009> EUR-Lex, 2009: REGULATION (EC) No 595/2009 OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 18 June 2009 on type-approval of motor vehicles and engines with respect to emissions from heavy duty vehicles (Euro VI) and on access to vehicle repair and maintenance information and amending Regulation (EC) No 715/2007 and Directive 2007/46/EC and repealing Directives 80/1269/EEC, 2005/55/EC and 2005/78/EC - https://data.europa.eu/eli/reg/2009/595/oj )]
