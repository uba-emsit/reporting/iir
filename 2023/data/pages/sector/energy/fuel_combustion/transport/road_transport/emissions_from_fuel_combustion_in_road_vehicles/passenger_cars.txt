====== 1.A.3.b i - Road transport: Passenger cars ======

===== Short description=====

In sub-category //1.A.3.b i - Road transport: Passenger cars// emissions from fuel combustion in passenger cars (PCs) are reported. 

^ Category Code  ^  Method                                                                           ||||^  AD                                           ||||^  EF                                     |||||
| 1.A.3.b i      |  T1, T3                                                                           |||||  NS, M                                        |||||  CS, M, D                               |||||
^                ^  NO<sub>x</sub>  ^  NMVOC  ^  SO<sub>2</sub>  ^  NH<sub>3</sub>  ^  PM<sub>2.5</sub>  ^  PM<sub>10</sub>  ^  TSP  ^  BC   ^ CO    ^  PB   ^  Cd        ^  Hg   ^  Diox  ^  PAH  ^  HCB  ^
| Key Category:  |  L/T             |  L/T    |  -/-             |  -/-             |  L/T               | L/T               |  -/-  |  L/T  |  L/T  |  L/T  |  -/-       |  -/-  |  -/-   |  -/-  |  -    |

===== Methodology =====

Detailed information on the methods applied is provided in the [[sector:energy:fuel_combustion:transport:road_transport:start | superordinate chapter ]].

==== Activity data ==== 

Specific consumption data for passenger cars is generated within TREMOD [(KNOERR2022a)]. 

The following table gives an overview of annual amounts of the fuels consumed by passenger cars in Germany.

__Table 1: Annual passenger car fuel consumption, in terajoule__
|                  ^  1990      ^  1995      ^  2000      ^  2005      ^  2010      ^  2015      ^  2016      ^  2017      ^  2018      ^  2019      ^  2020      ^  2021      ^
^ Diesel oil       |    266,175 |    321,615 |    348,554 |    465,228 |    500,087 |    621,924 |    650,647 |    670,928 |    639,059 |    636,988 |    527,477 |    540,968 |
^ Gasoline         |  1,273,347 |  1,258,708 |  1,194,743 |    958,090 |    765,293 |    684,668 |    684,770 |    694,572 |    668,293 |    674,830 |    604,043 |    609,663 |
^ LPG              |        138 |        138 |       94,0 |      2,357 |     21,823 |     18,963 |     16,799 |     15,377 |     16,153 |     14,602 |      9,551 |      9,500 |
^ CNG              |          0 |          0 |          0 |      1,625 |      5,366 |      4,419 |      3,533 |      3,590 |      3,271 |      3,766 |      3,754 |      4,199 |
^ Biodiesel        |          0 |        502 |      3,861 |     31,089 |     38,340 |     34,019 |     34,494 |     35,817 |     37,148 |     36,281 |     43,815 |     37,582 |
^ Biogasoline      |          0 |          0 |          0 |      6,582 |     29,568 |     29,695 |     29,744 |     29,283 |     30,049 |     29,105 |     27,577 |     29,004 |
^ Biogas           |          0 |          0 |          0 |          0 |          0 |        745 |        831 |        992 |        880 |      1,531 |      2,020 |      2,007 |
| **Ʃ 1.A.3.b i**  ^  1,539,661 ^  1,580,963 ^  1,547,252 ^  1,464,972 ^  1,360,476 ^  1,394,434 ^  1,420,817 ^  1,450,559 ^  1,394,852 ^  1,397,104 ^  1,218,239 ^ 1,232,922  ^

Here, the following charts underline the ongoing shift from gasoline to diesel-powered passenger cars, that started around 1999/2000.

{{:sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_ad.png?400|Annual over-all energy input }}
{{:sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_ad_Diesel.png?400|Annual energy input from diesel fuels }}
{{:sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_ad_gasoline.png?400|Annual energy input from gasoline fuels }}

<WRAP center round info 60%>
For information on mileage, please refer to sub-chapters on emissions from [[sector:energy:fuel_combustion:transport:road_transport:abrasive_emissions_from_road_vehicles:start | tyre & brake wear and road abrasion]].
</WRAP>

==== Emission factors ====

The majority of emission factors for exhaust emissions from road transport are taken from the 'Handbook Emission Factors for Road Transport' (HBEFA, version 4.1) [(KELLER2017)] where they are provided on a tier3 level mostly and processed within the TREMOD software used by the party [(KNOERR2022a)]. 

However, it is not possible to present these highly specific tier3 values in a comprehendible way here.

<WRAP center round info 100%>
With respect to the country-specific emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. ((During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions.))
</WRAP>

For heavy-metal (other then lead from leaded gasoline) and PAH exhaust-emissions, default emission factors from (EMEP/EEA, 2019) [(EMEPEEA2019)] have been applied.
Regarding PCDD/F, a tier1 EF from (Rentz et al., 2008) [(RENTZ2008)] is used.

__Table 2: tier1 emission factors__
|                                       |  **Pb**   |  **Cd**  |  **Hg**  |  **As**  |  **Cr**  |  **Cu**  |  **Ni**  |  **Se**  |  **Zn**  |  **B[a]P**  |  **B[b]F**  |  **B[k]F**  |  **I[1,2,3-c,d]p**  |  **PAH 1-4**  |  **PCDD/F**  |
|                                       |   [g/TJ]                                                                                  |||||||||   [mg/TJ]                                                                 |||||  [µg/km]     |
^ Diesel oil                            |     0.012 |    0.001 |    0.123 |    0.002 |    0.198 |    0.133 |    0.005 |    0.002 |    0.419 |         498 |         521 |         275 |                 493 |         1.788 |              |
^ Biodiesel<sup>1</sup>                 |     0.013 |    0.001 |    0.142 |    0.003 |    0.228 |    0.153 |    0.005 |    0.003 |    0.483 |         575 |         601 |         317 |                 569 |         2.062 |              |
^ Gasoline fuels                        |     0.037 |    0.005 |    0.200 |    0.007 |    0.145 |    0.103 |    0.053 |    0.005 |    0.758 |          96 |         140 |          69 |                 158 |           464 |              |
^ CNG<sup>2</sup> & biogas<sup>3</sup>  |  NE       |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE         |  NE         |  NE         |  NE                 |  NE           |              |
^ LPG<sup>4</sup>                       |  NE       |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |  NE      |        4.35 |        0.00 |        4.35 |                4.35 |          13.0 |              |
^ all fuels                                                                                                                                                                                                    |||||||||||||||     0.000006 |
<sup>1</sup> values differ from EFs applied for fossil diesel oil to take into account the specific NCV of biodiesel \\
<sup>2</sup> no specific default available from [(EMEPEEA2019)]; value derived from CNG powered busses \\
<sup>3</sup> no specific default available from [(EMEPEEA2019)]; values available for CNG also applied for biogas \\
<sup>4</sup> no specific default available from [(EMEPEEA2019)]; value derived from LPG powered passenger cars

===== Discussion of emission trends =====

__Table 3: Outcome of Key Category Analysis__
|  for: ^  NO<sub>x</sub>  ^  NMVOC  ^  CO   ^  PM<sub>10</sub>  ^  PM<sub>2.5</sub>  ^ BC    ^  Pb   ^  PCDD/F  ^
|   by: |  Level & Trend   |  L/T    |  L/T  |  L/T              |  L/T               |  L/T  |  L/T  |  L/-     |

====Non-methane volatile organic compounds, nitrogen oxides, and carbon monoxide====

Since 1990, exhaust emissions of **nitrogen oxides**, **NMVOC**,  and **carbon monoxide** have decreased sharply due to catalytic-converter use and engine improvements resulting from ongoing tightening of emissions laws and improved fuel quality. 

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_em_nox.png?700 |Annual nitrogen oxides emissions }}

__Table 4: EURO norms and their effect on limit values of NO<sub>x</sub> emissions from passenger cars, in [mg/km]__
| exhaust emission standard (EURO norm)  ^ **Euro 1**  ^  **Euro 2**  ^  **Euro 3**  ^  **Euro 4**  ^  **Euro 5**  ^  **Euro 6a/b**  ^  **Euro 6c**  ^  **Euro 6d**  ^
^ Diesel                                 |  -          |  -           |  500         |  250         |  180         |  80                                           |||
^ Gasoline                               |  -          |  -           |  150         |  80          |  60          |  60                                           |||

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_em_nmvoc.png?700 |Annual NMVOC emissions }}
{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_em_co.png?700 |Annual carbon monoxide emissions }}

__Table 5: EURO norms and their effect on limit values of CO emissions from passenger cars, in [mg/km]__
| exhaust emission standard (EURO norm)  ^ **Euro 1**      ^  **Euro 2**  ^  **Euro 3**  ^  **Euro 4**  ^  **Euro 5**  ^  **Euro 6a/b**  ^  **Euro 6c**  ^  **Euro 6d**  ^
^ Diesel                                 |  2,720 / 3,160  |  1,000       |  640         |  500         |  500         |  500                                          |||
^ Gasoline                               |  2,720 / 3,160  |  2,200       |  2,300       |  1,000       |  1,000       |  1,000                                        |||

==== Ammonia and sulphur dioxide====

As for the entire road transport sector, the trends for **sulphur dioxide** and **ammonia** exhaust emissions from passenger cars show charcteristics very different from those shown above.

Here, the strong dependence on increasing fuel qualities (sulphur content) leads to an cascaded downward trend of emissions , influenced only slightly by increases in fuel consumption and mileage. 

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_em_so2.png?700 |Annual sulphur oxides emissions}}

For **ammonia** emissions, the increasing use of catalytic converters in gasoline driven cars in the 1990s lead to a steep increase whereas both the technical development of the converters and the ongoing shift from gasoline to diesel cars resulted in decreasing emissions in the following years.

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_em_nh3.png?700 |Annual ammonia emissions}}


====Particulate matter & Black carbon====

//(from fuel combustion only; no wear/abrasion included)// 

Starting in the middle of the 1990s, a so-called "diesel boom" began, leading to a switch from gasoline to diesel powered passenger cars. As the newly registered diesel cars had to meet the EURO2 standard (in force since 1996/'97) with a PM limit value less than half the EURO1 value, the growing diesel consumption was overcompensated qickly by the mitigation technologies implemented due to the new EURO norm. During the following years, new EURO norms came into force. With the still ongoing "diesel boom" those norms led to a stabilisation (EURO3, 2000/'01) of emissions and to another strong decrease of PM emissions (EURO4, 2005/'06), respectively. Over-all, the increased consumption of diesel in passenger cars was overastimated by the implemented mitigation technologies. The table below shows the evolution of the limit value for particle emissions from passenger cars with diesel engines.

With this submission, Black Carbon (BC) emissions are reported for the first time. Here, EF are estimated based on as fractions of PM as provided in [(EMEPEEA2019)].
Due to this fuel-specific fractions, the trend of BC emissions reflects the ongoing shift from gasoline to diesel ("dieselisation"). 

{{ :sector:energy:fuel_combustion:transport:road_transport:emissions_from_fuel_combustion_in_road_vehicles:1a3bi_em_pm.png?700 | Annual particulate matter emissions}}

__Table 6: EURO norms and their effect on limit values of PM emissions from passenger cars__
| exhaust emission standard (EURO norm)  ^  **Euro 1**  ^  **Euro 2**               ^  **Euro 3**  ^  **Euro 4**  ^  **Euro 5**           ^  **Euro 6a/b**        ^ **Euro 6c**  ^ **Euro 6d**  ^
^ limit values in [mg/km]                                                                                                                                                               |||||||||
| Diesel                                 |  **180**     |  **80/100<sup>1</sup> **  |  **50**      |  **25**      |  **4,5**              |  **4,5**                                          |||
| Gasoline                               |  -           |  -                        |  -           |  -           |  **4,5**              |  **4,5**                                          |||
^ limit values in [number of particles]                                                                                                                                                 |||||||||
| Diesel                                 |  -           |  -                        |  -           |  -           |  6 x 10<sup>11</sup>                                                     ||||
| Gasoline                               |  -           |  -                        |  -           |  -           |  -                    |  6 x 10<sup>11</sup>                              |||
<sup>1</sup> for direct injection engines

===== Recalculations =====

Compared to submission 2022, recalculations were carried out due to a routine revision of the TREMOD software. Furthermore, for 2020, over-all activity data for NFR 1.A.3.b have been adapted to the final Energy Balance 2020. 

Here, for diesel oil, significant amounts have been re-allocated from heavy-duty vehicles (see NFR 1.A.3.b iii) whereas, for gasoline, hugher amounts have been re-allocated to light-duty vehicles (see NFR 1.A.3.b ii).

__Table 7: Revised fuel consumption data, in terajoules__
|                                    ^  1990      ^  1995      ^  2000      ^  2005      ^  2010      ^  2015      ^  2016      ^  2017      ^  2018      ^  2019      ^  2020      ^
| **DIESEL OIL**                                                                                                                                                         ||||||||||||
^ current Submission                 |    266,175 |    321,615 |    348,554 |    465,228 |    500,087 |    621,924 |    650,647 |    670,928 |    639,059 |    636,988 |    527,477 |
^ previous Submission                |    251,081 |    304,573 |    330,544 |    447,843 |    491,676 |    612,125 |    640,924 |    661,185 |    630,091 |    628,890 |    522,536 |
^ absolute change                    |     15,094 |     17,042 |     18,010 |     17,385 |      8,411 |      9,799 |      9,723 |      9,743 |      8,967 |      8,098 |      4,941 |
^ relative change                    |      6.01% |      5.60% |      5.45% |      3.88% |      1.71% |      1.60% |      1.52% |      1.47% |      1.42% |      1.29% |      0.95% |
| **BIODIESEL**                                                                                                                                                          ||||||||||||
^ current Submission                 |            |        502 |      3,861 |     31,089 |     38,340 |     34,019 |     34,494 |     35,817 |     37,148 |     36,281 |     43,815 |
^ previous Submission                |            |        475 |      3,662 |     29,928 |     37,695 |     33,483 |     33,979 |     35,297 |     36,626 |     35,820 |     43,406 |
^ absolute change                    |            |       26,6 |        200 |      1,162 |        645 |        536 |        515 |        520 |        521 |        461 |        410 |
^ relative change                    |            |      5.60% |      5.45% |      3.88% |      1.71% |      1.60% |      1.52% |      1.47% |      1.42% |      1.29% |      0.94% |
| **GASOLINE**                                                                                                                                                           ||||||||||||
^ current Submission                 |  1,273,347 |  1,258,708 |  1,194,743 |    958,090 |    765,293 |    684,668 |    684,770 |    694,572 |    668,293 |    674,830 |    604,043 |
^ previous Submission                |  1,280,592 |  1,263,563 |  1,198,941 |    960,365 |    766,348 |    685,451 |    685,537 |    695,328 |    669,083 |    675,721 |    605,570 |
^ absolute change                    |     -7,244 |     -4,854 |     -4,198 |     -2,275 |     -1,055 |       -782 |       -768 |       -756 |       -791 |       -891 |     -1,527 |
^ relative change                    |     -0.57% |     -0.38% |     -0.35% |     -0.24% |     -0.14% |     -0.11% |     -0.11% |     -0.11% |     -0.12% |     -0.13% |     -0.25% |
| **BIOGASOLINE**                                                                                                                                                        ||||||||||||
^ Submission 2023                    |            |            |            |      6,582 |     29,568 |     29,695 |     29,744 |     29,283 |     30,049 |     29,105 |     27,577 |
^ Submission 2022                    |            |            |            |      6,597 |     29,609 |     29,729 |     29,777 |     29,315 |     30,084 |     29,144 |     27,647 |
^ absolute change                    |            |            |            |      -15,6 |      -40,8 |      -33,9 |      -33,3 |      -31,9 |      -35,5 |      -38,4 |      -69,9 |
^ relative change                    |            |            |            |     -0.24% |     -0.14% |     -0.11% |     -0.11% |     -0.11% |     -0.12% |     -0.13% |     -0.25% |
| **COMPRESSED NATURAL GAS - CNG**                                                                                                                                       ||||||||||||
^ current Submission                 |            |            |            |      1,625 |      5,366 |      4,419 |      3,533 |      3,590 |      3,271 |      3,766 |      3,754 |
^ previous Submission                |            |            |            |      1,604 |      5,351 |      4,443 |      3,562 |      3,623 |      3,297 |      3,786 |      4,421 |
^ absolute change                    |            |            |            |       21,3 |       14,8 |      -24,2 |      -29,1 |      -33,0 |      -25,4 |      -19,6 |       -667 |
^ relative change                    |            |            |            |      1.33% |      0.28% |     -0.54% |     -0.82% |     -0.91% |     -0.77% |     -0.52% |     -15.1% |
| **BIOGAS**                                                                                                                                                             ||||||||||||
^ current Submission                 |            |            |            |            |            |        745 |        831 |        992 |        880 |      1,531 |      2,020 |
^ previous Submission                |            |            |            |            |            |        749 |        838 |      1,001 |        887 |      1,539 |      2,028 |
^ absolute change                    |            |            |            |            |            |      -4,08 |      -6,85 |      -9,11 |      -6,83 |      -7,99 |      -8,53 |
^ relative change                    |            |            |            |            |            |     -0.54% |     -0.82% |     -0.91% |     -0.77% |     -0.52% |     -0.42% |
| **LIQUEFIED PETROLEUM GAS - LPG**                                                                                                                                      ||||||||||||
^ current Submission                 |        138 |        138 |         94 |      2,357 |     21,823 |     18,963 |     16,799 |     15,377 |     16,153 |     14,602 |      9,551 |
^ previous Submission                |        138 |        138 |         94 |      2,357 |     21,823 |     18,963 |     16,799 |     15,377 |     16,153 |     14,602 |     13,667 |
^ absolute change                    |       0.00 |       0.00 |       0.00 |       0.00 |       0.00 |       0.00 |       0.00 |       0.00 |       0.00 |       0.00 |     -4,115 |
^ relative change                    |      0.00% |      0.00% |      0.00% |      0.00% |      0.00% |      0.00% |      0.00% |      0.00% |      0.00% |      0.00% |     -30.1% |
| **TOTAL FUEL CONSUMPTION**                                                                                                                                             ||||||||||||
^ current Submission                 |  1,539,661 |  1,580,963 |  1,547,252 |  1,464,972 |  1,360,476 |  1,394,434 |  1,420,817 |  1,450,559 |  1,394,852 |  1,397,104 |  1,218,239 |
^ previous Submission                |  1,531,811 |  1,568,749 |  1,533,241 |  1,448,694 |  1,352,502 |  1,384,943 |  1,411,416 |  1,441,125 |  1,386,222 |  1,389,502 |  1,219,276 |
^ absolute change                    |      7,850 |     12,214 |     14,011 |     16,278 |      7,974 |      9,491 |      9,401 |      9,434 |      8,630 |      7,602 |     -1,037 |
^ relative change                    |      0.51% |      0.78% |      0.91% |      1.12% |      0.59% |      0.69% |      0.67% |      0.65% |      0.62% |      0.55% |     -0.09% |

Due to the variety of tier3 **emission factors** applied, it is not possible to display any changes in these data sets in a comprehendible way.


<WRAP center round info 65%>
For **pollutant-specific information on recalculated emission estimates for Base Year and 2020**, please see the recalculation tables following [[general:recalculations:start|chapter 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

Besides a routine revision of the underlying model, no specific improvements are planned.

[(AGEB2022>AGEB, 2022: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2020/?wpv-jahresbereich-bilanz=2011-2020, (Aufruf: 23.11.2022), Köln & Berlin, 2022)]
[(BAFA2022>BAFA, 2022: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2021_12.xlsx;jsessionid=80E1FD32B36918F682608C03FDE79257.1_cid381?__blob=publicationFile&v=5, Eschborn, 2022.)]
[(KNOERR2022a> Knörr et al. (2022a): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Fortschreibung des Daten- und Rechenmodells: Energieverbrauch und Schadstoffemissionen des motorisierten Verkehrs in Deutschland 1960-2035, sowie TREMOD, im Auftrag des Umweltbundesamtes, Heidelberg & Berlin, 2022. )]
[(KELLER2017> Keller et al. (2017): Keller, M., Hausberger, S., Matzer, C., Wüthrich, P., & Notter, B.: Handbook Emission Factors for Road Transport, version 4.1 (Handbuch Emissionsfaktoren des Straßenverkehrs 4.1) URL: http://www.hbefa.net/e/index.html - Dokumentation, Bern, 2017. )]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook 2019; https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-3-b-i/view; Copenhagen, 2019.)]
[(RENTZ2008> Rentz et al., 2008: Nationaler Durchführungsplan unter dem Stockholmer Abkommen zu persistenten organischen Schadstoffen (POPs), im Auftrag des Umweltbundesamtes, FKZ 205 67 444, UBA Texte | 01/2008, January 2008 - URL: http://www.umweltbundesamt.de/en/publikationen/nationaler-durchfuehrungsplan-unter-stockholmer )]