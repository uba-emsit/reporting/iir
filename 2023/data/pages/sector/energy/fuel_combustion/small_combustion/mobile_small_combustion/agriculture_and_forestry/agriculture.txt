====== 1.A.4.c ii (a) - Off-road Vehicles and other Machinery: Agriculture ======

===== Short description =====

Under sub-category //1.A.4.c ii (a)// fuel combustion activities and resulting emissions from agricultural off-road vehicles and mobile machinery are reported.

^     NFR-Code ^ Source category                                      ^  Method  ^  AD     ^  EF   ^  Key Category Analysis                                ^
| 1.A.4.c ii (a) | Off-road Vehicles and Other Machinery: Agriculture |  T1, T2 | NS, M | CS, D, M | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| see superordinate chapter ]] |

{{  :sector:energy:fuel_combustion:small_combustion:traktor.png?nolink&300}}

======Methodology=====

====Activity data====

Subsector-specific consumption data is included in the primary fuel-delivery data are available from NEB line 67: 'Commercial, trade, services and other consumers' (AGEB, 2022) [(AGEB2022)].

__Table 1: Sources for primary fuel-delivery data__
|| through 1994 || **AGEB** - National Energy Balance, line 79: 'Haushalte und Kleinverbraucher insgesamt'  ||
|| as of 1995 || **AGEB** - National Energy Balance, line 67: 'Gewerbe, Handel, Dienstleistungen u. übrige Verbraucher'  ||

Following the deduction of energy inputs for military vehicles as provided in (BAFA, 2022) [(BAFA2022)], the remaining amounts of gasoline and diesel oil are apportioned onto off-road construction vehicles (NFR 1.A.2.g vii) and off-road vehicles in commercial/institutional use (1.A.4. ii) as well as agriculture and forestry (NFR 1.A.4.c ii) based upon annual shares derived from TREMOD-MM (Knörr et al. (2022b) [(KNOERR2022b)] (cf. NFR 1.A.4 - mobile).

__Table 2: Annual contribution of agricultural vehicles and mobile machinery to the primary diesel<sup>1</sup> fuels delivery data provided in NEB line 67__
^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^
|  47.5% |  45.6% |  43.8% |  46.2% |  47.4% |  47.2% |  47.2% |  48.0% |  47.8% |  48.2% |  48.4% |  48.5% |  48.4% |  48.4% |  48.2% |  48.6% |
<sup>1</sup> no gasoline used in agricultural vehicles and mobile machinery

__Table 3: Annual mobile fuel consumption in agriculture, in terajoules__
|                       ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2011   ^  2012   ^  2013   ^  2014   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^  2021   ^
^ Diesel Oil            |  53,188 |  44,553 |  41,633 |  37,893 |  41,973 |  42,813 |  42,087 |  44,479 |  46,205 |  48,848 |  50,968 |  52,500 |  48,950 |  49,622 |  50,901 |  51,619 |
^ Biodiesel             |       0 |       0 |       0 |   2,421 |   3,218 |   2,987 |   2,970 |   2,638 |   2,839 |   2,672 |   2,702 |   2,803 |   2,846 |   2,826 |   4,228 |   3,586 |
| **Ʃ 1.A.4.c ii (i)**  ^  53,188 ^  44,553 ^  41,633 ^  40,315 ^  45,191 ^  45,800 ^  45,057 ^  47,117 ^  49,045 ^  51,520 ^  53,670 ^  55,302 ^  51,795 ^  52,448 ^  55,129 ^  55,205 ^
 
==== Emission factors ====

The emission factors applied here are of rather different quality:
For all **main pollutants**, **carbon monoxide** and **particulate matter**, annual IEF modelled within TREMOD MM are used, representing the sector's vehicle-fleet composition, the development of mitigation technologies and the effect of fuel-quality legislation.  

__Table 4: Annual country-specific emission factors<sup>1</sup>, in kg/TJ__
|                 ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^
^ NH<sub>3</sub>  |   0.15 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |   0.16 |
^ NMVOC           |    258 |    231 |    205 |    165 |    124 |    118 |    112 |    106 |   99.7 |   93.8 |   88.6 |   83.8 |   79.1 |   74.8 |   70.6 |   66.4 |
^ NO<sub>x</sub>  |    873 |    886 |    916 |    832 |    682 |    655 |    629 |    605 |    581 |    560 |    541 |    522 |    505 |    489 |    471 |    451 |
^ SO<sub>x</sub>  |   79.6 |   60.5 |   14.0 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |   0.37 |
^ PM<sup>2</sup>  |    125 |    109 |   93.1 |   74.7 |   57.4 |   54.8 |   52.4 |   50.1 |   47.5 |   44.8 |   42.3 |   39.8 |   37.5 |   35.4 |   33.2 |   30.9 |
^ BC<sup>3</sup>  |    229 |    201 |    171 |    134 |   97.1 |   91.5 |   86.3 |   81.5 |   76.4 |   71.5 |   66.9 |   62.7 |   58.6 |   54.9 |   51.3 |   47.6 |
^ CO              |    882 |    834 |    779 |    674 |    555 |    536 |    518 |    500 |    479 |    459 |    441 |    424 |    407 |    391 |    375 |    359 |
<sup>1</sup> due to lack of better information: similar EF are applied for fossil and biofuels \\
<sup>2</sup> EF(PM<sub>2.5</sub>) also applied for PM<sub>10</sub> and TSP (assumption: > 99% of TSP consists of PM<sub>2.5</sub>)\\
<sup>3</sup> estimated via a f-BCs as provided in [(EMEPEEA2019)], Chapter 1.A.2.g vii, 1.A.4.a ii, b ii, c ii, 1.A.5.b i - Non-road, note to Table 3-1: Tier 1 emission factors for off-road machinery \\

> **NOTE:** With respect to the country-specific emission factors applied for particulate matter, given the circumstances during test-bench measurements, condensables are most likely included at least partly. During test-bench measurements, temperatures are likely to be significantly higher than under real-world conditions, thus reducing condensation. On the contrary, smaller dillution (higher number of primary particles acting as condensation germs) together with higher pressures increase the likeliness of condensation. So over-all condensables are very likely to occur but different to real-world conditions.

> For information on the **emission factors for heavy-metal and POP exhaust emissions**, please refer to Appendix 2.3 - Heavy Metal (HM) exhaust emissions from mobile sources and Appendix 2.4 - Persistent Organic Pollutant (POP) exhaust emissions from mobile sources.

===== Recalculations =====

Revisions in **activity data** result from slightly revised annual shares adapted EBZ 67 shares as well as the implementation of primary activity data from the now finalised NEB 2020.

__Table 5: Revised annual shares of NEB line 67, in %__
|                      ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2011   ^  2012   ^  2013   ^  2014   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^
^ current submission   |   0,475 |   0,456 |   0,438 |   0,462 |   0,474 |   0,472 |   0,472 |   0,480 |   0,478 |   0,482 |   0,484 |   0,485 |   0,484 |   0,484 |   0,482 |
^ previous submission  |   0,476 |   0,456 |   0,439 |   0,462 |   0,475 |   0,472 |   0,473 |   0,480 |   0,478 |   0,483 |   0,485 |   0,485 |   0,484 |   0,484 |   0,483 |
^ absolute change      |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |  -0,001 |
^ relative change      |  -0,14% |  -0,15% |  -0,15% |  -0,13% |  -0,12% |  -0,12% |  -0,12% |  -0,12% |  -0,12% |  -0,12% |  -0,12% |  -0,12% |   -0,1% |  -0,12% |   -0,1% |

__Table 6: Revised activity data, in terajoules__
|                      ^  1990   ^  1995   ^  2000   ^  2005   ^  2010   ^  2011   ^  2012   ^  2013   ^  2014   ^  2015   ^  2016   ^  2017   ^  2018   ^  2019   ^  2020   ^
^ current submission   |  53,188 |  44,553 |  41,633 |  40,315 |  45,191 |  45,800 |  45,057 |  47,117 |  49,045 |  51,520 |  53,670 |  55,302 |  51,795 |  52,448 |  55,129 |
^ previous submission  |  53,263 |  44,622 |  41,696 |  40,366 |  45,246 |  45,855 |  45,111 |  47,172 |  49,102 |  51,580 |  53,732 |  55,367 |  51,855 |  52,509 |  54,641 |
^ absolute change      |   -75.7 |   -68.7 |   -63.4 |   -51.4 |   -54.6 |   -54.8 |   -53.6 |   -54.7 |   -56.7 |   -59.9 |   -62.4 |   -64.3 |   -59.9 |   -60.6 |     488 |
^ relative change      |  -0.14% |  -0.15% |  -0.15% |   -0.1% |   -0.1% |  -0.12% |  -0.12% |  -0.12% |  -0.12% |  -0.12% |  -0.12% |  -0.12% |  -0.12% |  -0.12% |   0.89% |
<WRAP center round info 60%>
For **pollutant-specific information on recalculated emission estimates reported for Base Year and 2020**, please see the recalculation tables following chapter [[general:recalculations:start| 8.1 - Recalculations]].
</WRAP>

===== Planned improvements =====

Besides a routine revision of the underlying model, no specific improvements are planned. 

[(AGEB2022>AGEB, 2022: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2020/?wpv-jahresbereich-bilanz=2011-2020, (Aufruf: 23.11.2021), Köln & Berlin, 2022)]
[(BAFA2022>BAFA, 2022: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2021_12.xlsx;jsessionid=80E1FD32B36918F682608C03FDE79257.1_cid381?__blob=publicationFile&v=5, Eschborn, 2022.)]
[(KNOERR2022b>Knörr et al. (2022b): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Aktualisierung des Modells TREMOD-Mobile Machinery (TREMOD MM) 2022, Heidelberg, 2022.)]
[(EMEPEEA2019> EMEP/EEA, 2019: EMEP/EEA air pollutant emission inventory guidebook – 2019, Copenhagen, 2019.)]
[(KNOERR2009> Knörr et al. (2009): Knörr, W., Heldstab, J., & Kasser, F.: Ermittlung der Unsicherheiten der mit den Modellen TREMOD und TREMOD-MM berechneten Luftschadstoffemissionen des landgebundenen Verkehrs in Deutschland; final report; URL: https://www.umweltbundesamt.de/sites/default/files/medien/461/publikationen/3937.pdf, FKZ 360 16 023, Heidelberg & Zürich, 2009.)]