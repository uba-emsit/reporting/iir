====== 1.A.4 - Small Combustion: Mobile Sources (OVERVIEW) ======

===== Short description =====

^  NFR-Code                                     ^  Source category                                                                                                                                                           ^
| **1.A.4**                                     | **Small Combustion**                                                                                                                                                       |
| //including mobile sources sub-categories//:                                                                                                                                                                              ||
| 1.A.4.a ii                                    | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:commercial_and_institutional| Commercial / Institutional: Mobile ]]                               |
| 1.A.4.b ii                                    | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:residential| Residential: Household and Gardening: Mobile ]]                                      |
| 1.A.4.c ii                                    | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:agriculture_and_forestry| Agriculture/Forestry/Fishing: Off-road Vehicles and Other Machinery ]]  |
| 1.A.4.c iii                                   | [[sector:energy:fuel_combustion:small_combustion:mobile_small_combustion:fishing| Agriculture/Forestry/Fishing: National Fishing ]]                                        |

Mobile sources reported under //NFR 1.A.4 - Small combustion// comprise of such versatile mobile equipment as forklifters (1.A.a ii), gasoline-driven lawn mowers used for gardening (1.A.4.b ii) over tractors in agriculture and harvesters and chain saws in forestry (1.A.4.c ii) to the German deep sea fishing fleet (1.A.4.c iii). 

<WRAP center round info 100%>
For further information on sub-sector specific consumption data, emission factors and emissions as well as further information on emission trends, recalculations and planned improvements, please follow the links above.
</WRAP>


===== Method =====

==== Activity data ====

**Primary activity data** are available from National Energy Balances (NEBs) (AGEB, 2022) [(AGEB2022)]. 

Here, aggregated data for NFRs //1.A.a ii//,//1.A.4.c ii// and //1.A.2.g vii// are included in line 67: 'Commercial, trade, services and other consumers'.
In contrast, AD for  is available directly from line 66: 'Households'. 
Furthermore, AD for  is included partly in NEB lines 6: 'Maritime Bunkers' and 64: 'Coastal and inland navigation'.

Table 1 below tries to demonstrate the breaking-down of primary data in NEB line 67 onto NFRs //1.A.2.g vii//, //1.A.4a ii// and //1.A.4.c ii//.
For further information on the resulting specific shares as well as the fuel consumption in NFRs //1.A.4.b ii// and //1.A.4.c iii// please refer to the respective sub-chapters.

__Table 1:  Primary AD from NEB line 67: 'Commercial, trade, services and other consumers', in terajoules__ 
|              ^  1990    ^  1995    ^  2000   ^  2005   ^  2010   ^  2011   ^  2012   ^  2013   ^  2014   ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^
^ diesel oil   |  126,920 |  105,800 |  96,425 |  85,293 |  89,516 |  91,362 |  90,044 |  93,377 |  97,410 |  101,911 |  105,895 |  108,752 |  101,513 |  102,836 |  105,634 |  106,432 |
^ biodiesel    |          |          |         |   5,460 |   6,863 |   6,375 |   6,355 |   5,538 |   5,986 |    5,575 |    5,614 |    5,806 |    5,901 |    5,857 |    8,775 |    7,394 |
^ gasoline     |   26,036 |   17,264 |  14,881 |  14,151 |   9,204 |   8,637 |   5,358 |   5,257 |   4,941 |    8,329 |    7,991 |    7,484 |    7,315 |    6,913 |    8,410 |    8,691 |
^ biogasoline  |          |          |         |    97.2 |     356 |     354 |     237 |     225 |     215 |      361 |      347 |      316 |      329 |      298 |      384 |      413 |
^ LPG          |          |    7,963 |   9,238 |  28,246 |  24,605 |  19,193 |  19,582 |  19,559 |  17,945 |   19,916 |   23,260 |   16,971 |   19,426 |   22,054 |   16,960 |   14,810 |

In a first step, annual fuel deliveries to the military as provided in (BAFA, 2022) [(BAFA2022)]... 

__Table 2:  Annual fuel deliveries to the military as included in NEB line 67, in terajoules__
|              ^  1990   ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^
^ diesel oil   |  15,037 |  8,001 |  1,364 |  3,206 |    977 |    620 |    966 |    680 |    683 |    580 |    577 |    415 |    279 |    281 |    133 |    138 |
^ biodiesel    |         |        |        |    214 |   74.9 |   43.2 |   68.2 |   40.3 |   41.9 |   31.7 |   30.6 |   22.1 |   16.2 |   16.0 |   11.1 |   9.55 |
^ gasoline     |  21,508 |  9,800 |  7,477 |  6,838 |  4,792 |  4,624 |  4,106 |  4,027 |  3,635 |  3,287 |  2,959 |  2,463 |  2,300 |  2,269 |  1,770 |  1,921 |
^ biogasoline  |         |        |        |   47.0 |    185 |    190 |    182 |    173 |    158 |    143 |    129 |    104 |    103 |   97.9 |   80.8 |   91.4 |

...are deduced from these primary AD, giving the remaining amounts of gasoline and diesel oil for NFRs 1.A.2.g vii, 1.A.a ii and 1.A.4.c ii:

__Table 3:  Annual fuel deliveries to the remaining sectors covered by NEB line 67, in terajoules__ 
|              ^  1990    ^  1995   ^  2000   ^  2005   ^  2010   ^  2011   ^  2012   ^  2013   ^  2014   ^  2015    ^  2016    ^  2017    ^  2018    ^  2019    ^  2020    ^  2021    ^
^ diesel oil   |  111,883 |  97,799 |  95,061 |  82,087 |  88,539 |  90,742 |  89,078 |  92,697 |  96,727 |  101,331 |  105,318 |  108,337 |  101,234 |  102,555 |  105,501 |  106,294 |
^ biodiesel    |          |         |         |   5,245 |   6,788 |   6,331 |   6,287 |   5,497 |   5,944 |    5,543 |    5,583 |    5,784 |    5,885 |    5,841 |    8,764 |    7,384 |
^ gasoline     |    4,528 |   7,464 |   7,404 |   7,313 |   4,412 |   4,013 |   1,252 |   1,230 |   1,306 |    5,042 |    5,032 |    5,021 |    5,015 |    4,644 |    6,640 |    6,770 |
^ biogasoline  |          |         |         |    50.2 |     170 |     164 |    55.5 |    52.7 |    56.8 |      219 |      219 |      212 |      225 |      200 |      303 |      322 |
^ LPG          |          |   7,963 |   9,238 |  28,246 |  24,605 |  19,193 |  19,582 |  19,559 |  17,945 |   19,916 |   23,260 |   16,971 |   19,426 |   22,054 |   16,960 |   14,810 |

As the National Enregy Balances provide no consumption data for LPG before 1995 and as part of the LPG provided in NEB line 67 is used for stationary combustion (whereas all diesel and gasoline fuels are allocated to mobile combustion), activity data for LPG used in in NRMM are taken directly from TREMOD MM (Knörr et al. (2022b)) [(KNOERR2022b)].

In another step, the following sub-sectors specific annual percentual contributions to NEB line 67 as computed within TREMOD-MM are apllied to these primary AD to deduce sub-sectors specific AD.

__Table 4:  Annual percentual contributions of NFRs 1.A.2.g vii, 1.A.a ii and 1.A.4.c ii__
|                  ^  1990  ^  1995  ^  2000  ^  2005  ^  2010  ^  2011  ^  2012  ^  2013  ^  2014  ^  2015  ^  2016  ^  2017  ^  2018  ^  2019  ^  2020  ^  2021  ^
^ 1.A.2.g vii      |  43.0% |  46.4% |  47.1% |  43.8% |  43.2% |  43.5% |  43.8% |  43.3% |  43.6% |  43.2% |  43.1% |  43.0% |  42.6% |  42.5% |  42.2% |  41.9% |
^ 1.A.4.a ii       |  7.01% |  6.65% |  6.98% |  7.17% |  6.51% |  6.35% |  6.20% |  5.95% |  5.81% |  5.73% |  5.83% |  5.77% |  5.67% |  5.59% |  5.45% |  5.31% |
^ 1.A.4.c ii (i)   |  47.5% |  45.6% |  43.8% |  46.2% |  47.4% |  47.2% |  47.2% |  48.0% |  47.8% |  48.2% |  48.4% |  48.5% |  48.4% |  48.4% |  48.2% |  48.6% |
^ 1.A.4.c ii (ii)  |  2.41% |  1.36% |  2.15% |  2.88% |  2.91% |  2.99% |  2.77% |  2.76% |  2.81% |  2.89% |  2.72% |  2.79% |  3.35% |  3.54% |  4.15% |  4.25% |
| **TOTAL NRMM**   ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^
^ 1.A.2.g vii      |  31.5% |  59.7% |  55.1% |  58.6% |  64.5% |  64.4% |  66.9% |  67.1% |  66.9% |  66.7% |  68.4% |  68.1% |  64.2% |  63.2% |  59.7% |  59.2% |
^ 1.A.4.c ii (ii)  | 68.5%  | 40.3%  | 44.9%  | 41.4%  | 35.5%  | 35.6%  | 33.1%  | 32.9%  | 33.1%  | 33.3%  | 31.6%  | 31.9%  | 35.8%  | 36.8%  |  40.3% |  40.8% |
| **TOTAL NRMM**   ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^   100% ^
source: own estimates, based on TREMOD MM

<WRAP center round info 60%>
For the **NFR-specific activity data and emission factors** please refer to the corresponding chapters linked at the top of this page.
</WRAP>

===== Recalculations =====

**Primary activity data** for 2020 were revised in accordance with the now finalised National Energy Balance 2020.

Here, the mpost significant changes occur for gasoline fuels and LPG.

__Table 2: Revised primary activity data 2020, in terajoules__
|                      | **Diesel oil**  |  **Biodiesel**  |  **Gasoline**  |  **Biogasoline**  |  **LPG**  |
^ current submission   |  105,2634        |  8,775          |  8,410         |  384              |  16,960   |
^ previous submission  |  104,580        |  8,687          |  7,050         |  322              |  20,622   |
^ absolute change      |  1,054          |  87.4           |  1,360         |  62.1             |  -3,662   |
^ relative change      |  1.01%          |  1.01%          |  19.3%         |  19.3%            |  -17.8%   |


[(AGEB2022>AGEB, 2022: Working Group on Energy Balances (Arbeitsgemeinschaft Energiebilanzen (Hrsg.), AGEB): Energiebilanz für die Bundesrepublik Deutschland; https://ag-energiebilanzen.de/daten-und-fakten/bilanzen-1990-bis-2020/?wpv-jahresbereich-bilanz=2011-2020, (Aufruf: 23.11.2021), Köln & Berlin, 2022)]
[(BAFA2022>BAFA, 2022: Federal Office of Economics and Export Control (Bundesamt für Wirtschaft und Ausfuhrkontrolle, BAFA): Amtliche Mineralöldaten für die Bundesrepublik Deutschland;
https://www.bafa.de/SharedDocs/Downloads/DE/Energie/Mineraloel/moel_amtliche_daten_2021_12.xlsx;jsessionid=80E1FD32B36918F682608C03FDE79257.1_cid381?__blob=publicationFile&v=5, Eschborn, 2022.)]
[(KNOERR2022b>Knörr et al. (2022b): Knörr, W., Heidt, C., Gores, S., & Bergk, F.: ifeu Institute for Energy and Environmental Research (Institut für Energie- und Umweltforschung Heidelberg gGmbH, ifeu): Aktualisierung des Modells TREMOD-Mobile Machinery (TREMOD MM) 2022, Heidelberg, 2022.)]

