====== 1.A - Fuel Combustion Activities (OVERVIEW) ======


^ NFR-Code                                         ^ Name of Category                                                 |
| 1.A                                              | **Fuel Combustion**                                                 |
| //consisting of / including source categories//                                                                    |   |
| 1.A.1                                            | [[sector:energy:fuel_combustion:energy_industries:start|Energy Industries]]                                 |   
| 1.A.2                                            | [[sector:energy:fuel_combustion:industry:start|Fuel Combustion Activities in Industries and Construction]]  |
| 1.A.3                                            | [[sector:energy:fuel_combustion:transport:start|Transport]]                                                   |
| 1.A.4                                            | [[sector:energy:fuel_combustion:small_combustion:start| Small Combustion]]                                   |
| 1.A.5                                            | [[sector:energy:fuel_combustion:other_including_military:start| Other (including Military)]]                |