===== 11.C - Other Natural Sources =====

====Short description ====

^  NFR-Code  ^  Name of category  ^  Method ^  AD  ^  EF  ^  Key Category  ^
|  11.C      |  **Lightning**     |  T1     |  NS  |  D   |                |


Lightning and corona discharge during thunderstorm events cause atmospheric chemical reactions to take place at high voltages and high temperatures. These reactions cause the production of NO<sub>x</sub> in the atmosphere.

====Methodology====

The calculation of NO<sub>x</sub> emissions from lightning uses strike counts from the German weather service and default emission factors from the EMEP/EEA guidebook.

For the complete time series, the emissions are calculated as follows: 

<WRAP center round info 50%>
**EM** = **AD** (number of lightning strikes) * ** EF** (kg emission per strike)
</WRAP>

===Activity data ===
The number of lightning strikes in Germany is taken from a commercial system called "BLIDS" run by Siemens. The data has been cross-referenced with information from the German weather service. Consistent strike count data is available from 1992 onward and has been back-populated to cover the full time series since 1990. The following table shows the strike figures over time.

Table 1: Lightning strikes in Germany from 1990 onwards
^  Year  ^  Strike count [1000 strikes]  ^
|  1990  |  443                          |
|  1991  |  443                          |
|  1992  |  370                          |
|  1993  |  274                          |
|  1994  |  429                          |
|  1995  |  394                          |
|  1996  |  218                          |
|  1997  |  255                          |
|  1998  |  428                          |
|  1999  |  589                          |
|  2000  |  1,026                        |
|  2001  |  591                          |
|  2002  |  1,023                        |
|  2003  |  813                          |
|  2004  |  741                          |
|  2005  |  802                          |
|  2006  |  1,001                        |
|  2007  |  1,139                        |
|  2008  |  990                          |
|  2009  |  917                          |
|  2010  |  589                          |
|  2011  |  687                          |
|  2012  |  656                          |
|  2013  |  542                          |
|  2014  |  623                          |
|  2015  |  550                          |
|  2016  |  432                          |
|  2017  |  443                          |
|  2018  |  446                          |
|  2019  |  329                          |
|  2020  |  399                          |
|  2021  |  512                          |

===Emission factor===
For the calculation of emissions in this category, the Guidebook emission factor of 2.75 kg NOx per strike is used.

====Emission Trend====
The emission value is solely dependent on the strike count and varies between 1 to 3 kilotons of NOx per year. 

Figure 1: NFR 11.C, NOx emissions from lightning

{{ :sector:natural_sources:11c_nox_from_lightning.png?nolink&600 |}}

===== Recalculations =====

<WRAP center round info 60%>
As these activities and emissions are reported for the first time, no specific recalculations occur against a previous submission.
</WRAP>


==== Uncertainties ====
The AD from BLIDS does have a low uncertainty of ± 3%. The uncertainties for the emission factors are estimated to be relatively high, being a default value. Hence the overall uncertainty for the emission estimation of NO<sub>x</sub> from lightning is qualified estimated by expert judgement to be high (>50%).

====Quality checks====
No sector-specific quality checks are done.

====Planned Improvement====
Currently no improvements are planned.
