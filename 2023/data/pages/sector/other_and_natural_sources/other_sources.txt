====== 6 - Other Sources ======

Within NFR 6 - Other Sources, activities and resulting emissions are taken into account that cannot be related to any of the other NFR catgeories.

For the time being, the only emission source reported within NFR 6 is human sweating and breathing:
^  NFR code  ^  NFR name  ^
|  6.A       |    [[sector:other_and_natural_sources:other_sources:human_sweating&breathing| 6.A - Emissions from human sweating and breathing]]        |


----


===== Visual overview =====

__Emission trends for main pollutants in //NFR 6 - Other Sources//:__
{{ :sector:iir_nfr6.png?nolink&direct&700 |NFR 6 emission trends per category }}

__Contribution of NFRs 1 to 6 to the National Totals, for 2021__
{{ :sector:mainpollutants_sharesnfrs_incl_transport.png?direct&direct&700 | Percental contributions of NFRs 1 to 6 to the National Totals}}