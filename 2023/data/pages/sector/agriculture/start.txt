====== Chapter 5 - NFR 3 - Agriculture (OVERVIEW) ======


^  NFR-Code   ^  Name of Category                                                                       ^
|  3.B         | [[Sector:Agriculture:Manure_Management:Start| 3.B Manure Management]]                   |
|  3.D         | [[Sector:Agriculture:Agricultural_Soils:Start| 3.D Agricultural Soils]]                 |
|  3.F         | [[Sector:Agriculture:Field_Burning:Start | 3.F Field Burning Of Agricultural Residues]]  |
|  3.I         | [[Sector:Agriculture:Agricultural_Other:Start | 3.I Agricultural: Other]]                |

===== Short description =====


Emissions occurring in the agricultural sector in Germany derive from manure management (NFR 3.B), agricultural soils (NFR 3.D) and agriculture other (NFR 3.I).
Germany does not report emissions in category field burning (NFR 3.F) (key note: NO), because burning of agricultural residues is prohibited by law (see Rösemann et al., 2023)((Rösemann C, Vos C, Haenel H-D, Dämmgen U, Döring U, Wulf S, Eurich-Menden B, Freibauer A, Döhler H, Steuer, B, Osterburg B, Fuß R (2023) Calculations of gaseous and particulate emissions from German agriculture 1990 – 2021 : Report on methods and data (RMD) Submission 2023. www.eminv-agriculture.de)).

The pollutants reported are:

  *     ammonia (NH<sub>3</sub>),
  *     nitric oxides (NO<sub>x</sub>),
  *     volatile organic compounds (NMVOC),
  *     particulate matter (PM<sub>2.5</sub>, PM<sub>10</sub> and TSP) and
  *     hexachlorobenzene (HCB).

No heavy metal emissions are reported.

In 2021 the agricultural sector emitted  482.3 Gg of NH<sub>3</sub>, 108.0 Gg of NO<sub>x</sub>,  290.6 Gg of NMVOC,  60.6 Gg of TSP, 33.3 Gg of PM<sub>10</sub> and 5.3 Gg of PM<sub>2.5</sub> and 0.56 kg HCB. The trend from 1990 onwards is shown in the graph below. The sharp decrease of emissions from 1990 to 1991 is due to a reduction of livestock population in the New Länder (former GDR) following the German reunification. The increase of NH<sub>3</sub> emissions since 2005 is mostly due to the expansion of anaerobic digestion of energy crops, especially the application of the digestion residues. This is a new emission source which also effects NO<sub>x</sub> emissions. The decrease of NH<sub>3</sub> emissions since 2015 is mostly due to a decline in the amounts of mineral fertilizer sold and stricter regulations concerning application of urea fertilizers. Further details concerning trends can be found in Rösemann et al., 2023, chapter “Emissions results submission 2023”.

As depicted in the diagram below, in 2021 93.5 % of Germany’s total NH<sub>3</sub> emissions derived from the agricultural sector, while nitric oxides reported as NO<sub>x</sub> contributed 11.2 % and NMVOC 27.8 % to the total NOx and NMVOC emissions of Germany. Regarding the emissions of PM<sub>2.5</sub>, PM<sub>10</sub> and TSP the agricultural sector contributed 6.3 % (PM2.5),  and 18.0 %, respectively, to the national particle emissions.
HCB emissions of pesticide use contributed 12,3 % to the total German emissions.


====Mitigation measures====

The agricultural inventory model can represent several abatement measures for emissions of NH<sub>3</sub> and particles. The measures comprise:

  * changes in animal numbers and amount of applied fertilizers 

  * air scrubbing techniques: yearly updated data on frequencies of air scrubbing facilities and the removal efficiency are provided by KTBL (Kuratorium für Technik und Bauwesen in der Landwirtschaft / Association for Technology and Structures in Agriculture) and from the agricultural census 2020. The average removal efficiency of NH<sub>3</sub> is 80 % for swine and 70 % for poultry, while for TSP and PM<sub>10</sub> the rates are set to 90 % and for PM<sub>2.5</sub> to 70 % for both animal categories. For swine two types of air scrubbers are distinguished: first class systems that remove both NH<sub>3</sub> and particles, and second class systems that remove only particles reliably and have an ammonia removal efficiency of 20%.

  * reduced raw protein content in feeding of fattening pigs: the german animal nutrition association (DVT, Deutscher Verband Tiernahrung e.V.) provides data on the raw protein content of fattening pig feed, therefore enabling the inventory to depict the changes in N-excretions over the time series. The time series is calibrated using data from official and representative surveys conducted by the Federal Statistical Office.

  * reduced raw protein content in feeding and feed conversion rates of broilers: the German animal nutrition association (DVT, Deutscher Verband Tiernahrung e.V.) provides data on the raw protein content of fattening broiler feed, and feed conversion rates of broilers. This makes it possible to model the changes in N-excretions over the time series.

  * low emission spreading techniques of manure: official agricultural censuses survey the distribution of different manure spreading techniques and how fast organic fertilizers are incorporated into the soil. Germany uses distinct emission factors for different methods, techniques and incorporation durations.

  * covering of slurry storage: agricultural censuses survey the distribution of different slurry covers. Germany uses distinct emission factors for the different covers. 

  * use of urease inhibitors: for urea fertilizer the German fertilizer ordinance prescribes the use of urease inhibitors or the direct incorporation into the soil from 2020 onwards.The NH<sub>3</sub> emission factor for urea fertilizers is therefore reduced by 70% from 2020 onwards, according to Bittman et al. (2014, Table 15)((Bittman, S., Dedina, M., Howard C.M., Oenema, O., Sutton, M.A., (eds) (2014): Options for Ammonia Mitigation. Guidance from the UNECE task Force on Reactive Nitrogen. Centre for Ecology and Hydrology, Edinburgh, UK.)).


For NO<sub>x</sub> and NMVOC no mitigation measures are included.


===== Reasons for recalculations =====


(see [[general:recalculations:start|Chapter 8.1 - Recalculations]])

The following list summarizes the most important reasons for recalculations. Recalculations result from improvements in input data and methodologies (for details see Rösemann et al. (2023), Chapter 1.3). 

1) The results used from the 2020 agricultural census (LZ 2020) on the proportions of husbandry, storage or application methods and grazing were assumed to be true for the year 2019 and not for the year 2020 as in Submission 2022. This changes the data obtained by interpolation for the different proportions slightly, in some cases as far back as the year 2000. 

2) Deep bedding systems: As of the submission at hand, it is assumed that the NH<sub>3</sub> emissions from deep litter systems are fully covered by the housing emissions and that the emission factor for storage emissions is 0.  This was done because it can be assumed that in case of deep bedding systems manure will be spread immediately after removing it from the stable. This reduces the emissions from manure management while the emissions from application of manure (3.D.a.2.a) increases as more N is available for application.

3) Dairy cows: Milk yield and slaughter weights for 2020 have been slightly corrected in the official statistics. 

4) Heifers: 2020 slaughter weights have been slightly corrected in the official statistics.

5) Male beef cattle: In some years, slaughter ages and slaughter weights have been updated in the HIT database.

6) Pigs: Air scrubbing techniques: From the 2020 agricultural census, for the first time official data on the number of air scrubbing systems were available. These data were used to derive a distinction between systems of “first” and “second” class (the latter having normal removal efficiency concerning TSP, PM<sub>10</sub> and PM<sub>2.5</sub>
but reduced removal efficiency for NH<sub>3</sub>). The numbers of animal places equipped with “second class” systems were underestimated in previous submissions and therefore have a larger impact in the present submission. This influences mainly the emissions of TSP, PM<sub>10</sub> and PM<sub>2.5</sub> from the year 2005 onwards.

7) Sows: For Lower Saxony, the number of piglets per sow and year was corrected (reduced) for the years 2015-2020. 

8) Fattening pigs: The results of the additional survey "Protein use in pig fattening" by the Federal Statistical Office for the year 2020 were available for the feed parameter crude protein content of fattening pig feed (Federal Statistical Office, 2022). Through interpolation, the crude protein content and thus also the N excretions of the fattening pigs decrease back to the year 2011. For Lower Saxony, the growth rates for the years 2018 and 2019 and the final weight for the year 2019 were corrected.

9) Broilers: Update of the national gross production of broiler meat in 2020.

10) Laying hens: Introduction of grazing emissions for laying hens, since the proportion of excrements from free-range laying hens on the pasture can now be estimated (Rösemann et al. 2023, Chapter 2.5). Based on the results of the LZ2020, a new NH<sub>3</sub> emission factor for floor housing was derived for 2020 (for the years 2011-2019, new emission factors result from linear interpolation). The NH<sub>3</sub> emission factor for free range housing systems is now equal to the NH<sub>3</sub>-EF for floor housing for the time not spent on pasture.

11) Laying hens: Improved interpolation of start weights and final weights for the whole time series.
 
12) TSP, PM<sub>10</sub>, and PM<sub>2.5</sub> emissions from crop production: Emissions are now estimated using a Tier 2 methodology.

13) Application of sewage sludge to soils: Replacement of extrapolated activity data in 2020 with data from the Federal Statistical Office. 

14) Other organic fertilizers. As of the submission at hand, application emissions from digested waste, compost from biowaste, and compost from green waste are reported in the agriculture sector (3.D.a.2.c) for the first time. These emissions were included implicitly in the waste sector before.

15) Anaerobic digestion of energy crops: Update of activity data in 2020. 

16) Pesticides: Recalculations were made for the complete time series due to the changes and new information given by the BVL for the amount of domestic sales of the active substances atrazine, simazine, propazine and quintozine.



===== Visual overview =====

__Emission trends for main pollutants in //NFR 3 - Agriculture//:__
{{ :sector:iir_nfr3.png?nolink&direct&700 |NFR 3 emission trends per category }}
{{ :sector:iir_nfr3_from_2005.png?nolink&direct&700 |NFR 3 emission trends per category, from 2005 }}

__Contribution of NFRs 1 to 6 to the National Totals, for 2021__
{{ :sector:mainpollutants_sharesnfrs_incl_transport.png?direct&direct&700 | Percental contributions of NFRs 1 to 6 to the National Totals}}

===== Specific QA/QC procedures for the agriculture sector=====

Numerous input data were checked for errors resulting from erroneous transfer between data sources and the tabular database used for emission calculations.
The German IEFs and other data used for the emission calculations were compared with EMEP default values and data of other countries (see Rösemann et al., 2023).
Changes of data and methodologies are documented in detail (see  Rösemann et al. 2023, Chapter 1.3).

A comprehensive review of the emission calculations was carried out by comparisons with the results of Submission 2022 and by plausibility checks.

Once emission calculations with the German inventory model Py-GAS-EM are completed for a specific submission, activity data (AD) and implied emission factors (IEFs) are transferred to the CSE database (Central System of Emissions) to be used to calculate the respective emissions within the CSE. These CSE emission results are then cross-checked with the emission results obtained by Py-GAS-EM.

Model data have been verified in the context of a project by external experts (Zsolt Lengyel, Verico SCE). Results show that input data are consistent with other data sources (Eurostat, Statistisches Bundesamt / Federal Statistical Office) and that the performed calculations are consistently and correctly applied in line with the methodological requirements.

Furthermore, in addition to UNFCCC, UNECE and NEC reviews, the Py-GAS-EM model is continuously validated by experts of KTBL (Kuratorium für Technik und Bauwesen in der Landwirtschaft, Association for Technology and Structures in Agriculture) and the EAGER group (European Agricultural Gaseous Emissions Inventory Researchers Network).