====== 5.E.1 - Other Waste: Mechanical-biological Treatment of Waste ======
===== Short description =====
Under NFR category 5.E.1 - Other Waste: Mechanical-biological Treatment of Waste, Germany only reports greenhouse gas emissions from the mechanical biological treatment (MBT) of waste.

^ NFR Code   ^ Name of Category                                        ^  Method   ^  AD      ^  EF                                                     ^  Key Category  ^
|  5.E.1     | Other Waste: Mechanical-biological Treatment of Waste   |  NA       |  5.E.1   | Other Waste: Mechanical-biological Treatment of Waste   |  NA            |
